use std::cmp::Reverse;

use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::One, |input| {
        let mut elves = parse_input(input);
        elves.sort_by_key(|w| Reverse(*w));

        println!("Max weight: {}", elves[0]);
        println!("Top three weight: {}", elves.iter().take(3).sum::<u64>());
    })
}

fn parse_input(input: &str) -> Vec<u64> {
    input
        .split("\n\n")
        .map(|elf| {
            elf.lines()
                .map(|l| l.trim().parse::<u64>().unwrap())
                .sum::<u64>()
        })
        .collect::<Vec<_>>()
}
