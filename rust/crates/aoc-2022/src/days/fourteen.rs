use std::{collections::HashMap, io::stdout, time::Duration};

use crossterm::{
    cursor::{Hide, MoveTo, Show},
    event::{poll, read, Event, KeyCode, KeyEvent},
    style::{Color, Print, ResetColor, SetBackgroundColor, SetForegroundColor},
    terminal::{enable_raw_mode, Clear},
    ExecutableCommand,
};
use libaoc::{point::Point, DayNumber, Solver};

const SHOULD_ANIMATE: bool = true;

pub fn get() -> Solver {
    Solver::new(DayNumber::Fourteen, |input| {
        if SHOULD_ANIMATE {
            let input = example_input();
            let mut cave = parse_input(input);
            let _ = enable_raw_mode();
            let tick_time = Duration::from_millis(10);
            stdout()
                .execute(Hide)
                .unwrap()
                .execute(SetBackgroundColor(Color::Black))
                .unwrap();
            clear_screen().unwrap();
            let mut should_tick = true;
            let mut buffer: HashMap<Point, char> = HashMap::default();
            loop {
                if poll(tick_time).unwrap() {
                    match read().unwrap() {
                        Event::Key(KeyEvent { code, .. }) if code == KeyCode::Esc => break,
                        _ => {}
                    };
                }
                if should_tick {
                    should_tick = cave.tick(true);
                    display(&cave, &mut buffer).unwrap();
                }
            }
            stdout().execute(Show).unwrap();
        } else {
            let mut cave = parse_input(input);
            let mut should_tick = true;
            while should_tick {
                should_tick = cave.tick(false);
            }
            println!("Amount of sand: {}", cave.count_sand());

            should_tick = true;
            while should_tick {
                should_tick = cave.tick(true);
            }
            println!("Amount of sand: {}", cave.count_sand());
        }
    })
}

fn clear_screen() -> anyhow::Result<()> {
    stdout().execute(Clear(crossterm::terminal::ClearType::All))?;
    Ok(())
}

fn display(cave: &Cave, buffer: &mut HashMap<Point, char>) -> anyhow::Result<()> {
    stdout().execute(SetForegroundColor(Color::White))?;

    let (min, max) = cave.get_bounds();

    for x in min.x..=max.x {
        for y in min.y..=max.y {
            let point: Point = (x, y).into();
            let chr = if point.x == cave.source.x && point.y == cave.source.y {
                'x'
            } else if y == max.y {
                '#'
            } else {
                match cave.grid.get(&point) {
                    Some(Cell::Rock) => '#',
                    Some(Cell::Sand) => 'o',
                    _ => '.',
                }
            };
            if point.x < 0 || point.y < 0 {
                continue;
            }
            if buffer.get(&point) != Some(&chr) {
                buffer.insert(point, chr);
                stdout()
                    .execute(MoveTo(point.x as u16, point.y as u16))?
                    .execute(Print(chr))?;
            }
        }
    }

    if let Some(FallingSand(pos)) = cave.falling_sand {
        buffer.remove(&pos);
        stdout()
            .execute(SetForegroundColor(Color::Yellow))?
            .execute(MoveTo(pos.x as u16, pos.y as u16))?
            .execute(Print('o'))?;
    }

    stdout().execute(ResetColor)?;

    Ok(())
}

struct Cave {
    grid: HashMap<Point, Cell>,
    falling_sand: Option<FallingSand>,
    source: Point,
    floor_level: i32,
}

impl Cave {
    fn tick(&mut self, with_floor: bool) -> bool {
        if self.falling_sand.is_none() {
            self.falling_sand = Some(FallingSand(self.source));
        }

        let FallingSand(falling_sand) = self.falling_sand.unwrap();
        for delta in [(0, 1), (-1, 1), (1, 1)] {
            let new_pos = falling_sand + delta;
            if new_pos.y < self.floor_level {
                if self.grid.get(&new_pos).is_none() {
                    self.falling_sand = Some(FallingSand(new_pos));
                    return true;
                }
            } else if !with_floor {
                return false;
            }
        }
        self.grid.insert(falling_sand, Cell::Sand);
        if falling_sand == self.source {
            false
        } else {
            self.falling_sand = None;
            true
        }
    }

    fn get_bounds(&self) -> (Point, Point) {
        let mut min_x = i32::MAX;
        let mut max_x = 0;
        let mut min_y = i32::MAX;
        let mut max_y = 0;

        for point in self.grid.keys() {
            min_x = std::cmp::min(min_x, point.x);
            min_y = std::cmp::min(min_y, point.y);
            max_x = std::cmp::max(max_x, point.x);
            max_y = std::cmp::max(max_y, point.y);
        }
        let offset = 15;
        ((min_x - offset, 0).into(), (max_x + offset, max_y).into())
    }

    fn count_sand(&self) -> usize {
        self.grid.iter().filter(|(_, c)| **c == Cell::Sand).count()
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
struct FallingSand(Point);

#[derive(Clone, Copy, Eq, PartialEq)]
enum Cell {
    Rock,
    Sand,
}

impl std::fmt::Debug for Cell {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Rock => write!(f, "#"),
            Self::Sand => write!(f, "o"),
        }
    }
}

#[derive(Debug)]
struct Line {
    start: Point,
    end: Point,
}

impl Line {
    fn len(&self) -> i32 {
        std::cmp::max(
            (self.start.x - self.end.x).abs(),
            (self.start.y - self.end.y).abs(),
        )
    }
}

fn parse_input(input: &str) -> Cave {
    let mut lines = vec![];

    for line in input.lines() {
        let line = line.trim();
        let points = line
            .split(" -> ")
            .map(|point_str| {
                let mut point_split = point_str.split(',');
                let x = point_split.next().unwrap().parse::<i32>().unwrap();
                let y = point_split.next().unwrap().parse::<i32>().unwrap();
                Point { x, y }
            })
            .collect::<Vec<_>>();
        for line in points.windows(2) {
            lines.push(Line {
                start: line[0],
                end: line[1],
            });
        }
    }

    let min_x = lines
        .iter()
        .map(|line| std::cmp::min(line.start.x, line.end.x))
        .min()
        .unwrap()
        - 15;
    let min_y = 0;

    lines.iter_mut().for_each(|line| {
        line.start.x -= min_x;
        line.end.x -= min_x;
        line.start.y -= min_y;
        line.end.y -= min_y;
    });

    let max_y = lines
        .iter()
        .map(|line| std::cmp::max(line.start.y, line.end.y))
        .max()
        .unwrap();

    let floor_y = max_y + 2;

    let mut grid: HashMap<Point, Cell> = HashMap::new();

    grid.insert(Point::from((lines[0].start.x, floor_y)), Cell::Rock);
    for line in lines {
        let dx = (line.end.x - line.start.x).signum();
        let dy = (line.end.y - line.start.y).signum();

        let mut point = line.start;
        for _ in 0..=line.len() {
            grid.insert(point, Cell::Rock);
            point = point + (dx, dy);
        }
    }

    Cave {
        grid,
        falling_sand: None,
        source: Point::from((500 - min_x, 0)),
        floor_level: floor_y,
    }
}

fn example_input() -> &'static str {
    "498,4 -> 498,6 -> 496,6
        503,4 -> 502,4 -> 502,9 -> 494,9"
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        let input = example_input();

        let mut cave = parse_input(input);
        let mut should_tick = true;
        while should_tick {
            should_tick = cave.tick(false);
        }
        assert_eq!(cave.count_sand(), 24);
    }

    #[test]
    fn test_example_2() {
        let input = example_input();

        let mut cave = parse_input(input);
        let mut should_tick = true;
        while should_tick {
            should_tick = cave.tick(true);
        }
        assert_eq!(cave.count_sand(), 93);
    }
}
