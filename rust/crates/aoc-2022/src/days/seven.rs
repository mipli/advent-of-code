use libaoc::{DayNumber, Solver};
use nom::{branch, bytes::complete::tag, character::complete, combinator, multi, Finish, IResult};

pub fn get() -> Solver {
    Solver::new(DayNumber::Seven, |input| {
        let cmds = parse_input(input).unwrap();
        let root = perform_commands(&cmds);
        let sum = sum_lower_than(&root, 100000);
        println!("Sum of sizes: {sum}");
        let deletion = find_delete_size(&root, 30000000, 70000000);
        println!("Size to delete: {deletion}");
    })
}

fn find_delete_size(node: &Node, target: u64, total: u64) -> u64 {
    match node {
        Node::Dir(d) => {
            let required = d.size + target - total;
            find_delete_size_inner(d, required).unwrap()
        }
        Node::File(_) => panic!("root cannot be a file"),
    }
}

fn find_delete_size_inner(dir: &Dir, required: u64) -> Option<u64> {
    let mut options = vec![];

    for child in &dir.nodes {
        if let Node::Dir(child_dir) = child {
            if child_dir.size >= required {
                options.push(Some(child_dir.size));
            }
            if let Some(size) = find_delete_size_inner(child_dir, required) {
                options.push(Some(size));
            }
        }
    }

    if options.is_empty() {
        None
    } else {
        options.sort();
        options[0]
    }
}

fn sum_lower_than(node: &Node, limit: u64) -> u64 {
    let mut sum = 0;
    if let Node::Dir(dir) = node {
        if dir.size <= limit {
            sum += dir.size;
        }
        sum += dir
            .nodes
            .iter()
            .map(|child| sum_lower_than(child, limit))
            .sum::<u64>();
    }
    sum
}

#[derive(Debug, Eq, PartialEq, Clone)]
struct File {
    name: String,
    size: u64,
}

#[derive(Debug, Eq, PartialEq, Clone)]
struct Dir {
    name: String,
    nodes: Vec<Node>,
    size: u64,
}

#[derive(Debug, Eq, PartialEq, Clone)]
enum Node {
    Dir(Dir),
    File(File),
}

#[derive(Debug, Eq, PartialEq, Clone)]
enum Cmd {
    Up,
    Root,
    Dir(String),
    Ls(Vec<Node>),
}

fn parse_input(mut input: &str) -> anyhow::Result<Vec<Cmd>> {
    let mut cmds = vec![];
    while !input.is_empty() {
        let (input_new, cmd) = match parse_command(input.trim_start()).finish() {
            Ok((input, cmd)) => (input, cmd),
            Err(nom::error::Error { input, code }) => {
                return Err(anyhow::Error::new(nom::error::Error {
                    code,
                    input: input.to_string(),
                }))
            }
        };

        input = input_new;
        cmds.push(cmd);
    }
    Ok(cmds)
}

fn perform_commands(cmds: &[Cmd]) -> Node {
    let (node, _) = perform_commands_inner("root".to_string(), cmds, 0);
    node
}

fn perform_commands_inner(name: String, cmds: &[Cmd], mut idx: usize) -> (Node, usize) {
    let mut node = Dir {
        name,
        nodes: vec![],
        size: 0,
    };
    while idx < cmds.len() {
        match cmds[idx] {
            Cmd::Up => {
                idx += 1;
                break;
            }
            Cmd::Root => idx += 1,
            Cmd::Dir(ref name) => {
                let (new_node, idx_new) = perform_commands_inner(name.clone(), cmds, idx + 1);
                idx = idx_new;
                for child in &mut node.nodes {
                    if let Node::Dir(child_node) = child {
                        if child_node.name == *name {
                            *child = new_node;
                            break;
                        }
                    }
                }
            }
            Cmd::Ls(ref nodes) => {
                node.nodes = nodes.clone();
                idx += 1;
            }
        }
    }
    node.size = node
        .nodes
        .iter()
        .map(|child| match child {
            Node::Dir(d) => d.size,
            Node::File(f) => f.size,
        })
        .sum::<u64>();
    (Node::Dir(node), idx)
}

fn parse_command(input: &str) -> IResult<&str, Cmd> {
    let (input, cmd) = branch::alt((parse_cd, parse_ls))(input)?;
    let (input, _) = combinator::opt(complete::line_ending)(input)?;
    Ok((input, cmd))
}

fn parse_cd(input: &str) -> IResult<&str, Cmd> {
    let (input, _) = tag("$ cd ")(input)?;
    let (input, name) = branch::alt((complete::alpha1, tag("/"), tag("..")))(input)?;
    let cmd = match name {
        "/" => Cmd::Root,
        ".." => Cmd::Up,
        _ => Cmd::Dir(name.to_string()),
    };
    Ok((input, cmd))
}

fn parse_ls(input: &str) -> IResult<&str, Cmd> {
    let (input, _) = tag("$ ls")(input)?;
    let (input, _) = complete::line_ending(input)?;
    let (input, nodes) = multi::separated_list1(complete::line_ending, parse_node)(input)?;
    Ok((input, Cmd::Ls(nodes)))
}

fn parse_node(input: &str) -> IResult<&str, Node> {
    let input = input.trim();
    let (input, node) = branch::alt((parse_dir, parse_file))(input)?;

    Ok((input, node))
}

fn parse_dir(input: &str) -> IResult<&str, Node> {
    let (input, _) = tag("dir")(input)?;
    let (input, _) = complete::space0(input)?;
    let (input, name) = complete::alpha1(input)?;

    Ok((
        input,
        Node::Dir(Dir {
            name: name.to_string(),
            nodes: vec![],
            size: 0,
        }),
    ))
}

fn parse_file(input: &str) -> IResult<&str, Node> {
    let (input, size) = complete::u64(input)?;
    let (input, _) = complete::space0(input)?;
    let (input, parts) = multi::many1(branch::alt((complete::alpha1, tag("."))))(input)?;
    let name = parts.join("");

    Ok((input, Node::File(File { name, size })))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_cd() {
        let (_, cmd) = parse_cd("$ cd a").unwrap();
        assert_eq!(cmd, Cmd::Dir("a".to_string()));

        let (_, cmd) = parse_cd("$ cd /").unwrap();
        assert_eq!(cmd, Cmd::Root);
        let (_, cmd) = parse_cd("$ cd ..").unwrap();
        assert_eq!(cmd, Cmd::Up);
    }

    #[test]
    fn test_parse_ls() {
        let (_, content) = parse_ls("$ ls\n            dir e\n            29116 f.txt\n            2557 g\n            62596 h.lst").unwrap();
        dbg!(&content);
        assert!(matches!(content, Cmd::Ls(ref nodes) if nodes.len() == 4));
        let Cmd::Ls(nodes) = content else {
            panic!();
        };
        assert_eq!(
            nodes[0],
            Node::Dir(Dir {
                name: "e".to_string(),
                nodes: vec![],
                size: 0,
            })
        );
        assert_eq!(
            nodes[1],
            Node::File(File {
                name: "f.txt".to_string(),
                size: 29116
            })
        );
    }

    #[test]
    fn test_example_1() {
        let input = "$ cd /
            $ ls
            dir a
            14848514 b.txt
            8504156 c.dat
            dir d
            $ cd a
            $ ls
            dir e
            29116 f
            2557 g
            62596 h.lst
            $ cd e
            $ ls
            584 i
            $ cd ..
            $ cd ..
            $ cd d
            $ ls
            4060174 j
            8033020 d.log
            5626152 d.ext
            7214296 k";

        let cmds = parse_input(input).unwrap();
        let root = perform_commands(&cmds);
        let sum = sum_lower_than(&root, 100000);
        assert_eq!(sum, 95437);
    }

    #[test]
    fn test_example_2() {
        let input = "$ cd /
            $ ls
            dir a
            14848514 b.txt
            8504156 c.dat
            dir d
            $ cd a
            $ ls
            dir e
            29116 f
            2557 g
            62596 h.lst
            $ cd e
            $ ls
            584 i
            $ cd ..
            $ cd ..
            $ cd d
            $ ls
            4060174 j
            8033020 d.log
            5626152 d.ext
            7214296 k";

        let cmds = parse_input(input).unwrap();
        let root = perform_commands(&cmds);
        let deletion = find_delete_size(&root, 30000000, 70000000);
        assert_eq!(deletion, 24933642);
    }
}
