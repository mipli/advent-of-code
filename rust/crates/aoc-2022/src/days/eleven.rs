use std::{cmp::Reverse, collections::VecDeque};

use libaoc::{DayNumber, Solver};
use nom::{branch, bytes::complete::tag, character::complete, combinator, multi, Finish, IResult};

pub fn get() -> Solver {
    Solver::new(DayNumber::Eleven, |input| {
        let monkeys = parse_input(input);
        let monkey_business = get_monkey_business(monkeys, 20, false);
        println!("Monkey business: {monkey_business}");

        let monkeys = parse_input(input);
        let monkey_business = get_monkey_business(monkeys, 10000, true);
        println!("Monkey business: {monkey_business}");
    })
}

fn get_monkey_business(mut monkeys: Vec<Monkey>, rounds: usize, part_2: bool) -> u64 {
    let modulo = if part_2 {
        Some(monkeys.iter().map(|m| m.divisor).product())
    } else {
        None
    };
    for _ in 0..rounds {
        run_round(&mut monkeys, modulo);
    }

    let mut inspections = monkeys.iter().map(|m| m.inspections).collect::<Vec<_>>();
    inspections.sort_by_key(|v| Reverse(*v));

    inspections.iter().take(2).product()
}

fn run_round(monkeys: &mut Vec<Monkey>, modulo: Option<u64>) {
    for i in 0..monkeys.len() {
        while let Some(item) = monkeys[i].items.pop_front() {
            monkeys[i].inspections += 1;
            let new_level = if let Some(modulo) = modulo {
                (monkeys[i].operation)(item) % modulo
            } else {
                (monkeys[i].operation)(item) / 3
            };
            let target = (monkeys[i].target)(new_level);
            assert!(target != monkeys[i].id);
            monkeys[target].items.push_back(new_level);
        }
    }
}

type MonkeyId = usize;
type WorryLevel = u64;
type MonkeyOperation = Box<dyn Fn(WorryLevel) -> WorryLevel>;
type MonkeyTarget = Box<dyn Fn(WorryLevel) -> MonkeyId>;

struct Monkey {
    id: MonkeyId,
    items: VecDeque<WorryLevel>,
    operation: MonkeyOperation,
    target: MonkeyTarget,
    inspections: u64,
    divisor: u64,
}

fn parse_operation(input: &str) -> IResult<&str, MonkeyOperation> {
    let (input, _) = complete::space0(input)?;
    let (input, _) = tag("Operation: new = old ")(input)?;

    let (input, opt) = complete::anychar(input)?;
    let (input, _) = complete::space1(input)?;

    let (input, val) = combinator::opt(complete::u64)(input)?;

    let (input, _) = if val.is_none() {
        tag("old")(input)?
    } else {
        (input, "")
    };

    let (input, _) = complete::newline(input)?;

    let operator: MonkeyOperation = match (opt, val) {
        ('*', Some(val)) => Box::new(move |level| level * val),
        ('/', Some(val)) => Box::new(move |level| level / val),
        ('+', Some(val)) => Box::new(move |level| level + val),
        ('-', Some(val)) => Box::new(move |level| level - val),
        ('*', None) => Box::new(move |level| level * level),
        ('/', None) => Box::new(move |_| 1),
        ('+', None) => Box::new(move |level| level + level),
        ('-', None) => Box::new(move |_| 0),
        _ => panic!("Invalid operation operator: '{opt}'"),
    };
    Ok((input, operator))
}

fn parse_target(input: &str) -> IResult<&str, (MonkeyTarget, u64)> {
    let (input, _) = complete::space0(input)?;
    let (input, _) = tag("Test: divisible by ")(input)?;
    let (input, modulo) = complete::u64(input)?;
    let (input, _) = complete::newline(input)?;

    let (input, _) = complete::space0(input)?;
    let (input, _) = tag("If true: throw to monkey ")(input)?;
    let (input, truthy) = complete::u64(input)?;
    let (input, _) = complete::newline(input)?;

    let (input, _) = complete::space0(input)?;
    let (input, _) = tag("If false: throw to monkey ")(input)?;
    let (input, falsy) = complete::u64(input)?;
    let (input, _) = combinator::opt(complete::newline)(input)?;

    let target: MonkeyTarget = Box::new(move |level| {
        if level % modulo == 0 {
            truthy as usize
        } else {
            falsy as usize
        }
    });

    Ok((input, (target, modulo)))
}

fn parse_monkey(input: &str) -> IResult<&str, Monkey> {
    let (input, _) = tag("Monkey ")(input)?;
    let (input, id) = complete::u64(input)?;
    let (input, _) = tag(":")(input)?;
    let (input, _) = complete::newline(input)?;

    let (input, _) = complete::space0(input)?;
    let (input, _) = tag("Starting items: ")(input)?;
    let (input, targets) = multi::separated_list1(
        multi::many1(branch::alt((complete::char(','), complete::char(' ')))),
        complete::u64,
    )(input)?;
    let (input, _) = complete::newline(input)?;

    let (input, operation) = parse_operation(input)?;
    let (input, (target, divisor)) = parse_target(input)?;

    Ok((
        input,
        Monkey {
            id: id as usize,
            items: targets.into(),
            operation: Box::new(operation),
            target: Box::new(target),
            inspections: 0,
            divisor,
        },
    ))
}

fn parse_input(input: &str) -> Vec<Monkey> {
    let monkey_input = input.split("\n\n");

    monkey_input
        .map(|block| {
            let (_, monkey) = parse_monkey(block.trim()).finish().unwrap();
            monkey
        })
        .collect::<Vec<_>>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_monkey() {
        let input = "Monkey 0:
            Starting items: 79, 98
            Operation: new = old * 19
            Test: divisible by 23
            If true: throw to monkey 2
            If false: throw to monkey 3";

        let (_, monkey) = parse_monkey(input).finish().unwrap();
        assert_eq!(monkey.id, 0);
        assert_eq!(monkey.items, vec![79, 98]);
        assert_eq!((monkey.operation)(2), 38);
        assert_eq!((monkey.target)(500), 3);
        assert_eq!((monkey.target)(46), 2);
    }

    #[test]
    fn test_parse_monkey_old() {
        let input = "Monkey 0:
            Starting items: 79, 98
            Operation: new = old + old
            Test: divisible by 23
            If true: throw to monkey 2
            If false: throw to monkey 3";

        let (_, monkey) = parse_monkey(input).finish().unwrap();
        assert_eq!((monkey.operation)(2), 4);
    }

    #[test]
    fn test_example_round_1() {
        let input = "Monkey 0:
          Starting items: 79, 98
          Operation: new = old * 19
          Test: divisible by 23
            If true: throw to monkey 2
            If false: throw to monkey 3

        Monkey 1:
          Starting items: 54, 65, 75, 74
          Operation: new = old + 6
          Test: divisible by 19
            If true: throw to monkey 2
            If false: throw to monkey 0

        Monkey 2:
          Starting items: 79, 60, 97
          Operation: new = old * old
          Test: divisible by 13
            If true: throw to monkey 1
            If false: throw to monkey 3

        Monkey 3:
          Starting items: 74
          Operation: new = old + 3
          Test: divisible by 17
            If true: throw to monkey 0
            If false: throw to monkey 1";
        let mut monkeys = parse_input(input);
        assert_eq!(monkeys.len(), 4);

        run_round(&mut monkeys, None);

        assert_eq!(monkeys[0].items, vec![20, 23, 27, 26]);
        assert_eq!(monkeys[1].items, vec![2080, 25, 167, 207, 401, 1046]);
        assert_eq!(monkeys[2].items.len(), 0);
        assert_eq!(monkeys[3].items.len(), 0);
    }

    #[test]
    fn test_example_1() {
        let input = "Monkey 0:
          Starting items: 79, 98
          Operation: new = old * 19
          Test: divisible by 23
            If true: throw to monkey 2
            If false: throw to monkey 3

        Monkey 1:
          Starting items: 54, 65, 75, 74
          Operation: new = old + 6
          Test: divisible by 19
            If true: throw to monkey 2
            If false: throw to monkey 0

        Monkey 2:
          Starting items: 79, 60, 97
          Operation: new = old * old
          Test: divisible by 13
            If true: throw to monkey 1
            If false: throw to monkey 3

        Monkey 3:
          Starting items: 74
          Operation: new = old + 3
          Test: divisible by 17
            If true: throw to monkey 0
            If false: throw to monkey 1";
        let monkeys = parse_input(input);
        assert_eq!(monkeys.len(), 4);
        let monkey_business = get_monkey_business(monkeys, 20, false);
        assert_eq!(monkey_business, 10605);
    }

    #[test]
    fn test_example_2() {
        let input = "Monkey 0:
          Starting items: 79, 98
          Operation: new = old * 19
          Test: divisible by 23
            If true: throw to monkey 2
            If false: throw to monkey 3

        Monkey 1:
          Starting items: 54, 65, 75, 74
          Operation: new = old + 6
          Test: divisible by 19
            If true: throw to monkey 2
            If false: throw to monkey 0

        Monkey 2:
          Starting items: 79, 60, 97
          Operation: new = old * old
          Test: divisible by 13
            If true: throw to monkey 1
            If false: throw to monkey 3

        Monkey 3:
          Starting items: 74
          Operation: new = old + 3
          Test: divisible by 17
            If true: throw to monkey 0
            If false: throw to monkey 1";
        let monkeys = parse_input(input);
        assert_eq!(monkeys.len(), 4);
        let monkey_business = get_monkey_business(monkeys, 10000, true);
        assert_eq!(monkey_business, 2713310158);
    }
}
