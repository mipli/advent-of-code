use itertools::Itertools;
use libaoc::{DayNumber, Solver};
use nom::{branch, character::complete, multi, sequence, Finish, IResult};

pub fn get() -> Solver {
    Solver::new(DayNumber::Thirteen, |input| {
        let packets = parse_input(input);
        let indices_sum = get_valid_index_sum(&packets);
        println!("Indices sum: {indices_sum}");
        let mut packets = add_dividers(packets);
        packets.sort();
        assert!(is_ordered(&packets));
        println!(
            "Divider product generic: {}",
            find_divider_product(&packets)
        );
        let (pre_2, pre_6) = find_divider_indices(&packets);
        println!("Divider product fast: {}", pre_2 * pre_6);
    })
}

fn find_divider_indices(packets: &[Item]) -> (usize, usize) {
    let pre_2 = packets
        .iter()
        .filter(|item| match item.as_nums().first() {
            Some(v) => *v < 2,
            None => false,
        })
        .count();
    let pre_6 = packets
        .iter()
        .filter(|item| match item.as_nums().first() {
            Some(v) => *v < 6,
            None => false,
        })
        .count();
    (pre_2 + 1, pre_6 + 1)
}

fn add_dividers(mut packets: Vec<Item>) -> Vec<Item> {
    packets.push(Item::List(vec![Item::List(vec![Item::Num(2)])]));
    packets.push(Item::List(vec![Item::List(vec![Item::Num(6)])]));
    packets
}

fn find_divider_product(packets: &[Item]) -> u64 {
    packets
        .iter()
        .enumerate()
        .filter(|(_, item)| {
            **item == Item::List(vec![Item::List(vec![Item::Num(2)])])
                || **item == Item::List(vec![Item::List(vec![Item::Num(6)])])
        })
        .map(|(idx, _)| (idx + 1) as u64)
        .product::<u64>()
}

fn is_ordered(packets: &[Item]) -> bool {
    packets
        .iter()
        .tuples()
        .all(|(lhs, rhs)| is_valid_packet(lhs, rhs))
}

fn get_valid_index_sum(packets: &[Item]) -> i32 {
    packets
        .iter()
        .tuples()
        .enumerate()
        .filter_map(|(idx, (lhs, rhs))| {
            if is_valid_packet(lhs, rhs) {
                Some(idx as i32 + 1)
            } else {
                None
            }
        })
        .sum()
}

fn parse_input(input: &str) -> Vec<Item> {
    let mut packets = vec![];
    for packet_data in input.split('\n') {
        let packet_data = packet_data.trim();
        if packet_data.is_empty() {
            continue;
        }
        let (_, item) = parse_list(packet_data).finish().unwrap();
        packets.push(item)
    }
    packets
}

fn parse_num(input: &str) -> IResult<&str, Item> {
    let (input, num) = complete::i32(input)?;

    Ok((input, Item::Num(num)))
}

fn parse_list(input: &str) -> IResult<&str, Item> {
    let (input, list) = sequence::delimited(
        complete::char('['),
        multi::separated_list0(complete::char(','), branch::alt((parse_num, parse_list))),
        complete::char(']'),
    )(input)?;

    Ok((input, Item::List(list)))
}

#[derive(Eq, PartialEq, Clone)]
enum Item {
    Num(i32),
    List(Vec<Item>),
}

impl Item {
    fn as_nums(&self) -> Vec<i32> {
        match self {
            Item::Num(v) => vec![*v],
            Item::List(l) if l.is_empty() => vec![-10],
            Item::List(l) => l
                .iter()
                .flat_map(|i| {
                    let mut tmp = i.as_nums();
                    if tmp.len() == 1 && tmp[0] < -1 {
                        tmp[0] += 1;
                    }
                    tmp
                })
                .collect::<Vec<_>>(),
        }
    }
}

impl std::fmt::Debug for Item {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Num(a) => write!(f, "{a}"),
            Self::List(ls) => {
                write!(f, "[")?;
                for l in ls {
                    write!(f, "{l:?},")?;
                }
                write!(f, "]")
            }
        }
    }
}

impl Ord for Item {
    fn cmp(&self, right: &Self) -> std::cmp::Ordering {
        match (self, right) {
            (Item::Num(l), Item::Num(r)) => l.cmp(r),
            (Item::List(ls), Item::List(rl)) => ls
                .iter()
                .zip(rl.iter())
                .find_map(|(lt, rt)| {
                    let result = lt.cmp(rt);
                    result.is_ne().then_some(result)
                })
                .unwrap_or_else(|| ls.len().cmp(&rl.len())),
            (Item::Num(_), Item::List(_)) => Item::List(vec![self.clone()]).cmp(right),
            (Item::List(_), Item::Num(_)) => self.cmp(&Item::List(vec![right.clone()])),
        }
    }
}

impl PartialOrd for Item {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other)) //ordering is total
    }
}

#[derive(Debug)]
enum ValidatationResult {
    Valid,
    Invalid,
    Continue,
}

fn is_valid_packet(lhs: &Item, rhs: &Item) -> bool {
    match is_valid_items(lhs, rhs) {
        ValidatationResult::Valid => true,
        ValidatationResult::Invalid => false,
        ValidatationResult::Continue => false,
    }
}

fn is_valid_items(lhs: &Item, rhs: &Item) -> ValidatationResult {
    match (lhs, rhs) {
        (Item::Num(l), Item::Num(r)) if l < r => ValidatationResult::Valid,
        (Item::Num(l), Item::Num(r)) if l > r => ValidatationResult::Invalid,
        (Item::Num(_), Item::Num(_)) => ValidatationResult::Continue,
        (Item::Num(l), Item::List(_)) => is_valid_items(&Item::List(vec![Item::Num(*l)]), rhs),
        (Item::List(_), Item::Num(r)) => is_valid_items(lhs, &Item::List(vec![Item::Num(*r)])),
        (Item::List(l), Item::List(r)) => {
            let mut r_iter = r.iter();
            for l_item in l.iter() {
                let Some(r_item) = r_iter.next() else {
                        return ValidatationResult::Invalid;
                    };
                match is_valid_items(l_item, r_item) {
                    ValidatationResult::Continue => {}
                    ValidatationResult::Invalid => return ValidatationResult::Invalid,
                    ValidatationResult::Valid => return ValidatationResult::Valid,
                }
            }
            if r_iter.next().is_some() {
                ValidatationResult::Valid
            } else {
                ValidatationResult::Continue
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_example_parse() {
        use super::Item::*;

        let packet = parse_input("[1,1,3,1,1]\n[1,1,5,1,1]");
        assert_eq!(
            packet[0],
            List(vec![Num(1), Num(1), Num(3), Num(1), Num(1)])
        );
        assert_eq!(
            packet[1],
            List(vec![Num(1), Num(1), Num(5), Num(1), Num(1)])
        );

        let packet = parse_input("[[1],[2,3,4]]\n[[1],4]");
        assert_eq!(
            packet[0],
            List(vec![List(vec![Num(1)]), List(vec![Num(2), Num(3), Num(4)])]),
        );
        assert_eq!(packet[1], List(vec![List(vec![Num(1)]), Num(4)]));
    }

    #[test]
    fn test_example_1() {
        let input = "[1,1,3,1,1]
            [1,1,5,1,1]

            [[1],[2,3,4]]
            [[1],4]

            [9]
            [[8,7,6]]

            [[4,4],4,4]
            [[4,4],4,4,4]

            [7,7,7,7]
            [7,7,7]

            []
            [3]

            [[[]]]
            [[]]

            [1,[2,[3,[4,[5,6,7]]]],8,9]
            [1,[2,[3,[4,[5,6,0]]]],8,9]";

        let packets = parse_input(input);

        assert_eq!(get_valid_index_sum(&packets), 13);
    }

    #[test]
    fn test_example_2() {
        let input = "[1,1,3,1,1]
            [1,1,5,1,1]

            [[1],[2,3,4]]
            [[1],4]

            [9]
            [[8,7,6]]

            [[4,4],4,4]
            [[4,4],4,4,4]

            [7,7,7,7]
            [7,7,7]

            []
            [3]

            [[[]]]
            [[]]

            [1,[2,[3,[4,[5,6,7]]]],8,9]
            [1,[2,[3,[4,[5,6,0]]]],8,9]";

        let packets = parse_input(input);
        assert_eq!(packets.len(), 16);

        let mut packets = add_dividers(packets);
        packets.sort();

        assert!(is_ordered(&packets));
        assert_eq!(find_divider_product(&packets), 140);
    }

    #[test]
    fn test_is_ordered() {
        let input = "[]
            [[]]
            [[[]]]
            [1,1,3,1,1]
            [1,1,5,1,1]
            [[1],[2,3,4]]
            [1,[2,[3,[4,[5,6,0]]]],8,9]
            [1,[2,[3,[4,[5,6,7]]]],8,9]
            [[1],4]
            [[2]]
            [3]
            [[4,4],4,4]
            [[4,4],4,4,4]
            [[6]]
            [7,7,7]
            [7,7,7,7]
            [[8,7,6]]
            [9]";

        let packets = parse_input(input);

        assert!(is_ordered(&packets));
    }

    #[test]
    fn test_item_as_nums() {
        let input = "[]
            [[]]
            [[[]]]
            [1,1,3,1,1]
            [1,1,5,1,1]
            [[1],[2,3,4]]
            [1,[2,[3,[4,[5,6,0]]]],8,9]
            [1,[2,[3,[4,[5,6,7]]]],8,9]
            [[1],4]
            [[2]]
            [3]
            [[4,4],4,4]
            [[4,4],4,4,4]
            [[6]]
            [7,7,7]
            [7,7,7,7]
            [[8,7,6]]
            [9]";

        let packets = parse_input(input);

        assert_eq!(packets[0].as_nums(), vec![-10]);
        assert_eq!(packets[1].as_nums(), vec![-9]);
        assert_eq!(packets[2].as_nums(), vec![-8]);
        assert_eq!(packets[3].as_nums(), vec![1, 1, 3, 1, 1]);
        assert_eq!(packets[6].as_nums(), vec![1, 2, 3, 4, 5, 6, 0, 8, 9]);
    }
}
