use std::{fmt, str::FromStr};

use anyhow::anyhow;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Ten, |input| {
        let mut cpu = parse_input(input);
        let values = cpu.signal_strength_at(&[20, 60, 100, 140, 180, 220]);
        println!("Signal sum: {}", values.iter().sum::<i32>());

        let mut crt = Crt::new();
        cpu.reset();

        for _ in 0..240 {
            crt.sprite_pos = cpu.register as usize;
            cpu.cycle();
            crt.draw();
        }
        println!("{crt}");
    })
}

fn parse_input(input: &str) -> Cpu {
    let instructions = input
        .lines()
        .map(|line| {
            let inst: Instruction = line.trim().parse().unwrap();
            inst
        })
        .collect::<Vec<_>>();

    Cpu::new(instructions)
}

#[derive(Debug, Copy, Clone)]
enum Instruction {
    Addx(i32),
    Noop,
}

impl FromStr for Instruction {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut splits = s.split(' ');
        let cmd = splits.next().ok_or(anyhow!("Missing command"))?;
        match (cmd, splits.next()) {
            ("noop", _) => Ok(Instruction::Noop),
            ("addx", Some(v)) => {
                let val: i32 = v.parse()?;
                Ok(Instruction::Addx(val))
            }
            _ => Err(anyhow!("Invalid command: '{s}'")),
        }
    }
}

#[derive(Clone, Copy)]
enum Pixel {
    Lit,
    Dark,
}

impl fmt::Display for Pixel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Pixel::Lit => write!(f, "#"),
            Pixel::Dark => write!(f, "."),
        }
    }
}

impl Default for Pixel {
    fn default() -> Self {
        Pixel::Dark
    }
}

struct Crt {
    rows: [[Pixel; 40]; 6],
    current_row: usize,
    cursor: usize,
    sprite_pos: usize,
}

impl Crt {
    fn new() -> Self {
        Crt {
            cursor: 0,
            current_row: 0,
            sprite_pos: 0,
            rows: [[Pixel::Dark; 40]; 6],
        }
    }

    fn draw(&mut self) {
        let pixel = if (self.cursor as i32 - self.sprite_pos as i32).abs() <= 1 {
            Pixel::Lit
        } else {
            Pixel::Dark
        };
        self.rows[self.current_row][self.cursor] = pixel;
        self.cursor += 1;
        if self.cursor >= 40 {
            self.cursor = 0;
            self.current_row += 1;
        }
    }
}

impl fmt::Display for Crt {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for row in self.rows {
            for pixel in row {
                write!(f, "{pixel}")?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

struct Cpu {
    register: i32,
    cycle: usize,
    instructions: Vec<Instruction>,
    cursor: usize,
    is_adding: bool,
}

impl Cpu {
    fn new(instructions: Vec<Instruction>) -> Self {
        Cpu {
            register: 1,
            cycle: 0,
            cursor: 0,
            is_adding: false,
            instructions,
        }
    }

    fn reset(&mut self) {
        self.register = 1;
        self.cycle = 0;
        self.cursor = 0;
        self.is_adding = false;
    }

    fn signal_strength_at(&mut self, cycles: &[usize]) -> Vec<i32> {
        let mut values = vec![];
        for &cycle in cycles {
            let mut value = self.register;
            while self.cycle < cycle {
                value = self.register;
                self.cycle();
            }
            values.push(value * (cycle as i32))
        }
        values
    }

    fn cycle(&mut self) {
        match &self.instructions[self.cursor] {
            Instruction::Addx(v) => {
                if !self.is_adding {
                    self.is_adding = true;
                } else {
                    self.cursor += 1;
                    self.is_adding = false;
                    self.register += v;
                }
            }
            Instruction::Noop => {
                self.cursor += 1;
            }
        }
        self.cycle += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        let input = "noop
            addx 3
            addx -5";

        let mut device = parse_input(input);
        device.cycle();
        assert_eq!(device.register, 1);
        device.cycle();
        assert_eq!(device.register, 1);
        device.cycle();
        assert_eq!(device.register, 4);
        device.cycle();
        assert_eq!(device.register, 4);
        device.cycle();
        assert_eq!(device.register, -1);
    }

    #[test]
    fn test_example_1_long() {
        let input = "addx 15
            addx -11
            addx 6
            addx -3
            addx 5
            addx -1
            addx -8
            addx 13
            addx 4
            noop
            addx -1
            addx 5
            addx -1
            addx 5
            addx -1
            addx 5
            addx -1
            addx 5
            addx -1
            addx -35
            addx 1
            addx 24
            addx -19
            addx 1
            addx 16
            addx -11
            noop
            noop
            addx 21
            addx -15
            noop
            noop
            addx -3
            addx 9
            addx 1
            addx -3
            addx 8
            addx 1
            addx 5
            noop
            noop
            noop
            noop
            noop
            addx -36
            noop
            addx 1
            addx 7
            noop
            noop
            noop
            addx 2
            addx 6
            noop
            noop
            noop
            noop
            noop
            addx 1
            noop
            noop
            addx 7
            addx 1
            noop
            addx -13
            addx 13
            addx 7
            noop
            addx 1
            addx -33
            noop
            noop
            noop
            addx 2
            noop
            noop
            noop
            addx 8
            noop
            addx -1
            addx 2
            addx 1
            noop
            addx 17
            addx -9
            addx 1
            addx 1
            addx -3
            addx 11
            noop
            noop
            addx 1
            noop
            addx 1
            noop
            noop
            addx -13
            addx -19
            addx 1
            addx 3
            addx 26
            addx -30
            addx 12
            addx -1
            addx 3
            addx 1
            noop
            noop
            noop
            addx -9
            addx 18
            addx 1
            addx 2
            noop
            noop
            addx 9
            noop
            noop
            noop
            addx -1
            addx 2
            addx -37
            addx 1
            addx 3
            noop
            addx 15
            addx -21
            addx 22
            addx -6
            addx 1
            noop
            addx 2
            addx 1
            noop
            addx -10
            noop
            noop
            addx 20
            addx 1
            addx 2
            addx 2
            addx -6
            addx -11
            noop
            noop
            noop";

        let mut device = parse_input(input);
        let values = device.signal_strength_at(&[20, 60, 100, 140, 180, 220]);
        assert_eq!(values, vec![420, 1140, 1800, 2940, 2880, 3960]);
    }

    #[test]
    fn test_example_2() {
        let input = "addx 15
            addx -11
            addx 6
            addx -3
            addx 5
            addx -1
            addx -8
            addx 13
            addx 4
            noop
            addx -1
            addx 5
            addx -1
            addx 5
            addx -1
            addx 5
            addx -1
            addx 5
            addx -1
            addx -35
            addx 1
            addx 24
            addx -19
            addx 1
            addx 16
            addx -11
            noop
            noop
            addx 21
            addx -15
            noop
            noop
            addx -3
            addx 9
            addx 1
            addx -3
            addx 8
            addx 1
            addx 5
            noop
            noop
            noop
            noop
            noop
            addx -36
            noop
            addx 1
            addx 7
            noop
            noop
            noop
            addx 2
            addx 6
            noop
            noop
            noop
            noop
            noop
            addx 1
            noop
            noop
            addx 7
            addx 1
            noop
            addx -13
            addx 13
            addx 7
            noop
            addx 1
            addx -33
            noop
            noop
            noop
            addx 2
            noop
            noop
            noop
            addx 8
            noop
            addx -1
            addx 2
            addx 1
            noop
            addx 17
            addx -9
            addx 1
            addx 1
            addx -3
            addx 11
            noop
            noop
            addx 1
            noop
            addx 1
            noop
            noop
            addx -13
            addx -19
            addx 1
            addx 3
            addx 26
            addx -30
            addx 12
            addx -1
            addx 3
            addx 1
            noop
            noop
            noop
            addx -9
            addx 18
            addx 1
            addx 2
            noop
            noop
            addx 9
            noop
            noop
            noop
            addx -1
            addx 2
            addx -37
            addx 1
            addx 3
            noop
            addx 15
            addx -21
            addx 22
            addx -6
            addx 1
            noop
            addx 2
            addx 1
            noop
            addx -10
            noop
            noop
            addx 20
            addx 1
            addx 2
            addx 2
            addx -6
            addx -11
            noop
            noop
            noop";

        let mut cpu = parse_input(input);
        let mut crt = Crt::new();

        for _ in 0..240 {
            crt.sprite_pos = cpu.register as usize;
            cpu.cycle();
            crt.draw();
        }
        let output = format!("{crt}");
        assert_eq!(
            "##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....",
            output.trim()
        );
    }
}
