use std::collections::HashSet;

use libaoc::{point::Point, DayNumber, Solver};
use nom::{bytes::streaming::tag, character::complete, Finish, IResult};

pub fn get() -> Solver {
    Solver::new(DayNumber::Fifteen, |input| {
        let tunnels = parse_input(input);
        let known_empty = count_known_empty(&tunnels, 2_000_000);
        println!("Known empty spots: {known_empty}");

        let beacon = find_missing_beacon(&tunnels, 4_000_000);

        let freq = (beacon.y as u64) + (beacon.x as u64) * 4_000_000;
        println!("Missing beacon frequency: {freq}");
    })
}

fn count_known_empty(tunnels: &Tunnels, y_coor: i32) -> usize {
    let (min_x, max_x) = get_min_max(tunnels);

    (min_x..=max_x)
        .filter(|x| {
            let pos: Point = (*x, y_coor).into();
            is_covered(pos, &tunnels.sensors)
        })
        .count()
}

fn get_min_max(tunnels: &Tunnels) -> (i32, i32) {
    let min_x = tunnels
        .sensors
        .iter()
        .map(|s| s.pos.x - s.distance as i32)
        .min()
        .unwrap();
    let max_x = tunnels
        .sensors
        .iter()
        .map(|s| s.pos.x + s.distance as i32)
        .max()
        .unwrap();
    (min_x, max_x)
}

fn is_covered(point: Point, sensors: &[Sensor]) -> bool {
    sensors
        .iter()
        .any(|s| s.pos.manhattan_distance(&point) <= s.distance)
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
struct Sensor {
    pos: Point,
    beacon: Point,
    distance: u32,
}

struct Tunnels {
    sensors: Vec<Sensor>,
}

fn parse_input(input: &str) -> Tunnels {
    let mut sensors = vec![];
    for line in input.lines() {
        let line = line.trim();
        let (_, sensor) = parse_line(line).finish().unwrap();
        sensors.push(sensor);
    }

    sensors.sort_by(|a, b| {
        if a.pos.x == b.pos.x {
            a.pos.y.cmp(&b.pos.y)
        } else {
            a.pos.x.cmp(&b.pos.x)
        }
    });
    Tunnels { sensors }
}

fn parse_line(input: &str) -> IResult<&str, Sensor> {
    let (input, sensor_pos) = parse_sensor(input)?;
    let (input, _) = tag(": ")(input)?;
    let (input, beacon_pos) = parse_beacon(input)?;

    let sensor = Sensor {
        pos: sensor_pos,
        beacon: beacon_pos,
        distance: sensor_pos.manhattan_distance(&beacon_pos),
    };

    Ok((input, sensor))
}

fn parse_sensor(input: &str) -> IResult<&str, Point> {
    let (input, _) = tag("Sensor at x=")(input)?;
    let (input, x) = complete::i32(input)?;
    let (input, _) = tag(", y=")(input)?;
    let (input, y) = complete::i32(input)?;

    let sensor = Point { x, y };

    Ok((input, sensor))
}

fn parse_beacon(input: &str) -> IResult<&str, Point> {
    let (input, _) = tag("closest beacon is at x=")(input)?;
    let (input, x) = complete::i32(input)?;
    let (input, _) = tag(", y=")(input)?;
    let (input, y) = complete::i32(input)?;

    let beacon = Point { x, y };

    Ok((input, beacon))
}

// y = m * x + b
#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
struct Line {
    m: i32,
    b: i32,
    start: Point,
    end: Point,
}

impl Line {
    fn between(start: Point, end: Point) -> Self {
        let m = (end.y - start.y) / (end.x - start.x);
        let b = start.y - (m * start.x);

        assert_eq!(m * m, 1);

        Line { m, b, start, end }
    }

    fn points(&self, min: i32, max: i32) -> Vec<Point> {
        let mut points = vec![];
        let min_x = i32::min(self.start.x, self.end.x).clamp(min, max);
        let max_x = i32::max(self.start.x, self.end.x).clamp(min, max);
        for x in min_x..=max_x {
            points.push(Point {
                x,
                y: self.m * x + self.b,
            });
        }
        points
    }
}

fn generate_border_lines(sensor: &Sensor) -> [Line; 4] {
    let distance = sensor.distance as i32 + 1;
    let bl = Line::between(
        Point {
            x: sensor.pos.x - distance,
            y: sensor.pos.y,
        },
        Point {
            x: sensor.pos.x,
            y: sensor.pos.y + distance,
        },
    );
    let br = Line::between(
        Point {
            x: sensor.pos.x + distance,
            y: sensor.pos.y,
        },
        Point {
            x: sensor.pos.x,
            y: sensor.pos.y + distance,
        },
    );
    let tl = Line::between(
        Point {
            x: sensor.pos.x - distance,
            y: sensor.pos.y,
        },
        Point {
            x: sensor.pos.x,
            y: sensor.pos.y - distance,
        },
    );
    let tr = Line::between(
        Point {
            x: sensor.pos.x + distance,
            y: sensor.pos.y,
        },
        Point {
            x: sensor.pos.x,
            y: sensor.pos.y - distance,
        },
    );

    [bl, br, tl, tr]
}

fn find_missing_beacon(tunnels: &Tunnels, limit: i32) -> Point {
    let borders = tunnels
        .sensors
        .iter()
        .map(|s| generate_border_lines(&s))
        .flatten()
        .collect::<HashSet<_>>();

    let valid = borders
        .iter()
        .filter_map(|border| {
            let points = border
                .points(0, limit)
                .into_iter()
                .filter(|&p| !is_covered(p, &tunnels.sensors))
                .collect::<Vec<_>>();
            if points.len() > 0 {
                Some(points)
            } else {
                None
            }
        })
        .flatten()
        .filter(|p| p.x >= 0 && p.y >= 0 && p.x <= limit && p.y <= limit)
        .collect::<HashSet<_>>();

    assert_eq!(valid.len(), 1);
    valid.into_iter().next().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_line() {
        let start = Point { x: -2, y: 7 };
        let end = Point { x: 8, y: 17 };
        let line = Line::between(start, end);
        assert_eq!(
            Line {
                m: 1,
                b: 9,
                start,
                end
            },
            line
        );
    }

    #[test]
    fn test_example_1() {
        let input = "Sensor at x=2, y=18: closest beacon is at x=-2, y=15
            Sensor at x=9, y=16: closest beacon is at x=10, y=16
            Sensor at x=13, y=2: closest beacon is at x=15, y=3
            Sensor at x=12, y=14: closest beacon is at x=10, y=16
            Sensor at x=10, y=20: closest beacon is at x=10, y=16
            Sensor at x=14, y=17: closest beacon is at x=10, y=16
            Sensor at x=8, y=7: closest beacon is at x=2, y=10
            Sensor at x=2, y=0: closest beacon is at x=2, y=10
            Sensor at x=0, y=11: closest beacon is at x=2, y=10
            Sensor at x=20, y=14: closest beacon is at x=25, y=17
            Sensor at x=17, y=20: closest beacon is at x=21, y=22
            Sensor at x=16, y=7: closest beacon is at x=15, y=3
            Sensor at x=14, y=3: closest beacon is at x=15, y=3
            Sensor at x=20, y=1: closest beacon is at x=15, y=3";

        let tunnels = parse_input(input);
        assert_eq!(tunnels.sensors.len(), 14);

        assert_eq!(count_known_empty(&tunnels, 10), 26);
    }

    #[test]
    fn test_example_2() {
        let input = "Sensor at x=2, y=18: closest beacon is at x=-2, y=15
            Sensor at x=9, y=16: closest beacon is at x=10, y=16
            Sensor at x=13, y=2: closest beacon is at x=15, y=3
            Sensor at x=12, y=14: closest beacon is at x=10, y=16
            Sensor at x=10, y=20: closest beacon is at x=10, y=16
            Sensor at x=14, y=17: closest beacon is at x=10, y=16
            Sensor at x=8, y=7: closest beacon is at x=2, y=10
            Sensor at x=2, y=0: closest beacon is at x=2, y=10
            Sensor at x=0, y=11: closest beacon is at x=2, y=10
            Sensor at x=20, y=14: closest beacon is at x=25, y=17
            Sensor at x=17, y=20: closest beacon is at x=21, y=22
            Sensor at x=16, y=7: closest beacon is at x=15, y=3
            Sensor at x=14, y=3: closest beacon is at x=15, y=3
            Sensor at x=20, y=1: closest beacon is at x=15, y=3";

        let tunnels = parse_input(input);

        let beacon = find_missing_beacon(&tunnels, 20);

        assert_eq!(beacon, Point { x: 14, y: 11 });
    }
}
