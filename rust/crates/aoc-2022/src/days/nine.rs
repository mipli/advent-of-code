use std::collections::HashSet;

use libaoc::{point::Point, DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Nine, |input| {
        let instructions = parse_input(input);
        let rope_short = run(1, &instructions);
        println!("The tail has covered: {}", rope_short.get_tail_sposts());
        let rope_long = run(9, &instructions);
        println!("The tail has covered: {}", rope_long.get_tail_sposts());
    })
}

fn parse_input(input: &str) -> Vec<Instruction> {
    input
        .lines()
        .map(|line| {
            let mut splits = line.trim().split(' ');
            let dir = splits.next().unwrap();
            let num: usize = splits.next().unwrap().parse().unwrap();
            match dir {
                "U" => Instruction::Up(num),
                "R" => Instruction::Right(num),
                "D" => Instruction::Down(num),
                "L" => Instruction::Left(num),
                _ => panic!("invalid direction: '{dir}'"),
            }
        })
        .collect::<Vec<_>>()
}

fn run(size: usize, instructions: &[Instruction]) -> Rope {
    let mut rope = Rope::new(size);
    for inst in instructions {
        rope.run(inst);
    }
    rope
}

#[derive(Debug, Clone, Copy)]
enum Instruction {
    Up(usize),
    Right(usize),
    Down(usize),
    Left(usize),
}

struct Rope {
    knots: Vec<Point>,
    tail_spots: HashSet<Point>,
}

impl Rope {
    fn new(size: usize) -> Self {
        let mut set = HashSet::default();
        set.insert(Point::from((0, 0)));
        Rope {
            knots: vec![Point::from((0, 0)); size + 1],
            tail_spots: set,
        }
    }

    fn run(&mut self, inst: &Instruction) {
        let (delta, count): (Point, usize) = match *inst {
            Instruction::Up(c) => ((0, 1).into(), c),
            Instruction::Right(c) => ((1, 0).into(), c),
            Instruction::Down(c) => ((0, -1).into(), c),
            Instruction::Left(c) => ((-1, 0).into(), c),
        };
        let len = self.knots.len();
        for _ in 0..count {
            self.knots[0] = self.knots[0] + delta;
            for i in 1..len {
                let head = self.knots[i - 1];
                let tail = self.knots[i];
                if head.distance(&tail) <= 1 {
                    break;
                }

                let y = (head.y - tail.y).signum();
                let x = (head.x - tail.x).signum();
                self.knots[i] = self.knots[i] + (x, y);

                if i == len - 1 {
                    self.tail_spots.insert(self.knots[i]);
                }
            }
        }
    }

    fn get_tail_sposts(&self) -> usize {
        self.tail_spots.len()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        let input = "R 4
            U 4
            L 3
            D 1
            R 4
            D 1
            L 5
            R 2";

        let instructions = parse_input(input);
        let rope = run(1, &instructions);
        assert_eq!(rope.get_tail_sposts(), 13);
    }
    #[test]
    fn test_single_move() {
        let input = "R 5
            U 8
            L 8
            D 3
            R 17
            D 10
            L 25
            U 20";

        let instructions = parse_input(input);
        let rope = run(9, &[instructions[0]]);
        assert_eq!(rope.get_tail_sposts(), 1);
    }

    #[test]
    fn test_example_2() {
        let input = "R 5
            U 8
            L 8
            D 3
            R 17
            D 10
            L 25
            U 20";

        let instructions = parse_input(input);
        let rope = run(9, &instructions);
        assert_eq!(rope.get_tail_sposts(), 36);
    }
}
