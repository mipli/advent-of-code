use std::ops::RangeInclusive;

use libaoc::{DayNumber, Solver};
use nom::{character::complete, Finish, IResult};

pub fn get() -> Solver {
    Solver::new(DayNumber::Four, |input| {
        let pairs = parse_input(input).unwrap();
        println!("Contained: {}", count_contained_completely(&pairs));
        println!("Overlapped: {}", count_overlapped(&pairs));
    })
}

fn count_contained_completely(pairs: &[Pair]) -> usize {
    pairs
        .iter()
        .filter(|pair| is_contained_completely(pair))
        .count()
}

fn count_overlapped(pairs: &[Pair]) -> usize {
    pairs.iter().filter(|pair| is_overlap(pair)).count()
}

struct Pair {
    lhs: RangeInclusive<u64>,
    rhs: RangeInclusive<u64>,
}

fn is_contained_completely(pair: &Pair) -> bool {
    pair.lhs.contains(pair.rhs.start()) && pair.lhs.contains(pair.rhs.end())
        || pair.rhs.contains(pair.lhs.start()) && pair.rhs.contains(pair.lhs.end())
}

fn is_overlap(pair: &Pair) -> bool {
    pair.lhs.contains(pair.rhs.start())
        || pair.lhs.contains(pair.rhs.end())
        || pair.rhs.contains(pair.lhs.start())
        || pair.rhs.contains(pair.lhs.end())
}

fn parse_input(input: &str) -> anyhow::Result<Vec<Pair>> {
    let mut pairs = Vec::with_capacity(input.lines().count());
    for line in input.lines() {
        let pair = match parse_pair(line.trim()).finish() {
            Ok((_, pair)) => pair,
            Err(nom::error::Error { input, code }) => {
                return Err(anyhow::Error::new(nom::error::Error {
                    code,
                    input: input.to_string(),
                }))
            }
        };
        pairs.push(pair);
    }
    Ok(pairs)
}

fn parse_range(input: &str) -> IResult<&str, RangeInclusive<u64>> {
    let (input, a) = complete::u64(input)?;
    let (input, _) = complete::char('-')(input)?;
    let (input, b) = complete::u64(input)?;
    Ok((input, a..=b))
}

fn parse_pair(input: &str) -> IResult<&str, Pair> {
    let (input, lhs) = parse_range(input)?;
    let (input, _) = complete::char(',')(input)?;
    let (input, rhs) = parse_range(input)?;

    let pair = Pair { lhs, rhs };

    Ok((input, pair))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        let input = "2-4,6-8
            2-3,4-5
            5-7,7-9
            2-8,3-7
            6-6,4-6
            2-6,4-8";

        let pairs = parse_input(input).unwrap();
        assert_eq!(pairs.len(), 6);
        assert_eq!(pairs[0].lhs, 2..=4);
        assert_eq!(pairs[0].rhs, 6..=8);

        assert!(!is_contained_completely(&pairs[1]));
        assert!(is_contained_completely(&pairs[3]));
        assert!(is_contained_completely(&pairs[4]));

        assert_eq!(count_contained_completely(&pairs), 2);
    }

    #[test]
    fn test_example_2() {
        let input = "2-4,6-8
            2-3,4-5
            5-7,7-9
            2-8,3-7
            6-6,4-6
            2-6,4-8";

        let pairs = parse_input(input).unwrap();

        assert!(!is_overlap(&pairs[0]));
        assert!(is_overlap(&pairs[2]));
        assert!(is_overlap(&pairs[4]));

        assert_eq!(count_overlapped(&pairs), 4);
    }
}
