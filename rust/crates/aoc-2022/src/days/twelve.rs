use std::collections::{BinaryHeap, HashSet};

use libaoc::{
    grid::{Grid, NeighbourConnection},
    path,
    point::Point,
    DayNumber, Solver,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::Twelve, |input| {
        let (start, goal, grid) = parse_input(input);
        let path = find_path(start, goal, &grid);
        println!("Path length: {}", path.len());
        println!(
            "Optimal path length: {}",
            find_path_optimal_start(goal, &grid)
        );
    })
}

fn parse_input(input: &str) -> (Point, Point, Grid<char>) {
    let height = input.lines().count();
    let width = input.lines().next().unwrap().trim().len();

    let data = input
        .lines()
        .flat_map(|line| line.trim().chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let mut grid = Grid::new_with_data(width, height, NeighbourConnection::Four, data);
    let mut start = None;
    let mut goal = None;
    for point in grid.point_iter() {
        let c = grid.get_mut(point);
        if *c == 'S' {
            start = Some(point);
            *c = 'a';
        } else if *c == 'E' {
            goal = Some(point);
            *c = 'z';
        }
    }
    (start.unwrap(), goal.unwrap(), grid)
}
fn find_path_optimal_start(goal: Point, grid: &Grid<char>) -> u32 {
    let field = bfs(goal, grid);

    grid.point_iter()
        .filter_map(|point| {
            if grid.get(point) == &'a' {
                match *field.get(point) {
                    Some(score) if score > 0 => Some(score),
                    _ => None,
                }
            } else {
                None
            }
        })
        .min()
        .unwrap()
}

fn find_path(start: Point, end: Point, grid: &Grid<char>) -> Vec<Point> {
    let get_neighbours = |pos| {
        let base = *grid.get(pos) as u8;
        grid.get_neigbours(pos)
            .into_iter()
            .filter(|p| {
                let step = *grid.get(*p) as u8;
                step <= (base + 1)
            })
            .collect::<Vec<_>>()
    };
    let scorer = |start: Point, goal: Point| start.manhattan_distance(&goal);
    path::find(start, end, get_neighbours, scorer).unwrap_or_default()
}

#[allow(dead_code)]
fn display_grid_path(grid: &Grid<char>, path: &[Point]) {
    let mut pre_y = 0;
    grid.point_iter().for_each(|point| {
        if point.y != pre_y {
            println!();
        }
        if !path.contains(&point) {
            print!("{}", grid.get(point));
        } else {
            print!("{}", format!("{}", grid.get(point)).to_uppercase());
        }
        pre_y = point.y;
    });
    println!();
}

fn bfs(start: Point, grid: &Grid<char>) -> Grid<Option<u32>> {
    #[derive(Debug, Clone, Copy, PartialEq)]
    struct SearchNode {
        pos: Point,
        steps: u32,
    }

    impl std::cmp::Eq for SearchNode {}

    impl std::cmp::Ord for SearchNode {
        fn cmp(&self, other: &Self) -> std::cmp::Ordering {
            other.steps.cmp(&self.steps)
        }
    }

    impl std::cmp::PartialOrd for SearchNode {
        fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
            Some(self.cmp(other))
        }
    }

    let get_neighbours = |pos| {
        let base = *grid.get(pos) as u8;
        grid.get_neigbours(pos)
            .into_iter()
            .filter(|p| {
                let step = *grid.get(*p) as u8;
                base <= step + 1
            })
            .collect::<Vec<_>>()
    };
    let mut field = Grid::new(
        grid.get_width(),
        grid.get_height(),
        NeighbourConnection::Four,
        None,
    );
    let mut frontier: BinaryHeap<SearchNode> = BinaryHeap::default();
    frontier.push(SearchNode {
        pos: start,
        steps: 0,
    });
    let mut visisted: HashSet<Point> = HashSet::default();
    while let Some(node) = frontier.pop() {
        if field.get(node.pos).is_some() {
            continue;
        }
        visisted.insert(node.pos);
        field.set(node.pos, Some(node.steps));

        let neigbours = get_neighbours(node.pos);

        for neigbour in neigbours {
            if field.get(neigbour).is_some() {
                continue;
            }
            frontier.push(SearchNode {
                pos: neigbour,
                steps: node.steps + 1,
            });
        }
    }

    field
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        let input = "Sabqponm
            abcryxxl
            accszExk
            acctuvwj
            abdefghi";

        let (start, goal, grid) = parse_input(input);
        assert_eq!(start, (0, 0).into());
        assert_eq!(goal, (5, 2).into());

        let path = find_path(start, goal, &grid);
        assert_eq!(path.len(), 31);
    }

    #[test]
    fn test_example_2() {
        let input = "Sabqponm
            abcryxxl
            accszExk
            acctuvwj
            abdefghi";

        let (_, goal, grid) = parse_input(input);
        assert_eq!(find_path_optimal_start(goal, &grid), 29);
    }
}
