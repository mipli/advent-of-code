use std::str::FromStr;

use anyhow::bail;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Two, |input| {
        let rounds_1 = parse_input_part_1(input);

        let summed_1 = rounds_1.iter().map(|(e, m)| get_score(e, m)).sum::<u32>();
        println!("Score: {summed_1}");

        let rounds_2 = parse_input_part_2(input);

        let summed_2 = rounds_2
            .iter()
            .map(|(e, m)| get_score_part_2(e, m))
            .sum::<u32>();
        println!("Score: {summed_2}");
    })
}

enum ElfChoice {
    Rock,
    Paper,
    Scissor,
}

impl FromStr for ElfChoice {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let elf = match s {
            "A" => ElfChoice::Rock,
            "B" => ElfChoice::Paper,
            "C" => ElfChoice::Scissor,
            _ => bail!("Invalid input char: '{s}'"),
        };
        Ok(elf)
    }
}

enum MyChoice {
    Rock,
    Paper,
    Scissor,
}

impl FromStr for MyChoice {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let my = match s {
            "X" => MyChoice::Rock,
            "Y" => MyChoice::Paper,
            "Z" => MyChoice::Scissor,
            _ => bail!("Invalid input char: '{s}'"),
        };
        Ok(my)
    }
}

impl From<&MyChoice> for u32 {
    fn from(value: &MyChoice) -> Self {
        match value {
            MyChoice::Rock => 1,
            MyChoice::Paper => 2,
            MyChoice::Scissor => 3,
        }
    }
}

enum GameResult {
    Win,
    Draw,
    Loss,
}

impl FromStr for GameResult {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let res = match s {
            "X" => GameResult::Loss,
            "Y" => GameResult::Draw,
            "Z" => GameResult::Win,
            _ => bail!("Invalid input char: '{s}'"),
        };
        Ok(res)
    }
}

impl From<&GameResult> for u32 {
    fn from(value: &GameResult) -> Self {
        match value {
            GameResult::Win => 6,
            GameResult::Draw => 3,
            GameResult::Loss => 0,
        }
    }
}

fn get_result(elf: &ElfChoice, my: &MyChoice) -> GameResult {
    match (elf, my) {
        (ElfChoice::Rock, MyChoice::Rock) => GameResult::Draw,
        (ElfChoice::Rock, MyChoice::Paper) => GameResult::Win,
        (ElfChoice::Rock, MyChoice::Scissor) => GameResult::Loss,
        (ElfChoice::Paper, MyChoice::Rock) => GameResult::Loss,
        (ElfChoice::Paper, MyChoice::Paper) => GameResult::Draw,
        (ElfChoice::Paper, MyChoice::Scissor) => GameResult::Win,
        (ElfChoice::Scissor, MyChoice::Rock) => GameResult::Win,
        (ElfChoice::Scissor, MyChoice::Paper) => GameResult::Loss,
        (ElfChoice::Scissor, MyChoice::Scissor) => GameResult::Draw,
    }
}

fn get_score(elf: &ElfChoice, my: &MyChoice) -> u32 {
    let result = get_result(elf, my);
    u32::from(my) + u32::from(&result)
}

fn get_score_part_2(elf: &ElfChoice, res: &GameResult) -> u32 {
    let my = get_my_choice(elf, res);
    u32::from(&my) + u32::from(res)
}

fn get_my_choice(elf: &ElfChoice, res: &GameResult) -> MyChoice {
    match (elf, res) {
        (ElfChoice::Rock, GameResult::Win) => MyChoice::Paper,
        (ElfChoice::Rock, GameResult::Draw) => MyChoice::Rock,
        (ElfChoice::Rock, GameResult::Loss) => MyChoice::Scissor,
        (ElfChoice::Paper, GameResult::Win) => MyChoice::Scissor,
        (ElfChoice::Paper, GameResult::Draw) => MyChoice::Paper,
        (ElfChoice::Paper, GameResult::Loss) => MyChoice::Rock,
        (ElfChoice::Scissor, GameResult::Win) => MyChoice::Rock,
        (ElfChoice::Scissor, GameResult::Draw) => MyChoice::Scissor,
        (ElfChoice::Scissor, GameResult::Loss) => MyChoice::Paper,
    }
}

fn parse_input_part_2(input: &str) -> Vec<(ElfChoice, GameResult)> {
    input
        .lines()
        .map(|line| {
            let mut chars = line.trim().split(' ');

            match (chars.next(), chars.next()) {
                (Some(elf_char), Some(res_char)) => {
                    let elf = elf_char.parse().unwrap();
                    let res = res_char.parse().unwrap();
                    (elf, res)
                }
                _ => panic!("Invalid input line: '{line}'"),
            }
        })
        .collect()
}

fn parse_input_part_1(input: &str) -> Vec<(ElfChoice, MyChoice)> {
    input
        .lines()
        .map(|line| {
            let mut chars = line.trim().split(' ');

            match (chars.next(), chars.next()) {
                (Some(elf_char), Some(my_char)) => {
                    let elf = elf_char.parse().unwrap();
                    let my = my_char.parse().unwrap();
                    (elf, my)
                }
                _ => panic!("Invalid input line: '{line}'"),
            }
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        let input = "A Y
            B X
            C Z";

        let rounds = parse_input_part_1(input);

        let summed = rounds.iter().map(|(e, m)| get_score(e, m)).sum::<u32>();
        assert_eq!(summed, 15);
    }

    #[test]
    fn test_example_2() {
        let input = "A Y
            B X
            C Z";

        let rounds = parse_input_part_2(input);

        let summed = rounds
            .iter()
            .map(|(e, m)| get_score_part_2(e, m))
            .sum::<u32>();
        assert_eq!(summed, 12);
    }
}
