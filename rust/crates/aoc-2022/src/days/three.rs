use itertools::Itertools;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Three, |input| {
        let rucksacks = parse_input(input);
        let sum = union_values(&rucksacks);
        println!("Sum of values: {sum}");
        let group_sum = group_badges(&rucksacks);
        println!("Sum of groups: {group_sum}");
    })
}

type Rucksack = (Vec<u64>, Vec<u64>);

fn group_badges(sacks: &[Rucksack]) -> u64 {
    sacks
        .iter()
        .chunks(3)
        .into_iter()
        .map(|mut chunks| {
            let a = chunks.next().unwrap();
            let b = chunks.next().unwrap();
            let c = chunks.next().unwrap();
            find_badge(&[a, b, c])
        })
        .sum::<u64>()
}

fn find_badge(sacks: &[&Rucksack; 3]) -> u64 {
    let is_in = |c: &u64, sack: &Rucksack| sack.0.contains(c) || sack.1.contains(c);
    for c in sacks[0].0.iter().chain(sacks[0].1.iter()) {
        if is_in(c, sacks[1]) && is_in(c, sacks[2]) {
            return *c;
        }
    }
    panic!("There should always be a badge");
}

fn union_values(sacks: &[Rucksack]) -> u64 {
    sacks
        .iter()
        .map(|sack| get_union(sack).unwrap())
        .sum::<u64>()
}

fn parse_input(input: &str) -> Vec<Rucksack> {
    let to_value = |c: char| {
        let v = if c.is_uppercase() {
            c as u8 - 64 + 26
        } else {
            c as u8 - 96
        };
        v as u64
    };

    input
        .lines()
        .map(|line| {
            let line = line.trim();
            let len = line.len();
            let (fst, snd) = line.split_at(len / 2);
            (
                fst.chars().map(to_value).collect(),
                snd.chars().map(to_value).collect(),
            )
        })
        .collect()
}

fn get_union(sack: &Rucksack) -> Option<&u64> {
    sack.0.iter().find(|c| sack.1.contains(c))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        let input = "vJrwpWtwJgWrhcsFMMfFFhFp
            jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
            PmmdzqPrVvPwwTWBwg
            wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
            ttgJtRGJQctTZtZT
            CrZsJsPPZsGzwwsLwLmpwMDw";

        let rucksacks = parse_input(input);
        assert_eq!(rucksacks.len(), 6);
        (0..6).for_each(|i| {
            assert_eq!(rucksacks[i].0.len(), rucksacks[i].1.len());
        });
        assert_eq!(get_union(&rucksacks[0]), Some(&16));
        assert_eq!(get_union(&rucksacks[1]), Some(&38));
        assert_eq!(get_union(&rucksacks[2]), Some(&42));
        assert_eq!(get_union(&rucksacks[3]), Some(&22));
        assert_eq!(get_union(&rucksacks[4]), Some(&20));
        assert_eq!(get_union(&rucksacks[5]), Some(&19));
        let sum = union_values(&rucksacks);
        assert_eq!(sum, 157);
    }

    #[test]
    fn test_example_2() {
        let input = "vJrwpWtwJgWrhcsFMMfFFhFp
            jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
            PmmdzqPrVvPwwTWBwg
            wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
            ttgJtRGJQctTZtZT
            CrZsJsPPZsGzwwsLwLmpwMDw";

        let rucksacks = parse_input(input);
        let group_sum = group_badges(&rucksacks);
        assert_eq!(group_sum, 70);
    }
}
