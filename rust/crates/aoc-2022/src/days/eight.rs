use itertools::{FoldWhile, Itertools};
use libaoc::{
    grid::{Grid, NeighbourConnection},
    point::Point,
    DayNumber, Solver,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::Eight, |input| {
        let grid = parse_input(input);
        let visible = count_visible(&grid);
        println!("Visible trees: {visible}");
        let scenic_score = max_scenic_score(&grid);
        println!("Max scenic score: {scenic_score}");
    })
}

fn parse_input(input: &str) -> Grid<u8> {
    let height = input.lines().count();
    let width = input.lines().next().unwrap().trim().len();

    let data = input
        .lines()
        .flat_map(|line| {
            line.trim()
                .chars()
                .map(|c| u8::try_from(c).unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    Grid::new_with_data(width, height, NeighbourConnection::Four, data)
}

fn max_scenic_score(grid: &Grid<u8>) -> u32 {
    grid.point_iter()
        .map(|p| scenic_score(grid, p))
        .max()
        .unwrap()
}

fn scenic_score<P: Into<Point>>(grid: &Grid<u8>, point: P) -> u32 {
    let point: Point = point.into();
    if point.x == 0
        || point.y == 0
        || point.y == (grid.get_height() as i32) - 1
        || point.x == (grid.get_width() as i32) - 1
    {
        return 0;
    }

    let tree = grid.get(point);

    let mut score = (0..point.x)
        .rev()
        .fold_while(0u32, |acc, x| {
            let v = grid.get((x, point.y).into());
            if v < tree {
                FoldWhile::Continue(acc + 1)
            } else {
                FoldWhile::Done(acc + 1)
            }
        })
        .into_inner();
    score *= (point.x + 1..grid.get_width() as i32)
        .fold_while(0u32, |acc, x| {
            let v = grid.get((x, point.y).into());
            if v < tree {
                FoldWhile::Continue(acc + 1)
            } else {
                FoldWhile::Done(acc + 1)
            }
        })
        .into_inner();
    score *= (0..point.y)
        .rev()
        .fold_while(0u32, |acc, y| {
            let v = grid.get((point.x, y).into());
            if v < tree {
                FoldWhile::Continue(acc + 1)
            } else {
                FoldWhile::Done(acc + 1)
            }
        })
        .into_inner();
    score *= (point.y + 1..grid.get_height() as i32)
        .fold_while(0u32, |acc, y| {
            let v = grid.get((point.x, y).into());
            if v < tree {
                FoldWhile::Continue(acc + 1)
            } else {
                FoldWhile::Done(acc + 1)
            }
        })
        .into_inner();
    score
}

fn count_visible(grid: &Grid<u8>) -> usize {
    grid.point_iter()
        .filter(|p| visible_from_outside(grid, *p))
        .count()
}

fn visible_from_outside<P: Into<Point>>(grid: &Grid<u8>, point: P) -> bool {
    let point: Point = point.into();
    if point.x == 0
        || point.y == 0
        || point.y == (grid.get_height() as i32) - 1
        || point.x == (grid.get_width() as i32) - 1
    {
        // we're on the edge and thus always visible
        return true;
    }

    let tree = grid.get(point);

    if (0..point.x).all(|x| {
        let blocker = grid.get((x, point.y).into());
        blocker < tree
    }) {
        return true;
    }
    if (point.x + 1..grid.get_width() as i32).all(|x| {
        let blocker = grid.get((x, point.y).into());
        blocker < tree
    }) {
        return true;
    }
    if (0..point.y).all(|y| {
        let blocker = grid.get((point.x, y).into());
        blocker < tree
    }) {
        return true;
    }
    if (point.y + 1..grid.get_height() as i32).all(|y| {
        let blocker = grid.get((point.x, y).into());
        blocker < tree
    }) {
        return true;
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        let input = "30373
            25512
            65332
            33549
            35390";

        let grid = parse_input(input);
        assert_eq!(grid.get_width(), 5);
        assert_eq!(grid.get_height(), 5);
        assert!(visible_from_outside(&grid, (2, 1)));
        assert!(visible_from_outside(&grid, (0, 0)));
        assert!(visible_from_outside(&grid, (4, 4)));
        assert!(!visible_from_outside(&grid, (2, 2)));
        assert!(visible_from_outside(&grid, (1, 1)));
        assert!(!visible_from_outside(&grid, (3, 3)));
        assert_eq!(count_visible(&grid), 21);
    }

    #[test]
    fn test_example_2() {
        let input = "30373
            25512
            65332
            33549
            35390";

        let grid = parse_input(input);
        assert_eq!(max_scenic_score(&grid), 8);
    }
}
