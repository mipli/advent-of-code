use itertools::Itertools;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Six, |input| {
        let offset = get_marker::<4>(input);
        println!("Package markers ends at: {offset}");
        let offset = get_marker::<14>(input);
        println!("Message markers ends at: {offset}");
    })
}

pub fn get_marker<const T: usize>(input: &str) -> usize {
    input
        .as_bytes()
        .array_windows::<T>()
        .position(|window| {
            window.iter().tuple_combinations().all(|(a, b)| a != b)
            // for (idx, p) in window.iter().enumerate().take(T - 1) {
            //     if window[idx + 1..window.len()].iter().any(|c| c == p) {
            //         return false;
            //     }
            // }
            // true
        })
        .map(|o| o + T)
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_examples_1() {
        let input = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";
        let offset = get_marker::<4>(input);
        assert_eq!(offset, 7);
    }
}
