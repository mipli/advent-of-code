use libaoc::{DayNumber, Solver};
use nom::{
    branch, bytes::complete::tag, character::complete, combinator, multi, sequence, Finish, IResult,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::Five, |input| {
        let (mut loading_bay, instructions) = parse_input(input);
        loading_bay.run_9000(&instructions);
        println!("Loading bay message: {}", loading_bay.read());

        let (mut loading_bay, instructions) = parse_input(input);
        loading_bay.run_9001(&instructions);
        println!("Loading bay message, 9001 mode: {}", loading_bay.read());
    })
}

#[derive(Debug)]
struct LoadingBay {
    stacks: Vec<Vec<char>>,
}

impl LoadingBay {
    fn run_9000(&mut self, instructions: &[Instruction]) {
        for instruction in instructions {
            for _ in 0..instruction.count {
                let c = self.stacks[instruction.source].pop().unwrap();
                self.stacks[instruction.destination].push(c);
            }
        }
    }
    fn run_9001(&mut self, instructions: &[Instruction]) {
        for instruction in instructions {
            let (source, dest) = if instruction.source < instruction.destination {
                let (lhs, rhs) = self.stacks.split_at_mut(instruction.source + 1);
                (
                    &mut lhs[instruction.source],
                    &mut rhs[instruction.destination - instruction.source - 1],
                )
            } else {
                let (lhs, rhs) = self.stacks.split_at_mut(instruction.destination + 1);
                (
                    &mut rhs[instruction.source - instruction.destination - 1],
                    &mut lhs[instruction.destination],
                )
            };
            let len = source.len();
            dest.extend_from_slice(&source[(len - instruction.count)..len]);
            source.truncate(len - instruction.count);
        }
    }

    fn read(&self) -> String {
        let mut output = String::new();

        for stack in &self.stacks {
            output.push(*stack.last().unwrap());
        }

        output
    }
}

#[derive(Debug, Eq, PartialEq)]
struct Instruction {
    source: usize,
    destination: usize,
    count: usize,
}

#[derive(Debug)]
enum ParseStack {
    Full(char),
    Empty,
}

fn parse_input(input: &str) -> (LoadingBay, Vec<Instruction>) {
    let mut splits = input.split("\n\n");

    let input_bay = splits.next().unwrap();
    let input_inst = splits.next().unwrap();

    let loading_bay = parse_loading_bay(input_bay).unwrap();
    let instructions = parse_loading_instructions(input_inst).unwrap();

    (loading_bay, instructions)
}

fn parse_loading_instructions(input: &str) -> anyhow::Result<Vec<Instruction>> {
    let mut instructions = vec![];
    for line in input.lines() {
        let instruction = match parse_loading_instruction(line.trim()).finish() {
            Ok((_, inst)) => inst,
            Err(nom::error::Error { input, code }) => {
                return Err(anyhow::Error::new(nom::error::Error {
                    code,
                    input: input.to_string(),
                }))
            }
        };
        instructions.push(instruction);
    }
    Ok(instructions)
}

fn parse_loading_instruction(input: &str) -> IResult<&str, Instruction> {
    let (input, _) = tag("move ")(input)?;
    let (input, count) = complete::u32(input)?;
    let (input, _) = tag(" from ")(input)?;
    let (input, source) = complete::u32(input)?;
    let (input, _) = tag(" to ")(input)?;
    let (input, dest) = complete::u32(input)?;

    Ok((
        input,
        Instruction {
            source: source as usize - 1,
            destination: dest as usize - 1,
            count: count as usize,
        },
    ))
}

fn parse_loading_bay(input: &str) -> anyhow::Result<LoadingBay> {
    let mut bay = LoadingBay { stacks: vec![] };
    for line in input.lines() {
        if !line.contains('[') {
            continue;
        }
        let stacks: Vec<ParseStack> = match parse_stack_line(line).finish() {
            Ok((_, stacks)) => stacks,
            Err(nom::error::Error { input, code }) => {
                return Err(anyhow::Error::new(nom::error::Error {
                    code,
                    input: input.to_string(),
                }))
            }
        };
        stacks
            .into_iter()
            .enumerate()
            .filter_map(|(idx, val)| match val {
                ParseStack::Full(c) => Some((idx, c)),
                ParseStack::Empty => None,
            })
            .for_each(|(idx, val)| {
                if bay.stacks.len() <= idx {
                    bay.stacks.resize(idx + 1, vec![]);
                }
                bay.stacks[idx].insert(0, val);
            });
    }
    Ok(bay)
}

fn parse_stack_line(input: &str) -> IResult<&str, Vec<ParseStack>> {
    let (input, stacks) = multi::separated_list1(
        complete::char(' '),
        branch::alt((parse_full_stack, parse_empty_stack)),
    )(input)?;

    Ok((input, stacks))
}

fn parse_full_stack(input: &str) -> IResult<&str, ParseStack> {
    let (input, c) = sequence::delimited(
        complete::char('['),
        combinator::map(complete::alpha1, |s: &str| s.chars().next().unwrap()),
        complete::char(']'),
    )(input)?;

    Ok((input, ParseStack::Full(c)))
}

fn parse_empty_stack(input: &str) -> IResult<&str, ParseStack> {
    let (input, _) = tag("   ")(input)?;

    Ok((input, ParseStack::Empty))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parsing() {
        let input = "    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";

        let (loading_bay, instructions) = parse_input(input);
        assert_eq!(loading_bay.stacks[0], vec!['Z', 'N']);
        assert_eq!(loading_bay.stacks[1], vec!['M', 'C', 'D']);
        assert_eq!(loading_bay.stacks[2], vec!['P']);

        assert_eq!(instructions.len(), 4);
        assert_eq!(
            instructions[0],
            Instruction {
                source: 1,
                destination: 0,
                count: 1
            }
        );
        assert_eq!(
            instructions[1],
            Instruction {
                source: 0,
                destination: 2,
                count: 3
            }
        );
        assert_eq!(
            instructions[2],
            Instruction {
                source: 1,
                destination: 0,
                count: 2
            }
        );
        assert_eq!(
            instructions[3],
            Instruction {
                source: 0,
                destination: 1,
                count: 1
            }
        );
    }

    #[test]
    fn test_example_1() {
        let input = "    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";

        let (mut loading_bay, instructions) = parse_input(input);
        loading_bay.run_9000(&instructions);
        assert_eq!(loading_bay.read(), "CMZ".to_string());
    }

    #[test]
    fn test_example_2() {
        let input = "    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";

        let (mut loading_bay, instructions) = parse_input(input);
        loading_bay.run_9001(&instructions);
        assert_eq!(loading_bay.read(), "MCD".to_string());
    }
}
