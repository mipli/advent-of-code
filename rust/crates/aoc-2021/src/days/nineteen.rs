use std::collections::{HashMap, HashSet};

use itertools::Itertools;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Nineteen, |input| {
        let scanners = parse_input(input);
        let map = position_scanner_list(scanners);
        println!("Beacons: {}", map.beacons.len());
        println!("Cave Size: {}", map.get_max_distance());
    })
}

fn position_scanner_list(mut scanners: Vec<Scanner>) -> Map {
    scanners.reverse();
    let mut map = Map::default();

    let mut tried: HashSet<(i32, i32)> = HashSet::default();

    while let Some(scanner) = scanners.pop() {
        if let Some(scanner) = attempt_insertion(scanner, &mut map, &mut tried) {
            scanners.insert(0, scanner);
        }
    }
    map
}

fn attempt_insertion(
    mut scanner: Scanner,
    map: &mut Map,
    tried: &mut HashSet<(i32, i32)>,
) -> Option<Scanner> {
    if map.scanners.is_empty() {
        map.add_scanner(scanner);
        return None;
    }

    for i in 0..map.scanners.len() {
        if tried.contains(&(map.scanners[i].id, scanner.id)) {
            continue;
        }
        if let Some(rot) = get_overlap_rotation(&map.scanners[i], &scanner, 12) {
            scanner.rotate(rot);
            let translation = find_translation(&map.scanners[i], &scanner);
            scanner.translate(translation);
            map.add_scanner(scanner.clone());
            return None;
        } else {
            tried.insert((map.scanners[i].id, scanner.id));
            tried.insert((scanner.id, map.scanners[i].id));
        }
    }
    Some(scanner)
}

type Point = (i32, i32, i32);

#[derive(Debug, Default)]
struct Map {
    scanners: Vec<Scanner>,
    beacons: HashSet<Point>,
}

impl Map {
    fn add_scanner(&mut self, scanner: Scanner) {
        scanner.beacons.iter().for_each(|beacon| {
            self.beacons.insert(*beacon);
        });
        self.scanners.push(scanner);
    }

    fn get_max_distance(&self) -> i32 {
        self.scanners
            .iter()
            .permutations(2)
            .map(|p| manhattan_distance(p[0].position, p[1].position))
            .max()
            .expect("There is always a max")
    }
}

fn manhattan_distance(a: Point, b: Point) -> i32 {
    (a.0 - b.0).abs() + (a.1 - b.1).abs() + (a.2 - b.2).abs()
}

#[derive(Debug, Clone)]
struct Scanner {
    id: i32,
    position: Point,
    beacons: Vec<Point>,
    deltas: Vec<(Point, usize, usize)>,
    distances: HashMap<i32, Vec<(Point, Point)>>,
}

impl Scanner {
    fn new(id: i32, position: Point, beacons: Vec<Point>) -> Scanner {
        let mut deltas = vec![];
        let mut distances: HashMap<i32, Vec<(Point, Point)>> = HashMap::default();

        for i in 0..beacons.len() {
            for j in 0..beacons.len() {
                if i != j {
                    let delta = (
                        beacons[i].0 - beacons[j].0,
                        beacons[i].1 - beacons[j].1,
                        beacons[i].2 - beacons[j].2,
                    );
                    deltas.push((delta, i, j));

                    if j > i {
                        let distance = manhattan_distance(beacons[i], beacons[j]);
                        distances
                            .entry(distance)
                            .or_default()
                            .push((beacons[i], beacons[j]));
                    }
                }
            }
        }

        Scanner {
            id,
            position,
            beacons,
            deltas,
            distances,
        }
    }

    fn rotate(&mut self, rot: u8) {
        self.beacons
            .iter_mut()
            .for_each(|beacon| *beacon = rotate(*beacon, rot));
        self.deltas
            .iter_mut()
            .for_each(|delta| delta.0 = rotate(delta.0, rot));
    }

    fn match_rotated_deltas(&self, rot: u8, delta: Point) -> bool {
        self.deltas.iter().any(|d| rotate(d.0, rot) == delta)
    }

    fn translate(&mut self, translation: Point) {
        self.position = translation;
        self.beacons.iter_mut().for_each(|beacon| {
            *beacon = (
                beacon.0 + translation.0,
                beacon.1 + translation.1,
                beacon.2 + translation.2,
            )
        });
    }
}

fn get_combinations(requirement: usize) -> usize {
    // we know there should be at least 12 beacons that overlap, and the distances we compare are
    // done in pairs of two.
    //
    // So we need to figure out how many combinations of two there are within a set of 12, which
    // the is binomial coefficient of 12 over 2.
    //
    // Formula: n! / (k! (n - k)!)
    //
    //
    match requirement {
        6 => 15,
        12 => 66,
        _ => unimplemented!("Need to implement combinations of two for this value"),
    }
}

fn is_distance_match(scanner_one: &Scanner, scanner_two: &Scanner, requirement: usize) -> bool {
    let mut matches = 0;
    for (distance, beacons) in scanner_two.distances.iter() {
        if let Some(one_beacons) = scanner_one.distances.get(distance) {
            matches += std::cmp::min(beacons.len(), one_beacons.len());
        }
    }

    matches >= get_combinations(requirement)
}

fn get_overlap_rotation(
    scanner_one: &Scanner,
    scanner_two: &Scanner,
    requirement: usize,
) -> Option<u8> {
    if !is_distance_match(scanner_one, scanner_two, requirement) {
        return None;
    }
    for rot in 0..24 {
        let count = scanner_one
            .deltas
            .iter()
            .filter(|delta| scanner_two.match_rotated_deltas(rot, delta.0))
            .count();

        if count >= requirement * (requirement - 1) {
            return Some(rot);
        }
    }

    None
}

fn find_translation(origin: &Scanner, scanner: &Scanner) -> Point {
    let mut origin_beacons: HashSet<Point> = HashSet::default();
    let mut scanner_beacons: HashSet<Point> = HashSet::default();
    for delta in &origin.deltas {
        for sd in &scanner.deltas {
            if sd.0 == delta.0 {
                origin_beacons.insert(origin.beacons[delta.1]);
                scanner_beacons.insert(scanner.beacons[sd.1]);
            }
        }
    }

    for origin_beacon in origin_beacons.iter() {
        for scanner_beacon in scanner_beacons.iter() {
            let translation = (
                origin_beacon.0 - scanner_beacon.0,
                origin_beacon.1 - scanner_beacon.1,
                origin_beacon.2 - scanner_beacon.2,
            );
            let count = scanner
                .beacons
                .iter()
                .filter(|beacon| {
                    let new_current = (
                        beacon.0 + translation.0,
                        beacon.1 + translation.1,
                        beacon.2 + translation.2,
                    );
                    origin.beacons.iter().any(|&beacon| beacon == new_current)
                })
                .count();

            if count == scanner_beacons.len() {
                return (translation.0, translation.1, translation.2);
            }
        }
    }
    unreachable!();
}

const fn rotate(point: Point, rot: u8) -> Point {
    match rot {
        0 => (point.0, point.1, point.2),     //[x, y, z]
        1 => (point.0, point.2, -point.1),    //[x, z, -y],
        2 => (point.0, -point.1, -point.2),   //[x, -y, -z],
        3 => (point.0, -point.2, point.1),    //[x, -z, y],
        4 => (point.1, point.0, -point.2),    //[y, x, -z],
        5 => (point.1, point.2, point.0),     //[y, z, x],
        6 => (point.1, -point.0, point.2),    //[y, -x, z],
        7 => (point.1, -point.2, -point.0),   //[y, -z, -x],
        8 => (point.2, point.0, point.1),     //[z, x, y],
        9 => (point.2, point.1, -point.0),    //[z, y, -x],
        10 => (point.2, -point.0, -point.1),  //[z, -x, -y],
        11 => (point.2, -point.1, point.0),   //[z, -y, x],
        12 => (-point.0, point.1, -point.2),  //[-x, y, -z],
        13 => (-point.0, point.2, point.1),   //[-x, z, y],
        14 => (-point.0, -point.1, point.2),  //[-x, -y, z],
        15 => (-point.0, -point.2, -point.1), //[-x, -z, -y],
        16 => (-point.1, point.0, point.2),   //[-y, x, z],
        17 => (-point.1, point.2, -point.0),  //[-y, z, -x],
        18 => (-point.1, -point.0, -point.2), //[-y, -x, -z],
        19 => (-point.1, -point.2, point.0),  //[-y, -z, x],
        20 => (-point.2, point.0, -point.1),  //[-z, x, -y],
        21 => (-point.2, point.1, point.0),   //[-z, y, x],
        22 => (-point.2, -point.0, point.1),  //[-z, -x, y],
        23 => (-point.2, -point.1, -point.0), //[-z, -y, -x],
        _ => unreachable!(),
    }
}

fn parse_input(input: &str) -> Vec<Scanner> {
    enum ParseState {
        Beacon,
        Scanner,
    }

    let mut state = ParseState::Beacon;

    let mut scanners = vec![];
    let mut beacons = vec![];

    let mut current_id: i32 = 0;

    for line in input.lines().skip(1) {
        let line = line.trim();
        if line.is_empty() {
            state = ParseState::Scanner;
            continue;
        }
        match state {
            ParseState::Beacon => {
                let beacon: (i32, i32, i32) = serde_scan::scan!("{},{},{}"<- line)
                    .expect("Failed to parse beacon coordinates");
                beacons.push(beacon);
            }
            ParseState::Scanner => {
                let mut tmp = vec![];
                std::mem::swap(&mut tmp, &mut beacons);

                scanners.push(Scanner::new(current_id, (0, 0, 0), tmp));
                state = ParseState::Beacon;
                current_id = serde_scan::scan!("--- scanner {} ---"<- line)
                    .expect("Failed to parse beacon coordinates");
            }
        }
    }
    scanners.push(Scanner::new(current_id, (0, 0, 0), beacons));
    scanners
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn overlaping_scanners() {
        let input = "--- scanner 0 ---
            -1,-1,1
            -2,-2,2
            -3,-3,3
            -2,-3,1
            5,6,-4
            8,0,7

            --- scanner 1 ---
            1,-1,1
            2,-2,2
            3,-3,3
            2,-1,3
            -5,4,-6
            -8,-7,0

            --- scanner 2 ---
            -1,-1,-1
            -2,-2,-2
            -3,-3,-3
            -1,-3,-2
            4,6,5
            -7,0,8

            --- scanner 3 ---
            1,1,-1
            2,2,-2
            3,3,-3
            1,3,-2
            -4,-6,5
            7,0,8

            --- scanner 4 ---
            1,1,1
            2,2,2
            3,3,3
            3,1,2
            -6,-4,-5
            0,7,-8";

        let scanners = parse_input(input);
        assert_eq!(scanners[0].id, 0);
        assert_eq!(scanners[1].id, 1);
        assert_eq!(scanners[2].id, 2);
        assert_eq!(scanners[3].id, 3);
        assert_eq!(scanners[4].id, 4);

        assert!(get_overlap_rotation(&scanners[0], &scanners[1], 6).is_some());
        assert!(get_overlap_rotation(&scanners[1], &scanners[2], 6).is_some());
        assert!(get_overlap_rotation(&scanners[2], &scanners[3], 6).is_some());
        assert!(get_overlap_rotation(&scanners[3], &scanners[4], 6).is_some());
        assert!(get_overlap_rotation(&scanners[4], &scanners[0], 6).is_some());
    }

    #[test]
    pub fn test_find_translations() {
        let input = "--- scanner 0 ---
            404,-588,-901
            528,-643,409
            -838,591,734
            390,-675,-793
            -537,-823,-458
            -485,-357,347
            -345,-311,381
            -661,-816,-575
            -876,649,763
            -618,-824,-621
            553,345,-567
            474,580,667
            -447,-329,318
            -584,868,-557
            544,-627,-890
            564,392,-477
            455,729,728
            -892,524,684
            -689,845,-530
            423,-701,434
            7,-33,-71
            630,319,-379
            443,580,662
            -789,900,-551
            459,-707,401

            --- scanner 1 ---
            686,422,578
            605,423,415
            515,917,-361
            -336,658,858
            95,138,22
            -476,619,847
            -340,-569,-846
            567,-361,727
            -460,603,-452
            669,-402,600
            729,430,532
            -500,-761,534
            -322,571,750
            -466,-666,-811
            -429,-592,574
            -355,545,-477
            703,-491,-529
            -328,-685,520
            413,935,-424
            -391,539,-444
            586,-435,557
            -364,-763,-893
            807,-499,-711
            755,-354,-619
            553,889,-390
            ";

        let mut scanners = parse_input(input);
        let rot = get_overlap_rotation(&scanners[0], &scanners[1], 6)
            .expect("Should be able to find rotation");
        scanners[1].rotate(rot);
        let translation = find_translation(&scanners[0], &scanners[1]);
        assert_eq!(translation, (68, -1246, -43));
    }

    #[test]
    pub fn example_one_manual() {
        let input = "--- scanner 0 ---
            404,-588,-901
            528,-643,409
            -838,591,734
            390,-675,-793
            -537,-823,-458
            -485,-357,347
            -345,-311,381
            -661,-816,-575
            -876,649,763
            -618,-824,-621
            553,345,-567
            474,580,667
            -447,-329,318
            -584,868,-557
            544,-627,-890
            564,392,-477
            455,729,728
            -892,524,684
            -689,845,-530
            423,-701,434
            7,-33,-71
            630,319,-379
            443,580,662
            -789,900,-551
            459,-707,401

            --- scanner 1 ---
            686,422,578
            605,423,415
            515,917,-361
            -336,658,858
            95,138,22
            -476,619,847
            -340,-569,-846
            567,-361,727
            -460,603,-452
            669,-402,600
            729,430,532
            -500,-761,534
            -322,571,750
            -466,-666,-811
            -429,-592,574
            -355,545,-477
            703,-491,-529
            -328,-685,520
            413,935,-424
            -391,539,-444
            586,-435,557
            -364,-763,-893
            807,-499,-711
            755,-354,-619
            553,889,-390

            --- scanner 2 ---
            649,640,665
            682,-795,504
            -784,533,-524
            -644,584,-595
            -588,-843,648
            -30,6,44
            -674,560,763
            500,723,-460
            609,671,-379
            -555,-800,653
            -675,-892,-343
            697,-426,-610
            578,704,681
            493,664,-388
            -671,-858,530
            -667,343,800
            571,-461,-707
            -138,-166,112
            -889,563,-600
            646,-828,498
            640,759,510
            -630,509,768
            -681,-892,-333
            673,-379,-804
            -742,-814,-386
            577,-820,562

            --- scanner 3 ---
            -589,542,597
            605,-692,669
            -500,565,-823
            -660,373,557
            -458,-679,-417
            -488,449,543
            -626,468,-788
            338,-750,-386
            528,-832,-391
            562,-778,733
            -938,-730,414
            543,643,-506
            -524,371,-870
            407,773,750
            -104,29,83
            378,-903,-323
            -778,-728,485
            426,699,580
            -438,-605,-362
            -469,-447,-387
            509,732,623
            647,635,-688
            -868,-804,481
            614,-800,639
            595,780,-596

            --- scanner 4 ---
            727,592,562
            -293,-554,779
            441,611,-461
            -714,465,-776
            -743,427,-804
            -660,-479,-426
            832,-632,460
            927,-485,-438
            408,393,-506
            466,436,-512
            110,16,151
            -258,-428,682
            -393,719,612
            -211,-452,876
            808,-476,-593
            -575,615,604
            -485,667,467
            -680,325,-822
            -627,-443,-432
            872,-547,-609
            833,512,582
            807,604,487
            839,-516,451
            891,-625,532
            -652,-548,-490
            30,-46,-14";
        let mut map = Map::default();
        let mut scanners = parse_input(input);

        map.add_scanner(scanners[0].clone());

        let rot = get_overlap_rotation(&scanners[0], &scanners[1], 12).unwrap();
        assert_eq!(rot, 12);
        scanners[1].rotate(rot);
        let trans = find_translation(&scanners[0], &scanners[1]);
        assert_eq!(trans, (68, -1246, -43));
        scanners[1].translate(trans);
        map.add_scanner(scanners[1].clone());

        let rot = get_overlap_rotation(&scanners[1], &scanners[4], 12).unwrap();
        assert_eq!(rot, 19);
        scanners[4].rotate(rot);
        let trans = find_translation(&scanners[1], &scanners[4]);
        assert_eq!(trans, (-20, -1133, 1061));
        scanners[4].translate(trans);
        map.add_scanner(scanners[4].clone());

        let rot = get_overlap_rotation(&scanners[1], &scanners[3], 12).unwrap();
        scanners[3].rotate(rot);
        let trans = find_translation(&scanners[1], &scanners[3]);
        assert_eq!(trans, (-92, -2380, -20));
        scanners[3].translate(trans);
        map.add_scanner(scanners[3].clone());

        let rot = get_overlap_rotation(&scanners[4], &scanners[2], 12).unwrap();
        scanners[2].rotate(rot);
        let trans = find_translation(&scanners[4], &scanners[2]);
        assert_eq!(trans, (1105, -1205, 1229));
        scanners[2].translate(trans);
        map.add_scanner(scanners[2].clone());

        assert_eq!(map.beacons.len(), 79);
    }

    #[test]
    pub fn example_one() {
        let input = "--- scanner 0 ---
            404,-588,-901
            528,-643,409
            -838,591,734
            390,-675,-793
            -537,-823,-458
            -485,-357,347
            -345,-311,381
            -661,-816,-575
            -876,649,763
            -618,-824,-621
            553,345,-567
            474,580,667
            -447,-329,318
            -584,868,-557
            544,-627,-890
            564,392,-477
            455,729,728
            -892,524,684
            -689,845,-530
            423,-701,434
            7,-33,-71
            630,319,-379
            443,580,662
            -789,900,-551
            459,-707,401

            --- scanner 1 ---
            686,422,578
            605,423,415
            515,917,-361
            -336,658,858
            95,138,22
            -476,619,847
            -340,-569,-846
            567,-361,727
            -460,603,-452
            669,-402,600
            729,430,532
            -500,-761,534
            -322,571,750
            -466,-666,-811
            -429,-592,574
            -355,545,-477
            703,-491,-529
            -328,-685,520
            413,935,-424
            -391,539,-444
            586,-435,557
            -364,-763,-893
            807,-499,-711
            755,-354,-619
            553,889,-390

            --- scanner 2 ---
            649,640,665
            682,-795,504
            -784,533,-524
            -644,584,-595
            -588,-843,648
            -30,6,44
            -674,560,763
            500,723,-460
            609,671,-379
            -555,-800,653
            -675,-892,-343
            697,-426,-610
            578,704,681
            493,664,-388
            -671,-858,530
            -667,343,800
            571,-461,-707
            -138,-166,112
            -889,563,-600
            646,-828,498
            640,759,510
            -630,509,768
            -681,-892,-333
            673,-379,-804
            -742,-814,-386
            577,-820,562

            --- scanner 3 ---
            -589,542,597
            605,-692,669
            -500,565,-823
            -660,373,557
            -458,-679,-417
            -488,449,543
            -626,468,-788
            338,-750,-386
            528,-832,-391
            562,-778,733
            -938,-730,414
            543,643,-506
            -524,371,-870
            407,773,750
            -104,29,83
            378,-903,-323
            -778,-728,485
            426,699,580
            -438,-605,-362
            -469,-447,-387
            509,732,623
            647,635,-688
            -868,-804,481
            614,-800,639
            595,780,-596

            --- scanner 4 ---
            727,592,562
            -293,-554,779
            441,611,-461
            -714,465,-776
            -743,427,-804
            -660,-479,-426
            832,-632,460
            927,-485,-438
            408,393,-506
            466,436,-512
            110,16,151
            -258,-428,682
            -393,719,612
            -211,-452,876
            808,-476,-593
            -575,615,604
            -485,667,467
            -680,325,-822
            -627,-443,-432
            872,-547,-609
            833,512,582
            807,604,487
            839,-516,451
            891,-625,532
            -652,-548,-490
            30,-46,-14";
        let scanners = parse_input(input);

        let map = position_scanner_list(scanners);

        assert_eq!(map.beacons.len(), 79);
        assert_eq!(map.get_max_distance(), 3621);
    }
}
