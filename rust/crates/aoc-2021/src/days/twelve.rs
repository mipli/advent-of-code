use std::{collections::HashMap, str::FromStr};

use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Twelve, |input| {
        let system: CaveSystem = input.parse().expect("Invalid input");
        println!("Total paths: {}", system.get_all_paths().len());
        println!(
            "Total revisit paths: {}",
            system.get_all_paths_revisit().len()
        );
    })
}

#[derive(Debug, Clone, Copy)]
enum CaveSize {
    Small,
    Large,
}

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
enum CaveLabel {
    Start,
    End,
    Label(String),
}

impl FromStr for CaveLabel {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "start" => Ok(CaveLabel::Start),
            "end" => Ok(CaveLabel::End),
            c => Ok(CaveLabel::Label(c.to_string())),
        }
    }
}

#[derive(Debug, Clone)]
struct Cave {
    label: CaveLabel,
    size: CaveSize,
}

type CaveIndex = usize;

#[derive(Debug, Clone)]
struct CaveSystem {
    caves: Vec<Cave>,
    connections: HashMap<CaveLabel, Vec<CaveIndex>>,
    cave_index: HashMap<CaveLabel, CaveIndex>,
}

impl CaveSystem {
    fn get_neigbours(&self, label: &CaveLabel) -> Option<&Vec<CaveIndex>> {
        self.connections.get(label)
    }

    fn get_all_paths(&self) -> Vec<Vec<CaveIndex>> {
        struct SearchNode {
            path: Vec<CaveIndex>,
            next: CaveIndex,
        }
        let mut frontier = vec![SearchNode {
            path: vec![],
            next: *self
                .cave_index
                .get(&CaveLabel::Start)
                .expect("No path start"),
        }];
        let mut paths = vec![];

        while let Some(node) = frontier.pop() {
            if let Some(neigbours) = self.get_neigbours(&self.caves[node.next].label) {
                neigbours.iter().for_each(|next_index| {
                    let next_cave = &self.caves[*next_index];
                    let mut new_path = node.path.clone();
                    new_path.push(node.next);
                    match next_cave.size {
                        CaveSize::Large => {
                            frontier.push(SearchNode {
                                path: new_path,
                                next: *next_index,
                            });
                        }
                        CaveSize::Small => match next_cave.label {
                            CaveLabel::End => {
                                paths.push(new_path);
                            }
                            CaveLabel::Label(_) if !node.path.contains(next_index) => {
                                frontier.push(SearchNode {
                                    path: new_path,
                                    next: *next_index,
                                });
                            }
                            _ => {}
                        },
                    }
                })
            }
        }
        paths
    }

    fn get_all_paths_revisit(&self) -> Vec<Vec<CaveIndex>> {
        struct SearchNode {
            path: Vec<CaveIndex>,
            next: CaveIndex,
            revisit: bool,
        }
        let mut frontier = vec![SearchNode {
            path: vec![],
            revisit: false,
            next: *self
                .cave_index
                .get(&CaveLabel::Start)
                .expect("No path start"),
        }];
        let mut paths = vec![];

        while let Some(node) = frontier.pop() {
            if let Some(neigbours) = self.get_neigbours(&self.caves[node.next].label) {
                neigbours.iter().for_each(|next_index| {
                    let next_cave = &self.caves[*next_index];
                    let mut new_path = node.path.clone();
                    new_path.push(node.next);
                    match next_cave.size {
                        CaveSize::Large => {
                            frontier.push(SearchNode {
                                path: new_path,
                                next: *next_index,
                                revisit: node.revisit,
                            });
                        }
                        CaveSize::Small => match next_cave.label {
                            CaveLabel::End => {
                                paths.push(new_path);
                            }
                            CaveLabel::Label(_) if !node.path.contains(next_index) => {
                                frontier.push(SearchNode {
                                    path: new_path,
                                    next: *next_index,
                                    revisit: node.revisit,
                                });
                            }
                            CaveLabel::Label(_) if !node.revisit => {
                                frontier.push(SearchNode {
                                    path: new_path,
                                    next: *next_index,
                                    revisit: true,
                                });
                            }
                            _ => {}
                        },
                    }
                })
            }
        }
        paths
    }
}

impl FromStr for CaveSystem {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut caves = vec![];
        let mut connections: HashMap<CaveLabel, Vec<CaveIndex>> = HashMap::default();
        let mut cave_index: HashMap<CaveLabel, CaveIndex> = HashMap::default();

        s.lines().for_each(|line| {
            let mut inp = line.trim().split('-');
            let first: CaveLabel = inp
                .next()
                .expect("missing conncetion start")
                .parse()
                .expect("Invalid cave label");
            let second: CaveLabel = inp
                .next()
                .expect("missing conncetion start")
                .parse()
                .expect("Invalid cave label");

            let first_index = match cave_index.get(&first) {
                Some(index) => *index,
                None => {
                    let index = caves.len();
                    let size = match first {
                        CaveLabel::Start | CaveLabel::End => CaveSize::Small,
                        CaveLabel::Label(ref c)
                            if c.chars().next().expect("Invalid label").is_uppercase() =>
                        {
                            CaveSize::Large
                        }
                        _ => CaveSize::Small,
                    };
                    caves.push(Cave {
                        label: first.clone(),
                        size,
                    });
                    cave_index.insert(first.clone(), index);
                    index
                }
            };

            let second_index = match cave_index.get(&second) {
                Some(index) => *index,
                None => {
                    let index = caves.len();
                    let size = match second {
                        CaveLabel::Start | CaveLabel::End => CaveSize::Small,
                        CaveLabel::Label(ref c)
                            if c.chars().next().expect("Invalid label").is_uppercase() =>
                        {
                            CaveSize::Large
                        }
                        _ => CaveSize::Small,
                    };
                    caves.push(Cave {
                        label: second.clone(),
                        size,
                    });
                    cave_index.insert(second.clone(), index);
                    index
                }
            };

            connections.entry(first).or_default().push(second_index);
            connections.entry(second).or_default().push(first_index);
        });

        Ok(CaveSystem {
            caves,
            connections,
            cave_index,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let input = "start-A
                start-b
                A-c
                A-b
                b-d
                A-end
                b-end";
        let system: CaveSystem = input.parse().expect("Invalid input");
        let paths = system.get_all_paths();
        assert_eq!(paths.len(), 10);
    }

    #[test]
    pub fn example_one_part_two() {
        let input = "fs-end
                he-DX
                fs-he
                start-DX
                pj-DX
                end-zg
                zg-sl
                zg-pj
                pj-he
                RW-he
                fs-DX
                pj-RW
                zg-RW
                start-pj
                he-WI
                zg-he
                pj-fs
                start-RW";
        let system: CaveSystem = input.parse().expect("Invalid input");
        let paths = system.get_all_paths();
        assert_eq!(paths.len(), 226);
    }

    #[test]
    pub fn example_two() {
        let input = "start-A
                start-b
                A-c
                A-b
                b-d
                A-end
                b-end";
        let system: CaveSystem = input.parse().expect("Invalid input");
        let paths = system.get_all_paths_revisit();
        assert_eq!(paths.len(), 36);
    }

    #[test]
    pub fn example_two_part_two() {
        let input = "fs-end
                he-DX
                fs-he
                start-DX
                pj-DX
                end-zg
                zg-sl
                zg-pj
                pj-he
                RW-he
                fs-DX
                pj-RW
                zg-RW
                start-pj
                he-WI
                zg-he
                pj-fs
                start-RW";
        let system: CaveSystem = input.parse().expect("Invalid input");
        let paths = system.get_all_paths_revisit();
        assert_eq!(paths.len(), 3509);
    }
}
