use std::str::FromStr;

use anyhow::bail;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Two, |input| {
        let commands: Vec<Command> = input
            .lines()
            .map(|l| l.parse().expect("Failed to parse input line"))
            .collect::<Vec<_>>();

        let mut sub = Submarine::default();
        sub.handle(&commands);
        println!("Position value: {}", sub.horizontal * sub.depth);

        let mut sub = Submarine::default();
        sub.handle_aim(&commands);
        println!("Position by aimed value: {}", sub.horizontal * sub.depth);
    })
}

#[derive(Default)]
pub struct Submarine {
    pub horizontal: i32,
    pub depth: i32,
    pub aim: i32,
}

impl Submarine {
    pub fn handle(&mut self, commands: &[Command]) {
        commands.iter().for_each(|command| match command {
            Command::Forward(v) => self.horizontal += v,
            Command::Up(v) => self.depth -= v,
            Command::Down(v) => self.depth += v,
        });
    }

    pub fn handle_aim(&mut self, commands: &[Command]) {
        commands.iter().for_each(|command| match command {
            Command::Forward(v) => {
                self.horizontal += v;
                self.depth += self.aim * v;
            }
            Command::Up(v) => self.aim -= v,
            Command::Down(v) => self.aim += v,
        });
    }
}

pub enum Command {
    Forward(i32),
    Down(i32),
    Up(i32),
}

impl FromStr for Command {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut splits = s.split(' ');
        Ok(match (splits.next(), splits.next()) {
            (Some("forward"), Some(v)) => Command::Forward(v.parse()?),
            (Some("up"), Some(v)) => Command::Up(v.parse()?),
            (Some("down"), Some(v)) => Command::Down(v.parse()?),
            _ => bail!("Invalid command input"),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn submarine_command_handling() {
        let commands: Vec<Command> = "forward 5
down 5
forward 8
up 3
down 8
forward 2"
            .lines()
            .map(|l| l.parse().expect("Failed to parse input line"))
            .collect::<Vec<_>>();
        let mut sub = Submarine::default();

        sub.handle(&commands);

        assert_eq!(sub.horizontal * sub.depth, 150);
    }

    #[test]
    pub fn submarine_command_handling_by_aim() {
        let commands: Vec<Command> = "forward 5
down 5
forward 8
up 3
down 8
forward 2"
            .lines()
            .map(|l| l.parse().expect("Failed to parse input line"))
            .collect::<Vec<_>>();
        let mut sub = Submarine::default();

        sub.handle_aim(&commands);

        assert_eq!(sub.horizontal * sub.depth, 900);
    }
}
