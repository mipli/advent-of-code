use std::collections::HashMap;

use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::TwentyOne, |input| {
        let mut game = parse_input(input);
        let player_one = game.positions[0];
        let player_two = game.positions[1];

        while !game.take_turn() {}

        println!("Game value: {}", game.get_value());

        let wins = run_realities(player_one, player_two);
        println!("Max wins: {}", wins.iter().max().unwrap());
    })
}

fn run_realities(player_one: u32, player_two: u32) -> [u64; 2] {
    let game = DiracGame {
        scores: [0, 0],
        positions: [player_one as u8, player_two as u8],
        rolls_count: 0,
        rolls: [0, 0],
        player: 0,
    };

    let mut cache: HashMap<DiracGame, [u64; 2]> = HashMap::default();
    run_game(&game, &mut cache)
}

fn run_game(game: &DiracGame, cache: &mut HashMap<DiracGame, [u64; 2]>) -> [u64; 2] {
    if let Some(&wins) = cache.get(game) {
        return wins;
    }

    let run_roll =
        |mut game: DiracGame, roll: u8, cache: &mut HashMap<DiracGame, [u64; 2]>| -> [u64; 2] {
            if let Some(winner) = game.add_roll(game.player as usize, roll) {
                let mut wins = [0, 0];
                wins[winner] += 1;
                wins
            } else {
                game.player = (game.player + 1) % 2;
                run_game(&game, cache)
            }
        };

    let mut wins = run_roll(game.clone(), 1, cache);

    let b_wins = run_roll(game.clone(), 2, cache);
    wins[1] += b_wins[1];
    wins[0] += b_wins[0];

    let c_wins = run_roll(game.clone(), 3, cache);
    wins[1] += c_wins[1];
    wins[0] += c_wins[0];

    cache.insert(game.clone(), wins);
    wins
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct DiracGame {
    scores: [u8; 2],
    positions: [u8; 2],
    rolls_count: u8,
    rolls: [u8; 2],
    player: u8,
}

impl DiracGame {
    fn add_roll(&mut self, player: usize, roll: u8) -> Option<usize> {
        if self.rolls_count == 2 {
            let value = self.rolls.into_iter().sum::<u8>() + roll;
            self.rolls = [0, 0];
            self.rolls_count = 0;
            self.positions[player] = (self.positions[player] + value) % 10;
            self.scores[player] += if self.positions[player] == 0 {
                10
            } else {
                self.positions[player]
            };

            if self.scores[player] >= 21 {
                return Some(player);
            }
        } else {
            self.rolls[self.rolls_count as usize] = roll;
            self.rolls_count += 1;
        }
        None
    }
}

#[derive(Debug)]
struct Game {
    scores: [u32; 2],
    positions: [u32; 2],
    dice: Dice,
    turn: u32,
}

impl Game {
    fn get_value(&self) -> u32 {
        self.scores.iter().min().expect("Always has a min value") * self.dice.rolls
    }

    fn take_turn(&mut self) -> bool {
        let value = self.dice.roll() + self.dice.roll() + self.dice.roll();

        match self.turn % 2 {
            0 => {
                self.positions[0] = (self.positions[0] + value) % 10;
                self.scores[0] += if self.positions[0] == 0 {
                    10
                } else {
                    self.positions[0]
                }
            }
            1 => {
                self.positions[1] = (self.positions[1] + value) % 10;
                self.scores[1] += if self.positions[1] == 0 {
                    10
                } else {
                    self.positions[1]
                }
            }
            _ => unreachable!(),
        }
        self.turn += 1;

        self.scores[0] >= 1000 || self.scores[1] >= 1000
    }
}

#[derive(Debug)]
struct Dice {
    rolls: u32,
    value: u32,
    sides: u32,
}

impl Dice {
    fn new(sides: u32) -> Self {
        Dice {
            rolls: 0,
            value: 0,
            sides,
        }
    }

    fn roll(&mut self) -> u32 {
        self.rolls += 1;
        let val = self.value;
        self.value = (self.value + 1) % self.sides;
        val + 1
    }
}

fn parse_input(input: &str) -> Game {
    let mut lines = input.lines();
    let line = lines.next().unwrap().trim();
    let player_one: u32 = serde_scan::scan!("Player 1 starting position: {}" <- line)
        .expect("Failed to parse player one");
    let line = lines.next().unwrap().trim();
    let player_two: u32 = serde_scan::scan!("Player 2 starting position: {}" <- line)
        .expect("Failed to parse player one");

    Game {
        scores: [0, 0],
        positions: [player_one, player_two],
        turn: 0,
        dice: Dice::new(100),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let mut game = parse_input(
            "Player 1 starting position: 4
            Player 2 starting position: 8",
        );

        while !game.take_turn() {}

        assert_eq!(game.get_value(), 739785);
    }

    #[test]
    pub fn example_two() {
        let wins = run_realities(4, 8);
        assert_eq!(wins[0], 444356092776315);
        assert_eq!(wins[1], 341960390180808);
    }
}
