use colored::Colorize;
use libaoc::{
    grid::{Grid, NeighbourConnection},
    DayNumber, Solver,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::Eleven, |input| {
        let mut grid = parse_input(input);
        let mut count = 0;
        for _ in 1..=100 {
            count += step(&mut grid);
        }
        println!("Flashing: {count}");
        let sync = 100 + find_sync_step(&mut grid);
        println!("In sync: {sync}");
    })
}

fn find_sync_step(grid: &mut Grid<Light>) -> u32 {
    let octopus_count = grid.get_size() as u32;
    let mut turn = 0;
    loop {
        turn += 1;
        let flashes = step(grid);
        if flashes == octopus_count {
            break;
        }
    }
    turn
}

fn step(grid: &mut Grid<Light>) -> u32 {
    let mut to_flash = vec![];
    grid.point_iter().for_each(|point| {
        grid.get_mut(point).level += 1;
        if grid.get(point).level > 9 {
            to_flash.push(point);
        }
    });
    while let Some(point) = to_flash.pop() {
        if grid.get(point).should_flash() {
            grid.get_mut(point).flash();
            grid.get_neigbours(point).iter().for_each(|&neigbour| {
                if !grid.get(neigbour).has_flashed {
                    grid.get_mut(neigbour).level += 1;
                    if grid.get(neigbour).level > 9 {
                        to_flash.push(neigbour);
                    }
                }
            })
        }
    }
    let mut flash_count = 0;
    grid.point_iter().for_each(|point| {
        if grid.get(point).has_flashed {
            grid.get_mut(point).has_flashed = false;
            flash_count += 1;
        }
    });
    flash_count
}

fn parse_input(input: &str) -> Grid<Light> {
    let height = input.lines().count();
    let width = input.lines().next().expect("No lines in input").len();

    let data: Vec<Light> = input.lines().fold(vec![], |mut acc, line| {
        acc.extend_from_slice(
            line.trim()
                .chars()
                .map(|c| Light {
                    level: c as u8 - 48,
                    has_flashed: false,
                })
                .collect::<Vec<_>>()
                .as_slice(),
        );
        acc
    });

    Grid::new_with_data(width, height, NeighbourConnection::Eight, data)
}

#[derive(Debug, Copy, Clone)]
struct Light {
    level: u8,
    has_flashed: bool,
}

impl Light {
    fn should_flash(&self) -> bool {
        self.level > 9 && !self.has_flashed
    }

    fn flash(&mut self) {
        self.level = 0;
        self.has_flashed = true;
    }
}

impl std::fmt::Display for Light {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.level == 0 {
            write!(f, "{}", format!("{}", self.level).bold())
        } else {
            write!(f, "{}", format!("{}", self.level).dimmed())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let input = "5483143223
            2745854711
            5264556173
            6141336146
            6357385478
            4167524645
            2176841721
            6882881134
            4846848554
            5283751526";
        let mut grid = parse_input(input);
        let mut count = 0;
        for _ in 1..=100 {
            count += step(&mut grid);
        }
        assert_eq!(count, 1656);
    }

    #[test]
    pub fn example_two() {
        let input = "5483143223
            2745854711
            5264556173
            6141336146
            6357385478
            4167524645
            2176841721
            6882881134
            4846848554
            5283751526";
        let mut grid = parse_input(input);
        let sync = find_sync_step(&mut grid);
        assert_eq!(sync, 195);
    }
}
