use libaoc::{
    grid::{Grid, NeighbourConnection},
    point::Point,
    DayNumber, Solver,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::Thirteen, |input| {
        let (grid, folds) = parse_input(input);

        let folded = fold_grid(&grid, &folds[0]);
        println!("Dots after one fold: {}", count_dots(&folded));

        let folded = folds
            .iter()
            .skip(1)
            .fold(folded, |grid, fold| fold_grid(&grid, fold));
        println!("{folded}");
    })
}

fn count_dots(grid: &Grid<Dot>) -> usize {
    grid.point_iter()
        .filter(|point| *grid.get(*point) == Dot::Dot)
        .count()
}

fn fold_grid(grid: &Grid<Dot>, fold: &Fold) -> Grid<Dot> {
    match fold {
        Fold::Horiztonal(line) => {
            let mut mirrored = Grid::new(
                grid.get_width(),
                *line as usize,
                NeighbourConnection::Four,
                Dot::Empty,
            );
            grid.point_iter()
                .filter(|point| *grid.get(*point) == Dot::Dot)
                .for_each(|point| match point.y.cmp(line) {
                    std::cmp::Ordering::Less => mirrored.set(point, Dot::Dot),
                    std::cmp::Ordering::Greater => {
                        let folded_point = (point.x, *line * 2 - point.y).into();
                        mirrored.set(folded_point, Dot::Dot);
                    }
                    _ => {}
                });
            mirrored
        }
        Fold::Vertical(line) => {
            let mut mirrored = Grid::new(
                *line as usize,
                (*grid).get_height(),
                NeighbourConnection::Four,
                Dot::Empty,
            );
            grid.point_iter()
                .filter(|point| *grid.get(*point) == Dot::Dot)
                .for_each(|point| match point.x.cmp(line) {
                    std::cmp::Ordering::Less => mirrored.set(point, Dot::Dot),
                    std::cmp::Ordering::Greater => {
                        let folded_point = (*line * 2 - point.x, point.y).into();
                        mirrored.set(folded_point, Dot::Dot);
                    }
                    _ => {}
                });
            mirrored
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Fold {
    Horiztonal(i32),
    Vertical(i32),
}

#[derive(Clone, Copy, Eq, PartialEq)]
enum Dot {
    Dot,
    Empty,
}

impl std::fmt::Display for Dot {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Dot::Dot => write!(f, "#"),
            Dot::Empty => write!(f, "."),
        }
    }
}

impl std::fmt::Debug for Dot {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Dot::Dot => write!(f, "#"),
            Dot::Empty => write!(f, "."),
        }
    }
}

fn parse_input(input: &str) -> (Grid<Dot>, Vec<Fold>) {
    let (points, folds): (Vec<Point>, Vec<Fold>) =
        input
            .lines()
            .fold((vec![], vec![]), |(mut points, mut folds), line| {
                let line = line.trim();
                let point: Result<(i32, i32), _> = serde_scan::scan!("{},{}" <- line);
                if let Ok(point) = point {
                    points.push(point.into());
                }
                let fold: Result<(char, i32), _> = serde_scan::scan!("fold along {}={}" <- line);
                if let Ok(fold) = fold {
                    let fold = match fold.0 {
                        'y' => Fold::Horiztonal(fold.1),
                        'x' => Fold::Vertical(fold.1),
                        _ => panic!("Invalid fold"),
                    };
                    folds.push(fold);
                }
                (points, folds)
            });
    let dimension: Point = points.iter().fold((0, 0).into(), |mut dim, point| {
        dim.x = std::cmp::max(dim.x, point.x);
        dim.y = std::cmp::max(dim.y, point.y);
        dim
    });

    let mut grid = Grid::new(
        dimension.x as usize + 1,
        dimension.y as usize + 1,
        NeighbourConnection::Four,
        Dot::Empty,
    );

    points.into_iter().for_each(|point| {
        grid.set(point, Dot::Dot);
    });

    (grid, folds)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let input = "6,10
            0,14
            9,10
            0,3
            10,4
            4,11
            6,0
            6,12
            4,1
            0,13
            10,12
            3,4
            3,0
            8,4
            1,10
            2,14
            8,10
            9,0

            fold along y=7
            fold along x=5";

        let (grid, folds) = parse_input(input);

        let folded = fold_grid(&grid, &folds[0]);
        assert_eq!(count_dots(&folded), 17);

        let folded = fold_grid(&folded, &folds[1]);
        assert_eq!(count_dots(&folded), 16);
    }
}
