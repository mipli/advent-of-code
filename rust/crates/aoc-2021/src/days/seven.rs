use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Seven, |input| {
        let positions: Vec<i32> = input
            .split(',')
            .map(|n| n.trim().parse().expect("Failed to parse input"))
            .collect();

        println!("Crab cost: {}", get_fuel_cost_simple(&positions));
        println!("Crab cost, part 2: {}", get_fuel_cost(&positions));
    })
}

fn get_fuel_cost_simple(numbers: &[i32]) -> i32 {
    (0..=*numbers.iter().max().unwrap())
        .map(|i| numbers.iter().map(|num| (num - i).abs()).sum())
        .min()
        .expect("Could not find min value")
}

fn get_fuel_cost(numbers: &[i32]) -> i32 {
    (0..=*numbers.iter().max().unwrap())
        .map(|i| calculate_cost(numbers, i))
        .min()
        .expect("Could not find min value")
}

fn calculate_cost(numbers: &[i32], pos: i32) -> i32 {
    numbers
        .iter()
        .map(|num| {
            let dist = (num - pos).abs();
            (dist * (dist + 1)) / 2
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_example_one() {
        let positions: Vec<i32> = "16,1,2,0,4,2,7,1,2,14"
            .split(',')
            .map(|n| n.parse().expect("Failed to parse input"))
            .collect();

        assert_eq!(get_fuel_cost_simple(&positions), 37);
    }

    #[test]
    pub fn test_example_two() {
        let positions: Vec<i32> = "16,1,2,0,4,2,7,1,2,14"
            .split(',')
            .map(|n| n.parse().expect("Failed to parse input"))
            .collect();

        assert_eq!(calculate_cost(&[2], 3), 1);
        assert_eq!(calculate_cost(&[2], 5), 6);
        assert_eq!(calculate_cost(&[1], 5), 10);
        assert_eq!(calculate_cost(&[16], 5), 66);
        assert_eq!(get_fuel_cost(&positions), 168);
    }
}
