use std::{
    cmp::Ordering,
    collections::{BinaryHeap, HashSet},
};

use libaoc::{
    grid::{Grid, NeighbourConnection},
    point::Point,
    DayNumber, Solver,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::Fifteen, |input| {
        let grid = parse_input(input);

        let cost = find_path_cost(&grid).expect("No path found");
        println!("Cost to travel: {cost}");

        let big_grid = multiply_grid(grid);

        let cost = find_path_cost(&big_grid).expect("No path found");
        println!("Cost to travel the long way: {cost}");
    })
}

type RiskLevel = u32;

fn find_path_cost(grid: &Grid<RiskLevel>) -> Option<RiskLevel> {
    let mut frontier: BinaryHeap<PathNode> = BinaryHeap::default();
    let mut visited: HashSet<Point> = HashSet::default();
    frontier.push(PathNode {
        node: (0, 0).into(),
        cost: 0,
    });

    let goal: Point = (grid.get_width() as i32 - 1, grid.get_height() as i32 - 1).into();

    while let Some(current_node) = frontier.pop() {
        if visited.contains(&current_node.node) {
            continue;
        }
        if current_node.node == goal {
            return Some(current_node.cost);
        }
        for neighbour in grid.get_neigbours(current_node.node) {
            frontier.push(PathNode {
                node: neighbour,
                cost: current_node.cost + grid.get(neighbour),
            });
        }
        visited.insert(current_node.node);
    }

    None
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct PathNode {
    node: Point,
    cost: u32,
}

impl Ord for PathNode {
    fn cmp(&self, other: &Self) -> Ordering {
        other.cost.cmp(&self.cost)
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for PathNode {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn multiply_grid(grid: Grid<RiskLevel>) -> Grid<RiskLevel> {
    let width = grid.get_width() * 5;
    let height = grid.get_height() * 5;
    let mut new_grid = Grid::new(width, height, NeighbourConnection::Four, 0);

    for point in grid.point_iter() {
        let base = *grid.get(point) as i32;
        for x in 0..5 {
            for y in 0..5 {
                let np = (
                    point.x + x * grid.get_width() as i32,
                    point.y + y * grid.get_height() as i32,
                )
                    .into();
                let mut val = base + x + y;
                if val >= 10 {
                    val -= 9;
                }
                new_grid.set(np, val as u32);
            }
        }
    }

    new_grid
}

fn parse_input(input: &str) -> Grid<RiskLevel> {
    let width = input.lines().next().expect("No lines in input").len();
    let height = input.lines().count();
    let data: Vec<RiskLevel> = input.lines().fold(vec![], |mut data, line| {
        for c in line.trim().chars() {
            let risk: RiskLevel = (c as u8 - 48) as u32;
            data.push(risk);
        }
        data
    });

    Grid::new_with_data(width, height, NeighbourConnection::Four, data)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let input = "1163751742
            1381373672
            2136511328
            3694931569
            7463417111
            1319128137
            1359912421
            3125421639
            1293138521
            2311944581";

        let grid = parse_input(input);
        println!("{grid}");

        let cost = find_path_cost(&grid);
        assert_eq!(cost, Some(40));
    }

    #[test]
    pub fn example_two() {
        let input = "1163751742
            1381373672
            2136511328
            3694931569
            7463417111
            1319128137
            1359912421
            3125421639
            1293138521
            2311944581";

        let grid = parse_input(input);
        let big_grid = multiply_grid(grid);

        let cost = find_path_cost(&big_grid);
        assert_eq!(cost, Some(315));
    }
}
