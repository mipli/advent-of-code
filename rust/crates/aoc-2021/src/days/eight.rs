use std::collections::{HashMap, HashSet};

use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Eight, |input| {
        let outputs: Vec<Vec<InputLeds>> = input
            .lines()
            .map(|line| {
                let (_, outputs) = parse_input(line.trim());
                outputs
            })
            .collect();

        let simple_outputs: i32 = outputs
            .iter()
            .map(|out| out.iter().filter(|o| o.is_unique_len()).count() as i32)
            .sum();

        println!("Simple output values: {simple_outputs}");
        let total = get_total_value(input);
        println!("Display total: {total}");
    })
}

fn get_total_value(input: &str) -> i32 {
    input
        .lines()
        .map(|line| {
            let (inputs, outputs) = parse_input(line);
            let led_mapping = get_mapping(&inputs);

            Segment::create(&outputs[0], &led_mapping).to_num() * 1000
                + Segment::create(&outputs[1], &led_mapping).to_num() * 100
                + Segment::create(&outputs[2], &led_mapping).to_num() * 10
                + Segment::create(&outputs[3], &led_mapping).to_num()
        })
        .sum()
}

#[derive(Debug)]
struct InputLeds {
    pub leds: Vec<char>,
}

impl InputLeds {
    fn is_one(&self) -> bool {
        self.leds.len() == 2
    }

    fn is_four(&self) -> bool {
        self.leds.len() == 4
    }

    fn is_seven(&self) -> bool {
        self.leds.len() == 3
    }

    fn is_eight(&self) -> bool {
        self.leds.len() == 7
    }

    fn is_unique_len(&self) -> bool {
        [2, 3, 4, 7].contains(&self.leds.len())
    }
}

// A number segment, with leds ordered:
//   0
// 1   2
//   3
// 4   5
//   6
#[derive(Default, Debug)]
struct Segment {
    leds: [bool; 7],
}

impl Segment {
    fn create(output: &InputLeds, mapping: &HashMap<char, usize>) -> Segment {
        let mut leds = [false; 7];
        output.leds.iter().for_each(|c| {
            let led = mapping.get(c).expect("No mapping found for led");
            leds[*led] = true;
        });

        Segment { leds }
    }

    fn to_num(&self) -> i32 {
        match self.leds {
            [true, true, true, false, true, true, true] => 0,
            [false, false, true, false, false, true, false] => 1,
            [true, false, true, true, true, false, true] => 2,
            [true, false, true, true, false, true, true] => 3,
            [false, true, true, true, false, true, false] => 4,
            [true, true, false, true, false, true, true] => 5,
            [true, true, false, true, true, true, true] => 6,
            [true, false, true, false, false, true, false] => 7,
            [true, true, true, true, true, true, true] => 8,
            [true, true, true, true, false, true, true] => 9,
            _ => panic!("Invalid LED configuration: {:#?}", self.leds),
        }
    }
}

fn get_mapping(inputs: &[InputLeds]) -> HashMap<char, usize> {
    let mut led_mapping: HashMap<char, usize> = HashMap::default();
    let mut led_counts: HashMap<char, usize> = HashMap::default();

    inputs.iter().for_each(|input| {
        input.leds.iter().for_each(|&led| {
            *led_counts.entry(led).or_default() += 1;
        });
    });

    // find mapping for LED in spot 4, based on that being the only led used for 4 numbers
    let (value, _) = led_counts
        .iter()
        .find(|&(_, &v)| v == 4)
        .expect("Could not find valid LED for spot 4");
    led_mapping.insert(*value, 4);

    // find mapping for LED in spot 5, based on that being the only led used for 9 numbers
    let (value, _) = led_counts
        .iter()
        .find(|&(_, &v)| v == 9)
        .expect("Could not find valid LED for spot 4");
    led_mapping.insert(*value, 5);

    // ONE is two leds, one at spot 5 (found earlier) other at spot 2
    inputs
        .iter()
        .find(|&i| i.is_one())
        .expect("No configuration found for ONE")
        .leds
        .iter()
        .for_each(|&led| {
            led_mapping.entry(led).or_insert(2);
        });

    // SEVEN has three leds, two filled by ONE last at spot 0
    inputs
        .iter()
        .find(|&i| i.is_seven())
        .expect("No configuration found for SEVEN")
        .leds
        .iter()
        .for_each(|&led| {
            led_mapping.entry(led).or_insert(0);
        });

    // EIGHT and FOUR do not share a led at spot 6, while all the rest differ at are already used
    let eight_leds: HashSet<char> = inputs
        .iter()
        .find(|&i| i.is_eight())
        .expect("No configuration found for EIGHT")
        .leds
        .iter()
        .cloned()
        .collect();
    let four_leds: HashSet<char> = inputs
        .iter()
        .find(|&i| i.is_four())
        .expect("No configuration found for EIGHT")
        .leds
        .iter()
        .cloned()
        .collect();
    eight_leds.difference(&four_leds).for_each(|&led| {
        led_mapping.entry(led).or_insert(6);
    });

    // TWO, THREE, FIVE all have five leds, and only shared non-found led so far is at spot 3
    let led_sets: Vec<HashSet<char>> = inputs
        .iter()
        .filter_map(|i| {
            if i.leds.len() == 5 {
                let set: HashSet<char> = i.leds.iter().cloned().collect();
                Some(set)
            } else {
                None
            }
        })
        .collect();
    let unique = led_sets
        .iter()
        .skip(1)
        .fold(led_sets[0].clone(), |acc, set| {
            acc.intersection(set).cloned().collect()
        });
    unique.iter().for_each(|&led| {
        led_mapping.entry(led).or_insert(3);
    });

    // only unmatched LED is now at spot 1
    ['a', 'b', 'c', 'd', 'e', 'f', 'g']
        .into_iter()
        .for_each(|led| {
            led_mapping.entry(led).or_insert(1);
        });

    led_mapping
}

fn parse_input(input: &str) -> (Vec<InputLeds>, Vec<InputLeds>) {
    let mut inputs = vec![];
    let mut outputs = vec![];

    let mut parsing_input = true;

    input.split(' ').for_each(|val| {
        if val == "|" {
            parsing_input = false;
            return;
        }

        let leds = val.chars().collect();

        if parsing_input {
            inputs.push(InputLeds { leds });
        } else {
            outputs.push(InputLeds { leds });
        }
    });
    (inputs, outputs)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_parse() {
        let input =
            "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc";

        let (inputs, outputs) = parse_input(input);
        assert_eq!(inputs.len(), 10);
        assert_eq!(outputs.len(), 4);
    }

    #[test]
    pub fn test_example_one() {
        let input =
            "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc";

        let (_, outputs) = parse_input(input);
        assert_eq!(outputs.iter().filter(|o| o.is_unique_len()).count(), 3);
    }

    #[test]
    pub fn test_output_value() {
        let input =
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";

        let (inputs, outputs) = parse_input(input);
        let led_mapping = get_mapping(&inputs);
        assert_eq!(
            Segment::create(
                &InputLeds {
                    leds: vec!['a', 'b']
                },
                &led_mapping
            )
            .to_num(),
            1
        );

        assert_eq!(Segment::create(&outputs[0], &led_mapping).to_num(), 5);
        assert_eq!(Segment::create(&outputs[1], &led_mapping).to_num(), 3);
        assert_eq!(Segment::create(&outputs[2], &led_mapping).to_num(), 5);
        assert_eq!(Segment::create(&outputs[3], &led_mapping).to_num(), 3);
    }

    #[test]
    pub fn test_total() {
        let input =
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

        assert_eq!(get_total_value(input), 61229);
    }
}
