use std::str::FromStr;

use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Five, |input| {
        let lines: Vec<Line> = input
            .lines()
            .map(|line| line.parse().expect("Failed parsed line"))
            .collect::<Vec<_>>();
        let grid = Grid::from_perpendicular_lines(&lines);
        println!("Overlaps: {}", grid.count_overlaps());

        let grid = Grid::from_lines(&lines);
        println!("Overlaps: {}", grid.count_overlaps());
    })
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Grid {
    grid: Vec<u32>,
    width: u32,
    height: u32,
}

impl Grid {
    fn from_lines(lines: &[Line]) -> Grid {
        let width = 1 + lines
            .iter()
            .map(|line| std::cmp::max(line.start.x, line.end.x))
            .max()
            .expect("Failed to get grid with");
        let height = 1 + lines
            .iter()
            .map(|line| std::cmp::max(line.start.y, line.end.y))
            .max()
            .expect("Failed to get grid with");

        let mut grid = Grid {
            grid: vec![0; (width * height) as usize],
            width: width as u32,
            height: height as u32,
        };

        for line in lines {
            grid.add_line(line);
        }

        grid
    }

    fn from_perpendicular_lines(lines: &[Line]) -> Grid {
        let width = 1 + lines
            .iter()
            .map(|line| std::cmp::max(line.start.x, line.end.x))
            .max()
            .expect("Failed to get grid with");
        let height = 1 + lines
            .iter()
            .map(|line| std::cmp::max(line.start.y, line.end.y))
            .max()
            .expect("Failed to get grid with");

        let mut grid = Grid {
            grid: vec![0; (width * height) as usize],
            width: width as u32,
            height: height as u32,
        };

        for line in lines {
            if line.is_horizontal() || line.is_vertical() {
                grid.add_line(line);
            }
        }

        grid
    }

    fn get_index(&self, pos: Position) -> usize {
        (pos.x + pos.y * (self.width as i32)) as usize
    }

    fn add_line(&mut self, line: &Line) {
        use std::cmp::Ordering;
        let (pos, length): (Position, i32) =
            match (line.start.x.cmp(&line.end.x), line.start.y.cmp(&line.end.y)) {
                (Ordering::Less, Ordering::Less) => ((1, 1).into(), line.end.x - line.start.x),
                (Ordering::Greater, Ordering::Greater) => {
                    ((-1, -1).into(), line.start.x - line.end.x)
                }
                (Ordering::Less, Ordering::Greater) => ((1, -1).into(), line.end.x - line.start.x),
                (Ordering::Greater, Ordering::Less) => ((-1, 1).into(), line.start.x - line.end.x),
                (Ordering::Equal, Ordering::Less) => ((0, 1).into(), line.end.y - line.start.y),
                (Ordering::Equal, Ordering::Greater) => ((0, -1).into(), line.start.y - line.end.y),
                (Ordering::Less, Ordering::Equal) => ((1, 0).into(), line.end.x - line.start.x),
                (Ordering::Greater, Ordering::Equal) => ((-1, 0).into(), line.start.x - line.end.x),
                _ => unreachable!(),
            };
        let mut point = line.start;

        let index = self.get_index(point);
        self.grid[index] += 1;

        for _ in 0..length as usize {
            point.x += pos.x;
            point.y += pos.y;

            let index = self.get_index(point);
            self.grid[index] += 1;
        }
    }

    fn count_overlaps(&self) -> u32 {
        self.grid.iter().filter(|&&g| g > 1).count() as u32
    }
}

impl std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (i, g) in self.grid.iter().enumerate() {
            if i % (self.width as usize) == 0 {
                writeln!(f)?;
            }
            write!(f, "{g}")?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
struct Position {
    x: i32,
    y: i32,
}

impl From<(i32, i32)> for Position {
    fn from(other: (i32, i32)) -> Position {
        Position {
            x: other.0,
            y: other.1,
        }
    }
}

impl FromStr for Position {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(',');
        match (parts.next(), parts.next()) {
            (Some(x), Some(y)) => {
                let x: i32 = x.parse()?;
                let y: i32 = y.parse()?;
                Ok(Position { x, y })
            }
            _ => Err(anyhow::anyhow!("Failed to parse position")),
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
struct Line {
    start: Position,
    end: Position,
}

impl Line {
    fn is_horizontal(&self) -> bool {
        self.start.y == self.end.y
    }

    fn is_vertical(&self) -> bool {
        self.start.x == self.end.x
    }
}

impl FromStr for Line {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.trim().split(' ');
        match (parts.next(), parts.next(), parts.next()) {
            (Some(start), _, Some(end)) => {
                let start = start.parse::<Position>()?;
                let end = end.parse::<Position>()?;
                Ok(Line { start, end })
            }
            _ => Err(anyhow::anyhow!("Failed to parse position")),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn parse_line() {
        let line: Line = "0,9 -> 5,4".parse().expect("Failed to parse line");
        assert_eq!(
            line,
            Line {
                start: (0, 9).into(),
                end: (5, 4).into(),
            }
        );
    }

    #[test]
    pub fn example_one() {
        let lines: Vec<Line> = "0,9 -> 5,9
            8,0 -> 0,8
            9,4 -> 3,4
            2,2 -> 2,1
            7,0 -> 7,4
            6,4 -> 2,0
            0,9 -> 2,9
            3,4 -> 1,4
            0,0 -> 8,8
            5,5 -> 8,2"
            .lines()
            .map(|line| line.parse().expect("Failed parsed line"))
            .collect::<Vec<_>>();
        let grid = Grid::from_perpendicular_lines(&lines);
        assert_eq!(grid.count_overlaps(), 5);
    }

    #[test]
    pub fn example_two() {
        let lines: Vec<Line> = "0,9 -> 5,9
            8,0 -> 0,8
            9,4 -> 3,4
            2,2 -> 2,1
            7,0 -> 7,4
            6,4 -> 2,0
            0,9 -> 2,9
            3,4 -> 1,4
            0,0 -> 8,8
            5,5 -> 8,2"
            .lines()
            .map(|line| line.parse().expect("Failed parsed line"))
            .collect::<Vec<_>>();
        let grid = Grid::from_lines(&lines);
        assert_eq!(grid.count_overlaps(), 12);
    }
}
