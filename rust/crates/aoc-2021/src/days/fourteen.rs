use std::collections::HashMap;

use itertools::Itertools;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Fourteen, |input| {
        let (mut polymer, rules) = parse_input(input);
        let org_polymer = polymer.clone();
        for _ in 1..=10 {
            polymer = step(&polymer, &rules);
        }
        println!("Polymer score after 10: {}", polymer_score(&polymer));
        println!(
            "Polymer score afer 40: {}",
            polymer_score_at_step(&org_polymer, &rules, 40)
        );
    })
}

fn polymer_score_at_step(polymer: &[Element], rules: &Rules, steps: usize) -> usize {
    let mut pairs: HashMap<(Element, Element), usize> = HashMap::default();

    polymer.iter().tuple_windows().for_each(|(&left, &right)| {
        *pairs.entry((left, right)).or_default() += 1;
    });

    for _ in 1..=steps {
        let mut new_pairs: HashMap<(Element, Element), usize> = HashMap::default();

        pairs.iter().for_each(|(pair, count)| {
            if let Some(elem) = rules.get(pair) {
                *new_pairs.entry((pair.0, *elem)).or_default() += count;
                *new_pairs.entry((*elem, pair.1)).or_default() += count;
            } else {
                *new_pairs.entry(*pair).or_default() += count;
            }
        });

        std::mem::swap(&mut pairs, &mut new_pairs);
    }

    let mut counts: HashMap<Element, usize> = HashMap::default();
    pairs.iter().for_each(|(pair, count)| {
        *counts.entry(pair.0).or_default() += count;
        *counts.entry(pair.1).or_default() += count;
    });

    // Special handling of first and last element in polymer, since they are only part of one pair
    // and not two pairs as all other elements
    let first = polymer.first().unwrap();
    let last = polymer.last().unwrap();
    *counts.entry(*first).or_default() += 1;
    *counts.entry(*last).or_default() += 1;

    let max = counts.values().max().expect("No max found") / 2;
    let min = counts.values().min().expect("No min found") / 2;
    max - min
}

fn polymer_score(polymer: &[Element]) -> usize {
    let mut counts: HashMap<Element, usize> = HashMap::default();
    polymer.iter().for_each(|&elem| {
        *counts.entry(elem).or_default() += 1;
    });
    let max = counts.values().max().expect("No max found");
    let min = counts.values().min().expect("No min found");
    max - min
}

fn step(polymer: &[Element], rules: &Rules) -> Vec<Element> {
    let mut next_polymer =
        polymer
            .iter()
            .tuple_windows()
            .fold(vec![], |mut polymer, (&left, &right)| {
                polymer.push(left);
                if let Some(elem) = rules.get(&(left, right)) {
                    polymer.push(*elem);
                }
                polymer
            });
    next_polymer.push(*polymer.last().unwrap());
    next_polymer
}

type Element = char;

type Rules = HashMap<(Element, Element), Element>;

fn parse_input(input: &str) -> (Vec<Element>, Rules) {
    let lines = input.lines();

    let mut template = vec![];
    let mut rules = HashMap::default();

    lines.for_each(|line| {
        let line = line.trim();
        if line.is_empty() {
            return;
        }

        let mut parts = line.split(" -> ");
        match (parts.next(), parts.next()) {
            (Some(source), Some(to_insert)) => {
                let source = source.chars().collect::<Vec<_>>();
                let to_insert = to_insert
                    .chars()
                    .next()
                    .expect("Failed to get char to insert");
                rules.insert((source[0], source[1]), to_insert);
            }
            _ => template = line.chars().collect::<Vec<_>>(),
        }
    });

    (template, rules)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let input = "NNCB

            CH -> B
            HH -> N
            CB -> H
            NH -> C
            HB -> C
            HC -> B
            HN -> C
            NN -> C
            BH -> H
            NC -> B
            NB -> B
            BN -> B
            BB -> N
            BC -> B
            CC -> N
            CN -> C";

        let (polymer, rules) = parse_input(input);

        let mut polymer = step(&polymer, &rules);
        assert_eq!(polymer.len(), 7);
        for _ in 2..=10 {
            polymer = step(&polymer, &rules);
        }
        assert_eq!(polymer_score(&polymer), 1588);
    }

    #[test]
    pub fn smart_version() {
        let input = "NNCB

            CH -> B
            HH -> N
            CB -> H
            NH -> C
            HB -> C
            HC -> B
            HN -> C
            NN -> C
            BH -> H
            NC -> B
            NB -> B
            BN -> B
            BB -> N
            BC -> B
            CC -> N
            CN -> C";

        let (mut polymer, rules) = parse_input(input);
        let org_polymer = polymer.clone();

        for n in 1..=10 {
            polymer = step(&polymer, &rules);
            assert_eq!(
                polymer_score(&polymer),
                polymer_score_at_step(&org_polymer, &rules, n)
            );
        }
    }

    #[test]
    pub fn example_two() {
        let input = "NNCB

             CH -> B
             HH -> N
             CB -> H
             NH -> C
             HB -> C
             HC -> B
             HN -> C
             NN -> C
             BH -> H
             NC -> B
             NB -> B
             BN -> B
             BB -> N
             BC -> B
             CC -> N
             CN -> C";

        let (polymer, rules) = parse_input(input);
        let score = polymer_score_at_step(&polymer, &rules, 40);
        assert_eq!(score, 2188189693529);
    }
}
