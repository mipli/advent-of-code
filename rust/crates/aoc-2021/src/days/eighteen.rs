use std::{fmt::Display, str::FromStr};

use libaoc::{DayNumber, Solver};
use nom::{bytes::complete::tag, character::complete::digit1, combinator::opt, Finish, IResult};

pub fn get() -> Solver {
    Solver::new(DayNumber::Eighteen, |input| {
        let snails = parse_input(input);
        let base = snails[0].clone();
        let sum = snails
            .iter()
            .skip(1)
            .fold(base, |base, snail| base + snail.clone());
        println!("Magnitude: {}", sum.magnitude());

        let mut mags = vec![];
        for i in 0..snails.len() {
            for j in 0..snails.len() {
                if i != j {
                    mags.push((snails[i].clone() + snails[j].clone()).magnitude());
                }
            }
        }
        println!("Max magnitude: {}", mags.iter().max().unwrap());
    })
}

fn parse_input(input: &str) -> Vec<SnailNumber> {
    input
        .lines()
        .map(|line| {
            line.trim()
                .parse::<SnailNumber>()
                .expect("Failed to parse snail number")
        })
        .collect::<Vec<_>>()
}

#[derive(Clone, Debug)]
pub struct SnailNumber {
    left: Part,
    right: Part,
}

impl Display for SnailNumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{},{}]", self.left, self.right)
    }
}

impl FromStr for SnailNumber {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match parse_snail_number(s.trim()).finish() {
            Ok((_rem, inp)) => Ok(inp),
            Err(nom::error::Error { input, code }) => Err(anyhow::Error::new(nom::error::Error {
                code,
                input: input.to_string(),
            })),
        }
    }
}

impl std::ops::Add for SnailNumber {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let mut snail = SnailNumber {
            left: Part::Pair(Box::new(self)),
            right: Part::Pair(Box::new(rhs)),
        };

        while snail.explode() || snail.split() {}
        snail
    }
}

impl SnailNumber {
    fn magnitude(&self) -> u64 {
        let left = match self.left {
            Part::Pair(ref pair) => pair.magnitude(),
            Part::Regular(val) => val as u64,
        };
        let right = match self.right {
            Part::Pair(ref pair) => pair.magnitude(),
            Part::Regular(val) => val as u64,
        };
        3 * left + 2 * right
    }

    fn explode(&mut self) -> bool {
        if let Some(index) = self.get_explode_index(0, 0) {
            let (old_pair, number_index) = self
                .replace_pair(index, Part::Regular(0))
                .expect("We have already established that this is a pair we can explode");
            if let Some(value) = self.get_number(number_index + 1) {
                if let Part::Regular(old_value) = old_pair.right {
                    self.replace_number(number_index + 1, Part::Regular(value + old_value));
                }
            }
            if number_index > 0 {
                if let Some(value) = self.get_number(number_index - 1) {
                    if let Part::Regular(old_value) = old_pair.left {
                        self.replace_number(number_index - 1, Part::Regular(value + old_value));
                    }
                }
            }
            true
        } else {
            false
        }
    }

    fn split(&mut self) -> bool {
        if let (Some(index), _) = self.get_split_index(0) {
            let value = self.get_number(index).unwrap();
            let half = (value as f32) / 2.0;
            let new_pair = SnailNumber {
                left: Part::Regular(half.floor() as u8),
                right: Part::Regular(half.ceil() as u8),
            };
            self.replace_number(index, Part::Pair(Box::new(new_pair)));
            true
        } else {
            false
        }
    }

    fn get_explode_index(&self, depth: usize, offset: usize) -> Option<usize> {
        if depth >= 4 {
            return Some(offset);
        }

        if let Part::Pair(ref pair) = self.left {
            let ret = pair.get_explode_index(depth + 1, offset + 1);
            if ret.is_some() {
                return ret;
            }
        }
        if let Part::Pair(ref pair) = self.right {
            let ret = pair.get_explode_index(depth + 1, offset + 1);
            if ret.is_some() {
                return ret;
            }
        }

        None
    }

    fn get_split_index(&self, mut offset: usize) -> (Option<usize>, usize) {
        match self.left {
            Part::Regular(v) if v >= 10 => {
                return (Some(offset), offset);
            }
            Part::Regular(_) => offset += 1,
            Part::Pair(ref pair) => {
                let (v, new_offset) = pair.get_split_index(offset);
                if v.is_some() {
                    return (v, new_offset);
                }
                offset = new_offset;
            }
        }
        match self.right {
            Part::Regular(v) if v >= 10 => {
                return (Some(offset), offset);
            }
            Part::Regular(_) => offset += 1,
            Part::Pair(ref pair) => {
                let (v, new_offset) = pair.get_split_index(offset);
                if v.is_some() {
                    return (v, new_offset);
                }
                offset = new_offset;
            }
        }
        (None, offset)
    }

    fn get_number(&self, index: usize) -> Option<u8> {
        let (number, _) = self.get_number_inner(index, 0);
        number
    }

    fn get_number_inner(&self, index: usize, mut offset: usize) -> (Option<u8>, usize) {
        match self.left {
            Part::Regular(value) if offset == index => {
                return (Some(value), offset);
            }
            Part::Regular(_) => offset += 1,
            Part::Pair(ref pair) => {
                let (number, new_offset) = pair.get_number_inner(index, offset);
                offset = new_offset;
                if number.is_some() {
                    return (number, offset);
                }
            }
        }
        match self.right {
            Part::Regular(value) if offset == index => {
                return (Some(value), offset);
            }
            Part::Regular(_) => offset += 1,
            Part::Pair(ref pair) => {
                let (number, new_offset) = pair.get_number_inner(index, offset);
                offset = new_offset;
                if number.is_some() {
                    return (number, offset);
                }
            }
        }

        (None, offset)
    }

    fn replace_pair(&mut self, index: usize, value: Part) -> Option<(SnailNumber, usize)> {
        assert!(matches!(value, Part::Regular(_)));

        let (number, _, new_index) = self.replace_pair_inner(index, value, 0, 0);
        number.map(|num| (num, new_index))
    }

    fn replace_pair_inner(
        &mut self,
        index: usize,
        value: Part,
        offset: usize,
        mut new_index: usize,
    ) -> (Option<SnailNumber>, usize, usize) {
        match self.left {
            Part::Pair(ref p) if offset + 1 == index => {
                let num = *p.clone();
                self.left = value;
                return (Some(num), offset, new_index);
            }
            Part::Pair(ref mut pair) => {
                let (v, new_offset, ni) =
                    pair.replace_pair_inner(index, value.clone(), offset + 1, new_index);
                new_index = ni;
                if v.is_some() {
                    return (v, new_offset, new_index);
                }
            }
            _ => new_index += 1,
        }
        match self.right {
            Part::Pair(ref p) if offset + 1 == index => {
                let num = *p.clone();
                self.right = value;
                return (Some(num), offset, new_index);
            }
            Part::Pair(ref mut pair) => {
                let (v, new_offset, ni) =
                    pair.replace_pair_inner(index, value, offset + 1, new_index);
                new_index = ni;
                if v.is_some() {
                    return (v, new_offset, new_index);
                }
            }
            _ => new_index += 1,
        }

        (None, offset, new_index)
    }

    fn replace_number(&mut self, index: usize, value: Part) -> (Option<u8>, usize) {
        // TODO: returned index value is only valid if Part is a Pair
        let (number, _, new_index) = self.replace_number_inner(index, value, 0, 0);
        (number, new_index)
    }

    fn replace_number_inner(
        &mut self,
        index: usize,
        value: Part,
        mut offset: usize,
        mut new_index: usize,
    ) -> (Option<u8>, usize, usize) {
        match self.left {
            Part::Regular(v) if offset == index => {
                self.left = value;
                return (Some(v), offset, new_index + 1);
            }
            Part::Regular(_) => offset += 1,
            Part::Pair(ref mut pair) => {
                new_index += 1;
                let (num, new_offset, ni) =
                    pair.replace_number_inner(index, value.clone(), offset, new_index);
                if num.is_some() {
                    return (num, new_offset, ni);
                }
                offset = new_offset;
                new_index = ni;
            }
        }
        match self.right {
            Part::Regular(v) if offset == index => {
                self.right = value;
                return (Some(v), offset, new_index + 1);
            }
            Part::Regular(_) => offset += 1,
            Part::Pair(ref mut pair) => {
                new_index += 1;
                let (num, new_offset, ni) =
                    pair.replace_number_inner(index, value, offset, new_index);
                if num.is_some() {
                    return (num, new_offset, ni);
                }
                offset = new_offset;
                new_index = ni;
            }
        }

        (None, offset, new_index)
    }
}

#[derive(Clone, Debug)]
pub enum Part {
    Regular(u8),
    Pair(Box<SnailNumber>),
}

impl Display for Part {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Part::Regular(n) => write!(f, "{n}"),
            Part::Pair(p) => write!(f, "{p}"),
        }
    }
}

fn parse_snail_number(input: &str) -> IResult<&str, SnailNumber> {
    let (input, _) = tag("[")(input)?;
    let (input, left) = parse_part(input)?;
    let (input, _) = tag(",")(input)?;
    let (input, right) = parse_part(input)?;
    let (input, _) = tag("]")(input)?;

    Ok((input, SnailNumber { left, right }))
}

fn parse_part(input: &str) -> IResult<&str, Part> {
    let (input, num) = opt(digit1)(input)?;
    if let Some(num) = num {
        let number: u8 = num
            .parse()
            .expect("Failed to convert parsed string to number");
        Ok((input, Part::Regular(number)))
    } else {
        let (input, snail) = parse_snail_number(input)?;
        Ok((input, Part::Pair(Box::new(snail))))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_parsing() {
        let number: SnailNumber = "[1,2]".parse().expect("Failed to parse number");
        assert_eq!(format!("{number}"), "[1,2]");

        let number: SnailNumber = "[1,[3,4]]".parse().expect("Failed to parse number");
        assert_eq!(format!("{number}"), "[1,[3,4]]");

        let number: SnailNumber = "[[3,4],[3,2]]".parse().expect("Failed to parse number");
        assert_eq!(format!("{number}"), "[[3,4],[3,2]]");
    }

    #[test]
    pub fn test_get_number() {
        let number: SnailNumber = "[[3,4],[[3,2],4]]".parse().expect("Failed to parse number");
        assert_eq!(number.get_number(0), Some(3));
        assert_eq!(number.get_number(1), Some(4));
        assert_eq!(number.get_number(2), Some(3));
        assert_eq!(number.get_number(3), Some(2));
        assert_eq!(number.get_number(4), Some(4));
        assert!(number.get_number(5).is_none());
    }

    #[test]
    pub fn test_replace_number() {
        let mut number: SnailNumber = "[[5,4],[[3,2],8]]".parse().expect("Failed to parse number");
        let part = Part::Pair(Box::new("[0,0]".parse().expect("Failed to parse number")));
        let _ = number.replace_number(2, part);
        assert_eq!(format!("{number}"), "[[5,4],[[[0,0],2],8]]");
    }

    #[test]
    pub fn test_replace_pair() {
        let mut number: SnailNumber = "[[3,4],[[3,2],4]]".parse().expect("Failed to parse number");
        let (_, idx) = number.replace_pair(1, Part::Regular(5)).unwrap();
        assert_eq!(idx, 0);
        assert_eq!(format!("{number}"), "[5,[[3,2],4]]");
    }

    #[test]
    pub fn test_explode() {
        let mut a: SnailNumber = "[[[[[9,8],1],2],3],4]"
            .parse()
            .expect("Failed to parse number");
        a.explode();
        assert_eq!(format!("{a}"), "[[[[0,9],2],3],4]");

        let mut a: SnailNumber = "[7,[6,[5,[4,[3,2]]]]]"
            .parse()
            .expect("Failed to parse number");
        a.explode();
        assert_eq!(format!("{a}"), "[7,[6,[5,[7,0]]]]");

        let mut a: SnailNumber = "[[6,[5,[4,[3,2]]]],1]"
            .parse()
            .expect("Failed to parse number");
        a.explode();
        assert_eq!(format!("{a}"), "[[6,[5,[7,0]]],3]");

        let mut a: SnailNumber = "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"
            .parse()
            .expect("Failed to parse number");
        a.explode();
        assert_eq!(format!("{a}"), "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]");

        let mut a: SnailNumber = "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
            .parse()
            .expect("Failed to parse number");
        a.explode();
        assert_eq!(format!("{a}"), "[[3,[2,[8,0]]],[9,[5,[7,0]]]]");

        let mut a: SnailNumber =
            "[[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]],[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]]"
                .parse()
                .expect("Failed to parse number");
        a.explode();
        assert_eq!(
            format!("{a}"),
            "[[[[4,0],[5,0]],[[[4,5],[2,6]],[9,5]]],[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]]"
        );
    }

    #[test]
    pub fn test_split() {
        let mut a: SnailNumber = "[[[[0,7],4],[15,[0,13]]],[1,1]]"
            .parse()
            .expect("Failed to parse number");
        a.split();
        assert_eq!(format!("{a}"), "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]");

        let mut a: SnailNumber = "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]"
            .parse()
            .expect("Failed to parse number");
        a.split();
        assert_eq!(format!("{a}"), "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]");
    }

    #[test]
    pub fn test_addition() {
        let a: SnailNumber = "[1,2]".parse().expect("Failed to parse number");
        let b: SnailNumber = "[3,4]".parse().expect("Failed to parse number");
        let c = a + b;
        assert_eq!(format!("{c}"), "[[1,2],[3,4]]");

        let a: SnailNumber = "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]"
            .parse()
            .expect("Failed to parse number");
        let b: SnailNumber = "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]"
            .parse()
            .expect("Failed to parse number");
        let c = a + b;
        assert_eq!(
            format!("{c}"),
            "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]"
        );
    }

    #[test]
    pub fn test_example() {
        let input = "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
                 [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
                 [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
                 [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
                 [7,[5,[[3,8],[1,4]]]]
                 [[2,[2,2]],[8,[8,1]]]
                 [2,9]
                 [1,[[[9,3],9],[[9,0],[0,7]]]]
                 [[[5,[7,4]],7],1]
                 [[[[4,2],2],6],[8,7]]";
        let snails = parse_input(input);
        let base = snails[0].clone();
        let sum = snails
            .into_iter()
            .skip(1)
            .fold(base, |base, snail| base + snail);
        assert_eq!(
            format!("{sum}"),
            "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
        );
    }

    #[test]
    pub fn test_magnitude() {
        let a: SnailNumber = "[9,1]".parse().expect("Failed to parse number");
        assert_eq!(a.magnitude(), 29);

        let a: SnailNumber = "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
            .parse()
            .expect("Failed to parse number");
        assert_eq!(a.magnitude(), 3488);
    }

    #[test]
    pub fn test_example_one() {
        let input = "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
            [[[5,[2,8]],4],[5,[[9,9],0]]]
            [6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
            [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
            [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
            [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
            [[[[5,4],[7,7]],8],[[8,3],8]]
            [[9,3],[[9,9],[6,[4,9]]]]
            [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
            [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]";
        let snails = parse_input(input);
        let base = snails[0].clone();
        let sum = snails
            .into_iter()
            .skip(1)
            .fold(base, |base, snail| base + snail);
        assert_eq!(sum.magnitude(), 4140);
    }
}
