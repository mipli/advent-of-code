use std::collections::HashMap;

use libaoc::{point::Point, DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Twenty, |input| {
        let (mut image, enhancer) = parse_input(input);

        for i in 1..=50 {
            image = enhance(image, &enhancer);
            if i == 2 {
                println!("Lit pixels: {}", count_lit(&image));
            }
        }
        println!("Lit pixels: {}", count_lit(&image));
    })
}

fn enhance(image: Image, enhancer: &[u8]) -> Image {
    let mut enhanced: Image = Image::default();

    let (min_x, max_x, min_y, max_y) = image.dimensions;
    enhanced.dimensions = (min_x - 1, max_x + 1, min_y - 1, max_y + 1);

    // image fill is either the first pixel in enhancer data, or the last one
    let fill_index = if image.fill == 0 { 0 } else { 511 };
    enhanced.fill = enhancer[fill_index];

    for y in min_y - 1..=max_y + 1 {
        for x in min_x - 1..=max_x + 1 {
            let value = get_enhanced_value((x, y).into(), &image, enhancer);
            enhanced.data.insert((x, y).into(), value);
        }
    }

    enhanced
}

fn count_lit(image: &Image) -> usize {
    if image.fill == 1 {
        panic!("An infinite amount of pixels are lit");
    }
    image.data.values().filter(|&&c| c == 1).count()
}

fn get_enhanced_value(point: Point, image: &Image, enhancer: &[u8]) -> u8 {
    let points = [
        (point.x - 1, point.y - 1).into(),
        (point.x, point.y - 1).into(),
        (point.x + 1, point.y - 1).into(),
        (point.x - 1, point.y).into(),
        (point.x, point.y).into(),
        (point.x + 1, point.y).into(),
        (point.x - 1, point.y + 1).into(),
        (point.x, point.y + 1).into(),
        (point.x + 1, point.y + 1).into(),
    ];

    let index: usize = points
        .iter()
        .map(|point| match image.data.get(point) {
            Some(c) => *c,
            _ => image.fill,
        })
        .fold(0, |acc, b| (acc << 1) + b as usize);
    enhancer[index]
}

#[allow(dead_code)]
fn display_image(image: &Image) {
    let (min_x, max_x, min_y, max_y) = image.dimensions;
    for y in min_y..=max_y {
        for x in min_x..=max_x {
            match image.data.get(&(x, y).into()) {
                Some(1) => print!("#"),
                _ => print!("."),
            }
        }
        println!();
    }
}

#[derive(Default)]
struct Image {
    data: HashMap<Point, u8>,
    fill: u8,
    dimensions: (i32, i32, i32, i32),
}

impl Image {
    fn udpate_dimensions(&mut self) {
        let mut min_x = std::i32::MAX;
        let mut max_x = std::i32::MIN;
        let mut min_y = std::i32::MAX;
        let mut max_y = std::i32::MIN;

        self.data.keys().for_each(|p| {
            min_x = std::cmp::min(min_x, p.x);
            max_x = std::cmp::max(max_x, p.x);
            min_y = std::cmp::min(min_y, p.y);
            max_y = std::cmp::max(max_y, p.y);
        });

        self.dimensions = (min_x, max_x, min_y, max_y);
    }
}

fn parse_input(input: &str) -> (Image, Vec<u8>) {
    let mut lines = input.lines();

    let enhancer: Vec<u8> = lines
        .next()
        .map(|line| {
            line.trim()
                .chars()
                .map(|c| match c {
                    '.' => 0u8,
                    '#' => 1u8,
                    _ => unreachable!(),
                })
                .collect::<Vec<_>>()
        })
        .unwrap();

    let _ = lines.next();

    let mut image: Image = Image::default();
    for (y, line) in lines.enumerate() {
        line.trim().chars().enumerate().for_each(|(x, c)| {
            match c {
                '.' => image.data.insert((x as i32, y as i32).into(), 0u8),
                '#' => image.data.insert((x as i32, y as i32).into(), 1u8),
                _ => unreachable!(),
            };
        });
    }

    image.udpate_dimensions();

    (image, enhancer)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_input() -> String {
        "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

            #..#.
            #....
            ##..#
            ..#..
            ..###".to_string()
    }

    #[test]
    pub fn test_parsing() {
        let input = get_input();

        let (image, enhancer) = parse_input(&input);

        assert_eq!(image.data.len(), 25);
        assert_eq!(enhancer.len(), 512);
    }

    #[test]
    pub fn test_image_enhance() {
        let input = get_input();

        let (image, enhancer) = parse_input(&input);

        let value = get_enhanced_value((2, 2).into(), &image, &enhancer);
        assert_eq!(value, 1);

        let enhanced_image = enhance(image, &enhancer);
        let enhanced_image = enhance(enhanced_image, &enhancer);

        assert_eq!(count_lit(&enhanced_image), 35);
    }
}
