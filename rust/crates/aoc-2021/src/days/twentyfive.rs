use libaoc::{
    grid::{Grid, NeighbourConnection},
    point::Point,
    DayNumber, Solver,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::TwentyFive, |input| {
        let mut cave = parse_input(input);
        let mut iterations = 1;

        while let TickResult::Changed(c) = tick(cave) {
            cave = c;
            iterations += 1;
        }
        println!("Static after: {iterations}");
    })
}

struct Cave {
    grid: Grid<Floor>,
    right: Vec<Point>,
    down: Vec<Point>,
}

impl std::fmt::Display for Cave {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.grid)
    }
}

enum TickResult {
    Changed(Cave),
    Static(Cave),
}

fn tick(cave: Cave) -> TickResult {
    let mut new_cave = Cave {
        grid: Grid::new(
            cave.grid.get_width(),
            cave.grid.get_height(),
            NeighbourConnection::Four,
            Floor::Empty,
        ),
        right: Vec::with_capacity(cave.right.len()),
        down: Vec::with_capacity(cave.down.len()),
    };

    let mut changed = false;

    for point in cave.right {
        let move_point = ((point.x + 1) % (cave.grid.get_width() as i32), point.y).into();
        let new_point = if *cave.grid.get(move_point) == Floor::Empty {
            changed = true;
            move_point
        } else {
            point
        };
        new_cave
            .grid
            .set(new_point, Floor::Occupied(Cucumber::Right));
        new_cave.right.push(new_point);
    }

    for point in cave.down {
        let move_point = (point.x, (point.y + 1) % (cave.grid.get_height() as i32)).into();
        let new_point = if (*cave.grid.get(move_point) == Floor::Empty
            || *cave.grid.get(move_point) == Floor::Occupied(Cucumber::Right))
            && *new_cave.grid.get(move_point) == Floor::Empty
        {
            changed = true;
            move_point
        } else {
            point
        };
        new_cave
            .grid
            .set(new_point, Floor::Occupied(Cucumber::Down));
        new_cave.down.push(new_point);
    }

    if changed {
        TickResult::Changed(new_cave)
    } else {
        TickResult::Static(new_cave)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Cucumber {
    Right,
    Down,
}

impl std::fmt::Display for Cucumber {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Cucumber::Right => write!(f, ">"),
            Cucumber::Down => write!(f, "v"),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Floor {
    Empty,
    Occupied(Cucumber),
}

impl std::fmt::Display for Floor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Floor::Empty => write!(f, "."),
            Floor::Occupied(cucumber) => write!(f, "{cucumber}"),
        }
    }
}

fn parse_input(input: &str) -> Cave {
    let height = input.lines().count();
    let width = input.lines().next().expect("No lines in input").len();

    let data: Vec<Floor> = input.lines().fold(vec![], |mut acc, line| {
        acc.extend_from_slice(
            line.trim()
                .chars()
                .map(|c| match c {
                    '>' => Floor::Occupied(Cucumber::Right),
                    'v' => Floor::Occupied(Cucumber::Down),
                    _ => Floor::Empty,
                })
                .collect::<Vec<_>>()
                .as_slice(),
        );
        acc
    });
    let mut right = vec![];
    let mut down = vec![];
    let grid = Grid::new_with_data(width, height, NeighbourConnection::Four, data);
    for point in grid.point_iter() {
        match grid.get(point) {
            Floor::Occupied(Cucumber::Down) => down.push(point),
            Floor::Occupied(Cucumber::Right) => right.push(point),
            _ => {}
        }
    }

    Cave { grid, right, down }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn simple() {
        let input = "..v.>>....
                     ..>.......";
        let cave = parse_input(input);
        if let TickResult::Changed(ticked) = tick(cave) {
            assert_eq!(format!("{ticked}"), "....>.>...\n..v>......");
        } else {
            panic!();
        }
    }

    #[test]
    pub fn example_one() {
        let input = "v...>>.vv>
            .vv>>.vv..
            >>.>v>...v
            >>v>>.>.v.
            v>v.vv.v..
            >.>>..v...
            .vv..>.>v.
            v.v..>>v.v
            ....v..v.>";
        let mut cave = parse_input(input);
        let mut iterations = 1;

        while let TickResult::Changed(c) = tick(cave) {
            cave = c;
            iterations += 1;
        }
        assert_eq!(iterations, 58);
    }
}
