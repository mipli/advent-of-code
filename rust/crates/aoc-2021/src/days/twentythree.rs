use std::{
    cmp::Ordering,
    collections::{BinaryHeap, HashMap},
    hash::Hash,
};

use colored::Colorize;
use itertools::Itertools;
use libaoc::{
    grid::{Grid, NeighbourConnection},
    point::Point,
    DayNumber, Solver,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::TwentyThree, |input| {
        let level = parse_input(input);
        let extended = level.clone().extend();
        let (cost, _levels) = solve(level).unwrap();
        println!("Energy usage: {cost:?}");
        let (cost, _levels) = solve(extended).unwrap();
        println!("Energy usage: {cost:?}");
    })
}

fn solve(level: Level) -> Option<(u32, Vec<Level>)> {
    let mut frontier: BinaryHeap<PathNode> = BinaryHeap::default();
    let mut visited: HashMap<Level, u32> = HashMap::default();
    let mut steps: HashMap<Level, Level> = HashMap::default();
    frontier.push(PathNode {
        homes: level.count_home_amphoids(),
        level,
        cost: 0,
    });

    while let Some(current) = frontier.pop() {
        if let Some(&cost) = visited.get(&current.level) {
            if cost <= current.cost {
                continue;
            }
        }
        if current.level.is_finished() {
            let mut states = vec![current.level.clone()];
            let cost = current.cost;
            let mut c = current.level;
            while let Some(state) = steps.get(&c) {
                states.push(state.clone());
                c = state.clone();
            }
            return Some((cost, states));
        }

        for next in current.level.get_next_states() {
            match visited.get(&next) {
                Some(level) if *level <= next.cost => {}
                Some(_) => {
                    steps.insert(next, current.level.clone());
                }
                None => {
                    steps.insert(next.clone(), current.level.clone());
                    frontier.push(PathNode {
                        cost: next.cost,
                        homes: next.count_home_amphoids(),
                        level: next,
                    });
                }
            }
        }
        visited.insert(current.level, current.cost);
    }

    None
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct PathNode {
    level: Level,
    homes: u32,
    cost: u32,
}

impl Ord for PathNode {
    fn cmp(&self, other: &Self) -> Ordering {
        self.cost
            .cmp(&other.cost)
            .reverse()
            .then_with(|| self.homes.cmp(&other.homes))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for PathNode {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Tile {
    Wall,
    Floor,
    A,
    B,
    C,
    D,
}

impl From<char> for Tile {
    fn from(c: char) -> Self {
        match c {
            '.' => Tile::Floor,
            'A' => Tile::A,
            'B' => Tile::B,
            'C' => Tile::C,
            'D' => Tile::D,
            _ => Tile::Wall,
        }
    }
}

impl std::fmt::Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Tile::Wall => write!(f, "{}", "#".dimmed()),
            Tile::Floor => write!(f, "{}", ".".dimmed()),
            Tile::A => write!(f, "{}", "A".green()),
            Tile::B => write!(f, "{}", "B".cyan()),
            Tile::C => write!(f, "{}", "C".blue()),
            Tile::D => write!(f, "{}", "D".yellow()),
        }
    }
}

impl Tile {
    fn cost(&self) -> u32 {
        match self {
            Tile::A => 1,
            Tile::B => 10,
            Tile::C => 100,
            Tile::D => 1000,
            _ => unreachable!("Cannot find home room for non amphipod tile"),
        }
    }
}

#[derive(Debug, Clone)]
struct Level {
    grid: Grid<Tile>,
    cost: u32,
    extended: bool,
}

impl Level {
    fn is_finished(&self) -> bool {
        if self.extended {
            self.count_home_amphoids() == 16
        } else {
            self.count_home_amphoids() == 8
        }
    }

    fn extend(self) -> Self {
        let mut data = self.grid.get_data();
        data.extend(std::iter::repeat(Tile::Wall).take(self.grid.get_width() * 2));
        let mut grid = Grid::new_with_data(
            self.grid.get_width(),
            self.grid.get_height() + 2,
            NeighbourConnection::Four,
            data,
        );

        grid.set((3, 3).into(), Tile::D);
        grid.set((3, 4).into(), Tile::D);
        grid.set((3, 5).into(), *self.grid.get((3, 3).into()));

        grid.set((5, 3).into(), Tile::C);
        grid.set((5, 4).into(), Tile::B);
        grid.set((5, 5).into(), *self.grid.get((5, 3).into()));

        grid.set((7, 3).into(), Tile::B);
        grid.set((7, 4).into(), Tile::A);
        grid.set((7, 5).into(), *self.grid.get((7, 3).into()));

        grid.set((9, 3).into(), Tile::A);
        grid.set((9, 4).into(), Tile::C);
        grid.set((9, 5).into(), *self.grid.get((9, 3).into()));

        Level {
            cost: 0,
            extended: true,
            grid,
        }
    }

    fn get_home_pos(&self, amphipod: Tile) -> i32 {
        match amphipod {
            Tile::A => 3,
            Tile::B => 5,
            Tile::C => 7,
            Tile::D => 9,
            _ => unreachable!("Cannot find home room for non amphipod tile"),
        }
    }

    fn get_home_room_coord(&self, amphipod: Tile) -> impl Iterator<Item = &Point> {
        match (self.extended, amphipod) {
            (false, Tile::A) => [Point { x: 3, y: 3 }, Point { x: 3, y: 2 }].iter(),
            (false, Tile::B) => ([Point { x: 5, y: 3 }, Point { x: 5, y: 2 }]).iter(),
            (false, Tile::C) => ([Point { x: 7, y: 3 }, Point { x: 7, y: 2 }]).iter(),
            (false, Tile::D) => ([Point { x: 9, y: 3 }, Point { x: 9, y: 2 }]).iter(),
            (true, Tile::A) => ([
                Point { x: 3, y: 5 },
                Point { x: 3, y: 4 },
                Point { x: 3, y: 3 },
                Point { x: 3, y: 2 },
            ])
            .iter(),
            (true, Tile::B) => ([
                Point { x: 5, y: 5 },
                Point { x: 5, y: 4 },
                Point { x: 5, y: 3 },
                Point { x: 5, y: 2 },
            ])
            .iter(),
            (true, Tile::C) => ([
                Point { x: 7, y: 5 },
                Point { x: 7, y: 4 },
                Point { x: 7, y: 3 },
                Point { x: 7, y: 2 },
            ])
            .iter(),
            (true, Tile::D) => ([
                Point { x: 9, y: 5 },
                Point { x: 9, y: 4 },
                Point { x: 9, y: 3 },
                Point { x: 9, y: 2 },
            ])
            .iter(),
            _ => unreachable!("Cannot find home room for non amphipod tile"),
        }
    }

    fn count_home_amphoids(&self) -> u32 {
        let mut count = 0;
        for amphipod in &[Tile::A, Tile::B, Tile::C, Tile::D] {
            for home_point in self.get_home_room_coord(*amphipod) {
                if self.grid.get(*home_point) == amphipod {
                    count += 1;
                } else {
                    break;
                }
            }
        }
        count
    }

    fn is_home(&self, amphipod: Tile, pos: Point) -> bool {
        let home_points = self.get_home_room_coord(amphipod);
        for home_point in home_points {
            if *home_point == pos {
                return true;
            }
            if *self.grid.get(*home_point) != amphipod {
                return false;
            }
        }
        false
    }

    fn is_vacant(&self, point: Point) -> bool {
        *self.grid.get(point) == Tile::Floor
    }

    fn get_amphoid_positions(&self) -> Vec<(Tile, Point)> {
        self.grid
            .point_iter()
            .filter_map(|point| match self.grid.get(point) {
                Tile::A => Some((Tile::A, point)),
                Tile::B => Some((Tile::B, point)),
                Tile::C => Some((Tile::C, point)),
                Tile::D => Some((Tile::D, point)),
                _ => None,
            })
            .collect_vec()
    }

    fn get_next_states(&self) -> Vec<Level> {
        let mut states = vec![];

        let amphipods = self.get_amphoid_positions();

        for amp in amphipods {
            if let Some(moves) = self.get_valid_moves(amp.0, amp.1) {
                for (amp_move, cost) in moves {
                    states.push(self.create_state(amp.0, amp.1, amp_move, cost));
                }
            }
        }

        states
    }

    fn create_state(&self, amphipod: Tile, from: Point, to: Point, cost: u32) -> Level {
        let mut level = self.clone();

        level.grid.set(from, Tile::Floor);
        level.grid.set(to, amphipod);
        level.cost += cost;

        level
    }

    fn get_valid_moves(&self, amphipod: Tile, pos: Point) -> Option<Vec<(Point, u32)>> {
        if self.is_home(amphipod, pos) {
            return None;
        }
        let home_points = self.get_home_room_coord(amphipod);
        let home_x = self.get_home_pos(amphipod);
        if pos.y == 1 {
            // Amphipod is in cooridor, only valid move is to it's home room
            let dx = (home_x - pos.x).signum();

            let mut new_pos = pos;

            while new_pos.x != home_x {
                new_pos.x += dx;
                if !self.is_vacant(new_pos) {
                    return None;
                }
            }

            for home_point in home_points {
                if self.is_vacant(*home_point) {
                    let cost = pos.manhattan_distance(home_point) * amphipod.cost();
                    return Some(vec![(*home_point, cost)]);
                }
                if *self.grid.get(*home_point) != amphipod {
                    return None;
                }
            }
            None
        } else {
            let mut y = pos.y;
            while y != 1 {
                y -= 1;
                if !self.is_vacant((pos.x, y).into()) {
                    return None;
                }
            }
            let mut valid = vec![];
            for x in pos.x + 1..=11 {
                if [3, 5, 7, 9].contains(&x) {
                    continue;
                }
                let new_pos: Point = (x, 1).into();
                if self.is_vacant(new_pos) {
                    let cost = pos.manhattan_distance(&new_pos) * amphipod.cost();
                    valid.push((new_pos, cost));
                } else {
                    break;
                }
            }
            for x in (1..pos.x).rev() {
                if [3, 5, 7, 9].contains(&x) {
                    continue;
                }
                let new_pos: Point = (x, 1).into();
                if self.is_vacant(new_pos) {
                    let cost = pos.manhattan_distance(&new_pos) * amphipod.cost();
                    valid.push((new_pos, cost));
                } else {
                    break;
                }
            }

            let home_x = self.get_home_pos(amphipod);
            let dx = (pos.x - home_x).signum();
            if valid.iter().any(|(p, _)| p.x == home_x + dx) {
                for home_point in home_points {
                    if self.is_vacant(*home_point) {
                        let dist = (pos.y - 1) + (pos.x - home_point.x).abs() + home_point.y - 1;
                        let cost = dist as u32 * amphipod.cost();
                        valid.push((*home_point, cost));
                        return Some(valid);
                    }
                    if *self.grid.get(*home_point) != amphipod {
                        return Some(valid);
                    }
                }
            }

            Some(valid)
        }
    }
}

impl Hash for Level {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.get_amphoid_positions().hash(state);
        self.cost.hash(state);
    }
}

impl PartialEq for Level {
    fn eq(&self, other: &Self) -> bool {
        self.cost == other.cost && self.get_amphoid_positions() == other.get_amphoid_positions()
    }
}

impl Eq for Level {}

impl std::fmt::Display for Level {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "COST: {} / {}", self.cost, self.count_home_amphoids())?;
        write!(f, "{}", self.grid)
    }
}

fn parse_input(input: &str) -> Level {
    let height = input.lines().count();
    let width = input.lines().next().expect("No lines in input").len();

    let data: Vec<Tile> = input.lines().fold(vec![], |mut acc, line| {
        acc.extend_from_slice(
            line.chars()
                .chain(std::iter::repeat('#'))
                .take(width)
                .map(|c| c.into())
                .collect::<Vec<_>>()
                .as_slice(),
        );
        acc
    });

    Level {
        grid: Grid::new_with_data(width, height, NeighbourConnection::Four, data),
        cost: 0,
        extended: false,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_input() -> String {
        "#############\n#...........#\n###B#C#B#D###\n  #A#D#C#A#\n  #########".to_string()
    }

    #[test]
    pub fn check_finished_state() {
        let level =
            parse_input("#############\n#...........#\n###A#B#C#D###\n  #A#B#C#D#\n  #########");
        assert!(level.is_finished());
    }

    #[test]
    pub fn test_simple_solve() {
        let level =
            parse_input("#############\n#A.........B#\n###.#.#C#D###\n  #A#B#C#D#\n  #########");
        assert!(!level.is_finished());
        let (cost, _) = solve(level).unwrap();
        assert_eq!(cost, 73);
    }

    #[test]
    pub fn test_step_two() {
        let level =
            parse_input("#############\n#...B.......#\n###B#C#.#D###\n  #A#D#C#A#\n  #########");
        assert!(!level.is_finished());
        let (cost, levels) = solve(level).unwrap();
        for level in levels.iter().rev() {
            println!("{level}");
        }
        assert_eq!(cost, 12481);
    }

    #[test]
    pub fn test_step_three() {
        let level =
            parse_input("#############\n#...B.......#\n###B#.#C#D###\n  #A#D#C#A#\n  #########");
        assert!(!level.is_finished());
        let (cost, levels) = solve(level).unwrap();
        for level in levels.iter().rev() {
            println!("{level}");
        }
        assert_eq!(cost, 12081);
    }

    #[test]
    pub fn test_step_five() {
        let level =
            parse_input("#############\n#.....D.....#\n###.#B#C#D###\n  #A#B#C#A#\n  #########");
        assert!(!level.is_finished());
        let (cost, levels) = solve(level).unwrap();
        for level in levels.iter().rev() {
            println!("{level}");
        }
        assert_eq!(cost, 9011);
    }

    #[test]
    pub fn test_step_six() {
        let level =
            parse_input("#############\n#.....D.D.A.#\n###.#B#C#.###\n  #A#B#C#.#\n  #########");
        assert!(!level.is_finished());
        let (cost, levels) = solve(level).unwrap();
        for level in levels.iter().rev() {
            println!("{level}");
        }
        assert_eq!(cost, 7008);
    }

    #[test]
    pub fn example_one() {
        let level = parse_input(&get_input());
        let (cost, levels) = solve(level).unwrap();
        for level in levels.iter().rev() {
            println!("{level}");
        }
        assert_eq!(cost, 12521);
    }

    #[test]
    pub fn example_two() {
        let level = parse_input(&get_input());
        let (cost, levels) = solve(level.extend()).unwrap();
        for level in levels.iter().rev() {
            println!("{level}");
        }
        assert_eq!(cost, 44169);
    }
}
