use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::One, |input| {
        let sea_levels: Vec<i32> = input
            .lines()
            .map(|line| line.parse().expect("Invalid sea level"))
            .collect::<Vec<_>>();

        println!(
            "Sea level increases: {}",
            count_individual_increases(&sea_levels)
        );

        println!(
            "Grouped sea level increases: {}",
            count_grouped_increases(&sea_levels)
        );
    })
}

pub fn count_individual_increases(sea_levels: &[i32]) -> usize {
    sea_levels.windows(2).filter(|&w| w[1] > w[0]).count()
}

pub fn count_grouped_increases(sea_levels: &[i32]) -> usize {
    sea_levels
        .iter()
        .zip(sea_levels.iter().skip(3))
        .filter(|(a, b)| b > a)
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn individual_increase_counting() {
        let sea_levels = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
        assert_eq!(count_individual_increases(&sea_levels), 7);
    }

    #[test]
    pub fn grouped_increase_counting() {
        let sea_levels = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
        assert_eq!(count_grouped_increases(&sea_levels), 5);
    }
}
