use libaoc::{point::Point, DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Seventeen, |input| {
        let target = parse_input(input.trim());
        println!("Trajectory height: {}", find_max_height(&target));
        println!("Valid trajectories: {}", count_valid_trajectories(&target));
    })
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Target {
    top: i32,
    left: i32,
    bottom: i32,
    right: i32,
}

impl Target {
    fn contains(&self, point: &Point) -> bool {
        point.x >= self.left
            && point.x <= self.right
            && point.y <= self.top
            && point.y >= self.bottom
    }
}

fn count_valid_trajectories(target: &Target) -> usize {
    let mut count = 0;

    for x in 1..=target.right {
        for y in target.bottom..=-target.bottom {
            if is_valid_trajectory((x, y).into(), target) {
                count += 1;
            }
        }
    }
    count
}

fn is_valid_trajectory(trajectory: Point, target: &Target) -> bool {
    let b = (2 * trajectory.y + 1) as f32;
    let min_time = (b + (b * b - 8.0 * target.top as f32).sqrt()) / 2.0;
    let max_time = (b + (b * b - 8.0 * target.bottom as f32).sqrt()) / 2.0;
    if max_time < min_time {
        return false;
    }

    let time = max_time.floor();
    let y_pos = position_after_time(trajectory.y, time);
    let x_pos = if (trajectory.x as f32) > max_time.ceil() {
        position_after_time(trajectory.x, time)
    } else {
        position_after_time(trajectory.x, trajectory.x as f32)
    };

    if target.contains(&(x_pos.ceil() as i32, y_pos.ceil() as i32).into()) {
        return true;
    }

    let time = min_time.ceil();
    let y_pos = position_after_time(trajectory.y, time);
    let x_pos = if (trajectory.x as f32) > min_time.floor() {
        position_after_time(trajectory.x, time)
    } else {
        position_after_time(trajectory.x, trajectory.x as f32)
    };
    target.contains(&(x_pos.ceil() as i32, y_pos.ceil() as i32).into())
}

fn position_after_time(val: i32, time: f32) -> f32 {
    let val = val as f32;
    (time / 2.0) * (2.0 * val - (time - 1.0))
}

fn find_max_height(target: &Target) -> i32 {
    let y_vel = (target.bottom + 1).abs();
    (y_vel * (y_vel + 1)) / 2
}

fn parse_input(input: &str) -> Target {
    let (left, right, bottom, top): (i32, i32, i32, i32) =
        serde_scan::scan!("target area: x={}..{}, y={}..{}" <- input)
            .expect("Failed to parse input");
    Target {
        left,
        right,
        bottom,
        top,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let input = "target area: x=20..30, y=-10..-5";
        let target = parse_input(input);
        assert_eq!(
            target,
            Target {
                top: -5,
                left: 20,
                bottom: -10,
                right: 30
            }
        );

        assert_eq!(find_max_height(&target), 45);
    }

    #[test]
    pub fn validate_trajectory() {
        let input = "target area: x=20..30, y=-10..-5";
        let target = parse_input(input);

        assert!(is_valid_trajectory((6, 9).into(), &target));
        assert!(!is_valid_trajectory((7, 12).into(), &target));
        assert!(is_valid_trajectory((23, -10).into(), &target));
        assert!(is_valid_trajectory((8, -1).into(), &target));
        assert!(is_valid_trajectory((24, -6).into(), &target));
        assert!(is_valid_trajectory((8, 1).into(), &target));
    }

    #[test]
    pub fn example_two() {
        let input = "target area: x=20..30, y=-10..-5";
        let target = parse_input(input);

        assert_eq!(count_valid_trajectories(&target), 112);
    }
}
