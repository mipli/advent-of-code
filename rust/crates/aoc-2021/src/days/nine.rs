use std::collections::{HashMap, HashSet};

use libaoc::{
    grid::{Grid, NeighbourConnection},
    point::Point,
    DayNumber, Solver,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::Nine, |input| {
        let grid = parse_input(input);
        println!("Low risk value: {}", sum_low_risk(&grid));
        let mut sizes = get_basin_sizes(&grid);
        sizes.sort_unstable();
        let basin_product = sizes.into_iter().rev().take(3).product::<u32>();
        println!("Basin sizes: {basin_product}");
    })
}

fn get_basin_sizes(grid: &Grid<u8>) -> Vec<u32> {
    struct Edge {
        point: Point,
        basin: u32,
    }
    let mut basin_sizes: HashMap<u32, u32> = HashMap::default();
    let mut tested: HashSet<Point> = HashSet::default();
    let mut edges = get_local_minima(grid)
        .into_iter()
        .enumerate()
        .map(|(i, p)| Edge {
            point: p,
            basin: i as u32,
        })
        .collect::<Vec<_>>();

    while let Some(edge) = edges.pop() {
        let val = grid.get(edge.point);
        if *val == 9 || tested.contains(&edge.point) {
            continue;
        }
        tested.insert(edge.point);

        *basin_sizes.entry(edge.basin).or_default() += 1;

        grid.get_neigbours(edge.point).iter().for_each(|&n| {
            if !tested.contains(&n) {
                edges.push(Edge {
                    point: n,
                    basin: edge.basin,
                });
            }
        });
    }

    basin_sizes.into_values().collect::<Vec<_>>()
}

fn sum_low_risk(grid: &Grid<u8>) -> i32 {
    get_local_minima(grid)
        .iter()
        .map(|&p| (grid.get(p) + 1) as i32)
        .sum()
}

fn get_local_minima(grid: &Grid<u8>) -> Vec<Point> {
    grid.point_iter()
        .filter(|&p| {
            let neigbours = grid.get_neigbours(p);
            neigbours.iter().all(|&n| grid.get(n) > grid.get(p))
        })
        .collect::<Vec<_>>()
}

fn parse_input(input: &str) -> Grid<u8> {
    let height = input.lines().count();
    let width = input.lines().next().expect("No lines in input").len();
    let mut grid = Grid::new(width, height, NeighbourConnection::Four, 0);

    let data: Vec<u8> = input.lines().fold(vec![], |mut acc, line| {
        acc.extend_from_slice(
            line.trim()
                .chars()
                .map(|c| c as u8 - 48)
                .collect::<Vec<_>>()
                .as_slice(),
        );
        acc
    });

    grid.set_data(data);
    grid
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let input = "2199943210
            3987894921
            9856789892
            8767896789
            9899965678";

        let grid = parse_input(input);
        assert_eq!(sum_low_risk(&grid), 15);
    }

    #[test]
    pub fn example_two() {
        let input = "2199943210
            3987894921
            9856789892
            8767896789
            9899965678";

        let grid = parse_input(input);
        let mut sizes = get_basin_sizes(&grid);
        sizes.sort_unstable();
        assert_eq!(sizes, vec![3, 9, 9, 14]);
    }
}
