use libaoc::{bits::*, DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Sixteen, |input| {
        let packet = parse_input(input);
        let (value, version, _) = parse_packet(&packet, 0);
        println!("Packet version sum: {version}");
        println!("Packet value: {value}");
    })
}

type Packet = Bits;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum PacketKind {
    Sum,
    Product,
    Min,
    Max,
    Greater,
    Less,
    Equal,
    Literal,
}

impl From<u32> for PacketKind {
    fn from(other: u32) -> PacketKind {
        match other {
            0 => PacketKind::Sum,
            1 => PacketKind::Product,
            2 => PacketKind::Min,
            3 => PacketKind::Max,
            4 => PacketKind::Literal,
            5 => PacketKind::Greater,
            6 => PacketKind::Less,
            7 => PacketKind::Equal,
            _ => unreachable!(),
        }
    }
}

fn parse_packet(packet: &[u8], mut offset: usize) -> (u64, u32, usize) {
    let mut version = packet[offset..offset + 3].to_u32();
    offset += 3;
    let kind = PacketKind::from(packet[offset..offset + 3].to_u32());
    offset += 3;
    if kind == PacketKind::Literal {
        let (number, offset) = parse_number(packet, offset);
        (number, version, offset)
    } else {
        let length_type_id = packet[offset];
        offset += 1;
        let (numbers, ver, offset) = match length_type_id {
            0 => parse_type_zero(packet, offset),
            1 => parse_type_one(packet, offset),
            _ => unreachable!(),
        };
        version += ver;
        match kind {
            PacketKind::Sum => (numbers.iter().sum::<u64>(), version, offset),
            PacketKind::Product => (numbers.iter().product::<u64>(), version, offset),
            PacketKind::Min => (numbers.into_iter().min().unwrap(), version, offset),
            PacketKind::Max => (numbers.into_iter().max().unwrap(), version, offset),
            PacketKind::Greater => {
                assert!(numbers.len() == 2);
                if numbers[0] > numbers[1] {
                    (1, version, offset)
                } else {
                    (0, version, offset)
                }
            }
            PacketKind::Less => {
                assert!(numbers.len() == 2);
                if numbers[0] < numbers[1] {
                    (1, version, offset)
                } else {
                    (0, version, offset)
                }
            }
            PacketKind::Equal => {
                assert!(numbers.len() == 2);
                if numbers[0] == numbers[1] {
                    (1, version, offset)
                } else {
                    (0, version, offset)
                }
            }
            PacketKind::Literal => unreachable!(),
        }
    }
}

fn parse_type_zero(packet: &[u8], mut offset: usize) -> (Vec<u64>, u32, usize) {
    let length = packet[offset..offset + 15].to_u32();
    offset += 15;
    let packet_end = offset + length as usize;
    let mut version = 0;
    let mut numbers = vec![];
    while offset < packet_end {
        let (num, ver, new_offset) = parse_packet(packet, offset);
        version += ver;
        offset = new_offset;
        numbers.push(num);
    }
    (numbers, version, offset)
}

fn parse_type_one(packet: &[u8], mut offset: usize) -> (Vec<u64>, u32, usize) {
    let packet_count = packet[offset..offset + 11].to_u32();
    offset += 11;
    let mut version = 0;
    let mut numbers = vec![];
    for _ in 0..packet_count {
        let (num, ver, new_offset) = parse_packet(packet, offset);
        version += ver;
        offset = new_offset;
        numbers.push(num);
    }
    (numbers, version, offset)
}

fn parse_number(packet: &[u8], mut offset: usize) -> (u64, usize) {
    let mut num: Packet = vec![];
    loop {
        let flag = packet[offset];
        offset += 1;

        num.extend_from_slice(&packet[offset..offset + 4]);
        offset += 4;

        if flag == 0 {
            break;
        }
    }

    (num.to_u64(), offset)
}

fn parse_input(input: &str) -> Packet {
    input
        .trim()
        .chars()
        .flat_map(|c| {
            let num = u32::from_str_radix(&format!("{c}"), 16).expect("Invalid char");
            num.to_bits(4)
        })
        .collect::<Vec<_>>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn parse_simple() {
        let input = "D2FE28";
        let packet = parse_input(input);
        let (_, version, offset) = parse_packet(&packet, 0);
        assert_eq!(version, 6);
        assert_eq!(offset, 21);
    }

    #[test]
    pub fn parse_type_zero() {
        let input = "38006F45291200";
        let packet = parse_input(input);
        let (_, version, offset) = parse_packet(&packet, 0);
        assert_eq!(version, 9);
        assert_eq!(offset, 49);
    }

    #[test]
    pub fn parse_type_one() {
        let input = "EE00D40C823060";
        let packet = parse_input(input);
        let (_, version, offset) = parse_packet(&packet, 0);
        assert_eq!(version, 14);
        assert_eq!(offset, 51);
    }

    #[test]
    pub fn parse_simple_sum() {
        let input = "C200B40A82";
        let packet = parse_input(input);
        let (number, _, _) = parse_packet(&packet, 0);
        assert_eq!(number, 3);
    }

    #[test]
    pub fn parse_prduct() {
        let input = "04005AC33890";
        let packet = parse_input(input);
        let (number, _, _) = parse_packet(&packet, 0);
        assert_eq!(number, 54);
    }

    #[test]
    pub fn parse_min() {
        let input = "880086C3E88112";
        let packet = parse_input(input);
        let (number, _, _) = parse_packet(&packet, 0);
        assert_eq!(number, 7);
    }

    #[test]
    pub fn parse_max() {
        let input = "CE00C43D881120";
        let packet = parse_input(input);
        let (number, _, _) = parse_packet(&packet, 0);
        assert_eq!(number, 9);
    }

    #[test]
    pub fn parse_less() {
        let input = "D8005AC2A8F0";
        let packet = parse_input(input);
        let (number, _, _) = parse_packet(&packet, 0);
        assert_eq!(number, 1);
    }

    #[test]
    pub fn parse_greater() {
        let input = "F600BC2D8F";
        let packet = parse_input(input);
        let (number, _, _) = parse_packet(&packet, 0);
        assert_eq!(number, 0);
    }

    #[test]
    pub fn parse_equal() {
        let input = "9C005AC2F8F0";
        let packet = parse_input(input);
        let (number, _, _) = parse_packet(&packet, 0);
        assert_eq!(number, 0);
    }

    #[test]
    pub fn parse_complex() {
        let input = "9C0141080250320F1802104A08";
        let packet = parse_input(input);
        let (number, _, _) = parse_packet(&packet, 0);
        assert_eq!(number, 1);
    }
}
