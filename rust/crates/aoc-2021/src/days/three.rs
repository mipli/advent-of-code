use std::str::FromStr;

use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Three, |input| {
        let report: Report = input.parse().expect("failed to parse input");

        let (gamma, epsilon) = report.get_gamma_epsilon();
        println!("gamma * epsilon: {}", gamma * epsilon);

        let co2 = report.get_co2_rating();
        let oxygen = report.get_oxygen_rating();
        println!("co2 * oxygen: {}", co2 * oxygen);
    })
}

#[derive(Debug)]
struct Report {
    lines: Vec<Vec<u32>>,
}

impl Report {
    fn get_gamma_epsilon(&self) -> (u32, u32) {
        let width = self.lines[0].len();
        let ones = self.lines.iter().fold(vec![0; width], |acc, line| {
            acc.iter().zip(line).map(|(a, l)| a + l).collect()
        });
        let (gamma_bits, epsilon_bits) = ones.iter().enumerate().fold(
            (vec![0; width], vec![0; width]),
            |(mut gam, mut eps), (i, &v)| {
                if v > (self.lines.len() / 2) as u32 {
                    eps[i] = 0;
                    gam[i] = 1;
                } else {
                    gam[i] = 0;
                    eps[i] = 1;
                }
                (gam, eps)
            },
        );

        (gamma_bits.to_u32(), epsilon_bits.to_u32())
    }

    fn get_oxygen_rating(&self) -> u32 {
        let mut remaining = self.lines.clone();
        let mut index = 0;

        while remaining.len() > 1 {
            let most_common = self.most_common_by_index(index, &remaining);
            remaining.retain(|rem| rem[index] == most_common);
            index += 1;
        }

        remaining
            .pop()
            .expect("Should have one value left")
            .to_u32()
    }

    fn get_co2_rating(&self) -> u32 {
        let mut remaining = self.lines.clone();
        let mut index = 0;

        while remaining.len() > 1 {
            let most_common = self.most_common_by_index(index, &remaining);
            remaining.retain(|rem| rem[index] != most_common);
            index += 1;
        }

        remaining
            .pop()
            .expect("Should have one value left")
            .to_u32()
    }

    fn most_common_by_index(&self, index: usize, lines: &[Vec<u32>]) -> u32 {
        let ones = lines.iter().fold(0, |acc, line| acc + line[index]) as usize;
        u32::from((lines.len() - ones) <= ones)
    }
}

impl FromStr for Report {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lines = s
            .lines()
            .map(|line| {
                line.trim()
                    .chars()
                    .map(|c| match c {
                        '0' => 0,
                        '1' => 1,
                        _ => panic!("Invalid input"),
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        Ok(Report { lines })
    }
}

pub trait ToInt {
    fn to_u32(&self) -> u32;
}

impl ToInt for Vec<u32> {
    fn to_u32(&self) -> u32 {
        self.iter().fold(0, |acc, &b| (acc << 1) + b)
    }
}

pub trait FromInt {
    fn to_bits(&self) -> Vec<u32>;
}

impl FromInt for u32 {
    fn to_bits(&self) -> Vec<u32> {
        let mut bits = vec![];
        let mut rem = *self;
        while rem > 0 {
            bits.push(rem % 2);
            rem /= 2;
        }
        while bits.len() < 36 {
            bits.push(0);
        }
        bits.into_iter().rev().collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn calculate_gamma_epsilon() {
        let report: Report = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"
            .parse()
            .expect("failed to parse input");

        let (gamma, epsilon) = report.get_gamma_epsilon();
        assert_eq!(gamma, 22);
        assert_eq!(epsilon, 9);
    }

    #[test]
    pub fn calculate_oxygen_rating() {
        let report: Report = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"
            .parse()
            .expect("failed to parse input");

        let rating = report.get_oxygen_rating();
        assert_eq!(rating, 23);
    }

    #[test]
    pub fn calculate_co2_rating() {
        let report: Report = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"
            .parse()
            .expect("failed to parse input");

        let rating = report.get_co2_rating();
        assert_eq!(rating, 10);
    }
}
