use std::str::FromStr;

use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Four, |input| {
        let (numbers, mut boards) = parse_input(input);

        let results = run_bingo(&numbers, &mut boards);
        let first = results
            .iter()
            .min_by_key(|res| res.turn)
            .expect("Failed to find first successful bingo board");
        let last = results
            .iter()
            .max_by_key(|res| res.turn)
            .expect("Failed to find last successful bingo board");

        println!(
            "First value: {}",
            first.last_num * first.board.sum_unmarked()
        );
        println!("Last value: {}", last.last_num * last.board.sum_unmarked());
    })
}

struct BingoResult {
    board: BingoBoard,
    turn: u32,
    last_num: u32,
}

fn run_bingo(numbers: &[u32], boards: &mut [BingoBoard]) -> Vec<BingoResult> {
    let mut results = vec![];
    for (turn, num) in numbers.iter().enumerate() {
        for board in &mut boards.iter_mut().filter(|b| !b.has_bingo()) {
            board.add_number(*num);
            if board.has_bingo() {
                results.push(BingoResult {
                    board: board.clone(),
                    last_num: *num,
                    turn: (turn as u32) + 1,
                });
            }
        }
    }
    results
}

fn parse_input(input: &str) -> (Vec<u32>, Vec<BingoBoard>) {
    let mut numbers = vec![];

    let mut lines = input.lines();
    if let Some(first_line) = lines.next() {
        numbers = first_line
            .split(',')
            .map(|n| n.parse::<u32>().expect("Failed to parse numbers"))
            .collect::<Vec<_>>();
    }
    let boards = lines
        .fold(vec![], |mut board_input, line| {
            if line.is_empty() {
                board_input.push(String::new());
            } else if let Some(board) = board_input.last_mut() {
                board.push_str(line);
                board.push('\n');
            }
            board_input
        })
        .iter()
        .map(|boards| {
            boards
                .parse::<BingoBoard>()
                .expect("Failed to parse bingo board")
        })
        .collect::<Vec<_>>();

    (numbers, boards)
}

#[derive(Debug, Clone)]
struct BingoBoard {
    board: Vec<u32>,
    width: usize,
    selected: Vec<bool>,
}

impl BingoBoard {
    fn add_number(&mut self, number: u32) {
        if let Some(pos) = self.board.iter().position(|&b| b == number) {
            self.selected[pos] = true;
        }
    }

    fn has_bingo(&self) -> bool {
        for row_start in (0..self.board.len()).step_by(self.width) {
            if self
                .selected
                .iter()
                .skip(row_start)
                .take(self.width)
                .filter(|&&s| s)
                .count()
                == self.width
            {
                return true;
            }
        }
        for col_start in 0..self.width {
            if self
                .selected
                .iter()
                .skip(col_start)
                .step_by(self.width)
                .filter(|&&s| s)
                .count()
                == self.width
            {
                return true;
            }
        }
        false
    }

    fn sum_unmarked(&self) -> u32 {
        self.board
            .iter()
            .zip(self.selected.iter())
            .filter_map(|(b, &s)| if s { None } else { Some(b) })
            .sum()
    }
}

impl FromStr for BingoBoard {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let board = s.trim().lines().fold(vec![], |mut board, line| {
            line.split(' ').filter(|n| !n.is_empty()).for_each(|num| {
                board.push(num.trim().parse::<u32>().expect("Failed to parse number"));
            });
            board
        });

        let selected = vec![false; board.len()];

        Ok(BingoBoard {
            width: 5,
            board,
            selected,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_row_bingo() {
        let (numbers, mut boards) = parse_input(
            "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

            22 13 17 11  0
            8  2 23  4 24
            21  9 14 16  7
            6 10  3 18  5
            1 12 20 15 19

            3 15  0  2 22
            9 18 13 17  5
            19  8  7 25 23
            20 11 10 24  4
            14 21 16 12  6

            14 21 17 24  4
            10 16 15  9 19
            18  8 23 26 20
            22 11 13  6  5
            2  0 12  3  7",
        );
        let mut has_bingo = false;
        for num in numbers {
            for board in &mut boards {
                board.add_number(num);
                if board.has_bingo() {
                    has_bingo = true;
                }
            }
            if has_bingo {
                break;
            }
        }
        assert!(!boards[0].has_bingo());
        assert!(!boards[1].has_bingo());
        assert!(boards[2].has_bingo());
    }

    #[test]
    pub fn test_column_bingo() {
        let (numbers, mut boards) = parse_input(
            "7,4,9,5,11,17,23,10,0,14,21,24,2,16,13,6,15,25,12,22,18,20,8,19,3,26,1

            22 13 17 11  0
            8  2 23  4 24
            21  9 14 16  7
            6 10  3 18  5
            1 12 20 15 19

            3 15  4  2 22
            13 18  9 16  5
            19  8  7 25 23
            20 11 10 24  4
            14 21 17 12  6

            14 21 17 24  4
            10 16 15  9 19
            18  8 23 26 20
            22 11 13  6  5
            2  0 12  3  7",
        );
        let mut has_bingo = false;
        for num in numbers {
            for board in &mut boards {
                board.add_number(num);
                if board.has_bingo() {
                    has_bingo = true;
                }
            }
            if has_bingo {
                break;
            }
        }
        assert!(!boards[0].has_bingo());
        assert!(boards[1].has_bingo());
        assert!(!boards[2].has_bingo());
    }

    #[test]
    pub fn test_bingo_sum() {
        let (numbers, mut boards) = parse_input(
            "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

            22 13 17 11  0
            8  2 23  4 24
            21  9 14 16  7
            6 10  3 18  5
            1 12 20 15 19

            3 15  0  2 22
            9 18 13 17  5
            19  8  7 25 23
            20 11 10 24  4
            14 21 16 12  6

            14 21 17 24  4
            10 16 15  9 19
            18  8 23 26 20
            22 11 13  6  5
            2  0 12  3  7",
        );
        let mut has_bingo = false;
        for num in numbers {
            for board in &mut boards {
                board.add_number(num);
                if board.has_bingo() {
                    has_bingo = true;
                }
            }
            if has_bingo {
                break;
            }
        }
        assert!(!boards[0].has_bingo());
        assert!(!boards[1].has_bingo());
        assert!(boards[2].has_bingo());

        assert_eq!(boards[2].sum_unmarked(), 188);
    }
}
