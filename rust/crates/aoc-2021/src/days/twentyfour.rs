use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::TwentyFour, |input| {
        let blocks = parse_input(input);
        println!(
            "Largest type: {:?}",
            find_model_number_by_type(&blocks, Criteria::Largest).unwrap()
        );
        println!(
            "Smallest type: {:?}",
            find_model_number_by_type(&blocks, Criteria::Smallest).unwrap()
        );
    })
}

const BLOCK_LEN: usize = 18;
const DIV_Z_OFFSET: usize = 4;
const ADD_X_OFFSET: usize = 5;
const ADD_Y_OFFSET: usize = 15;

#[derive(Debug)]
struct Block {
    div_z: i64,
    add_x: i64,
    add_y: i64,
}

fn parse_input(input: &str) -> Vec<Block> {
    let mut blocks = vec![];
    let instructions = input.lines().collect::<Vec<_>>();

    for i in 0..14 {
        blocks.push(Block {
            div_z: instructions[i * BLOCK_LEN + DIV_Z_OFFSET]
                .split_whitespace()
                .last()
                .unwrap()
                .parse()
                .unwrap(),
            add_x: instructions[i * BLOCK_LEN + ADD_X_OFFSET]
                .split_whitespace()
                .last()
                .unwrap()
                .parse()
                .unwrap(),
            add_y: instructions[i * BLOCK_LEN + ADD_Y_OFFSET]
                .split_whitespace()
                .last()
                .unwrap()
                .parse()
                .unwrap(),
        })
    }

    blocks
}

struct Node {
    index: usize,
    z_input: i64,
    total_input: i64,
}

enum Criteria {
    Largest,
    Smallest,
}

fn find_model_number_by_type(blocks: &[Block], model_number_type: Criteria) -> Option<i64> {
    let mut nodes = vec![Node {
        index: 0,
        z_input: 0,
        total_input: 0,
    }];

    while let Some(node) = nodes.pop() {
        let block = match blocks.get(node.index) {
            Some(block) => block,
            None => {
                if node.z_input == 0 {
                    return Some(node.total_input);
                } else {
                    continue;
                }
            }
        };

        for w in match model_number_type {
            Criteria::Largest => (1..=9).collect::<Vec<_>>(),
            Criteria::Smallest => (1..=9).rev().collect::<Vec<_>>(),
        } {
            if block.div_z == 26 && node.z_input % 26 + block.add_x != w {
                continue;
            }

            let z_output = if node.z_input % 26 + block.add_x == w {
                node.z_input / block.div_z
            } else {
                node.z_input / block.div_z * 26 + w + block.add_y
            };

            nodes.push(Node {
                index: node.index + 1,
                z_input: z_output,
                total_input: node.total_input * 10 + w,
            })
        }
    }

    None
}
