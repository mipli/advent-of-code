use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Six, |input| {
        let input: Vec<u32> = input
            .split(',')
            .map(|n| n.trim().parse().expect("Failed to parse fish number"))
            .collect::<Vec<_>>();

        println!("Fish count: {}", get_fish_count(&input, 80));
        println!("Fish count: {}", get_fish_count(&input, 256));
    })
}

fn get_fish_count(fishes: &[u32], days: u32) -> usize {
    let mut fish_counts: [usize; 9] = [0; 9];

    fishes.iter().for_each(|&fish| {
        fish_counts[fish as usize] += 1;
    });

    for _ in 1..=days {
        let mut next_generation: [usize; 9] = [0; 9];

        fish_counts
            .iter()
            .enumerate()
            .filter(|(_, &count)| count > 0)
            .for_each(|(timer, &count)| {
                if timer == 0 {
                    next_generation[8] += count;
                    next_generation[6] += count;
                } else {
                    next_generation[timer - 1] += count;
                }
            });

        std::mem::swap(&mut fish_counts, &mut next_generation);
    }
    fish_counts.iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let input: Vec<u32> = "3,4,3,1,2"
            .split(',')
            .map(|n| n.parse().expect("Failed to parse fish number"))
            .collect::<Vec<_>>();

        assert_eq!(get_fish_count(&input, 80), 5934);
    }

    #[test]
    pub fn example_two() {
        let input: Vec<u32> = "3,4,3,1,2"
            .split(',')
            .map(|n| n.parse().expect("Failed to parse fish number"))
            .collect::<Vec<_>>();

        assert_eq!(get_fish_count(&input, 256), 26984457539);
    }
}
