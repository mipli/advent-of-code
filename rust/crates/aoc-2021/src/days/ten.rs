use std::str::FromStr;

use anyhow::bail;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Ten, |input| {
        let lines: Vec<Line> = input
            .lines()
            .map(|line| line.parse().expect("Invalid input"))
            .collect();
        let statuses = lines.iter().map(|line| line.validate()).collect::<Vec<_>>();
        println!("Corruption score: {}", corrupt_line_score(&statuses));
        println!("Fix score: {}", get_statuses_fix_score(&statuses));
    })
}

fn get_statuses_fix_score(statuses: &[LineStatus]) -> u64 {
    let mut scores = statuses
        .iter()
        .filter_map(|s| match s {
            LineStatus::Invalid(stack) => Some(stack.clone()),
            _ => None,
        })
        .map(score_line_fixing)
        .collect::<Vec<_>>();

    scores.sort_unstable();

    scores[scores.len() / 2]
}

fn score_line_fixing(mut stack: Vec<Token>) -> u64 {
    let mut score = 0;

    while let Some(token) = stack.pop() {
        let points = match token {
            Token::ParenLeft => 1,
            Token::BracketLeft => 2,
            Token::CurlyLeft => 3,
            Token::AngleLeft => 4,
            _ => 0,
        };
        score = (score * 5) + points;
    }

    score
}

fn corrupt_line_score(statuses: &[LineStatus]) -> i32 {
    statuses
        .iter()
        .map(|status| match status {
            LineStatus::Corrupt(token) => match *token {
                Token::ParenRight => 3,
                Token::BracketRight => 57,
                Token::CurlyRight => 1197,
                Token::AngleRight => 25137,
                _ => 0,
            },
            _ => 0,
        })
        .sum()
}
#[derive(Debug, Clone, Eq, PartialEq)]
enum LineStatus {
    Corrupt(Token),
    Invalid(Vec<Token>),
    Valid,
}

#[derive(Debug)]
struct Line {
    tokens: Vec<Token>,
}

impl Line {
    fn validate(&self) -> LineStatus {
        let remainder =
            self.tokens
                .iter()
                .try_fold(vec![], |mut stack, token| -> Result<_, LineStatus> {
                    if token.is_opening() {
                        stack.push(token);
                        Ok(stack)
                    } else if let Some(popped) = stack.pop() {
                        if !popped.is_paired(token) {
                            return Err(LineStatus::Corrupt(*token));
                        }
                        Ok(stack)
                    } else {
                        unreachable!();
                    }
                });
        match remainder {
            Ok(rem) if rem.is_empty() => LineStatus::Valid,
            Ok(rem) => LineStatus::Invalid(rem.into_iter().copied().collect::<Vec<_>>()),
            Err(err) => err,
        }
    }
}

impl FromStr for Line {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let tokens =
            s.trim()
                .chars()
                .try_fold(vec![], |mut acc, c| -> Result<_, anyhow::Error> {
                    acc.push(Token::from_char(c)?);
                    Ok(acc)
                })?;
        Ok(Line { tokens })
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Token {
    ParenLeft,
    ParenRight,
    BracketLeft,
    BracketRight,
    AngleLeft,
    AngleRight,
    CurlyLeft,
    CurlyRight,
}

impl Token {
    fn is_paired(&self, other: &Token) -> bool {
        use Token::*;
        matches!(
            (self, other),
            (ParenLeft, ParenRight)
                | (BracketLeft, BracketRight)
                | (AngleLeft, AngleRight)
                | (CurlyLeft, CurlyRight)
        )
    }

    fn is_opening(&self) -> bool {
        !self.is_closing()
    }

    fn is_closing(&self) -> bool {
        use Token::*;
        matches!(self, ParenRight | BracketRight | AngleRight | CurlyRight)
    }

    fn from_char(c: char) -> Result<Token, anyhow::Error> {
        use Token::*;

        Ok(match c {
            '(' => ParenLeft,
            ')' => ParenRight,
            '[' => BracketLeft,
            ']' => BracketRight,
            '<' => AngleLeft,
            '>' => AngleRight,
            '{' => CurlyLeft,
            '}' => CurlyRight,
            _ => bail!("Invalid token"),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn example_one() {
        let input = "[({(<(())[]>[[{[]{<()<>>
            [(()[<>])]({[<{<<[]>>(
            {([(<{}[<>[]}>{[]{[(<()>
            (((({<>}<{<{<>}{[]{[]{}
            [[<[([]))<([[{}[[()]]]
            [{[{({}]{}}([{[{{{}}([]
            {<[[]]>}<{[{[{[]{()[[[]
            [<(<(<(<{}))><([]([]()
            <{([([[(<>()){}]>(<<{{
            <{([{{}}[<[[[<>{}]]]>[]]";
        let lines: Vec<Line> = input
            .lines()
            .map(|line| line.parse().expect("Invalid input"))
            .collect();
        let statuses = lines.iter().map(|line| line.validate()).collect::<Vec<_>>();
        assert_eq!(corrupt_line_score(&statuses), 26397);
    }

    #[test]
    pub fn fixing_line_score() {
        let line: Line = "<{([{{}}[<[[[<>{}]]]>[]]".parse().expect("invalid input");
        match line.validate() {
            LineStatus::Invalid(stack) => assert_eq!(score_line_fixing(stack), 294),
            _ => panic!(),
        }
    }

    #[test]
    pub fn example_two() {
        let input = "[({(<(())[]>[[{[]{<()<>>
            [(()[<>])]({[<{<<[]>>(
            {([(<{}[<>[]}>{[]{[(<()>
            (((({<>}<{<{<>}{[]{[]{}
            [[<[([]))<([[{}[[()]]]
            [{[{({}]{}}([{[{{{}}([]
            {<[[]]>}<{[{[{[]{()[[[]
            [<(<(<(<{}))><([]([]()
            <{([([[(<>()){}]>(<<{{
            <{([{{}}[<[[[<>{}]]]>[]]";
        let lines: Vec<Line> = input
            .lines()
            .map(|line| line.parse().expect("Invalid input"))
            .collect();
        let statuses = lines.iter().map(|line| line.validate()).collect::<Vec<_>>();
        assert_eq!(get_statuses_fix_score(&statuses), 288957);
    }
}
