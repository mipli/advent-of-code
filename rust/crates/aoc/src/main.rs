use anyhow::Result;
use clap::Parser;
use dotenv::dotenv;
use libaoc::{Aoc, AocRunner};

#[derive(Parser)]
struct Args {
    #[arg(short, long, default_value = "2024")]
    year: u32,
    #[arg(short, long)]
    day: Option<u8>,
}

fn main() -> Result<()> {
    dotenv().ok();

    let args = Args::parse();

    let aoc: Box<dyn AocRunner> = match args.year {
        2020 => {
            let mut aoc = Aoc::<2020>::default();
            aoc_2020::days::register(&mut aoc);
            Box::new(aoc)
        }
        2021 => {
            let mut aoc = Aoc::<2021>::default();
            aoc_2021::days::register(&mut aoc);
            Box::new(aoc)
        }
        2022 => {
            let mut aoc = Aoc::<2022>::default();
            aoc_2022::days::register(&mut aoc);
            Box::new(aoc)
        }
        2024 => {
            let mut aoc = Aoc::<2024>::default();
            aoc_2024::days::register(&mut aoc);
            Box::new(aoc)
        }
        _ => panic!("invalid year"),
    };

    match args.day {
        Some(day) => aoc.run(day.into())?,
        None => aoc.run_newest()?,
    }

    Ok(())
}
