use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Nine, |input| {
        let numbers: Vec<u64> = input
            .lines()
            .map(|line| line.parse().expect("Failed to parse number"))
            .collect::<Vec<_>>();
        let weakness = find_weak_number(&numbers, 25).expect("No weakness found");
        println!("Weak number: {}", weakness);
        let seq = find_number_sequence(&numbers, weakness);

        let min = seq.iter().min().expect("Sequence min not found");
        let max = seq.iter().max().expect("Sequence max not found");
        println!("Sequence value: {}", min + max);
    })
}

pub fn find_weak_number(numbers: &[u64], preamble: usize) -> Option<u64> {
    for slice in numbers.windows(preamble + 1) {
        let target = slice[preamble];
        let mut found = false;
        for i in 0..preamble {
            let a = slice[i];
            for val in slice.iter().skip(i).take(preamble) {
                if a + val == target {
                    found = true;
                }
            }
        }
        if !found {
            return Some(target);
        }
    }
    None
}

pub fn find_number_sequence(numbers: &[u64], target: u64) -> Vec<u64> {
    for i in 0..numbers.len() {
        let mut res = vec![];
        let mut acc = 0;
        for num in numbers.iter().skip(i) {
            acc += num;

            if acc <= target {
                res.push(*num);
            }

            match acc.cmp(&target) {
                std::cmp::Ordering::Equal => return res,
                std::cmp::Ordering::Greater => break,
                _ => {}
            }
        }
    }
    vec![]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_finding_weakness() {
        let numbers = [
            35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
            576,
        ];

        assert_eq!(find_weak_number(&numbers, 5), Some(127));
    }

    #[test]
    pub fn test_finding_sequence() {
        let numbers = [
            35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
            576,
        ];

        assert_eq!(find_number_sequence(&numbers, 127), vec![15, 25, 47, 40]);
    }
}
