use libaoc::{DayNumber, Solver};
use std::collections::HashMap;
use std::str::FromStr;

pub fn get() -> Solver {
    Solver::new(DayNumber::Six, |input| {
        let passengers: Passengers = input.parse().expect("Failed to parse input");
        println!("Yesses: {}", passengers.count_yesses());
        println!("All Yesses: {}", passengers.count_all_yesses());
    })
}

#[derive(Debug)]
struct Group {
    count: usize,
    yesses: HashMap<char, usize>,
}

impl Default for Group {
    fn default() -> Self {
        Group {
            count: 0,
            yesses: HashMap::default(),
        }
    }
}

#[derive(Debug)]
struct Passengers {
    groups: Vec<Group>,
}

impl Passengers {
    fn count_yesses(&self) -> usize {
        self.groups.iter().map(|set| set.yesses.len()).sum()
    }

    fn count_all_yesses(&self) -> usize {
        self.groups
            .iter()
            .map(|set| set.yesses.values().filter(|&&c| c == set.count).count())
            .sum()
    }
}

impl FromStr for Passengers {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (mut groups, set) =
            s.lines()
                .fold((vec![], Group::default()), |(mut groups, mut set), line| {
                    if line.is_empty() {
                        groups.push(set);
                        (groups, Group::default())
                    } else {
                        set.count += 1;
                        for c in line.chars() {
                            *set.yesses.entry(c).or_insert(0) += 1;
                        }
                        (groups, set)
                    }
                });
        groups.push(set);
        Ok(Passengers { groups })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_example_1() {
        let passengers: Passengers = "abc

a
b
c

ab
ac

a
a
a
a

b"
        .parse()
        .expect("Failed to parse input");
        assert_eq!(passengers.count_yesses(), 11);
    }

    #[test]
    pub fn test_example_2() {
        let passengers: Passengers = "abc

a
b
c

ab
ac

a
a
a
a

b"
        .parse()
        .expect("Failed to parse input");
        assert_eq!(passengers.count_all_yesses(), 6);
    }
}
