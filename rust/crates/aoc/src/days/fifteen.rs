use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Fifteen, |input| {
        let initial: Vec<u32> = input
            .trim()
            .split(',')
            .map(|line| line.parse().expect("Failed to parse input"))
            .collect();
        println!("Last number spoken: {}", speak_numbers(&initial, 2020));

        println!(
            "Last number spoken: {}",
            speak_numbers(&initial, 30_000_000)
        );
    })
}

pub fn speak_numbers(initial: &[u32], max_turn: u32) -> u32 {
    let mut spoken: Vec<(Option<u32>, Option<u32>)> = vec![];
    spoken.resize(1_000, (None, None));

    initial.iter().enumerate().for_each(|(idx, &val)| {
        spoken[val as usize] = (Some(idx as u32 + 1), None);
    });

    let mut turn = initial.len() as u32;
    let mut spoken_word = initial[initial.len() - 1];
    while turn < max_turn {
        turn += 1;
        spoken_word = match spoken[spoken_word as usize] {
            (Some(a), Some(b)) => b - a,
            _ => 0,
        };
        if spoken.len() < spoken_word as usize {
            spoken.resize((spoken_word * 2) as usize, (None, None));
        }
        match spoken[spoken_word as usize] {
            (None, None) => spoken[spoken_word as usize] = (Some(turn), None),
            (Some(a), None) => spoken[spoken_word as usize] = (Some(a), Some(turn)),
            (Some(_), Some(b)) => spoken[spoken_word as usize] = (Some(b), Some(turn)),
            _ => unreachable!(),
        }
    }
    spoken_word
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_example_1() {
        let spoken = speak_numbers(vec![0, 3, 6].as_slice(), 2020);
        assert_eq!(spoken, 436);
    }
}
