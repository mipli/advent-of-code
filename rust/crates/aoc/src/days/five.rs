use itertools::Itertools;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Five, |input| {
        let mut seat_ids = input
            .lines()
            .map(|line| BoardingPass(line.to_string()).seat_id())
            .collect::<Vec<_>>();

        let max_seat_id = seat_ids.iter().max().unwrap();
        println!("Max seat id: {}", max_seat_id);

        seat_ids.sort_unstable(); // unstable is faster, and still safe for i32

        let (n, _) = seat_ids
            .iter()
            .tuple_windows()
            .find(|(&a, &b)| b - a == 2)
            .expect("Could not find matching seat");
        println!("Your seat: {}", n + 1);
    })
}

pub struct BoardingPass(String);

impl BoardingPass {
    pub fn seat_id(&self) -> i32 {
        let row = self.get_row();
        let seat = self.get_seat();
        (row * 8) + seat
    }

    fn get_row(&self) -> i32 {
        let mut row_max = 127;
        let mut row_min = 0;
        for code in self.0.chars().take(6) {
            let mid = (row_max - row_min) / 2;
            match code {
                'F' => row_max = row_min + mid,
                'B' => row_min = row_max - mid,
                c => panic!("Invalid row designator: {}", c),
            }
        }
        match self.0.chars().nth(6) {
            Some('F') => row_min,
            Some('B') => row_max,
            c => panic!("Invalid last row designator: {:?}", c),
        }
    }

    fn get_seat(&self) -> i32 {
        let mut row_max = 7;
        let mut row_min = 0;
        for code in self.0.chars().skip(7).take(2) {
            let mid = (row_max - row_min) / 2;
            match code {
                'L' => row_max = row_min + mid,
                'R' => row_min = row_max - mid,
                c => panic!("Invalid seat designator: {}", c),
            }
        }
        match self.0.chars().skip(7).nth(2) {
            Some('L') => row_min,
            Some('R') => row_max,
            c => panic!("Invalid last seat designator: {:?}", c),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_parsing() {
        assert_eq!(BoardingPass("FBFBBFFRLR".to_string()).seat_id(), 357);
        assert_eq!(BoardingPass("FFFBBBFRRR".to_string()).seat_id(), 119);
        assert_eq!(BoardingPass("BBFFBBFRLL".to_string()).seat_id(), 820);
        assert_eq!(BoardingPass("BFFFBBFRRR".to_string()).seat_id(), 567);
    }
}
