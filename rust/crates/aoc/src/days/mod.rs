use libaoc::Aoc;

mod eight;
mod eleven;
mod fifteen;
mod five;
mod four;
mod fourteen;
mod nine;
mod one;
mod seven;
mod six;
mod sixteen;
mod ten;
mod thirteen;
mod three;
mod twelve;
mod two;

pub fn register(aoc: &mut Aoc) {
    aoc.add(one::get());
    aoc.add(two::get());
    aoc.add(three::get());
    aoc.add(four::get());
    aoc.add(five::get());
    aoc.add(six::get());
    aoc.add(seven::get());
    aoc.add(eight::get());
    aoc.add(nine::get());
    aoc.add(ten::get());
    aoc.add(eleven::get());
    aoc.add(twelve::get());
    aoc.add(thirteen::get());
    aoc.add(fourteen::get());
    aoc.add(fifteen::get());
    aoc.add(sixteen::get());
}
