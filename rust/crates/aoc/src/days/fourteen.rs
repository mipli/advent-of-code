use anyhow::bail;
use libaoc::{DayNumber, Solver};
use nom::{
    branch::alt,
    bytes::complete::{tag, take_while},
    character::complete::{self, alpha1, digit1, one_of, space0},
    character::is_alphanumeric,
    combinator::{opt, value},
    multi::many1,
    Finish, IResult,
};
use std::collections::HashMap;
use std::str::FromStr;

pub fn get() -> Solver {
    Solver::new(DayNumber::Fourteen, |input| {
        let inputs: Vec<Input> = input
            .lines()
            .map(|line| line.parse().expect("Failed to parse mask"))
            .collect::<Vec<_>>();

        let mut machine = Machine::default();

        machine.parse_input_basic(&inputs);
        println!("Memory sum: {}", machine.sum_memory());

        let mut machine = Machine::default();

        machine.parse_input_advanced(&inputs);
        println!("Modified Memory sum: {}", machine.sum_memory());
    })
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Machine {
    mask: Mask,
    memory: HashMap<u64, u64>,
}

impl Machine {
    pub fn sum_memory(&self) -> u64 {
        self.memory.values().sum()
    }

    pub fn parse_input_basic(&mut self, inputs: &[Input]) {
        for input in inputs {
            match input {
                Input::Mask(mask) => self.mask = mask.clone(),
                Input::MemOp(mem_op) => {
                    self.memory.insert(
                        mem_op.addr,
                        apply_mask(&self.mask, mem_op.value.to_bits().as_slice()).to_u64(),
                    );
                }
            }
        }
    }

    pub fn parse_input_advanced(&mut self, inputs: &[Input]) {
        for input in inputs {
            match input {
                Input::Mask(mask) => self.mask = mask.clone(),
                Input::MemOp(mem_op) => {
                    let addrs = mask_memory_addr(&self.mask, mem_op.addr.to_bits().as_slice());
                    for addr in addrs {
                        self.memory.insert(addr.to_u64(), mem_op.value);
                    }
                }
            }
        }
    }
}

impl Default for Machine {
    fn default() -> Self {
        Machine {
            mask: Mask(vec![]),
            memory: HashMap::default(),
        }
    }
}

pub fn apply_mask(mask: &Mask, num: &[u8]) -> Vec<u8> {
    let mut new_number = num.to_vec();

    mask.0.iter().enumerate().for_each(|(i, v)| match v {
        MaskValue::Null => {}
        MaskValue::Zero => new_number[i] = 0,
        MaskValue::One => new_number[i] = 1,
    });

    new_number
}

pub fn mask_memory_addr(mask: &Mask, num: &[u8]) -> Vec<Vec<u8>> {
    let mut addrs = vec![num.to_vec()];

    mask.0.iter().enumerate().for_each(|(i, v)| match v {
        MaskValue::Null => {
            let mut new_addrs = vec![];
            for addr in &mut addrs {
                let mut new_addr = addr.clone();
                addr[i] = 0;
                new_addr[i] = 1;
                new_addrs.push(new_addr);
            }
            addrs.append(&mut new_addrs);
        }
        MaskValue::Zero => {}
        MaskValue::One => {
            for addr in &mut addrs {
                addr[i] = 1;
            }
        }
    });

    addrs
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Input {
    Mask(Mask),
    MemOp(MemOp),
}

impl FromStr for Input {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match parse_input(s.trim()).finish() {
            Ok((_rem, inp)) => Ok(inp),
            Err(nom::error::Error { input, code }) => Err(anyhow::Error::new(nom::error::Error {
                code,
                input: input.to_string(),
            })),
        }
    }
}

fn parse_input(input: &str) -> IResult<&str, Input> {
    let (input, mask) = opt(parse_mask)(input)?;
    let (input, mem_op) = opt(parse_mem_op)(input)?;
    match (mask, mem_op) {
        (Some(mask), _) => Ok((input, Input::Mask(mask))),
        (_, Some(mem_op)) => Ok((input, Input::MemOp(mem_op))),
        _ => panic!("invalid input"),
    }
}

#[derive(Clone, Eq, PartialEq, Copy, Debug)]
pub enum MaskValue {
    One,
    Zero,
    Null,
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Mask(Vec<MaskValue>);

fn parse_mask(input: &str) -> IResult<&str, Mask> {
    let (input, _) = tag("mask = ")(input)?;
    let (input, mask_value) = many1(parse_mask_value)(input)?;
    Ok((input, Mask(mask_value)))
}
fn parse_mask_value(input: &str) -> IResult<&str, MaskValue> {
    alt((
        value(MaskValue::One, tag("1")),
        value(MaskValue::Zero, tag("0")),
        value(MaskValue::Null, tag("X")),
    ))(input)
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct MemOp {
    addr: u64,
    value: u64,
}

fn parse_mem_op(input: &str) -> IResult<&str, MemOp> {
    let (input, _) = tag("mem[")(input)?;
    let (input, addr) = complete::u64(input)?;
    let (input, _) = tag("] = ")(input)?;
    let (input, value) = complete::u64(input)?;
    Ok((input, MemOp { addr, value }))
}

pub trait ToInt {
    fn to_u64(&self) -> u64;
}

impl ToInt for Vec<u8> {
    fn to_u64(&self) -> u64 {
        self.iter().fold(0, |acc, &b| (acc << 1) + b as u64)
    }
}

pub trait FromInt {
    fn to_bits(&self) -> Vec<u8>;
}

impl FromInt for u64 {
    fn to_bits(&self) -> Vec<u8> {
        let mut bits = vec![];
        let mut rem = *self;
        while rem > 0 {
            bits.push((rem % 2) as u8);
            rem = rem / 2;
        }
        while bits.len() < 36 {
            bits.push(0);
        }
        bits.into_iter().rev().collect()
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_mask_application() {
        let mask: Input = "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"
            .parse()
            .expect("Failed to parse mask");

        let num: Input = "mem[9] = 11".parse().expect("Failed to parse mask");

        match (mask, num) {
            (Input::Mask(mask), Input::MemOp(mem_op)) => {
                let new_num = apply_mask(&mask, &mem_op.value.to_bits());

                assert_eq!(new_num.to_u64(), 73u64);
            }
            _ => assert!(false),
        }
    }

    #[test]
    pub fn test_input_handling() {
        let inputs: Vec<Input> = "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0"
            .lines()
            .map(|line| line.parse().expect("Failed to parse mask"))
            .collect::<Vec<_>>();

        let mut machine = Machine::default();

        machine.parse_input_basic(&inputs);
        assert_eq!(machine.sum_memory(), 165);
    }

    #[test]
    pub fn test_adv_input_handling() {
        let inputs: Vec<Input> = "mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1"
            .lines()
            .map(|line| line.parse().expect("Failed to parse mask"))
            .collect::<Vec<_>>();

        let mut machine = Machine::default();

        machine.parse_input_advanced(&inputs);
        dbg!(&machine);
        assert_eq!(machine.sum_memory(), 208);
    }
}
