use anyhow::anyhow;
use libaoc::{DayNumber, Solver};
use std::str::FromStr;

pub fn get() -> Solver {
    Solver::new(DayNumber::Eleven, |input| {
        let mut layout: Layout = input.parse().expect("Input invalid");
        layout.stabilize_simple();
        println!("Occupied seats: {}", layout.count_occupied());
        let mut layout: Layout = input.parse().expect("Input invalid");
        layout.stabilize_advanced();
        println!("Occupied seats by visibility: {}", layout.count_occupied());
    })
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Tile {
    Floor,
    Seat,
    Occupied,
}

impl FromStr for Tile {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "." => Ok(Tile::Floor),
            "L" => Ok(Tile::Seat),
            "#" => Ok(Tile::Occupied),
            _ => Err(anyhow!("Invalid tile value")),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Layout {
    seats: Vec<Tile>,
    width: i32,
    height: i32,
}

impl FromStr for Layout {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut seats = vec![];
        let mut width = 0;
        let mut height = 0;
        for line in s.lines() {
            height += 1;
            width = line.len();
            for c in line.chars() {
                seats.push(c.to_string().parse()?);
            }
        }
        Ok(Layout {
            seats,
            width: width as i32,
            height: height as i32,
        })
    }
}

impl std::fmt::Display for Layout {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                match self.get_tile(x, y) {
                    Tile::Seat => write!(f, "L")?,
                    Tile::Occupied => write!(f, "#")?,
                    Tile::Floor => write!(f, ".")?,
                };
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Layout {
    fn stabilize_simple(&mut self) {
        while self.step_simple() != 0 {}
    }

    fn stabilize_advanced(&mut self) {
        while self.step_advanced() != 0 {}
    }

    fn count_occupied(&self) -> usize {
        self.seats.iter().filter(|&&t| t == Tile::Occupied).count()
    }

    fn get_tile(&self, x: i32, y: i32) -> Tile {
        let index = x + (y * self.width);
        assert!(index >= 0 && index < self.seats.len() as i32);
        self.seats[index as usize]
    }

    fn set_seat(&mut self, x: i32, y: i32, tile: Tile) {
        let index = x + (y * self.width);
        assert!(index >= 0 && index < self.seats.len() as i32);
        self.seats[index as usize] = tile;
    }

    pub fn step_simple(&mut self) -> usize {
        let mut flips = vec![];
        for x in 0..self.width {
            for y in 0..self.height {
                let adjacent = self.count_adjacent(x, y);
                match self.get_tile(x, y) {
                    Tile::Seat if adjacent == 0 => flips.push((x, y, Tile::Occupied)),
                    Tile::Occupied if adjacent >= 4 => flips.push((x, y, Tile::Seat)),
                    _ => {}
                };
            }
        }
        let flipped = flips.len();

        flips.into_iter().for_each(|(x, y, tile)| {
            self.set_seat(x, y, tile);
        });

        flipped
    }

    pub fn step_advanced(&mut self) -> usize {
        let mut flips = vec![];
        for x in 0..self.width {
            for y in 0..self.height {
                let adjacent = self.count_visible(x, y);
                match self.get_tile(x, y) {
                    Tile::Seat if adjacent == 0 => flips.push((x, y, Tile::Occupied)),
                    Tile::Occupied if adjacent >= 5 => flips.push((x, y, Tile::Seat)),
                    _ => {}
                };
            }
        }
        let flipped = flips.len();

        flips.into_iter().for_each(|(x, y, tile)| {
            self.set_seat(x, y, tile);
        });

        flipped
    }

    fn count_adjacent(&self, x: i32, y: i32) -> usize {
        let x = x as i32;
        let y = y as i32;
        let coords = vec![
            (x - 1, y - 1),
            (x, y - 1),
            (x + 1, y - 1),
            (x - 1, y),
            (x + 1, y),
            (x - 1, y + 1),
            (x, y + 1),
            (x + 1, y + 1),
        ];

        coords
            .iter()
            .filter(|(x, y)| {
                if *x < 0 || *y < 0 || *x >= self.width as i32 || *y >= self.height as i32 {
                    false
                } else {
                    self.get_tile(*x, *y) == Tile::Occupied
                }
            })
            .count()
    }

    pub fn count_visible(&self, x: i32, y: i32) -> usize {
        let mut visible = vec![];
        let dirs = vec![
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        ];

        for dir in dirs.iter() {
            let mut coor = (x, y);
            loop {
                coor.0 += dir.0;
                coor.1 += dir.1;

                if !self.is_valid(coor.0, coor.1) {
                    break;
                }

                match self.get_tile(coor.0, coor.1) {
                    Tile::Occupied => {
                        visible.push(coor);
                        break;
                    }
                    Tile::Seat => {
                        break;
                    }
                    Tile::Floor => {}
                }
            }
        }
        visible.len()
    }

    fn is_valid(&self, x: i32, y: i32) -> bool {
        x >= 0 && y >= 0 && x < self.width && y < self.height
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_example_1() {
        let mut layout: Layout = "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"
            .parse()
            .expect("Failed to parse input");
        layout.stabilize_simple();
        assert_eq!(layout.count_occupied(), 37);
    }

    #[test]
    pub fn test_example_2() {
        let mut layout: Layout = "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"
            .parse()
            .expect("Failed to parse input");
        layout.stabilize_advanced();
        assert_eq!(layout.count_occupied(), 26);
    }
}
