use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Thirteen, |input| {
        let (arrival, bus_ids) = parse_input(input);
        let filtered_busses = bus_ids.iter().map(|(_, id)| *id).collect::<Vec<_>>();
        let (bus_id, time) = select_bus(arrival, &filtered_busses);
        println!("Bus multiplier: {}", bus_id * time);

        let time_start = get_start_time(&bus_ids);
        println!("Start time for bus sequence: {}", time_start);
    })
}

pub fn select_bus(arrival: i32, bus_ids: &[i32]) -> (i32, i32) {
    let waiting_times = get_waiting_times(arrival, bus_ids);

    let selection = bus_ids
        .into_iter()
        .zip(waiting_times.into_iter())
        .min_by_key(|(_, v)| *v)
        .expect("No valid bus route found");
    (*selection.0, selection.1)
}

fn get_waiting_times(arrival: i32, bus_ids: &[i32]) -> Vec<i32> {
    bus_ids
        .iter()
        .map(|id| {
            let next = ((arrival / id) + 1) * id;
            next - arrival
        })
        .collect::<Vec<_>>()
}

pub fn parse_input(input: &str) -> (i32, Vec<(usize, i32)>) {
    let mut lines = input.lines();
    let arrival: i32 = lines
        .next()
        .unwrap()
        .parse()
        .expect("Failed to parse arrival time");
    let bus_ids: Vec<(usize, i32)> = parse_bus_line(lines.next().unwrap());

    (arrival, bus_ids)
}

pub fn parse_bus_line(line: &str) -> Vec<(usize, i32)> {
    line.split(",")
        .enumerate()
        .filter_map(|(i, id)| {
            if id == "x" {
                None
            } else {
                let id = id.parse().expect("Failed to parse bus id");
                Some((i, id))
            }
        })
        .collect::<Vec<_>>()
}

pub fn get_start_time(bus_ids: &[(usize, i32)]) -> u64 {
    let sets = bus_ids
        .iter()
        .map(|(offset, bus_id)| {
            if *offset == 0usize {
                (0u64, *bus_id as u64)
            } else {
                (
                    (bus_id - ((*offset as i32) - bus_id * (*offset as i32 / bus_id))) as u64,
                    *bus_id as u64,
                )
            }
        })
        .collect::<Vec<_>>();

    brute_force_congruence(&sets)
}

// TODO: Should solve this using Chinese Remeinder Theory instead
//
// Solution based on the fact that for solving
// x = 0 (mod M)
// x = 2 (mod N)
// x = 3 (mod O)
// we know that a solution for the first two can be written as
// x = (M * N) * y + R  => x = R (mod M * N)
//  - for any y and suitable R and we can then use that
// and then we can solve that congruence equation together with the next part of the set
pub fn brute_force_congruence(sets: &[(u64, u64)]) -> u64 {
    let mut i = 0;
    let mut found = 1;
    let mut interval = sets[0].1;
    let mut rem = 0;
    loop {
        i += 1;
        let solution = interval * (i as u64) + rem;
        for (offset, modulo) in sets.iter().skip(found) {
            let part_solution =
                (solution as u64 / *modulo as u64) * *modulo as u64 + (*offset as u64);
            if part_solution != solution {
                break;
            }
            found += 1;

            let n: u64 = sets.iter().take(found).map(|(_, v)| *v as u64).product();
            rem = solution % n;
            interval = n;
            i = 0;
        }
        if found == sets.len() {
            return solution;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_waiting_time() {
        let input = parse_input("939\n7,13,x,x,59,x,31,19");
        let filtered_busses = input.1.into_iter().map(|(_, id)| id).collect::<Vec<_>>();
        let (bus_id, time) = select_bus(input.0, &filtered_busses);
        assert_eq!(bus_id * time, 295);
    }

    #[test]
    pub fn test_bus_departure() {
        /*
         * x = 0 mod 17
         * x = 2 mod 13
         * x = 3 mod 19
         */
        let bus_ids = parse_bus_line("17,x,13,19");
        let time_start = get_start_time(&bus_ids);
        assert_eq!(time_start, 3417);

        let bus_ids = parse_bus_line("67,7,59,61");
        let time_start = get_start_time(&bus_ids);
        assert_eq!(time_start, 754018);

        let bus_ids = parse_bus_line("67,x,7,59,61");
        let time_start = get_start_time(&bus_ids);
        assert_eq!(time_start, 779210);

        let bus_ids = parse_bus_line("67,7,x,59,61");
        let time_start = get_start_time(&bus_ids);
        assert_eq!(time_start, 1261476);

        let bus_ids = parse_bus_line("1789,37,47,1889");
        let time_start = get_start_time(&bus_ids);
        assert_eq!(time_start, 1202161486);
    }
}
