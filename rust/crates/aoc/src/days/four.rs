use anyhow::{anyhow, bail};
use libaoc::{DayNumber, Solver};
use nom::{
    bytes::complete::{tag, take_until, take_while},
    Finish, IResult,
};
use std::str::FromStr;

pub fn get() -> Solver {
    Solver::new(DayNumber::Four, |input| {
        let passports: Passports = input.parse().expect("Failed to parse passport input");
        println!("Part 1: {}", passports.count_has_required_fields());
        println!("Part 2: {}", passports.count_valid());
    })
}

struct Passports {
    inner: Vec<Passport>,
}

impl Passports {
    pub fn count_has_required_fields(&self) -> usize {
        self.inner
            .iter()
            .filter(|pass| pass.has_required_fields())
            .count()
    }

    pub fn count_valid(&self) -> usize {
        self.inner
            .iter()
            .filter(|pass| pass.has_required_fields() && pass.is_valid())
            .count()
    }
}

impl FromStr for Passports {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (mut passports, rem) =
            s.lines()
                .fold((vec![], "".to_string()), |(mut passports, rem), line| {
                    if line.is_empty() {
                        passports.push(rem + " ");
                        (passports, "".to_string())
                    } else {
                        (passports, rem + " " + line)
                    }
                });
        passports.push(rem + " ");
        Ok(Passports {
            inner: passports.into_iter().map(Passport).collect(),
        })
    }
}

struct Passport(String);

impl Passport {
    pub fn has_required_fields(&self) -> bool {
        let required = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"];

        required.iter().all(|field| self.0.contains(field))
    }

    pub fn is_valid(&self) -> bool {
        self.valid_byr()
            && self.valid_iyr()
            && self.valid_eyr()
            && self.valid_hgt()
            && self.valid_hcl()
            && self.valid_ecl()
            && self.valid_pid()
    }

    pub fn valid_byr(&self) -> bool {
        matches!(self.get_tag_value::<i32>("byr:"), Ok(val) if (1920..=2002).contains(&val))
    }

    pub fn valid_iyr(&self) -> bool {
        matches!(self.get_tag_value::<i32>("iyr:"), Ok(val) if (2010..=2020).contains(&val))
    }

    pub fn valid_eyr(&self) -> bool {
        matches!(self.get_tag_value::<i32>("eyr:"), Ok(val) if (2020..=2030).contains(&val))
    }

    pub fn valid_hgt(&self) -> bool {
        match self.get_tag_value::<Height>("hgt:") {
            Ok(Height::Cm(val)) if (150..=193).contains(&val) => true,
            Ok(Height::In(val)) if (59..=76).contains(&val) => true,
            _ => false,
        }
    }

    pub fn valid_hcl(&self) -> bool {
        self.get_tag_value::<HairColor>("hcl:").is_ok()
    }

    pub fn valid_ecl(&self) -> bool {
        self.get_tag_value::<EyeColor>("ecl:").is_ok()
    }

    pub fn valid_pid(&self) -> bool {
        matches!(self.get_tag_value::<Pid>("pid:"), Ok(val) if val.0.len() == 9)
    }

    fn get_tag_value<T>(&self, tag: &str) -> Result<T, anyhow::Error>
    where
        T: FromStr + std::fmt::Debug,
        <T as FromStr>::Err: std::fmt::Debug,
    {
        match parse_tag(&self.0, tag).finish() {
            Ok((_, value)) => value.parse::<T>().map_err(|err| anyhow!("{:?}", err)),
            Err(nom::error::Error { input, code }) => Err(anyhow::Error::new(nom::error::Error {
                code,
                input: input.to_string(),
            })),
        }
    }
}

fn parse_tag<'a>(input: &'a str, tag_name: &str) -> IResult<&'a str, &'a str> {
    let (input, _) = take_until(tag_name)(input)?;
    let (input, _) = tag(tag_name)(input)?;
    take_until(" ")(input)
}

#[derive(Debug, Clone, Copy)]
enum Height {
    In(i32),
    Cm(i32),
}

impl FromStr for Height {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let number = match get_digits(s, 10).finish() {
            Ok((_, number)) => number,
            _ => bail!("Failed"),
        };
        let number = number
            .parse()
            .map_err(|_| anyhow!("Failed to convert heiht number to i32"))?;
        if s.contains("cm") {
            Ok(Height::Cm(number))
        } else {
            Ok(Height::In(number))
        }
    }
}

fn get_digits(input: &str, radix: u32) -> IResult<&str, &str> {
    let is_digit = move |c: char| c.is_digit(radix);
    take_while(is_digit)(input)
}

#[derive(Debug, Clone)]
struct HairColor(String);

impl FromStr for HairColor {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let color = match get_hex(s).finish() {
            Ok((_, color)) if color.len() == 6 => color,
            _ => bail!("Failed to parse hair color"),
        };

        Ok(HairColor(color.to_string()))
    }
}

fn get_hex(input: &str) -> IResult<&str, &str> {
    let (input, _) = tag("#")(input)?;
    get_digits(input, 16)
}

#[derive(Debug, Clone)]
enum EyeColor {
    Amb,
    Blu,
    Brn,
    Gry,
    Grn,
    Hzl,
    Oth,
}

impl FromStr for EyeColor {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "amb" => Ok(EyeColor::Amb),
            "blu" => Ok(EyeColor::Blu),
            "brn" => Ok(EyeColor::Brn),
            "gry" => Ok(EyeColor::Gry),
            "grn" => Ok(EyeColor::Grn),
            "hzl" => Ok(EyeColor::Hzl),
            "oth" => Ok(EyeColor::Oth),
            _ => Err(anyhow!("Invalid eye color")),
        }
    }
}

#[derive(Debug, Clone)]
struct Pid(String);

impl FromStr for Pid {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let number = match get_digits(s, 10).finish() {
            Ok((_, number)) => number,
            _ => bail!("Failed"),
        };
        Ok(Pid(number.to_string()))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_input() {
        let passports: Passports = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
            byr:1937 iyr:2017 cid:147 hgt:183cm

            iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
            hcl:#cfa07d byr:1929

            hcl:#ae17e1 iyr:2013
            eyr:2024
            ecl:brn pid:760753108 byr:1931
            hgt:179cm

            hcl:#cfa07d eyr:2025 pid:166559648
            iyr:2011 ecl:brn hgt:59in"
            .parse()
            .expect("Failed to parse input");
        let valid = passports.count_has_required_fields();
        assert_eq!(valid, 2);
    }

    #[test]
    fn test_invalid() {
        let invalids: Passports = "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"
            .parse()
            .expect("Failed to parse input");
        assert_eq!(invalids.count_valid(), 0);
    }

    #[test]
    fn test_valid() {
        let valids: Passports = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"
            .parse()
            .expect("Failed to parse input");
        assert_eq!(valids.count_valid(), 4);
    }
}
