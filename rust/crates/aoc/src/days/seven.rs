use libaoc::{DayNumber, Solver};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{self, alpha1, one_of, space0},
    multi::many0,
    Finish, IResult,
};
use std::collections::{HashMap, HashSet};
use std::str::FromStr;

pub fn get() -> Solver {
    Solver::new(DayNumber::Seven, |input| {
        let processor: LuggageProcessor = input.parse().expect("Failed to parse input");
        println!(
            "Bag options: {}",
            processor.contain_count("shiny gold".to_string())
        );

        println!(
            "Bag contents: {}",
            processor.bag_content("shiny gold".to_string())
        );
    })
}

type Color = String;

#[derive(Debug)]
struct BagRule {
    color: Color,
    contains: HashMap<Color, u8>,
}

struct LuggageProcessor {
    rules: Vec<BagRule>,
}

impl LuggageProcessor {
    pub fn contain_count(&self, color: Color) -> usize {
        let mut to_check = vec![color];
        let mut count: HashSet<Color> = HashSet::default();

        while let Some(checking) = to_check.pop() {
            for rule in self.rules.iter() {
                if rule.contains.contains_key(&checking) {
                    to_check.push(rule.color.clone());
                    count.insert(rule.color.clone());
                }
            }
        }
        count.len()
    }

    pub fn bag_content(&self, color: Color) -> usize {
        let rule = self
            .rules
            .iter()
            .find(|r| r.color == color)
            .expect("Rule missing for color");
        if rule.contains.is_empty() {
            0
        } else {
            let mut count = 0;
            for (col, &num) in rule.contains.iter() {
                count += (num as usize) * (1 + self.bag_content(col.clone()));
            }
            count
        }
    }
}

impl FromStr for LuggageProcessor {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let rules = s
            .lines()
            .map(|line| line.parse().expect("Failed to parse rule line"))
            .collect::<Vec<_>>();

        Ok(LuggageProcessor { rules })
    }
}

impl FromStr for BagRule {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match parse_rule(s.trim()).finish() {
            Ok((_rem, rule)) => Ok(rule),
            Err(nom::error::Error { input, code }) => Err(anyhow::Error::new(nom::error::Error {
                code,
                input: input.to_string(),
            })),
        }
    }
}

fn parse_color(input: &str) -> IResult<&str, Color> {
    let (input, part_1) = alpha1(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, part_2) = alpha1(input)?;

    Ok((input, format!("{} {}", part_1, part_2)))
}

fn parse_color_count(input: &str) -> IResult<&str, (Color, u8)> {
    let (input, count) = complete::u8(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, color) = parse_color(input)?;

    Ok((input, (color, count)))
}

fn parse_content(input: &str) -> IResult<&str, (Color, u8)> {
    let (input, content) = parse_color_count(input)?;
    let (input, _) = alt((tag(" bags"), tag(" bag")))(input)?;
    let (input, _) = one_of(".,")(input)?;
    let (input, _) = space0(input)?;

    Ok((input, content))
}

fn parse_rule(input: &str) -> IResult<&str, BagRule> {
    let (input, color) = parse_color(input)?;
    let (input, _) = tag(" bags contain ")(input)?;

    let (input, contents) = many0(parse_content)(input)?;

    let contains: HashMap<Color, u8> = contents.into_iter().collect();

    Ok((input, BagRule { color, contains }))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_parse_rule() {
        let rule: BagRule = "light red bags contain 1 bright white bag, 2 muted yellow bags."
            .parse()
            .expect("Failed to parse bag rule");
        assert_eq!(rule.color, "light red");
        assert_eq!(rule.contains.len(), 2);
        assert_eq!(rule.contains.get("bright white"), Some(&1));
        assert_eq!(rule.contains.get("muted yellow"), Some(&2));

        let rule: BagRule = "faded blue bags contain no other bags."
            .parse()
            .expect("Failed to parse bag rule");
        assert_eq!(rule.color, "faded blue");
        assert_eq!(rule.contains.len(), 0);
    }

    #[test]
    pub fn test_example_1() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
            dark orange bags contain 3 bright white bags, 4 muted yellow bags.
            bright white bags contain 1 shiny gold bag.
            muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
            shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
            dark olive bags contain 3 faded blue bags, 4 dotted black bags.
            vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
            faded blue bags contain no other bags.
            dotted black bags contain no other bags.";
        let processor: LuggageProcessor = input.parse().expect("Failed to parse input");
        assert_eq!(processor.contain_count("shiny gold".to_string()), 4);
    }

    #[test]
    pub fn test_example_2() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
            dark orange bags contain 3 bright white bags, 4 muted yellow bags.
            bright white bags contain 1 shiny gold bag.
            muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
            shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
            dark olive bags contain 3 faded blue bags, 4 dotted black bags.
            vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
            faded blue bags contain no other bags.
            dotted black bags contain no other bags.";
        let processor: LuggageProcessor = input.parse().expect("Failed to parse input");
        assert_eq!(processor.bag_content("shiny gold".to_string()), 32);
    }

    #[test]
    pub fn test_bag_content() {
        let input = "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
        let processor: LuggageProcessor = input.parse().expect("Failed to parse input");
        assert_eq!(processor.bag_content("shiny gold".to_string()), 126);
    }
}
