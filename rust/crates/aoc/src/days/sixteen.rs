use libaoc::{DayNumber, Solver};
use nom::{
    branch::alt,
    bytes::complete::{tag, take_until},
    character::complete::{self, alpha1, one_of, space0},
    combinator::{iterator, opt},
    multi::many0,
    Finish, IResult,
};
use std::collections::{HashMap, HashSet};
use std::ops::Range;
use std::str::FromStr;

pub fn get() -> Solver {
    Solver::new(DayNumber::Sixteen, |input| {
        let (rules, my_ticket, other_tickets) = parse_input(input);
        let invalid = get_invalid_numbers(&other_tickets, &rules);
        println!("Invalid sum: {}", invalid.iter().sum::<u32>());

        let other_tickets = remove_invalid_tickets(other_tickets, &rules);
        let ordered = order_rules(&other_tickets, &rules);

        let departure_values = ordered
            .iter()
            .enumerate()
            .filter_map(|(idx, rule)| {
                if rule.desc.contains("departure") {
                    Some(my_ticket[idx])
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        dbg!(&departure_values);
        println!(
            "Departure multiplication: {:?}",
            departure_values.iter().map(|v| *v as u64).product::<u64>()
        );
    })
}

fn parse_input(input: &str) -> (Vec<Rule>, TicketNumbers, Vec<TicketNumbers>) {
    let mut state = ParseState::Rules;

    let mut rules = vec![];
    let mut my = vec![];
    let mut others = vec![];

    for line in input.lines() {
        if line.is_empty() {
            state = state.next();
        }
        let line = line.trim();

        match state {
            ParseState::Rules => {
                if let Ok(rule) = line.parse::<Rule>() {
                    rules.push(rule);
                }
            }
            ParseState::MyTicket => {
                if !line.is_empty() && line.split(",").count() > 1 {
                    my = line
                        .split(",")
                        .map(|l| l.parse::<u32>().expect("failed to parse my ticket number"))
                        .collect::<Vec<_>>();
                }
            }
            ParseState::OtherTickets => {
                if !line.is_empty() && line.split(",").count() > 1 {
                    others.push(
                        line.split(",")
                            .map(|l| l.parse::<u32>().expect("failed to parse my ticket number"))
                            .collect::<Vec<_>>(),
                    );
                }
            }
        }
    }
    (rules, my, others)
}

fn remove_invalid_tickets(other_tickets: Vec<TicketNumbers>, rules: &[Rule]) -> Vec<TicketNumbers> {
    other_tickets
        .into_iter()
        .filter(|ticket| find_invalid_number(ticket, rules).is_none())
        .collect::<Vec<_>>()
}

fn order_rules(tickets: &[TicketNumbers], rules: &[Rule]) -> Vec<Rule> {
    let mut orderings = rules
        .iter()
        .map(|rule| {
            let positions = find_valid_rule_positions(tickets, rule);
            (rule.clone(), positions)
        })
        .collect::<Vec<_>>();

    orderings.sort_by(|a, b| b.1.len().cmp(&a.1.len()));

    let mut ordered_rules = vec![None; rules.len()];

    while let Some((rule, positions)) = orderings.pop() {
        assert!(positions.len() == 1);
        for ord in &mut orderings {
            ord.1 = ord
                .1
                .clone()
                .into_iter()
                .filter(|&p| p != positions[0])
                .collect::<Vec<_>>();
        }
        ordered_rules[positions[0] as usize] = Some(rule);
        orderings.sort_by(|a, b| b.1.len().cmp(&a.1.len()));
    }

    ordered_rules.into_iter().flatten().collect::<Vec<_>>()
}

fn find_valid_rule_positions(tickets: &[TicketNumbers], rule: &Rule) -> Vec<u32> {
    let mut positions = vec![];

    for i in 0..tickets[0].len() {
        if tickets.iter().all(|ticket| rule.accepts_number(ticket[i])) {
            positions.push(i as u32);
        }
    }

    positions
}

enum ParseState {
    Rules,
    MyTicket,
    OtherTickets,
}

impl ParseState {
    pub fn next(self) -> Self {
        match self {
            ParseState::Rules => ParseState::MyTicket,
            ParseState::MyTicket => ParseState::OtherTickets,
            ParseState::OtherTickets => panic!("No further states"),
        }
    }
}

type TicketNumbers = Vec<u32>;

fn get_invalid_numbers(tickets: &[TicketNumbers], rules: &[Rule]) -> Vec<u32> {
    tickets
        .into_iter()
        .filter_map(|ticket| find_invalid_number(ticket, rules))
        .collect()
}

fn find_invalid_number(numbers: &TicketNumbers, rules: &[Rule]) -> Option<u32> {
    numbers
        .into_iter()
        .find(|&num| !rules.iter().any(|rule| rule.accepts_number(*num)))
        .cloned()
}

#[derive(Debug, Clone)]
struct Rule {
    desc: String,
    ranges: Vec<Range<u32>>,
}

impl Rule {
    fn accepts_number(&self, num: u32) -> bool {
        self.ranges.iter().any(|range| range.contains(&num))
    }
}

impl FromStr for Rule {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match parse_rule(s.trim()).finish() {
            Ok((_rem, rule)) => Ok(rule),
            Err(nom::error::Error { input, code }) => Err(anyhow::Error::new(nom::error::Error {
                code,
                input: input.to_string(),
            })),
        }
    }
}

fn parse_rule(input: &str) -> IResult<&str, Rule> {
    let (input, desc) = take_until(": ")(input)?;
    let (input, _) = tag(": ")(input)?;

    let mut it = iterator(input, parse_range);
    let ranges = it.map(|v| v).collect::<Vec<Range<u32>>>();
    let (input, _) = it.finish()?;

    Ok((
        input,
        Rule {
            desc: desc.to_string(),
            ranges,
        },
    ))
}

fn parse_range(input: &str) -> IResult<&str, Range<u32>> {
    let (input, start) = complete::u32(input)?;
    let (input, _) = tag("-")(input)?;
    let (input, end) = complete::u32(input)?;
    let (input, _) = opt(tag(" or "))(input)?;
    Ok((
        input,
        Range {
            start,
            end: end + 1,
        },
    ))
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::ops::Range;

    #[test]
    pub fn test_parse_rule() {
        let rule: Rule = "departure station: 40-480 or 491-949"
            .parse()
            .expect("Failed to parse rule");

        assert_eq!(rule.desc, "departure station".to_string());
        assert_eq!(
            rule.ranges[0],
            Range {
                start: 40,
                end: 481
            }
        );
        assert_eq!(
            rule.ranges[1],
            Range {
                start: 491,
                end: 950
            }
        );
    }

    #[test]
    pub fn test_parse_input() {
        let (rules, my_ticket, other_tickets) = parse_input(
            "class: 1-3 or 5-7
            row: 6-11 or 33-44
            seat: 13-40 or 45-50

            your ticket:
            7,1,14

            nearby tickets:
            7,3,47
            40,4,50
            55,2,20
            38,6,12",
        );

        assert_eq!(rules[0].desc, "class".to_string());
        assert_eq!(rules[0].ranges[0], Range { start: 1, end: 4 });
        assert_eq!(rules[0].ranges[1], Range { start: 5, end: 8 });

        assert_eq!(rules[1].desc, "row".to_string());
        assert_eq!(rules[1].ranges[0], Range { start: 6, end: 12 });
        assert_eq!(rules[1].ranges[1], Range { start: 33, end: 45 });

        assert_eq!(rules[2].desc, "seat".to_string());
        assert_eq!(rules[2].ranges[0], Range { start: 13, end: 41 });
        assert_eq!(rules[2].ranges[1], Range { start: 45, end: 51 });

        assert_eq!(my_ticket, vec![7, 1, 14]);
        assert_eq!(
            other_tickets,
            vec![
                vec![7, 3, 47],
                vec![40, 4, 50],
                vec![55, 2, 20],
                vec![38, 6, 12],
            ]
        );
    }

    #[test]
    pub fn test_number_validation() {
        let (rules, _, _) = parse_input(
            "class: 1-3 or 5-7
            row: 6-11 or 33-44
            seat: 13-40 or 45-50",
        );

        dbg!(&rules);
        assert!(rules[0].accepts_number(3));
        let invalid = find_invalid_number(&vec![3, 4, 10], &rules);
        assert_eq!(invalid, Some(4));
    }

    #[test]
    pub fn test_find_invalid_numbers() {
        let (rules, my_ticket, other_tickets) = parse_input(
            "class: 1-3 or 5-7
            row: 6-11 or 33-44
            seat: 13-40 or 45-50

            your ticket:
            7,1,14

            nearby tickets:
            7,3,47
            40,4,50
            55,2,20
            38,6,12",
        );

        let invalid = get_invalid_numbers(&other_tickets, &rules);
        assert_eq!(invalid, vec![4, 55, 12]);
    }

    #[test]
    pub fn test_find_position() {
        let (rules, my_ticket, other_tickets) = parse_input(
            "class: 0-1 or 4-19
            row: 0-5 or 8-19
            seat: 0-13 or 16-19

            your ticket:
            11,12,13

            nearby tickets:
            3,9,18
            15,1,5
            5,14,9",
        );

        let ordered = order_rules(&other_tickets, &rules);
        assert_eq!(ordered[0].desc, "row".to_string());
        assert_eq!(ordered[1].desc, "class".to_string());
        assert_eq!(ordered[2].desc, "seat".to_string());
    }

    #[test]
    pub fn test_valid_positions() {
        let (rules, my_ticket, other_tickets) = parse_input(
            "class: 0-1 or 4-19
            row: 0-5 or 8-19
            seat: 0-13 or 16-19

            your ticket:
            11,12,13

            nearby tickets:
            3,9,18
            15,1,5
            5,14,9",
        );

        let positions = find_valid_rule_positions(&other_tickets, &rules[0]);
        assert_eq!(positions, vec![1, 2]);

        let positions = find_valid_rule_positions(&other_tickets, &rules[1]);
        assert_eq!(positions, vec![0, 1, 2]);

        let positions = find_valid_rule_positions(&other_tickets, &rules[2]);
        assert_eq!(positions, vec![2]);
    }

    #[test]
    pub fn test_remove_invalid() {
        let (rules, my_ticket, other_tickets) = parse_input(
            "class: 0-1 or 4-19
            row: 0-5 or 8-19
            seat: 0-13 or 16-19

            your ticket:
            11,12,13

            nearby tickets:
            3,9,18
            15,1,5
            55,1,5
            5,14,9",
        );

        let other_tickets = remove_invalid_tickets(other_tickets, &rules);
        assert_eq!(other_tickets.len(), 3);
    }

    #[test]
    pub fn test_order_rules() {
        let (rules, my_ticket, other_tickets) = parse_input(
            "class: 0-1 or 4-19
            row: 0-5 or 8-19
            seat: 0-13 or 16-19

            your ticket:
            11,12,13

            nearby tickets:
            3,9,18
            15,1,5
            5,14,9",
        );

        let ordered = order_rules(&other_tickets, &rules);

        assert_eq!(ordered[0].desc, "row".to_owned());
        assert_eq!(ordered[1].desc, "class".to_owned());
        assert_eq!(ordered[2].desc, "seat".to_owned());
    }
}
