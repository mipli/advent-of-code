use libaoc::{DayNumber, Solver};
use std::str::FromStr;

pub fn get() -> Solver {
    Solver::new(DayNumber::Three, |input| {
        let map: Map = input.parse().expect("Failed to parse input");

        let answer = count_hits(&Direction { right: 3, down: 1 }, &map);
        println!("Part 1: {}", answer);
        println!("Part 2: {}", solve_second(&map));
    })
}

fn solve_second(map: &Map) -> i32 {
    let hits = [
        count_hits(&Direction { right: 1, down: 1 }, map),
        count_hits(&Direction { right: 3, down: 1 }, map),
        count_hits(&Direction { right: 5, down: 1 }, map),
        count_hits(&Direction { right: 7, down: 1 }, map),
        count_hits(&Direction { right: 1, down: 2 }, map),
    ];
    hits.iter().product()
}

fn count_hits(direction: &Direction, map: &Map) -> i32 {
    let (hits, _) = map.tiles.iter().step_by(direction.down as usize).fold(
        (0, -direction.right),
        |(mut hits, mut x), row| {
            x = (x + direction.right) % (row.len() as i32);
            if row.get(x as usize) == Some(&Tile::Tree) {
                hits += 1;
            }
            (hits, x)
        },
    );
    hits
}

#[derive(Eq, PartialEq)]
enum Tile {
    Tree,
    Empty,
}

struct Direction {
    right: i32,
    down: i32,
}

struct Map {
    tiles: Vec<Vec<Tile>>,
}

impl FromStr for Map {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let tiles = s
            .lines()
            .map(|line| {
                line.chars()
                    .map(|c| match c {
                        '#' => Tile::Tree,
                        '.' => Tile::Empty,
                        _ => panic!("Invalid map data"),
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        Ok(Map { tiles })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_example_1() {
        let map: Map = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"
            .parse()
            .expect("Failed to parse map data");

        let solution = count_hits(&Direction { right: 3, down: 1 }, &map);
        assert_eq!(7, solution);
    }

    #[test]
    pub fn test_example_2() {
        let map: Map = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"
            .parse()
            .expect("Failed to parse map data");

        let solution = solve_second(&map);
        assert_eq!(336, solution);
    }
}
