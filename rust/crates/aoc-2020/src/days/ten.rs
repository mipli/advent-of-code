use std::collections::HashMap;

use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Ten, |input| {
        let mut numbers: Vec<u64> = input
            .lines()
            .map(|line| line.parse().expect("Failed to parse number"))
            .collect::<Vec<_>>();
        numbers.sort_unstable();
        let diffs = find_jolt_differences(&numbers);

        println!("Jolt multiplier: {}", (diffs[0] + 1) * (diffs[2] + 1));

        numbers.push(numbers[numbers.len() - 1] + 3);
        numbers.insert(0, 0);

        println!("Jolt configurations: {}", count_configurations(&numbers));
    })
}

fn find_jolt_differences(numbers: &[u64]) -> [usize; 3] {
    let mut diffs = [1, 0, 0];

    numbers.windows(2).for_each(|pair| {
        let diff = (pair[1] - pair[0]) as usize;
        assert!(diff < 4);
        if diff > 0 {
            diffs[diff - 1] += 1;
        }
    });
    diffs[2] += 1;
    diffs
}

fn count_configurations(numbers: &[u64]) -> usize {
    let mut cache: HashMap<u64, usize> = HashMap::default();
    calculate_configuration(numbers, 0, &mut cache)
}

fn calculate_configuration(
    numbers: &[u64],
    index: usize,
    cache: &mut HashMap<u64, usize>,
) -> usize {
    if index == numbers.len() - 1 {
        return 1;
    }

    let current = numbers[index];
    let mut count = 0;

    for i in (index + 1)..numbers.len() {
        if numbers[i] <= current + 3 {
            let res = if let Some(res) = cache.get(&numbers[i]) {
                *res
            } else {
                let res = calculate_configuration(numbers, i, cache);
                cache.insert(numbers[i], res);
                res
            };
            count += res;
        } else {
            return count;
        }
    }
    count
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_jolt_differences_simple() {
        let mut numbers = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4];
        numbers.sort();
        let diffs = find_jolt_differences(&numbers);
        assert_eq!(diffs[0], 7);
        assert_eq!(diffs[2], 5);
    }

    #[test]
    pub fn test_jolt_differences() {
        let mut numbers = [
            28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35,
            8, 17, 7, 9, 4, 2, 34, 10, 3,
        ];
        numbers.sort();
        let diffs = find_jolt_differences(&numbers);
        assert_eq!(diffs[0], 22);
        assert_eq!(diffs[2], 10);
    }

    #[test]
    pub fn test_count_configurations_simple() {
        let mut numbers = [0, 16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4, 22];
        numbers.sort();
        let configurations = count_configurations(&numbers);
        assert_eq!(configurations, 8);
    }

    #[test]
    pub fn test_count_configurations() {
        let mut numbers = [
            0, 28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25,
            35, 8, 17, 7, 9, 4, 2, 34, 10, 3, 52,
        ];
        numbers.sort();
        let configurations = count_configurations(&numbers);
        assert_eq!(configurations, 19208);
    }
}
