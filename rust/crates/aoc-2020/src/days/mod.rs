use libaoc::Aoc;

mod eight;
mod eighteen;
mod eleven;
mod fifteen;
mod five;
mod four;
mod fourteen;
mod nine;
mod nineteen;
mod one;
mod seven;
mod seventeen;
mod six;
mod sixteen;
mod ten;
mod thirteen;
mod three;
mod twelve;
mod two;

pub fn register(aoc: &mut Aoc<2020>) {
    aoc.add(one::get());
    aoc.add(two::get());
    aoc.add(three::get());
    aoc.add(four::get());
    aoc.add(five::get());
    aoc.add(six::get());
    aoc.add(seven::get());
    aoc.add(eight::get());
    aoc.add(nine::get());
    aoc.add(ten::get());
    aoc.add(eleven::get());
    aoc.add(twelve::get());
    aoc.add(thirteen::get());
    aoc.add(fourteen::get());
    aoc.add(fifteen::get());
    aoc.add(sixteen::get());
    aoc.add(seventeen::get());
    aoc.add(eighteen::get());
    aoc.add(nineteen::get());
}
