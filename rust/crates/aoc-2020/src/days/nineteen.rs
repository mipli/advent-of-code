use std::str::FromStr;

use libaoc::{DayNumber, Solver};
use nom::{
    branch::alt,
    bytes::complete::{tag, take_until},
    character::complete,
    multi, sequence, Finish, IResult,
};

/// TODO: Needs to be solved as a proper back propagating solver

pub fn get() -> Solver {
    Solver::new(DayNumber::Nineteen, |input| {
        let (mut rules, messages) = parse_input(input).unwrap();
        let valid_count = count_matches(&rules, &messages);
        println!("Valid messages: {valid_count}");

        rules.fix();
        let valid_count = count_matches(&rules, &messages);
        println!("Valid messages, with loops: {valid_count}");
    })
}

fn count_matches(rules: &Rules, messages: &[Message]) -> usize {
    messages.iter().filter(|msg| rules.is_valid(msg)).count()
}

fn parse_input(input: &str) -> anyhow::Result<(Rules, Vec<Message>)> {
    let mut split = input.split("\n\n");
    let pre = split.next().unwrap();
    let post = split.next().unwrap();
    let rules = Rules::from_str(pre)?;
    let messages = post
        .lines()
        .map(|line| String::from(line.trim()))
        .collect::<Vec<_>>();

    Ok((rules, messages))
}

type RuleId = usize;
type Message = String;

#[derive(Debug, Clone, Eq, PartialEq)]
enum Rule {
    Literal(char),
    List(Vec<RuleId>),
    Or((Vec<RuleId>, Vec<RuleId>)),
    Endless(RuleId),
    Midless((RuleId, RuleId)),
}

#[derive(Debug, Clone)]
struct Rules {
    values: Vec<Rule>,
}

enum RuleResult {
    Invalid,
    Overflow,
    Success(usize),
}

impl Rules {
    fn fix(&mut self) {
        self.values[8] = Rule::Endless(42);
        self.values[11] = Rule::Midless((42, 21));
    }

    fn is_valid(&self, message: &Message) -> bool {
        let chars = message.chars().collect::<Vec<_>>();

        match self.is_valid_rule(0, 0, &chars) {
            RuleResult::Invalid => false,
            RuleResult::Overflow => false,
            RuleResult::Success(offset) if offset != message.len() => false,
            RuleResult::Success(_) => true,
        }
    }

    fn is_valid_rule(
        &self,
        current_idx: RuleId,
        mut cursor: usize,
        message: &[char],
    ) -> RuleResult {
        println!("CHECKING: {current_idx} @ {cursor}");
        if cursor >= message.len() {
            return RuleResult::Overflow;
        }
        let current_char = message[cursor];
        match &self.values[current_idx] {
            Rule::Literal(c) => {
                if *c != current_char {
                    return RuleResult::Invalid;
                }
                println!("Found char: {c} @ {cursor}");
                cursor += 1;
            }
            Rule::List(list) => {
                for id in list {
                    match self.is_valid_rule(*id, cursor, message) {
                        RuleResult::Overflow => return RuleResult::Overflow,
                        RuleResult::Invalid => return RuleResult::Invalid,
                        RuleResult::Success(offset) => cursor = offset,
                    }
                }
            }
            Rule::Or((lhs, rhs)) => {
                let base_cursor = cursor;
                let mut failed = false;
                for id in lhs {
                    match self.is_valid_rule(*id, cursor, message) {
                        RuleResult::Overflow => return RuleResult::Overflow,
                        RuleResult::Invalid => {
                            failed = true;
                            break;
                        }
                        RuleResult::Success(offset) => cursor = offset,
                    }
                }
                if failed {
                    cursor = base_cursor;
                    for id in rhs {
                        match self.is_valid_rule(*id, cursor, message) {
                            RuleResult::Overflow => return RuleResult::Overflow,
                            RuleResult::Invalid => return RuleResult::Invalid,
                            RuleResult::Success(offset) => cursor = offset,
                        }
                    }
                }
            }
            Rule::Endless(rule_id) => {
                let mut last_success_cursor = cursor;
                loop {
                    match self.is_valid_rule(*rule_id, last_success_cursor, message) {
                        RuleResult::Overflow => {
                            break;
                        }
                        RuleResult::Invalid => return RuleResult::Invalid,
                        RuleResult::Success(offset) => {
                            last_success_cursor = offset;
                        }
                    }
                }
                cursor = last_success_cursor;
            }
            Rule::Midless((lhs, rhs)) => {
                let mut loops = 0;
                loop {
                    loops += 1;
                    let mut tmp_cursor = cursor;
                    for _ in 0..loops {
                        match self.is_valid_rule(*lhs, tmp_cursor, message) {
                            RuleResult::Overflow => return RuleResult::Overflow,
                            RuleResult::Invalid => return RuleResult::Invalid,
                            RuleResult::Success(offset) => {
                                tmp_cursor = offset;
                            }
                        }
                    }
                    for _ in 0..loops {
                        match self.is_valid_rule(*rhs, tmp_cursor, message) {
                            RuleResult::Overflow => {
                                dbg!("overflow");
                                return RuleResult::Overflow;
                            }
                            RuleResult::Invalid => {
                                dbg!("invalid");
                                break;
                            }
                            RuleResult::Success(offset) => {
                                dbg!("suceess");
                                tmp_cursor = offset;
                            }
                        }
                    }

                    if tmp_cursor == message.len() {
                        return RuleResult::Success(tmp_cursor);
                    }
                }
            }
        }
        RuleResult::Success(cursor)
    }
}

impl FromStr for Rules {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lines = s.lines();
        let mut rules = vec![None; lines.clone().count()];
        for line in lines {
            let (id, rule) = match parse_rule_line(line.trim()).finish() {
                Ok((_, val)) => val,
                Err(nom::error::Error { input, code }) => {
                    return Err(anyhow::Error::new(nom::error::Error {
                        code,
                        input: input.to_string(),
                    }))
                }
            };
            rules[id] = Some(rule);
        }
        let rules = rules.into_iter().flatten().collect::<Vec<_>>();
        Ok(Rules { values: rules })
    }
}

fn parse_literal(input: &str) -> IResult<&str, char> {
    let (input, _) = tag("\"")(input)?;
    let (input, val) = take_until("\"")(input)?;

    let lit = val.chars().next().unwrap();
    Ok((input, lit))
}

fn parse_id_list(input: &str) -> IResult<&str, Vec<RuleId>> {
    let (input, rules) = multi::many1(parse_id)(input)?;
    Ok((input, rules))
}

fn parse_id(input: &str) -> IResult<&str, RuleId> {
    let (input, (_, id)) = sequence::tuple((complete::space0, complete::digit1))(input)?;

    let id: usize = id.parse().unwrap();
    Ok((input, id))
}

fn parse_rule_literal(input: &str) -> IResult<&str, Rule> {
    let (input, val) = parse_literal(input)?;

    Ok((input, Rule::Literal(val)))
}

fn parse_rule_list(input: &str) -> IResult<&str, Rule> {
    let (input, val) = parse_id_list(input)?;

    Ok((input, Rule::List(val)))
}

fn parse_rule_or(input: &str) -> IResult<&str, Rule> {
    let (input, lhs) = parse_id_list(input)?;
    let (input, _) = tag(" | ")(input)?;
    let (input, rhs) = parse_id_list(input)?;

    Ok((input, Rule::Or((lhs, rhs))))
}

fn parse_rule_line(input: &str) -> IResult<&str, (RuleId, Rule)> {
    let (input, id) = take_until(":")(input)?;
    let id: usize = id.parse().unwrap();
    let (input, _) = tag(": ")(input)?;

    let (input, rule) = alt((parse_rule_literal, parse_rule_or, parse_rule_list))(input)?;

    Ok((input, (id, rule)))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_rules() {
        let input = "0: 1 2
            2: 1 3 | 3 1
            1: \"a\"
            3: \"b\"";
        let rules: Rules = input.parse().unwrap();

        assert_eq!(rules.values.len(), 4);
        assert_eq!(rules.values[0], Rule::List(vec![1, 2]));
        assert_eq!(rules.values[1], Rule::Literal('a'));
        assert_eq!(rules.values[2], Rule::Or((vec![1, 3], vec![3, 1])));
        assert_eq!(rules.values[3], Rule::Literal('b'));
    }

    #[test]
    fn test_input() {
        let input = "0: 4 1 5
            1: 2 3 | 3 2
            2: 4 4 | 5 5
            3: 4 5 | 5 4
            4: \"a\"
            5: \"b\"

            ababbb
            bababa
            abbbab
            aaabbb
            aaaabbb";

        let (rules, messages) = parse_input(input).unwrap();

        assert_eq!(rules.values.len(), 6);
        assert_eq!(messages.len(), 5);
    }

    #[test]
    fn test_validation_simple() {
        let input = "0: 1 2
            2: 1 3 | 3 1
            1: \"a\"
            3: \"b\"

            aab
            aba
            baa
            ";

        let (rules, messages) = parse_input(input).unwrap();

        assert!(rules.is_valid(&messages[0]));
        assert!(rules.is_valid(&messages[1]));
        assert!(!rules.is_valid(&messages[2]));
    }

    #[test]
    fn test_validation_advanced() {
        let input = "0: 4 1 5
            1: 2 3 | 3 2
            2: 4 4 | 5 5
            3: 4 5 | 5 4
            4: \"a\"
            5: \"b\"

            ababbb
            bababa
            abbbab
            aaabbb
            aaaabbb";

        let (rules, messages) = parse_input(input).unwrap();

        assert!(rules.is_valid(&messages[0]));
        assert!(!rules.is_valid(&messages[1]));
        assert!(rules.is_valid(&messages[2]));
        assert!(!rules.is_valid(&messages[3]));
        assert!(!rules.is_valid(&messages[4]));
    }

    #[test]
    fn test_validation_loop() {
        let input = "0: 8 11
            1: \"a\"
            2: 1 24 | 14 4
            3: 5 14 | 16 1
            4: 1 1
            5: 1 14 | 15 1
            6: 14 14 | 1 14
            7: 14 5 | 1 21
            8: 42
            9: 14 27 | 1 26
            10: 23 14 | 28 1
            11: 42 31
            12: 24 14 | 19 1
            13: 14 3 | 1 12
            14: \"b\"
            15: 1 | 14
            16: 15 1 | 14 14
            17: 14 2 | 1 7
            18: 15 15
            19: 14 1 | 14 14
            20: 14 14 | 1 15
            21: 14 1 | 1 14
            22: 14 14
            23: 25 1 | 22 14
            24: 14 1
            25: 1 1 | 1 14
            26: 14 22 | 1 20
            27: 1 6 | 14 18
            28: 16 1
            29: 29
            30: 30
            31: 14 17 | 1 13
            32: 32
            33: 33
            34: 34
            35: 35
            36: 36
            37: 37
            38: 38
            39: 39
            40: 40
            41: 41
            42: 9 14 | 10 1

            abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
            bbabbbbaabaabba
            babbbbaabbbbbabbbbbbaabaaabaaa
            aaabbbbbbaaaabaababaabababbabaaabbababababaaa
            bbbbbbbaaaabbbbaaabbabaaa
            bbbababbbbaaaaaaaabbababaaababaabab
            ababaaaaaabaaab
            ababaaaaabbbaba
            baabbaaaabbaaaababbaababb
            abbbbabbbbaaaababbbbbbaaaababb
            aaaaabbaabaaaaababaa
            aaaabbaaaabbaaa
            aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
            babaaabbbaaabaababbaabababaaab
            aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba";

        let (mut rules, messages) = parse_input(input).unwrap();

        rules.fix();
        assert!(rules.is_valid(&messages[1]));
        assert!(rules.is_valid(&messages[2]));
        assert!(rules.is_valid(&messages[3]));
        assert!(rules.is_valid(&messages[4]));
        assert!(rules.is_valid(&messages[5]));
        assert!(rules.is_valid(&messages[6]));
        assert!(rules.is_valid(&messages[7]));
        assert!(rules.is_valid(&messages[8]));
        assert!(rules.is_valid(&messages[9]));
        assert!(rules.is_valid(&messages[10]));
        assert!(rules.is_valid(&messages[12]));
        assert!(rules.is_valid(&messages[14]));

        assert!(!rules.is_valid(&messages[0]));
        assert!(!rules.is_valid(&messages[11]));
        assert!(!rules.is_valid(&messages[13]));
    }

    #[test]
    fn test_validation_loop_simple_1() {
        let input = "0: 1 2
            1: 3 4
            2: 1 2 | 1
            3: \"a\"
            4: \"b\"

            ababab";

        let (mut rules, messages) = parse_input(input).unwrap();
        rules.values[2] = Rule::Endless(1);

        assert!(rules.is_valid(&messages[0]));
    }

    #[test]
    fn test_validation_loop_simple_2() {
        let input = "0: 1 2
            1: 4
            2: 1 2 3 | 1 3
            3: \"a\"
            4: \"b\"

            bba
            bbbbaaa
            bbbbbbbaa";
        //  01234
        let (mut rules, messages) = parse_input(input).unwrap();
        rules.values[2] = Rule::Midless((1, 3));

        assert!(rules.is_valid(&messages[0]));
        assert!(rules.is_valid(&messages[1]));
        assert!(!rules.is_valid(&messages[2]));
    }

    #[test]
    fn test_validation_loop_simple_3() {
        let input = "0: 1 2
            1: 5
            2: 1 2 4 | 1 4
            3: 6
            4: 6 | 6 4
            5: \"a\"
            6: \"b\"

            aab
            aabbb
            aaaabbbbbbbb";
        let (mut rules, messages) = parse_input(input).unwrap();
        rules.values[2] = Rule::Midless((1, 4));
        rules.values[4] = Rule::Endless(6);

        // assert!(rules.is_valid(&messages[0]));
        // assert!(rules.is_valid(&messages[1]));
        assert!(rules.is_valid(&messages[2]));
    }
}
