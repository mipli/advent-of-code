use std::{collections::HashSet, str::FromStr};

use anyhow::bail;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Eight, |input| {
        let mut machine: Machine = input.parse().expect("Failed to parse input");
        println!("Loop accumulation value: {:?}", machine.run());
        println!("Fixed accumulation value: {:?}", machine.fix());
    })
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum MachineResult {
    Success(i32),
    Loop(i32),
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum StepResult {
    Done,
    Success,
}

#[derive(Debug)]
pub struct Machine {
    commands: Vec<Command>,
    cursor: usize,
    accumulator: i32,
}

impl Machine {
    fn reset(&mut self) {
        self.cursor = 0;
        self.accumulator = 0;
    }
    pub fn fix(&mut self) -> i32 {
        let mut pos = self
            .commands
            .iter()
            .position(|c| c.op == Op::Jmp || c.op == Op::Nop)
            .expect("Should have at least one jmp or npo command");
        let mut command = self.commands[pos];

        loop {
            match command.op {
                Op::Jmp => self.commands[pos].op = Op::Nop,
                Op::Nop => self.commands[pos].op = Op::Jmp,
                Op::Acc => unreachable!(),
            }
            match self.run() {
                MachineResult::Success(v) => return v,
                MachineResult::Loop(_) => {
                    self.commands[pos] = command;
                    pos = pos
                        + 1
                        + self
                            .commands
                            .iter()
                            .skip(pos + 1)
                            .position(|c| c.op == Op::Jmp || c.op == Op::Nop)
                            .expect("Should have at least one jmp or npo command");
                    command = self.commands[pos];
                }
            }
        }
    }

    pub fn run(&mut self) -> MachineResult {
        self.reset();
        let mut executed: HashSet<usize> = HashSet::default();
        loop {
            if executed.contains(&self.cursor) {
                return MachineResult::Loop(self.accumulator);
            }
            executed.insert(self.cursor);
            if matches!(self.step(), StepResult::Done) {
                return MachineResult::Success(self.accumulator);
            }
        }
    }

    pub fn step(&mut self) -> StepResult {
        if self.cursor >= self.commands.len() {
            return StepResult::Done;
        }
        let command = self.commands[self.cursor];
        self.cursor = match command.op {
            Op::Nop => self.cursor + 1,
            Op::Acc => {
                self.accumulator += command.value;
                self.cursor + 1
            }
            Op::Jmp => ((self.cursor as i32) + command.value) as usize,
        };
        StepResult::Success
    }
}

impl FromStr for Machine {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let commands = s
            .lines()
            .map(|line| line.parse().expect("Failed to parse line"))
            .collect::<Vec<_>>();
        Ok(Machine {
            commands,
            cursor: 0,
            accumulator: 0,
        })
    }
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub struct Command {
    op: Op,
    value: i32,
}

impl FromStr for Command {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(' ');
        let op: Op = split.next().expect("Invalid command op code").parse()?;
        let value: i32 = split.next().expect("Invalid command value").parse()?;
        Ok(Command { op, value })
    }
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum Op {
    Nop,
    Jmp,
    Acc,
}

impl FromStr for Op {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "nop" => Ok(Op::Nop),
            "jmp" => Ok(Op::Jmp),
            "acc" => Ok(Op::Acc),
            _ => bail!("Invalid OP code"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_parse_commands() {
        let machine: Machine = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"
            .parse()
            .expect("Failed to parse input");
        assert_eq!(machine.commands.len(), 9);
    }

    #[test]
    pub fn test_find_loop() {
        let mut machine: Machine = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"
            .parse()
            .expect("Failed to parse input");
        assert_eq!(machine.run(), MachineResult::Loop(5));
    }

    #[test]
    pub fn test_fix() {
        let mut machine: Machine = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"
            .parse()
            .expect("Failed to parse input");
        let val = machine.fix();
        assert_eq!(val, 8);
    }
}
