use anyhow::bail;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Eighteen, |input| {
        let summed = input
            .lines()
            .map(evaluate_simple)
            .map(|r| r.unwrap())
            .sum::<i64>();
        let summed_adv = input.lines().map(evaluate).map(|r| r.unwrap()).sum::<i64>();

        println!("Solution to part 1: {summed}");
        println!("Solution to part 2: {summed_adv}");
    })
}

fn evaluate_simple(line: &str) -> anyhow::Result<i64> {
    let tokens = tokenize(line)?;
    let mut parser = ParserSimple::new(tokens);
    let expr = parser.parse();

    Ok(expr.evaluate())
}

fn evaluate(line: &str) -> anyhow::Result<i64> {
    let tokens = tokenize(line)?;
    let mut parser = ParserAdvanced::new(tokens);
    let expr = parser.parse();

    Ok(expr.evaluate())
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum Expr {
    Paren(Box<Expr>),
    Addition(Box<Expr>, Box<Expr>),
    Multiplication(Box<Expr>, Box<Expr>),
    Literal(i64),
}

impl Expr {
    fn evaluate(&self) -> i64 {
        match self {
            Expr::Paren(expr) => expr.evaluate(),
            Expr::Addition(lhs, rhs) => lhs.evaluate() + rhs.evaluate(),
            Expr::Multiplication(lhs, rhs) => lhs.evaluate() * rhs.evaluate(),
            Expr::Literal(val) => *val,
        }
    }
}
#[derive(Default)]
struct ParserAdvanced {
    tokens: Vec<Token>,
    cursor: usize,
}

impl ParserAdvanced {
    fn new(tokens: Vec<Token>) -> Self {
        ParserAdvanced { tokens, cursor: 0 }
    }

    fn parse(&mut self) -> Expr {
        self.tokens.reverse();
        let expr = self.parse_mult();
        assert_eq!(self.tokens.len(), self.cursor);
        expr
    }

    fn parse_mult(&mut self) -> Expr {
        let mut expr = self.parse_plus();
        if self.is_empty() {
            return expr;
        }
        if self.advance_if(&[Token::Star]).is_some() {
            let rhs = self.parse_mult();
            expr = Expr::Multiplication(Box::new(expr), Box::new(rhs));
        }
        expr
    }

    fn parse_plus(&mut self) -> Expr {
        let mut expr = self.parse_primary();
        if self.is_empty() {
            return expr;
        }
        if self.advance_if(&[Token::Plus]).is_some() {
            let rhs = self.parse_plus();
            expr = Expr::Addition(Box::new(expr), Box::new(rhs));
        }
        expr
    }

    fn parse_primary(&mut self) -> Expr {
        match self.peek() {
            Token::ParenLeft => {
                self.advance();
                let expr = self.parse_mult();
                if self.peek() != &Token::ParenRight {
                    panic!("Unmatched right paren");
                }
                self.advance();

                Expr::Paren(Box::new(expr))
            }
            Token::Literal(num) => {
                let expr = Expr::Literal(*num as i64);
                self.advance();
                expr
            }
            _ => panic!("Invalid primary token"),
        }
    }

    fn peek(&self) -> &Token {
        &self.tokens[self.cursor]
    }

    fn advance(&mut self) {
        self.cursor += 1;
    }

    fn advance_if(&mut self, tokens: &[Token]) -> Option<Token> {
        if tokens.contains(&self.tokens[self.cursor]) {
            self.cursor += 1;
            Some(self.tokens[self.cursor - 1])
        } else {
            None
        }
    }

    fn is_empty(&self) -> bool {
        self.cursor == self.tokens.len()
    }
}

#[derive(Default)]
struct ParserSimple {
    tokens: Vec<Token>,
    cursor: usize,
}

impl ParserSimple {
    fn new(tokens: Vec<Token>) -> Self {
        ParserSimple { tokens, cursor: 0 }
    }

    fn parse(&mut self) -> Expr {
        let mut expr = self.parse_primary();
        if self.is_empty() {
            return expr;
        }
        if let Some(token) = self.advance_if(&[Token::Plus, Token::Star]) {
            match token {
                Token::Plus => {
                    let rhs = self.parse();
                    expr = Expr::Addition(Box::new(expr), Box::new(rhs));
                }
                Token::Star => {
                    let rhs = self.parse();
                    expr = Expr::Multiplication(Box::new(expr), Box::new(rhs));
                }
                _ => panic!("Invalid binary token"),
            };
        }
        expr
    }

    fn parse_primary(&mut self) -> Expr {
        match self.peek() {
            Token::ParenRight => {
                self.advance();
                let expr = self.parse();
                if self.peek() != &Token::ParenLeft {
                    panic!("Unmatched right paren");
                }
                self.advance();

                Expr::Paren(Box::new(expr))
            }
            Token::Literal(num) => {
                let expr = Expr::Literal(*num as i64);
                self.advance();
                expr
            }
            _ => panic!("Invalid primary token"),
        }
    }

    fn peek(&self) -> &Token {
        &self.tokens[self.cursor]
    }

    fn advance(&mut self) {
        self.cursor += 1;
    }

    fn advance_if(&mut self, tokens: &[Token]) -> Option<Token> {
        if tokens.contains(&self.tokens[self.cursor]) {
            self.cursor += 1;
            Some(self.tokens[self.cursor - 1])
        } else {
            None
        }
    }

    fn is_empty(&self) -> bool {
        self.cursor == self.tokens.len()
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Token {
    Literal(u32),
    ParenLeft,
    ParenRight,
    Plus,
    Star,
}

fn tokenize(input: &str) -> anyhow::Result<Vec<Token>> {
    let mut tokens = vec![];

    for c in input.chars() {
        let token = match c {
            ' ' => None,
            '(' => Some(Token::ParenLeft),
            ')' => Some(Token::ParenRight),
            '+' => Some(Token::Plus),
            '*' => Some(Token::Star),
            '0'..='9' => Some(Token::Literal(c.to_digit(10).unwrap())),
            _ => bail!("Invalid token: '{c}'"),
        };

        if let Some(token) = token {
            tokens.push(token);
        }
    }
    tokens.reverse();
    Ok(tokens)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        let input = "1 + 2 * 3 + 4 * 5 + 6";

        let tokens = tokenize(input).unwrap();

        assert_eq!(
            tokens,
            vec![
                Token::Literal(6),
                Token::Plus,
                Token::Literal(5),
                Token::Star,
                Token::Literal(4),
                Token::Plus,
                Token::Literal(3),
                Token::Star,
                Token::Literal(2),
                Token::Plus,
                Token::Literal(1),
            ]
        );
        let mut parser = ParserSimple::new(tokens);
        let expr = parser.parse();

        let val = expr.evaluate();
        assert_eq!(val, 71);
    }

    #[test]
    fn test_paren() {
        let input = "1 + (2 * 3) + (4 * (5 + 6))";

        let tokens = tokenize(input).unwrap();
        let mut parser = ParserSimple::new(tokens);
        let expr = parser.parse();

        let val = expr.evaluate();
        assert_eq!(val, 51);
    }

    #[test]
    fn test_example() {
        let input = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2";

        let tokens = tokenize(input).unwrap();
        let mut parser = ParserSimple::new(tokens);
        let expr = parser.parse();

        let val = expr.evaluate();
        assert_eq!(val, 13632);
    }

    #[test]
    fn test_advanced() {
        let input = "2 + 2 * 3";

        let tokens = tokenize(input).unwrap();
        let mut parser = ParserAdvanced::new(tokens);
        let expr = parser.parse();

        let val = expr.evaluate();
        assert_eq!(val, 12);
    }

    #[test]
    fn test_example_1_adv() {
        let input = "1 + (2 * 3) + (4 * (5 + 6))";

        let tokens = tokenize(input).unwrap();
        let mut parser = ParserAdvanced::new(tokens);
        let expr = parser.parse();

        let val = expr.evaluate();
        assert_eq!(val, 51);
    }

    #[test]
    fn test_example_2_adv() {
        let input = "2 * 3 + (4 * 5)";

        let tokens = tokenize(input).unwrap();
        let mut parser = ParserAdvanced::new(tokens);
        let expr = parser.parse();

        let val = expr.evaluate();
        assert_eq!(val, 46);
    }

    #[test]
    fn test_example_3_adv() {
        let input = "5 + (8 * 3 + 9 + 3 * 4 * 3)";

        let tokens = tokenize(input).unwrap();
        let mut parser = ParserAdvanced::new(tokens);
        let expr = parser.parse();

        let val = expr.evaluate();
        assert_eq!(val, 1445);
    }

    #[test]
    fn test_example_4_adv() {
        let input = "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))";

        let tokens = tokenize(input).unwrap();
        let mut parser = ParserAdvanced::new(tokens);
        let expr = parser.parse();

        let val = expr.evaluate();
        assert_eq!(val, 669060);
    }

    #[test]
    fn test_example_5_adv() {
        let input = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2";

        let tokens = tokenize(input).unwrap();
        let mut parser = ParserAdvanced::new(tokens);
        let expr = parser.parse();

        let val = expr.evaluate();
        assert_eq!(val, 23340);
    }
}
