use std::str::FromStr;

use libaoc::{DayNumber, Solver};
use nom::{bytes::complete::tag, character, Finish, IResult};

pub fn get() -> Solver {
    Solver::new(DayNumber::Two, |input| {
        let passwords = input
            .lines()
            .map(|line| line.parse().expect("Failed to parse line"))
            .collect::<Vec<_>>();
        let mut problem = Problem { passwords };

        println!("Part 1: {}", problem.solve_part_1());
        println!("Part 2: {}", problem.solve_part_2());
    })
}

#[derive(Debug, Clone, Copy)]
struct Rule {
    glyph: char,
    fst: i32,
    snd: i32,
}

#[derive(Debug, Clone)]
struct Password {
    policy: Rule,
    value: String,
}

impl FromStr for Password {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match parse_password(s).finish() {
            Ok((_rem, pass)) => Ok(pass),
            Err(nom::error::Error { input, code }) => Err(Box::new(nom::error::Error {
                code,
                input: input.to_string(),
            })),
        }
    }
}

fn parse_password(input: &str) -> IResult<&str, Password> {
    let (input, rule) = parse_rule(input)?;
    let (input, _) = tag(": ")(input)?;
    let (input, value) = character::complete::alphanumeric1(input)?;

    Ok((
        input,
        Password {
            policy: rule,
            value: value.to_string(),
        },
    ))
}

fn parse_rule(input: &str) -> IResult<&str, Rule> {
    let (input, fst) = character::complete::i32(input)?;
    let (input, _) = tag("-")(input)?;
    let (input, snd) = character::complete::i32(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, glyph) = character::complete::anychar(input)?;

    Ok((input, Rule { fst, snd, glyph }))
}

impl Password {
    pub fn is_valid_old(&self) -> bool {
        let count = self
            .value
            .chars()
            .filter(|&c| c == self.policy.glyph)
            .count() as i32;
        count >= self.policy.fst && count <= self.policy.snd
    }

    pub fn is_valid(&self) -> bool {
        let fst = self.value.chars().nth((self.policy.fst as usize) - 1) == Some(self.policy.glyph);
        let snd = self.value.chars().nth((self.policy.snd as usize) - 1) == Some(self.policy.glyph);

        fst != snd && (fst || snd)
    }
}

pub struct Problem {
    passwords: Vec<Password>,
}

impl Problem {
    fn solve_part_1(&mut self) -> i32 {
        self.passwords
            .iter()
            .filter(|pass| pass.is_valid_old())
            .count() as i32
    }

    fn solve_part_2(&mut self) -> i32 {
        self.passwords.iter().filter(|pass| pass.is_valid()).count() as i32
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_parsing() {
        let password: Password = "1-3 a: acd".parse().expect("Failed to parse");
        assert_eq!(password.value, "acd".to_string());
        assert_eq!(password.policy.fst, 1);
        assert_eq!(password.policy.snd, 3);
        assert_eq!(password.policy.glyph, 'a');
        assert!(password.is_valid());

        let password: Password = "1-3 b: cdefg".parse().expect("Failed to parse");
        assert!(!password.is_valid());
    }

    #[test]
    pub fn test_example_1() {
        let mut problem = Problem {
            passwords: vec![
                "1-3 a: abcde".parse::<Password>().expect("Failed to parse"),
                "1-3 b: cdefg".parse::<Password>().expect("Failed to parse"),
                "2-9 c: ccccccccc"
                    .parse::<Password>()
                    .expect("Failed to parse"),
            ],
        };

        assert_eq!(problem.solve_part_1(), 2);
    }

    #[test]
    pub fn test_example_2() {
        let mut problem = Problem {
            passwords: vec![
                "1-3 a: abcde".parse::<Password>().expect("Failed to parse"),
                "1-3 b: cdefg".parse::<Password>().expect("Failed to parse"),
                "2-9 c: ccccccccccc"
                    .parse::<Password>()
                    .expect("Failed to parse"),
            ],
        };

        assert_eq!(problem.solve_part_2(), 1);
    }
}
