use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::One, |input| {
        let entries = input
            .lines()
            .map(|line| line.parse().expect("Failed to parse number"))
            .collect::<Vec<_>>();
        println!("Part 1: {}", solve_part_1(&entries));
        println!("Part 2: {}", solve_part_2(&entries));
    })
}

pub fn solve_part_1(entries: &[i32]) -> i32 {
    let (_, a) = entries
        .iter()
        .enumerate()
        .find(|(i, &a)| entries.iter().skip(*i).any(|&b| a + b == 2020))
        .expect("Could not find matching values");
    a * (2020 - a)
}

pub fn solve_part_2(entries: &[i32]) -> i32 {
    let (a, b) = entries
        .iter()
        .enumerate()
        .find_map(|(i, &a)| {
            if let Some((_, b)) = entries.iter().enumerate().skip(i).find(|(j, &b)| {
                entries
                    .iter()
                    .enumerate()
                    .skip(*j)
                    .any(|(_, &c)| a + b + c == 2020)
            }) {
                Some((a, b))
            } else {
                None
            }
        })
        .expect("Could not find matching values");
    a * b * (2020 - a - b)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_example_1() {
        let solution = solve_part_1(vec![1721, 979, 366, 299, 675, 1456].as_slice());
        assert_eq!(514579, solution);
    }

    #[test]
    pub fn test_example_2() {
        let solution = solve_part_2(vec![1721, 979, 366, 299, 675, 1456].as_slice());
        assert_eq!(241861950, solution);
    }
}
