use std::{
    collections::{HashMap, HashSet},
    fmt,
    str::FromStr,
};

use anyhow::bail;
use libaoc::{point::Neighbours, point3::Point3, point4::Point4, DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Seventeen, |input| {
        let conway_cube: ConwayCube<Point3> = input.parse().unwrap();

        let conway_cube = conway_cube.step(6);
        println!("Lit after 6 steps: {}", conway_cube.count_active());

        let conway_cube: ConwayCube<Point4> = input.parse().unwrap();

        let conway_cube = conway_cube.step(6);
        println!(
            "Lit after 6 steps, 4d version: {}",
            conway_cube.count_active()
        );
    })
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum CubeState {
    Active,
    Inactive,
}
impl fmt::Display for CubeState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            CubeState::Active => write!(f, "#"),
            CubeState::Inactive => write!(f, "."),
        }
    }
}

#[derive(Clone, Default)]
struct ConwayCube<T: Neighbours> {
    state: HashMap<T, CubeState>,
}

#[derive(Debug)]
struct MinMax {
    pub x_max: i32,
    pub x_min: i32,
    pub y_max: i32,
    pub y_min: i32,
    pub z_max: i32,
    pub z_min: i32,
}

impl Default for MinMax {
    fn default() -> Self {
        Self {
            x_max: i32::MIN,
            x_min: i32::MAX,
            y_max: i32::MIN,
            y_min: i32::MAX,
            z_max: i32::MIN,
            z_min: i32::MAX,
        }
    }
}

impl<T: Neighbours + std::hash::Hash + std::cmp::Eq + Copy> ConwayCube<T> {
    fn get(&self, point: T) -> CubeState {
        match self.state.get(&point) {
            Some(cell) => *cell,
            _ => CubeState::Inactive,
        }
    }

    fn count_active(&self) -> usize {
        self.state
            .values()
            .filter(|&&s| s == CubeState::Active)
            .count()
    }

    fn step(self, steps: usize) -> ConwayCube<T> {
        let mut new_state = self.state;
        let mut to_check: HashSet<T> = HashSet::default();

        for _ in 0..steps {
            let state = std::mem::take(&mut new_state);
            for point in state.keys() {
                to_check.insert(*point);
                for n in point.neighbours() {
                    to_check.insert(n);
                }
            }

            for point in to_check.drain() {
                let active = point
                    .neighbours()
                    .into_iter()
                    .filter(|p| state.get(p) == Some(&CubeState::Active))
                    .take(4)
                    .count();
                let new_cube = match state.get(&point) {
                    Some(CubeState::Active) if active == 2 || active == 3 => CubeState::Active,
                    Some(CubeState::Active) => CubeState::Inactive,
                    Some(CubeState::Inactive) | None if active == 3 => CubeState::Active,
                    Some(CubeState::Inactive) | None => CubeState::Inactive,
                };
                if new_cube == CubeState::Active {
                    new_state.insert(point, new_cube);
                }
            }
        }

        ConwayCube { state: new_state }
    }
}

impl ConwayCube<Point3> {
    fn min_max(&self) -> MinMax {
        self.state.keys().fold(MinMax::default(), |min_max, point| {
            let (x_min, x_max) = (
                std::cmp::min(min_max.x_min, point.x),
                std::cmp::max(min_max.x_max, point.x),
            );
            let (y_min, y_max) = (
                std::cmp::min(min_max.y_min, point.y),
                std::cmp::max(min_max.y_max, point.y),
            );
            let (z_min, z_max) = (
                std::cmp::min(min_max.z_min, point.z),
                std::cmp::max(min_max.z_max, point.z),
            );
            MinMax {
                x_min,
                x_max,
                y_min,
                y_max,
                z_min,
                z_max,
            }
        })
    }
}

impl fmt::Display for ConwayCube<Point3> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let min_max = self.min_max();
        for z in min_max.z_min..=min_max.z_max {
            writeln!(f, "Z: {z}")?;
            for y in min_max.y_min..=min_max.y_max {
                for x in min_max.x_min..=min_max.x_max {
                    let cube = self.get((x, y, z).into());
                    write!(f, "{cube}")?;
                }

                #[allow(clippy::writeln_empty_string)]
                writeln!(f, "")?;
            }
        }
        Ok(())
    }
}

impl FromStr for ConwayCube<Point3> {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let z = 0;
        let mut x = 0;
        let mut y = 0;
        let mut state = HashMap::default();
        for c in s.chars() {
            match c {
                '\n' => {
                    y += 1;
                    x = 0;
                }
                '#' => {
                    state.insert((x, y, z).into(), CubeState::Active);
                    x += 1;
                }
                '.' => {
                    x += 1;
                }
                ' ' => {}
                _ => bail!("Could not parse: '{}'", c),
            }
        }
        Ok(ConwayCube { state })
    }
}

impl FromStr for ConwayCube<Point4> {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let z = 0;
        let w = 0;
        let mut x = 0;
        let mut y = 0;
        let mut state = HashMap::default();
        for c in s.chars() {
            match c {
                '\n' => {
                    y += 1;
                    x = 0;
                }
                '#' => {
                    state.insert((x, y, z, w).into(), CubeState::Active);
                    x += 1;
                }
                '.' => {
                    x += 1;
                }
                ' ' => {}
                _ => bail!("Could not parse: '{}'", c),
            }
        }
        Ok(ConwayCube { state })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        let input = ".#.
            ..#
            ###";
        let conway_cube: ConwayCube<Point3> = input.parse().unwrap();
        assert_eq!(conway_cube.get((0, 0, 0).into()), CubeState::Inactive);
        assert_eq!(conway_cube.get((1, 0, 0).into()), CubeState::Active);
        assert_eq!(conway_cube.get((2, 0, 0).into()), CubeState::Inactive);
        assert_eq!(conway_cube.get((0, 1, 0).into()), CubeState::Inactive);
        assert_eq!(conway_cube.get((1, 1, 0).into()), CubeState::Inactive);
        assert_eq!(conway_cube.get((2, 1, 0).into()), CubeState::Active);
        assert_eq!(conway_cube.get((0, 2, 0).into()), CubeState::Active);
        assert_eq!(conway_cube.get((1, 2, 0).into()), CubeState::Active);
        assert_eq!(conway_cube.get((2, 2, 0).into()), CubeState::Active);
    }

    #[test]
    fn test_step() {
        let input = ".#.
            ..#
            ###";
        let conway_cube: ConwayCube<Point3> = input.parse().unwrap();

        let conway_cube = conway_cube.step(1);
        assert_eq!(conway_cube.count_active(), 11);
        let conway_cube = conway_cube.step(5);
        assert_eq!(conway_cube.count_active(), 112);
    }

    #[test]
    fn test_step_4d() {
        let input = ".#.
            ..#
            ###";
        let conway_cube: ConwayCube<Point4> = input.parse().unwrap();

        let conway_cube = conway_cube.step(6);
        assert_eq!(conway_cube.count_active(), 848);
    }
}
