use std::str::FromStr;

use anyhow::anyhow;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Twelve, |input| {
        let instructions: Vec<Instruction> = input
            .lines()
            .map(|line| line.parse().expect("Failed to parse instruction"))
            .collect::<Vec<_>>();
        let mut boat = Boat::default();

        boat.handle_simple(&instructions);
        println!(
            "Distance moved with wrong interpretation: {}",
            manhattan_distance((0, 0).into(), boat.position)
        );

        let mut boat = Boat::default();

        boat.handle_waypoint(&instructions);
        println!(
            "Moved by waypoint: {}",
            manhattan_distance((0, 0).into(), boat.position)
        );
    })
}

#[derive(Debug)]
pub struct Boat {
    position: Position,
    facing: Direction,
    waypoint: Position,
}

impl Default for Boat {
    fn default() -> Self {
        Boat {
            position: (0, 0).into(),
            facing: Direction::East,
            waypoint: (10, 1).into(),
        }
    }
}

impl Boat {
    pub fn handle_simple(&mut self, instructions: &[Instruction]) {
        for inst in instructions {
            match inst.action {
                Action::North => self.position = self.position + (0, -inst.value).into(),
                Action::South => self.position = self.position + (0, inst.value).into(),
                Action::East => self.position = self.position + (inst.value, 0).into(),
                Action::West => self.position = self.position + (-inst.value, 0).into(),
                Action::Forward => {
                    self.position = self.position + get_velocity(self.facing, inst.value)
                }
                Action::Right => self.facing = rotate_right(self.facing, inst.value),
                Action::Left => self.facing = rotate_left(self.facing, inst.value),
            }
        }
    }

    pub fn handle_waypoint(&mut self, instructions: &[Instruction]) {
        for inst in instructions {
            match inst.action {
                Action::North => self.waypoint = self.waypoint + (0, inst.value).into(),
                Action::South => self.waypoint = self.waypoint + (0, -inst.value).into(),
                Action::East => self.waypoint = self.waypoint + (inst.value, 0).into(),
                Action::West => self.waypoint = self.waypoint + (-inst.value, 0).into(),
                Action::Forward => self.position = self.position + (self.waypoint * inst.value),
                Action::Right => self.rotate_waypoint_right(inst.value),
                Action::Left => self.rotate_waypoint_left(inst.value),
            }
        }
    }

    fn rotate_waypoint_left(&mut self, degrees: i32) {
        let steps = degrees / 90;
        for _ in 0..steps {
            let ox = self.waypoint.x;
            self.waypoint.x = -self.waypoint.y;
            self.waypoint.y = ox;
        }
    }

    fn rotate_waypoint_right(&mut self, degrees: i32) {
        let steps = degrees / 90;
        for _ in 0..steps {
            let ox = self.waypoint.x;
            self.waypoint.x = self.waypoint.y;
            self.waypoint.y = -ox;
        }
    }
}

fn manhattan_distance(a: Position, b: Position) -> i32 {
    (a.x - b.x).abs() + (a.y - b.y).abs()
}

fn get_velocity(dir: Direction, speed: i32) -> Position {
    match dir {
        Direction::North => (0, -speed),
        Direction::South => (0, speed),
        Direction::East => (speed, 0),
        Direction::West => (-speed, 0),
    }
    .into()
}

fn rotate_left(mut facing: Direction, degrees: i32) -> Direction {
    use Direction::*;

    let steps = degrees / 90;
    for _ in 0..steps {
        facing = match facing {
            North => West,
            West => South,
            South => East,
            East => North,
        }
    }
    facing
}

fn rotate_right(mut facing: Direction, degrees: i32) -> Direction {
    use Direction::*;

    let steps = degrees / 90;
    for _ in 0..steps {
        facing = match facing {
            North => East,
            East => South,
            South => West,
            West => North,
        }
    }
    facing
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
struct Position {
    x: i32,
    y: i32,
}

impl From<(i32, i32)> for Position {
    fn from(other: (i32, i32)) -> Self {
        Position {
            x: other.0,
            y: other.1,
        }
    }
}

impl std::ops::Add for Position {
    type Output = Position;

    fn add(self, other: Self) -> Self {
        Position {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl std::ops::Mul<i32> for Position {
    type Output = Position;

    fn mul(self, other: i32) -> Self {
        Position {
            x: self.x * other,
            y: self.y * other,
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Direction {
    North,
    South,
    East,
    West,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Action {
    North,
    South,
    West,
    East,
    Forward,
    Right,
    Left,
}

impl FromStr for Action {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "N" => Ok(Action::North),
            "S" => Ok(Action::South),
            "W" => Ok(Action::West),
            "E" => Ok(Action::East),
            "F" => Ok(Action::Forward),
            "R" => Ok(Action::Right),
            "L" => Ok(Action::Left),
            _ => Err(anyhow!("Invalid Action value")),
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Instruction {
    action: Action,
    value: i32,
}

impl FromStr for Instruction {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (action, value) = s.trim().split_at(1);
        let action: Action = action.parse()?;
        let value: i32 = value.parse()?;
        Ok(Instruction { action, value })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_simple() {
        let instructions: Vec<Instruction> = "F10
            N3
            F7
            R90
            F11"
        .lines()
        .map(|line| line.parse().expect("Failed to parse instruction"))
        .collect::<Vec<_>>();
        let mut boat = Boat::default();

        boat.handle_simple(&instructions);
        assert_eq!(manhattan_distance((0, 0).into(), boat.position), 25);
    }

    #[test]
    pub fn test_waypoint() {
        let instructions: Vec<Instruction> = "F10
            N3
            F7
            R90
            F11"
        .lines()
        .map(|line| line.parse().expect("Failed to parse instruction"))
        .collect::<Vec<_>>();
        let mut boat = Boat::default();

        boat.handle_waypoint(&instructions);
        assert_eq!(manhattan_distance((0, 0).into(), boat.position), 286);
    }
}
