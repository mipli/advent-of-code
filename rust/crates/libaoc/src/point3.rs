use std::str::FromStr;

use crate::point::Neighbours;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct Point3 {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl Point3 {
    pub fn manhattan_distance(&self, other: &Point3) -> u32 {
        ((self.z - other.z).abs() + (self.x - other.x).abs() + (self.y - other.y).abs()) as u32
    }
}

impl Neighbours for Point3 {
    type Output = [Point3; 26];
    fn neighbours(&self) -> Self::Output {
        [
            self + (0, 0, -1),
            self + (-1, 0, -1),
            self + (1, 0, -1),
            self + (0, -1, -1),
            self + (0, 1, -1),
            self + (-1, -1, -1),
            self + (1, 1, -1),
            self + (1, -1, -1),
            self + (-1, 1, -1),
            self + (0, 0, 1),
            self + (-1, 0, 1),
            self + (1, 0, 1),
            self + (0, -1, 1),
            self + (0, 1, 1),
            self + (-1, -1, 1),
            self + (1, 1, 1),
            self + (1, -1, 1),
            self + (-1, 1, 1),
            self + (-1, 0, 0),
            self + (1, 0, 0),
            self + (0, -1, 0),
            self + (0, 1, 0),
            self + (-1, -1, 0),
            self + (1, 1, 0),
            self + (1, -1, 0),
            self + (-1, 1, 0),
        ]
    }
}

impl From<(i32, i32, i32)> for Point3 {
    fn from(other: (i32, i32, i32)) -> Point3 {
        Point3 {
            x: other.0,
            y: other.1,
            z: other.2,
        }
    }
}

impl FromStr for Point3 {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(',');
        match (parts.next(), parts.next(), parts.next()) {
            (Some(x), Some(y), Some(z)) => {
                let x: i32 = x.parse()?;
                let y: i32 = y.parse()?;
                let z: i32 = z.parse()?;
                Ok(Point3 { x, y, z })
            }
            _ => Err(anyhow::anyhow!("Failed to parse position")),
        }
    }
}

impl std::ops::Add for Point3 {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl std::ops::Add<&Point3> for Point3 {
    type Output = Self;

    fn add(self, other: &Point3) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl std::ops::Add<(i32, i32, i32)> for Point3 {
    type Output = Self;

    fn add(self, other: (i32, i32, i32)) -> Self {
        Self {
            x: self.x + other.0,
            y: self.y + other.1,
            z: self.z + other.2,
        }
    }
}

impl std::ops::Add<(i32, i32, i32)> for &Point3 {
    type Output = Point3;

    fn add(self, other: (i32, i32, i32)) -> Point3 {
        Point3 {
            x: self.x + other.0,
            y: self.y + other.1,
            z: self.z + other.2,
        }
    }
}
