use std::{
    collections::HashMap,
    path::{Path, PathBuf},
    str::FromStr,
};

use anyhow::{bail, Result};
use colored::Colorize;

pub mod bits;
mod daynumber;
pub mod grid;
pub mod path;
pub mod point;
pub mod point3;
pub mod point4;
use curl::easy::List;
pub use daynumber::DayNumber;

pub trait AocRunner {
    fn run_newest(&self) -> Result<()>;
    fn run_all(&self) -> Result<()>;
    fn run(&self, day: DayNumber) -> Result<()>;
    fn run_solver(&self, solver: &Solver) -> Result<()>;
}

#[derive(Default)]
pub struct Aoc<const T: usize> {
    days: HashMap<DayNumber, Solver>,
}

impl<const T: usize> Aoc<T> {
    pub fn add(&mut self, solver: Solver) {
        if self.days.contains_key(&solver.day) {
            panic!("Tried to add a day that already exists");
        }
        self.days.insert(solver.day, solver);
    }

    fn get_input(&self, solver: &Solver) -> Result<String> {
        let store_string = format!("./inputs/{}/{}", T, solver.day);
        let mut store_path = PathBuf::from_str(&store_string).unwrap();
        std::fs::create_dir_all(Path::new(&store_path)).expect("Failed to create directory");
        store_path.push("input.txt");

        if !store_path.exists() {
            let input = self.fetch_input(solver)?;
            std::fs::write(Path::new(&store_path), input).expect("Failed to write input");
        }
        Ok(std::fs::read_to_string(Path::new(&store_path)).expect("Unable to read file"))
    }

    fn fetch_input(&self, solver: &Solver) -> Result<Vec<u8>> {
        let url = format!(
            "https://www.adventofcode.com/{}/day/{}/input",
            T, solver.day
        );
        let sessionid = match std::env::var("CURL_AOC_SESSION") {
            Ok(id) => id,
            Err(_) => bail!("Missing CURL_AOC_SESSION env variable"),
        };
        println!(
            "{}",
            format!(" -- Downloading input for day {}", solver.day).red()
        );
        let mut curl = curl::easy::Easy::new();
        curl.url(&url).unwrap();
        curl.follow_location(true).unwrap();
        curl.cookie(&format!("session={sessionid}")).unwrap();
        let mut headers = List::new();
        let _ = headers.append("User-Agent: https://gitlab.com/mipli/advent-of-code");
        curl.http_headers(headers).unwrap();
        let mut buf = vec![];
        {
            let mut transfer = curl.transfer();
            transfer
                .write_function(|input| {
                    buf.extend_from_slice(input);
                    Ok(input.len())
                })
                .unwrap();
            transfer.perform().unwrap();
        }
        Ok(buf)
    }
}

impl<const T: usize> AocRunner for Aoc<T> {
    fn run_newest(&self) -> Result<()> {
        if let Some(newest) = self.days.values().max_by_key(|s| s.day) {
            self.run_solver(newest)?;
        } else {
            bail!("No newest solver found");
        }
        Ok(())
    }

    fn run_all(&self) -> Result<()> {
        let mut solvers = self.days.values().collect::<Vec<_>>();
        solvers.sort_by_key(|s| s.day);
        for solver in solvers {
            self.run_solver(solver)?;
        }

        Ok(())
    }

    fn run(&self, day: DayNumber) -> Result<()> {
        if let Some(solver) = self.days.get(&day) {
            self.run_solver(solver)?;
        } else {
            bail!("Missing solver for day: {}", day);
        }
        Ok(())
    }
    fn run_solver(&self, solver: &Solver) -> Result<()> {
        let input = self.get_input(solver)?;
        solver.run(&input)?;
        Ok(())
    }
}

pub struct Solver {
    pub day: DayNumber,
    task: Box<dyn Fn(&str)>,
}

impl Solver {
    pub fn new(day: DayNumber, task: impl Fn(&str) + 'static) -> Self {
        Self {
            day,
            task: Box::new(task),
        }
    }

    pub fn run(&self, input: &str) -> Result<()> {
        println!("{}", format!("Solving Day {}", self.day).bold());
        let start = std::time::Instant::now();
        (self.task)(input);
        let duration = start.elapsed();
        println!("{}", format!("Time: {duration:?}").dimmed());
        Ok(())
    }
}
