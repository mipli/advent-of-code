use std::str::FromStr;

use crate::point::Neighbours;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct Point4 {
    pub x: i32,
    pub y: i32,
    pub z: i32,
    pub w: i32,
}

impl Point4 {
    pub fn manhattan_distance(&self, other: &Point4) -> u32 {
        ((self.z - other.z).abs()
            + (self.x - other.x).abs()
            + (self.y - other.y).abs()
            + (self.w - other.w).abs()) as u32
    }
}

impl Neighbours for Point4 {
    type Output = [Point4; 80];
    fn neighbours(&self) -> Self::Output {
        [
            self + (0, 0, -1, 0),
            self + (-1, 0, -1, 0),
            self + (1, 0, -1, 0),
            self + (0, -1, -1, 0),
            self + (0, 1, -1, 0),
            self + (-1, -1, -1, 0),
            self + (1, 1, -1, 0),
            self + (1, -1, -1, 0),
            self + (-1, 1, -1, 0),
            self + (0, 0, 1, 0),
            self + (-1, 0, 1, 0),
            self + (1, 0, 1, 0),
            self + (0, -1, 1, 0),
            self + (0, 1, 1, 0),
            self + (-1, -1, 1, 0),
            self + (1, 1, 1, 0),
            self + (1, -1, 1, 0),
            self + (-1, 1, 1, 0),
            self + (-1, 0, 0, 0),
            self + (1, 0, 0, 0),
            self + (0, -1, 0, 0),
            self + (0, 1, 0, 0),
            self + (-1, -1, 0, 0),
            self + (1, 1, 0, 0),
            self + (1, -1, 0, 0),
            self + (-1, 1, 0, 0),
            self + (0, 0, -1, -1),
            self + (-1, 0, -1, -1),
            self + (1, 0, -1, -1),
            self + (0, -1, -1, -1),
            self + (0, 1, -1, -1),
            self + (-1, -1, -1, -1),
            self + (1, 1, -1, -1),
            self + (1, -1, -1, -1),
            self + (-1, 1, -1, -1),
            self + (0, 0, 1, -1),
            self + (-1, 0, 1, -1),
            self + (1, 0, 1, -1),
            self + (0, -1, 1, -1),
            self + (0, 1, 1, -1),
            self + (-1, -1, 1, -1),
            self + (1, 1, 1, -1),
            self + (1, -1, 1, -1),
            self + (-1, 1, 1, -1),
            self + (-1, 0, 0, -1),
            self + (1, 0, 0, -1),
            self + (0, -1, 0, -1),
            self + (0, 1, 0, -1),
            self + (-1, -1, 0, -1),
            self + (1, 1, 0, -1),
            self + (1, -1, 0, -1),
            self + (-1, 1, 0, -1),
            self + (0, 0, -1, 1),
            self + (-1, 0, -1, 1),
            self + (1, 0, -1, 1),
            self + (0, -1, -1, 1),
            self + (0, 1, -1, 1),
            self + (-1, -1, -1, 1),
            self + (1, 1, -1, 1),
            self + (1, -1, -1, 1),
            self + (-1, 1, -1, 1),
            self + (0, 0, 1, 1),
            self + (-1, 0, 1, 1),
            self + (1, 0, 1, 1),
            self + (0, -1, 1, 1),
            self + (0, 1, 1, 1),
            self + (-1, -1, 1, 1),
            self + (1, 1, 1, 1),
            self + (1, -1, 1, 1),
            self + (-1, 1, 1, 1),
            self + (-1, 0, 0, 1),
            self + (1, 0, 0, 1),
            self + (0, -1, 0, 1),
            self + (0, 1, 0, 1),
            self + (-1, -1, 0, 1),
            self + (1, 1, 0, 1),
            self + (1, -1, 0, 1),
            self + (-1, 1, 0, 1),
            self + (0, 0, 0, 1),
            self + (0, 0, 0, -1),
        ]
    }
}

impl From<(i32, i32, i32, i32)> for Point4 {
    fn from(other: (i32, i32, i32, i32)) -> Point4 {
        Point4 {
            x: other.0,
            y: other.1,
            z: other.2,
            w: other.3,
        }
    }
}

impl FromStr for Point4 {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(',');
        match (parts.next(), parts.next(), parts.next(), parts.next()) {
            (Some(x), Some(y), Some(z), Some(w)) => {
                let x: i32 = x.parse()?;
                let y: i32 = y.parse()?;
                let z: i32 = z.parse()?;
                let w: i32 = w.parse()?;
                Ok(Point4 { x, y, z, w })
            }
            _ => Err(anyhow::anyhow!("Failed to parse position")),
        }
    }
}

impl std::ops::Add for Point4 {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
            w: self.w + other.w,
        }
    }
}

impl std::ops::Add<&Point4> for Point4 {
    type Output = Self;

    fn add(self, other: &Point4) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
            w: self.w + other.w,
        }
    }
}

impl std::ops::Add<(i32, i32, i32, i32)> for Point4 {
    type Output = Self;

    fn add(self, other: (i32, i32, i32, i32)) -> Self {
        Self {
            x: self.x + other.0,
            y: self.y + other.1,
            z: self.z + other.2,
            w: self.w + other.3,
        }
    }
}

impl std::ops::Add<(i32, i32, i32, i32)> for &Point4 {
    type Output = Point4;

    fn add(self, other: (i32, i32, i32, i32)) -> Point4 {
        Point4 {
            x: self.x + other.0,
            y: self.y + other.1,
            z: self.z + other.2,
            w: self.w + other.3,
        }
    }
}
