pub type Bits = Vec<u8>;

pub trait FromInt {
    fn to_bits(&self, length: usize) -> Bits;
}

impl FromInt for u32 {
    fn to_bits(&self, length: usize) -> Bits {
        let mut bits = vec![];
        let mut rem = *self;
        while rem > 0 {
            bits.push((rem % 2) as u8);
            rem /= 2;
        }
        while bits.len() < length {
            bits.push(0);
        }
        bits.into_iter().rev().collect()
    }
}

pub trait ToInt {
    fn to_u32(&self) -> u32;
    fn to_u64(&self) -> u64;
}

impl ToInt for Bits {
    fn to_u32(&self) -> u32 {
        self.iter().fold(0, |acc, &b| (acc << 1) + b as u32)
    }

    fn to_u64(&self) -> u64 {
        self.iter().fold(0, |acc, &b| (acc << 1) + b as u64)
    }
}

impl ToInt for [u8] {
    fn to_u32(&self) -> u32 {
        self.iter().fold(0, |acc, &b| (acc << 1) + b as u32)
    }
    fn to_u64(&self) -> u64 {
        self.iter().fold(0, |acc, &b| (acc << 1) + b as u64)
    }
}
