use crate::point::Point;

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum NeighbourConnection {
    Four,
    Eight,
}

#[derive(Clone, Eq, PartialEq)]
pub struct Grid<T: Clone> {
    grid: Vec<T>,
    width: usize,
    height: usize,
    neigbour_connection: NeighbourConnection,
}

impl<T: Clone> Grid<T> {
    pub fn new(
        width: usize,
        height: usize,
        neigbour_connection: NeighbourConnection,
        val: T,
    ) -> Grid<T> {
        Grid {
            grid: vec![val; width * height],
            width,
            height,
            neigbour_connection,
        }
    }

    pub fn new_with_data(
        width: usize,
        height: usize,
        neigbour_connection: NeighbourConnection,
        data: Vec<T>,
    ) -> Grid<T> {
        Grid {
            grid: data,
            width,
            height,
            neigbour_connection,
        }
    }

    pub fn get_size(&self) -> usize {
        self.grid.len()
    }

    pub fn get_width(&self) -> usize {
        self.width
    }

    pub fn get_height(&self) -> usize {
        self.height
    }

    pub fn set_data(&mut self, mut data: Vec<T>) {
        assert!(data.len() == self.grid.len());
        std::mem::swap(&mut self.grid, &mut data);
    }

    pub fn get_data(&self) -> Vec<T> {
        self.grid.clone()
    }

    pub fn set(&mut self, point: Point, val: T) {
        let index = self.get_index(point);
        self.grid[index] = val;
    }

    pub fn get(&self, point: Point) -> &T {
        let index = self.get_index(point);
        &self.grid[index]
    }

    pub fn get_mut(&mut self, point: Point) -> &mut T {
        let index = self.get_index(point);
        &mut self.grid[index]
    }

    pub fn get_neigbours(&self, point: Point) -> Vec<Point> {
        match self.neigbour_connection {
            NeighbourConnection::Four => self.get_four_neigbours(point),
            NeighbourConnection::Eight => self.get_eight_neigbours(point),
        }
    }

    fn get_eight_neigbours(&self, point: Point) -> Vec<Point> {
        let points = [
            (point.x - 1, point.y - 1).into(),
            (point.x, point.y - 1).into(),
            (point.x + 1, point.y - 1).into(),
            (point.x - 1, point.y).into(),
            (point.x + 1, point.y).into(),
            (point.x - 1, point.y + 1).into(),
            (point.x, point.y + 1).into(),
            (point.x + 1, point.y + 1).into(),
        ];

        points
            .into_iter()
            .filter(|&p| self.is_valid_point(p))
            .collect::<Vec<_>>()
    }

    fn get_four_neigbours(&self, point: Point) -> Vec<Point> {
        let points = [
            (point.x - 1, point.y).into(),
            (point.x + 1, point.y).into(),
            (point.x, point.y - 1).into(),
            (point.x, point.y + 1).into(),
        ];

        points
            .into_iter()
            .filter(|&p| self.is_valid_point(p))
            .collect::<Vec<_>>()
    }

    fn get_index(&self, point: Point) -> usize {
        assert!(
            point.x >= 0
                && point.x < self.width as i32
                && point.y >= 0
                && point.y < self.height as i32
        );
        (point.x + point.y * (self.width as i32)) as usize
    }

    pub fn is_valid_point(&self, point: Point) -> bool {
        point.x >= 0 && point.x < self.width as i32 && point.y >= 0 && point.y < self.height as i32
    }

    pub fn point_iter(&self) -> GridPointIterator {
        GridPointIterator {
            current: (0, 0).into(),
            width: self.width,
            height: self.height,
        }
    }
}

pub struct GridPointIterator {
    current: Point,
    width: usize,
    height: usize,
}

impl Iterator for GridPointIterator {
    type Item = Point;

    fn next(&mut self) -> Option<Self::Item> {
        let is_valid_point = |point: Point| {
            point.x >= 0
                && point.x < self.width as i32
                && point.y >= 0
                && point.y < self.height as i32
        };
        if is_valid_point(self.current) {
            let next = self.current;
            self.current = self.current + (1, 0);
            if !is_valid_point(self.current) {
                self.current = (0, self.current.y + 1).into();
            }
            Some(next)
        } else {
            None
        }
    }
}

impl<T: std::fmt::Debug + Clone> std::fmt::Debug for Grid<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "size: {} x {}", self.width, self.height)?;
        for (i, g) in self.grid.iter().enumerate() {
            if i % self.width == 0 {
                writeln!(f)?;
            }
            write!(f, "{g:?}")?;
        }
        Ok(())
    }
}

impl<T: std::fmt::Display + Clone> std::fmt::Display for Grid<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (i, g) in self.grid.iter().enumerate() {
            if i > 0 && i % self.width == 0 {
                writeln!(f)?;
            }
            write!(f, "{g}")?;
        }
        Ok(())
    }
}
