use std::collections::{hash_map::Entry, BinaryHeap, HashMap};

/// Finds the shortest path from `start` to `end` using an A-star algorithm
/// Returned path is in 'reverse' order, so you can easily use .pop() to get the steps required
pub fn find<T: Clone + Copy + Eq + std::hash::Hash + std::fmt::Debug>(
    start: T,
    end: T,
    get_neighbours: impl Fn(T) -> Vec<T>,
    scorer: impl Fn(T, T) -> u32,
) -> Option<Vec<T>> {
    let mut frontier: BinaryHeap<SearchNode<T>> = BinaryHeap::default();
    frontier.push(SearchNode {
        pos: start,
        steps: 1,
        score: 0,
    });

    if start == end {
        return Some(Vec::new());
    }

    let mut steps_so_far: HashMap<T, u32> = HashMap::default();
    let mut came_from: HashMap<T, T> = HashMap::default();

    steps_so_far.insert(start, 0);

    while let Some(node) = frontier.pop() {
        if node.pos == end {
            let mut path = Vec::with_capacity(node.steps as usize);
            path.push(end);
            let mut path_node = node.pos;
            while let Some(&step) = came_from.get(&path_node) {
                path.push(step);
                path_node = step;
            }
            path.pop(); // remove starting location
            return Some(path);
        }

        let neigbours = get_neighbours(node.pos);

        for neigbour in neigbours {
            match steps_so_far.entry(neigbour) {
                Entry::Vacant(entry) => {
                    came_from.entry(neigbour).or_insert(node.pos);
                    entry.insert(node.steps + 1);

                    frontier.push(SearchNode {
                        pos: neigbour,
                        steps: node.steps + 1,
                        score: node.steps + 1 + scorer(neigbour, end),
                    });
                }
                Entry::Occupied(mut entry) if *entry.get() > node.steps + 1 => {
                    came_from.entry(neigbour).or_insert(node.pos);
                    entry.insert(node.steps + 1);

                    frontier.push(SearchNode {
                        pos: neigbour,
                        steps: node.steps + 1,
                        score: node.steps + 1 + scorer(neigbour, end),
                    });
                }
                _ => {}
            }
        }
    }

    None
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct SearchNode<T: Clone + Copy + Eq> {
    pos: T,
    steps: u32,
    score: u32,
}
impl<T: Clone + Copy + Eq> std::cmp::Eq for SearchNode<T> {}

impl<T: Clone + Copy + Eq> std::cmp::Ord for SearchNode<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        if self.score == other.score {
            other.steps.cmp(&self.steps)
        } else {
            other.score.cmp(&self.score)
        }
    }
}

impl<T: Clone + Copy + Eq> std::cmp::PartialOrd for SearchNode<T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
