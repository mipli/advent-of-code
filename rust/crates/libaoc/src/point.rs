use std::str::FromStr;

pub trait Neighbours {
    type Output: IntoIterator<Item = Self>;
    fn neighbours(&self) -> Self::Output;
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Point {
    pub fn manhattan_distance(&self, other: &Point) -> u32 {
        ((self.x - other.x).abs() + (self.y - other.y).abs()) as u32
    }

    pub fn distance(&self, other: &Point) -> u32 {
        std::cmp::max((self.x - other.x).abs(), (self.y - other.y).abs()) as u32
    }
}

impl From<(i32, i32)> for Point {
    fn from(other: (i32, i32)) -> Point {
        Point {
            x: other.0,
            y: other.1,
        }
    }
}

impl FromStr for Point {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(',');
        match (parts.next(), parts.next()) {
            (Some(x), Some(y)) => {
                let x: i32 = x.parse()?;
                let y: i32 = y.parse()?;
                Ok(Point { x, y })
            }
            _ => Err(anyhow::anyhow!("Failed to parse position")),
        }
    }
}

impl std::ops::Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl std::ops::Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl std::ops::Add<&Point> for Point {
    type Output = Self;

    fn add(self, other: &Point) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl std::ops::Add<(i32, i32)> for Point {
    type Output = Self;

    fn add(self, other: (i32, i32)) -> Self {
        Self {
            x: self.x + other.0,
            y: self.y + other.1,
        }
    }
}
