use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::One, |input| {
        let mut pairs = parse_input(input);

        pairs.0.sort_unstable();
        pairs.1.sort_unstable();

        let sorted_pairs = pairs.0.iter().zip(pairs.1.iter()).collect::<Vec<_>>();

        let diff = sorted_pairs
            .iter()
            .fold(0u64, |acc, pair| acc + pair.0.abs_diff(*pair.1));

        println!("list diff: {diff}");

        let similartity = pairs.0.iter().fold(0, |acc, num| {
            let count = pairs.1.iter().filter(|&n| n == num).count();
            acc + (num * count as u64)
        });

        println!("similarity score: {similartity}");
    })
}

fn parse_input(input: &str) -> (Vec<u64>, Vec<u64>) {
    let pairs = input.split("\n").fold((vec![], vec![]), |mut acc, pair| {
        if pair.trim().is_empty() {
            return acc;
        }
        let mut splt = pair.split_whitespace();
        acc.0.push(splt.next().unwrap().parse::<u64>().unwrap());
        acc.1.push(splt.next().unwrap().parse::<u64>().unwrap());
        acc
    });

    pairs
}
