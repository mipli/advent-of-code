use itertools::Itertools;
use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Five, |input| {
        let (rules, books) = parse_input(input);
        let (middle_sum, fixed_sum) = get_sums(&books, &rules);

        println!("Sum of valid middle pages: {middle_sum}");
        println!("Sum of fiex middle pages: {fixed_sum}");
    })
}

fn get_sums(books: &[Vec<u32>], rules: &[Rule]) -> (u32, u32) {
    books.iter().fold((0, 0), |(valid, fixed), book| {
        if is_valid_book(book, rules) {
            (valid + get_middle_page(book), fixed)
        } else if let Some(fixed_book) = fix_book(book, rules) {
            (valid, fixed + get_middle_page(&fixed_book))
        } else {
            unreachable!()
        }
    })
}

fn fix_book(book: &[u32], rules: &[Rule]) -> Option<Vec<u32>> {
    let relevant_rules = rules
        .iter()
        .filter(|&rule| book.contains(&rule.0) && book.contains(&rule.1))
        .collect::<Vec<_>>();

    let mut valid_book = vec![0; book.len()];
    valid_book[0] = book[0];

    for p in 1..book.len() {
        valid_book[p] = book[p];

        for i in (1..=p).rev() {
            if is_valid_book_filtered(&valid_book, book[p], &relevant_rules) {
                break;
            }
            valid_book.swap(i, i - 1);
        }
    }

    Some(valid_book)
}

fn get_middle_page(book: &[u32]) -> u32 {
    book[book.len() / 2]
}

fn is_valid_book_filtered(book: &[u32], page: u32, rules: &[&Rule]) -> bool {
    rules
        .iter()
        // since we start testing by having the new page at the very back we only need to validate other pages position relative to this one
        .all(|r| r.0 != page || is_valid_rule(book, r))
}

fn is_valid_book(book: &[u32], rules: &[Rule]) -> bool {
    rules.iter().all(|r| is_valid_rule(book, r))
}

fn is_valid_rule(book: &[u32], rule: &Rule) -> bool {
    let li = book.iter().find_position(|&&p| p == rule.0);
    let ri = book.iter().find_position(|&&p| p == rule.1);

    match (li, ri) {
        (Some((l, _)), Some((r, _))) if l > r => return false,
        _ => return true,
    }
}

#[derive(Copy, Clone, Debug)]
struct Rule(u32, u32);

fn parse_input(input: &str) -> (Vec<Rule>, Vec<Vec<u32>>) {
    enum ParseState {
        Rules,
        Pages,
    }

    let mut state = ParseState::Rules;

    let mut rules = vec![];
    let mut books = vec![];

    for line in input.lines() {
        if line.trim().is_empty() {
            state = ParseState::Pages;
            continue;
        }
        match state {
            ParseState::Rules => {
                let mut splt = line.trim().split('|');
                let lhs = splt.next().unwrap().parse::<u32>().unwrap();
                let rhs = splt.next().unwrap().parse::<u32>().unwrap();
                rules.push(Rule(lhs, rhs));
            }
            ParseState::Pages => {
                let splt = line.trim().split(',');
                books.push(
                    splt.into_iter()
                        .map(|num| num.parse::<u32>().unwrap())
                        .collect::<Vec<_>>(),
                );
            }
        }
    }
    (rules, books)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = "47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47";

        let (rules, books) = parse_input(input);
        assert!(is_valid_book(&books[0], &rules));
        assert_eq!(get_middle_page(&books[0]), 61);

        assert_eq!(
            fix_book(&[75, 97, 47, 61, 53], &rules),
            Some(vec![97, 75, 47, 61, 53])
        );

        let (valids, fixed) = get_sums(&books, &rules);

        assert_eq!(valids, 143);
        assert_eq!(fixed, 123);
    }
}
