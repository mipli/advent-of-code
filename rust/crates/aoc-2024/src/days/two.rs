use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Two, |input| {
        let reports = parse_input(input);

        let valid_count = reports.iter().filter(|report| is_valid(report)).count();
        println!("Valid reports: {valid_count}");

        let valid_dampened_count = reports
            .iter()
            .filter(|report| is_valid_dampened(report))
            .count();
        println!("Valid dampened reports: {valid_dampened_count}");
    })
}

fn parse_input(input: &str) -> Vec<Vec<i64>> {
    input
        .split("\n")
        .map(|line| {
            line.split_whitespace()
                .map(|n| n.parse::<i64>().unwrap())
                .collect::<Vec<_>>()
        })
        .filter(|r| !r.is_empty())
        .collect::<Vec<_>>()
}

fn is_valid(report: &[i64]) -> bool {
    let dir = (report[0] - report[1]).signum();

    report.windows(2).all(|window| {
        let diff = window[0] - window[1];
        if diff.abs() == 0 || diff.abs() > 3 {
            return false;
        }
        diff.signum() == dir
    })
}

fn is_valid_dampened(report: &[i64]) -> bool {
    let mut idx = 0;

    while idx < report.len() {
        let subset = [&report[..idx], &report[(idx + 1)..]].concat();
        if is_valid(&subset) {
            return true;
        }
        idx += 1;
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = "7 6 4 2 1
            1 2 7 8 9
            9 7 6 2 1
            1 3 2 4 5
            8 6 4 4 1
            1 3 6 7 9";

        let reports = parse_input(input);
        assert!(is_valid(&[7, 6, 4, 2, 1]));
        assert!(!is_valid(&[1, 2, 7, 8, 9]));
        let valid_count = reports.iter().filter(|report| is_valid(report)).count();

        assert_eq!(valid_count, 2);
    }

    #[test]
    fn test_part_two() {
        let input = "7 6 4 2 1
            1 2 7 8 9
            9 7 6 2 1
            1 3 2 4 5
            8 6 4 4 1
            1 3 6 7 9";

        let reports = parse_input(input);
        let valid_count = reports
            .iter()
            .filter(|report| is_valid_dampened(report))
            .count();

        assert_eq!(valid_count, 4);
    }
}
