use libaoc::{DayNumber, Solver};
use regex::Regex;

pub fn get() -> Solver {
    Solver::new(DayNumber::Three, |input| {
        let muls = parse_input(input, true);
        let added = calc(&muls);
        println!("Added value: {added}");

        let muls = parse_input(input, false);
        let added = calc(&muls);
        println!("Filtered value: {added}");
    })
}

fn parse_input(input: &str, part_one: bool) -> Vec<(u64, u64)> {
    let reg = Regex::new(r"((?<dont>don't)|(?<do>do\(\))|mul\((?<lhs>\d{1,3}),(?<rhs>\d{1,3})\))")
        .unwrap();

    let (_, muls) =
        reg.captures_iter(input)
            .fold((true, vec![]), |(mut enabled, mut acc), caps| {
                if caps.name("do").is_some() {
                    enabled = part_one || true;
                } else if caps.name("dont").is_some() {
                    enabled = part_one || false;
                } else if enabled {
                    let lhs = caps.name("lhs").unwrap().as_str();
                    let rhs = caps.name("rhs").unwrap().as_str();
                    acc.push((lhs.parse::<u64>().unwrap(), rhs.parse::<u64>().unwrap()));
                }
                (enabled, acc)
            });
    muls
}

fn calc(muls: &[(u64, u64)]) -> u64 {
    muls.iter().map(|(l, r)| l * r).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))";
        let muls = parse_input(input, true);
        assert_eq!(4, muls.len());
        assert_eq!(161, calc(&muls));
    }

    #[test]
    fn test_part_two() {
        let input = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))";
        let muls = parse_input(input, false);
        assert_eq!(2, muls.len());
        assert_eq!(48, calc(&muls));
    }
}
