use libaoc::{
    grid::{Grid, NeighbourConnection},
    point::Point,
    DayNumber, Solver,
};

pub fn get() -> Solver {
    Solver::new(DayNumber::Four, |input| {
        let grid = parse_input(input);
        let xmases = count_all_xmases(&grid);

        println!("X-mas count: {xmases}");

        let crosses = count_crosses(&grid);
        println!("cross count: {crosses}");
    })
}

fn parse_input(input: &str) -> Grid<char> {
    let height = input.lines().count();
    let width = input.lines().next().unwrap().trim().len();

    let data = input
        .lines()
        .flat_map(|line| line.trim().chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    Grid::new_with_data(width, height, NeighbourConnection::Eight, data)
}

fn count_all_xmases(grid: &Grid<char>) -> usize {
    grid.point_iter().map(|point| count_xmas(grid, point)).sum()
}

fn count_xmas(grid: &Grid<char>, point: Point) -> usize {
    grid.get_neigbours(point)
        .into_iter()
        .filter(|&n| {
            let dir = n - point;
            is_xmas(grid, point, dir)
        })
        .count()
}

fn is_xmas(grid: &Grid<char>, mut point: Point, dir: Point) -> bool {
    assert!(dir.x.abs() <= 1 && dir.y.abs() <= 1);

    let xmas = ['X', 'M', 'A', 'S'];

    xmas.iter().all(|c| {
        if !grid.is_valid_point(point) || grid.get(point) != c {
            return false;
        }
        point = point + dir;
        true
    })
}

fn count_crosses(grid: &Grid<char>) -> usize {
    grid.point_iter()
        .filter(|point| {
            if point.x == 0
                || point.y == 0
                || point.x == (grid.get_width() - 1) as i32
                || point.y == (grid.get_height() - 1) as i32
            {
                return false;
            }
            is_cross(grid, *point)
        })
        .count()
}

fn is_cross(grid: &Grid<char>, point: Point) -> bool {
    if *grid.get(point) != 'A' {
        return false;
    }

    let mut l = [*grid.get(point + (-1, -1)), *grid.get(point + (1, 1))];
    let mut r = [*grid.get(point + (1, -1)), *grid.get(point + (-1, 1))];

    l.sort_unstable();
    r.sort_unstable();

    l == r && l == ['M', 'S']
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = "MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX";

        let grid = parse_input(input);
        assert!(is_xmas(&grid, (5, 0).into(), (1, 0).into()));
        assert_eq!(1, count_xmas(&grid, (5, 0).into()));
        assert_eq!(18, count_all_xmases(&grid));
    }

    #[test]
    fn test_part_two() {
        let input = "MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX";

        let grid = parse_input(input);
        assert!(is_cross(&grid, (2, 1).into()));
        assert_eq!(9, count_crosses(&grid));
    }
}
