use libaoc::{DayNumber, Solver};

pub fn get() -> Solver {
    Solver::new(DayNumber::Nine, |input| {})
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {}
}
