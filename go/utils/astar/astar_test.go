package astar

import (
	"math"
	"testing"

	u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

type TestGraph struct {
	grid [][]int
	size int
}

// Distance implements Graph.
func (*TestGraph) Distance(a Point, b Point) int {
	return int(math.Abs(float64(a.X-b.X)) + math.Abs(float64(a.Y-b.Y)))
}

// FindNeighbours implements Graph.
func (tg *TestGraph) FindNeighbours(p Point) []Point {
	nodes := []Point{}
	if p.X > 0 {
		nodes = append(nodes, Point{
			X: p.X - 1,
			Y: p.Y,
		})
	}
	if p.Y > 0 {
		nodes = append(nodes, Point{
			X: p.X,
			Y: p.Y - 1,
		})
	}
	if p.X < tg.size-1 {
		nodes = append(nodes, Point{
			X: p.X + 1,
			Y: p.Y,
		})
	}
	if p.Y < tg.size-1 {
		nodes = append(nodes, Point{
			X: p.X,
			Y: p.Y + 1,
		})
	}

	return u.Filter(nodes, func(n Point) bool {
		return tg.grid[n.Y][n.X] > 0
	})
}

var _ Graph = (*TestGraph)(nil)

func TestNeighbourPath(t *testing.T) {
	graph := TestGraph{
		grid: [][]int{
			{1, 1, 1},
			{1, 1, 1},
			{1, 1, 1},
		},
		size: 3,
	}

	path, ok := FindShortest(Point{X: 1, Y: 1}, Point{X: 0, Y: 0}, &graph)
	assert.True(t, ok)
	assert.Equal(t, 3, len(path))
}

func TestPathAroundHole(t *testing.T) {
	graph := TestGraph{
		grid: [][]int{
			{1, 1, 1, 1, 1},
			{1, 0, 0, 0, 1},
			{1, 0, 0, 0, 1},
			{1, 0, 0, 0, 1},
			{1, 1, 1, 1, 1},
		},
		size: 5,
	}

	path, ok := FindShortest(Point{X: 0, Y: 2}, Point{X: 4, Y: 3}, &graph)
	assert.True(t, ok)
	assert.Equal(t, 8, len(path))
}

func TestNoPath(t *testing.T) {
	graph := TestGraph{
		grid: [][]int{
			{1, 1, 1, 1, 1},
			{1, 0, 1, 0, 1},
			{1, 1, 1, 0, 0},
			{1, 0, 0, 0, 1},
			{1, 1, 0, 1, 1},
		},
		size: 5,
	}

	_, ok := FindShortest(Point{X: 0, Y: 2}, Point{X: 4, Y: 3}, &graph)
	assert.False(t, ok)
}
