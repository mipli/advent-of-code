package astar

import (
	"aoc/utils/priorityqueue"
)

type Point struct {
	X int
	Y int
}

func (p *Point) GetNeighbours() []Point {
	points := []Point{}
	for x := -1; x <= 1; x++ {
		for y := -1; y <= 1; y++ {
			if x == 0 && y == 0 {
				continue
			}
			points = append(points, Point{
				X: p.X + x,
				Y: p.Y + y,
			})
		}
	}
	return points
}

func (p *Point) Diff(o Point) Point {
	return Point{
		X: p.X - o.X,
		Y: p.Y - o.Y,
	}
}

type Graph interface {
	FindNeighbours(Point) []Point
	Distance(Point, Point) int
}

type PathOption struct {
	prev Point
	cost int
}

func FindShortest(start Point, target Point, graph Graph) ([]Point, bool) {
	visited := make(map[Point]struct{}, 0)
	costs := make(map[Point]PathOption, 0)

	frontier := priorityqueue.New[Point]()
	frontier.Push(start, 0)

	costs[start] = PathOption{
		prev: start,
		cost: 0,
	}

	for {
		edge, ok := frontier.Pop()
		if !ok {
			break
		}

		if edge == target {
			var current = target
			var path []Point = []Point{current}
			for {
				if node, ok := costs[current]; ok {
					path = append(path, node.prev)
					current = node.prev
					if node.prev == start {
						return path, true
					}
				} else {
					return nil, false
				}
			}
		}

		if _, exists := visited[edge]; exists {
			continue
		}
		visited[edge] = struct{}{}

		po, _ := costs[edge]

		neigbhours := graph.FindNeighbours(edge)

		for _, n := range neigbhours {
			prio := graph.Distance(target, n)
			frontier.Push(n, prio)
			if prev, ok := costs[n]; ok {
				if prev.cost > po.cost+1 {
					costs[n] = PathOption{
						prev: edge,
						cost: po.cost + 1,
					}
				}
			} else {
				costs[n] = PathOption{
					prev: edge,
					cost: po.cost + 1,
				}
			}
		}
	}

	return nil, false
}
