package priorityqueue

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPriorityQueueAddItem(t *testing.T) {
	pq := New[int]()
	assert.Equal(t, 0, pq.Len())

	pq.Push(10, 1)
	pq.Push(15, 3)
	pq.Push(20, 2)
	assert.Equal(t, 3, pq.Len())
	if val, ok := pq.Pop(); ok {
		assert.Equal(t, 15, val)
	} else {
		assert.Fail(t, "Failed to pop item")
	}

}

func TestPriorityQueueRemove(t *testing.T) {
	pq := New[int]()
	assert.Equal(t, 0, pq.Len())

	pq.Push(10, 1)
	pq.Push(15, 3)
	pq.Push(20, 2)

	popped, ok := pq.Pop()
	assert.Equal(t, 15, popped)
	assert.Equal(t, true, ok)
	assert.Equal(t, 2, pq.Len())

	popped, ok = pq.Pop()
	assert.Equal(t, 20, popped)
	assert.Equal(t, true, ok)
	assert.Equal(t, 1, pq.Len())

	popped, ok = pq.Pop()
	assert.Equal(t, 10, popped)
	assert.Equal(t, true, ok)
	assert.Equal(t, 0, pq.Len())

	popped, ok = pq.Pop()
	assert.Equal(t, false, ok)
}
