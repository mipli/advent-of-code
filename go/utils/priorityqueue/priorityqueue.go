package priorityqueue

import "container/heap"

type PriorityQueue[T any] struct {
	inner innerPriorityQueue[T]
}

func New[T any]() PriorityQueue[T] {
	return PriorityQueue[T]{
		inner: make(innerPriorityQueue[T], 0),
	}
}

func (pq *PriorityQueue[T]) Len() int {
	return pq.inner.Len()
}

func (pq *PriorityQueue[T]) Push(value T, priority int) {
	item := &Item[T]{
		value:    value,
		priority: priority,
		index:    0,
	}
	heap.Push(&pq.inner, item)
}

func (pq *PriorityQueue[T]) Pop() (T, bool) {
	if pq.Len() == 0 {
		return *new(T), false
	}
	item := (heap.Pop(&pq.inner)).(*Item[T])
	return item.value, true
}

// An Item is something we manage in a priority queue.
type Item[T any] struct {
	value    T
	priority int
	// The index is needed by update and is maintained by the heap.Interface methods.
	index int // The index of the item in the heap.
}

// A innerPriorityQueue implements heap.Interface and holds Items.
type innerPriorityQueue[T any] []*Item[T]

func (pq innerPriorityQueue[T]) Len() int { return len(pq) }

func (pq innerPriorityQueue[T]) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority so we use greater than here.
	return pq[i].priority > pq[j].priority
}

func (pq innerPriorityQueue[T]) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *innerPriorityQueue[T]) Push(x any) {
	n := len(*pq)
	item := x.(*Item[T])
	item.index = n
	*pq = append(*pq, item)
}

func (pq *innerPriorityQueue[T]) Pop() any {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

// update modifies the priority and value of an Item in the queue.
func (pq *innerPriorityQueue[T]) update(item *Item[T], value T, priority int) {
	item.value = value
	item.priority = priority
	heap.Fix(pq, item.index)
}
