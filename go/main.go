package main

import (
	"aoc/solver"
	"aoc/solver/day11"
	"aoc/solver/day12"
	"aoc/solver/day13"
	"aoc/solver/day14"
	"aoc/solver/day15"
	"aoc/solver/day16"
	"aoc/solver/day17"
	"aoc/solver/day3"
	"aoc/solver/day4"
	"aoc/solver/day5"
	"flag"
	"fmt"
	"os"

	"github.com/joho/godotenv"
	u "github.com/rjNemo/underscore"
	"golang.org/x/exp/maps"
)

var flagDay int

func registerSolvers(solvers *map[int]solver.Solver) {
	(*solvers)[1] = &solver.DayOne{}
	(*solvers)[2] = &solver.DayTwo{}
	(*solvers)[3] = day3.New()
	(*solvers)[4] = day4.New()
	(*solvers)[5] = day5.New()
	(*solvers)[11] = day11.New()
	(*solvers)[12] = day12.New()
	(*solvers)[13] = day13.New()
	(*solvers)[14] = day14.New()
	(*solvers)[15] = day15.New()
	(*solvers)[16] = day16.New()
	(*solvers)[17] = day17.New()
}

func main() {
	if err := godotenv.Load(); err != nil {
		fmt.Fprintf(os.Stderr, "Error parsing .env file: %+v\n", err)
		os.Exit(1)
	}

	session := os.Getenv("SESSION")

	flag.IntVar(&flagDay, "d", 0, "Day to solve")
	flag.Parse()

	solvers := make(map[int]solver.Solver)
	registerSolvers(&solvers)

	if flagDay == 0 {
		flagDay = u.Max[int](maps.Keys(solvers))
	}

	solver, ok := solvers[flagDay]
	if !ok {
		fmt.Fprintf(os.Stderr, "No solver for day: %v\n", flagDay)
		os.Exit(1)

	}

	fmt.Printf("Solving day: %v\n", flagDay)

	lines, err := getInput(flagDay, session)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to read input file: %+v\n", err)
		os.Exit(1)
	}

	solver.ParseInput(lines)
	solver.Solve()
}
