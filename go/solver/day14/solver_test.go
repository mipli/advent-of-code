package day14

import (
	"testing"

	// u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

func TestExampleOne(t *testing.T) {
	solver := New()

	solver.ParseInput(`O....#....
		O.OO#....#
		.....##...
		OO.#O....O
		.O.....O#.
		O.#..O.#.#
		..O..#O..O
		.......O..
		#....###..
		#OO..#....`)

	solver.FlipNorth()
	assert.Equal(t, 136, solver.GetTotalLoad())
}

func TestExampleTwo(t *testing.T) {
	solver := New()

	solver.ParseInput(`O....#....
		O.OO#....#
		.....##...
		OO.#O....O
		.O.....O#.
		O.#..O.#.#
		..O..#O..O
		.......O..
		#....###..
		#OO..#....`)

	solver.Cycle(1000000000)
	assert.Equal(t, 64, solver.GetTotalLoad())
}
