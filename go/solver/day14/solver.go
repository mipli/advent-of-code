package day14

import (
	"aoc/solver"
	"bytes"
	"encoding/gob"
	"fmt"
	// u "github.com/rjNemo/underscore"
)

type Solver struct {
	grid [][]rune
}

type Rock struct {
	x int
	y int
}

func New() *Solver {
	return &Solver{
		grid: make([][]rune, 1),
	}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	lines := solver.InputToLines(input)
	for y, line := range lines {
		for _, r := range line {
			s.grid[y] = append(s.grid[y], r)
		}
		s.grid = append(s.grid, []rune{})
	}
	s.grid = s.grid[:len(s.grid)-1]
}

func (s *Solver) Display() {
	fmt.Printf("\nGRID:\n")
	for _, row := range s.grid {
		for _, r := range row {
			fmt.Printf("%v", string(r))
		}
		fmt.Printf("\n")
	}
}

func (s *Solver) Solve() {
	// s.FlipNorth()
	s.Cycle(1000000000)
	fmt.Printf("Total load: %v\n", s.GetTotalLoad())
}

type Item struct {
	grid  [][]rune
	cycle int
}

var cache = map[string]Item{}

func (s *Solver) Cycle(n int) {
	rem := 0
	for i := 0; i < n; i++ {
		key := Hash(s.grid)
		if val, ok := cache[key]; ok {
			cycle := i - val.cycle
			left := n - i
			rem = left - ((left / cycle) * cycle)

			copy(s.grid, val.grid)
			break
		}
		s.FlipNorth()
		s.FlipWest()
		s.FlipSouth()
		s.FlipEast()
		item := Item{
			grid:  make([][]rune, len(s.grid)),
			cycle: i,
		}
		copy(item.grid, s.grid)
		cache[key] = item
	}
	for i := 0; i < rem; i++ {
		s.FlipNorth()
		s.FlipWest()
		s.FlipSouth()
		s.FlipEast()
	}
}

func (s *Solver) FlipNorth() {
	for y := 1; y < len(s.grid); y++ {
		for x := 0; x < len(s.grid[y]); x++ {
			if s.grid[y][x] == 'O' {
				iy := y - 1
				for ; iy >= 0; iy-- {
					if s.grid[iy][x] != '.' {
						break
					}
				}
				s.grid[y][x] = '.'
				s.grid[iy+1][x] = 'O'
			}
		}
	}
}

func (s *Solver) FlipSouth() {
	for y := len(s.grid) - 2; y >= 0; y-- {
		for x := 0; x < len(s.grid[y]); x++ {
			if s.grid[y][x] == 'O' {
				iy := y + 1
				for ; iy < len(s.grid); iy++ {
					if s.grid[iy][x] != '.' {
						break
					}
				}
				s.grid[y][x] = '.'
				s.grid[iy-1][x] = 'O'
			}
		}
	}
}

func (s *Solver) FlipWest() {
	for y := 0; y < len(s.grid); y++ {
		for x := 1; x < len(s.grid[y]); x++ {
			if s.grid[y][x] == 'O' {
				ix := x - 1
				for ; ix >= 0; ix-- {
					if s.grid[y][ix] != '.' {
						break
					}
				}
				s.grid[y][x] = '.'
				s.grid[y][ix+1] = 'O'
			}
		}
	}
}

func (s *Solver) FlipEast() {
	for y := 0; y < len(s.grid); y++ {
		for x := len(s.grid[y]) - 2; x >= 0; x-- {
			if s.grid[y][x] == 'O' {
				ix := x + 1
				for ; ix < len(s.grid); ix++ {
					if s.grid[y][ix] != '.' {
						break
					}
				}
				s.grid[y][x] = '.'
				s.grid[y][ix-1] = 'O'
			}
		}
	}
}

func (s *Solver) GetTotalLoad() int {
	max := len(s.grid)
	load := 0
	for y := 0; y < len(s.grid); y++ {
		for x := 0; x < len(s.grid[y]); x++ {
			if s.grid[y][x] == 'O' {
				load += (max - y)
			}
		}
	}

	return load
}

func Hash(s [][]rune) string {
	var b bytes.Buffer
	gob.NewEncoder(&b).Encode(s)
	return string(b.Bytes())
}
