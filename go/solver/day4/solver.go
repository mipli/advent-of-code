package day4

import (
	"aoc/solver"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"

	u "github.com/rjNemo/underscore"
)

type Card struct {
	winning []int
	numbers []int
}

func (c *Card) getPoints() int {
	return u.Reduce(c.numbers, func(n int, acc int) int {
		if u.Contains(c.winning, n) {
			return acc + 1
		} else {
			return acc
		}
	}, 0)
}

type CardCounter struct {
	id    int
	card  Card
	count int
}

type Solver struct {
	cards []CardCounter
}

func New() *Solver {
	return &Solver{
		cards: []CardCounter{},
	}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	lines := solver.InputToLines(input)
	for _, line := range lines {
		fst := strings.Split(line, ":")
		id, _ := strconv.Atoi(regexp.MustCompile("Card +").ReplaceAllString(fst[0], ""))
		cardText := regexp.MustCompile(" +").ReplaceAllString(strings.TrimSpace(fst[1]), " ")
		cards := strings.Split(cardText, "|")
		winning := u.Map(strings.Split(strings.TrimSpace(cards[0]), " "), func(num string) int {
			n, _ := strconv.Atoi(num)
			return n
		})
		numbers := u.Map(strings.Split(strings.TrimSpace(cards[1]), " "), func(num string) int {
			n, _ := strconv.Atoi(num)
			return n
		})
		s.cards = append(s.cards, CardCounter{
			id:    id,
			count: 1,
			card: Card{
				winning: winning,
				numbers: numbers,
			},
		})
	}

}

func (s *Solver) Solve() {
	points := s.GetPoints()

	fmt.Printf("Points: %+v\n", points)
	s.CollectCards()

	fmt.Printf("Cards: %+v\n", s.CountCards())
}

func (s *Solver) CountCards() int {
	return u.Sum(u.Map(s.cards, func(c CardCounter) int { return c.count }))
}

func (s *Solver) CollectCards() {
	for i := 0; i < len(s.cards); i++ {
		wins := s.cards[i].card.getPoints()
		for j := 1; j <= wins; j++ {
			s.cards[i+j].count += s.cards[i].count
		}
	}
}

func (s *Solver) GetPoints() int {
	return u.Sum(u.Map(s.cards, func(c CardCounter) int { return int(math.Pow(2.0, float64(c.card.getPoints()-1))) }))
}
