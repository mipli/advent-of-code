package day16

import (
	"aoc/solver"
	"aoc/utils/astar"
	"fmt"
	"strings"

	u "github.com/rjNemo/underscore"
)

type Solver struct {
	grid      [][]rune
	paths     [][]string
	energized [][]bool
	beams     []Beam
}

type Beam struct {
	pos astar.Point
	dir astar.Point
}

func New() *Solver {
	return &Solver{
		grid:      make([][]rune, 0),
		paths:     make([][]string, 0),
		energized: make([][]bool, 0),
		beams: []Beam{{
			pos: astar.Point{
				X: -1,
				Y: 0,
			},
			dir: astar.Point{
				X: 1,
				Y: 0,
			},
		}},
	}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	lines := solver.InputToLines(input)
	s.grid = make([][]rune, len(lines))
	s.energized = make([][]bool, len(lines))
	s.paths = make([][]string, len(lines))
	for y, line := range lines {
		for _, r := range line {
			s.grid[y] = append(s.grid[y], r)
			s.energized[y] = append(s.energized[y], false)
			s.paths[y] = append(s.paths[y], "")
		}
	}
}
func (s *Solver) Reset() {
	for y, row := range s.grid {
		for x := range row {
			s.energized[y][x] = false
			s.paths[y][x] = ""
		}
	}
}
func (s *Solver) Maximize() int {
	origins := []Beam{}
	for y := 0; y < len(s.grid); y++ {
		start := astar.Point{
			X: -1,
			Y: y,
		}
		dir := astar.Point{
			X: 1,
			Y: 0,
		}
		origins = append(origins, Beam{
			pos: start,
			dir: dir,
		})
		start = astar.Point{
			X: len(s.grid[0]),
			Y: y,
		}
		dir = astar.Point{
			X: -1,
			Y: 0,
		}
		origins = append(origins, Beam{
			pos: start,
			dir: dir,
		})
	}
	for x := 0; x < len(s.grid[0]); x++ {
		start := astar.Point{
			X: x,
			Y: -1,
		}
		dir := astar.Point{
			X: 0,
			Y: 1,
		}
		origins = append(origins, Beam{
			pos: start,
			dir: dir,
		})
		start = astar.Point{
			X: x,
			Y: len(s.grid),
		}
		dir = astar.Point{
			X: 0,
			Y: -1,
		}
		origins = append(origins, Beam{
			pos: start,
			dir: dir,
		})
	}

	return u.Max(u.Map(origins, func(beam Beam) int {
		s.Reset()

		s.beams = []Beam{beam}
		s.Beamify()
		return s.CountEnergized()
	}))
}

func (s *Solver) Solve() {
	s.Beamify()
	fmt.Printf("Total Energy: %v\n", s.CountEnergized())
	fmt.Printf("Max Energy: %v\n", s.Maximize())
}

func (s *Solver) Display() {
	fmt.Printf("\n\n")
	for _, row := range s.energized {
		for _, r := range row {
			if r {
				fmt.Printf("#")
			} else {
				fmt.Printf(".")
			}
		}
		fmt.Printf("\n")
	}
	for _, beam := range s.beams {
		fmt.Printf("Beam (%v, %v) --> (%v, %v)\n", beam.pos.X, beam.pos.Y, beam.dir.X, beam.dir.Y)
	}
}

func (s *Solver) Beamify() {
	for {
		if len(s.beams) == 0 {
			break
		}
		s.Tick()
	}
}

func DirToPath(dir astar.Point) string {
	switch dir {
	case astar.Point{X: -1, Y: 0}:
		return "<"
	case astar.Point{X: 1, Y: 0}:
		return ">"
	case astar.Point{X: 0, Y: -1}:
		return "v"
	case astar.Point{X: 0, Y: 1}:
		return "^"
	default:
		panic("Invalid direction")
	}
}

func (s *Solver) Tick() {
	beams := []Beam{}
	for _, beam := range s.beams {
		if beam.pos.X < len(s.grid[0]) && beam.pos.X >= 0 && beam.pos.Y < len(s.grid) && beam.pos.Y >= 0 {
			s.energized[beam.pos.Y][beam.pos.X] = true
		}
		beam.pos.X = beam.pos.X + beam.dir.X
		beam.pos.Y = beam.pos.Y + beam.dir.Y

		if beam.pos.X >= len(s.grid[0]) || beam.pos.X < 0 || beam.pos.Y >= len(s.grid) || beam.pos.Y < 0 {
			continue
		}

		path := DirToPath(beam.dir)
		if strings.Contains(s.paths[beam.pos.Y][beam.pos.X], path) {
			continue
		} else {
			s.paths[beam.pos.Y][beam.pos.X] += path
		}

		switch s.grid[beam.pos.Y][beam.pos.X] {
		case '|':
			if beam.dir.Y == 0 {
				beam.dir.Y = -1
				beam.dir.X = 0
				beams = append(beams, Beam{
					pos: beam.pos,
					dir: astar.Point{
						Y: 1,
						X: 0,
					},
				})
			}
		case '-':
			if beam.dir.X == 0 {
				beam.dir.Y = 0
				beam.dir.X = -1
				beams = append(beams, Beam{
					pos: beam.pos,
					dir: astar.Point{
						Y: 0,
						X: 1,
					},
				})
			}
		case '/':
			if beam.dir.X == 1 {
				beam.dir.X = 0
				beam.dir.Y = -1
			} else if beam.dir.X == -1 {
				beam.dir.X = 0
				beam.dir.Y = 1
			} else if beam.dir.Y == 1 {
				beam.dir.X = -1
				beam.dir.Y = 0
			} else if beam.dir.Y == -1 {
				beam.dir.X = 1
				beam.dir.Y = 0
			} else {
				panic("Invalid beam direction")
			}
		case '\\':
			if beam.dir.X == 1 {
				beam.dir.X = 0
				beam.dir.Y = 1
			} else if beam.dir.X == -1 {
				beam.dir.X = 0
				beam.dir.Y = -1
			} else if beam.dir.Y == 1 {
				beam.dir.X = 1
				beam.dir.Y = 0
			} else if beam.dir.Y == -1 {
				beam.dir.X = -1
				beam.dir.Y = 0
			} else {
				panic("Invalid beam direction")
			}
		}
		beams = append(beams, beam)
	}
	s.beams = beams
}

func (s *Solver) CountEnergized() int {
	return u.Sum(u.Map(s.energized, func(e []bool) int { return len(u.Filter(e, func(b bool) bool { return b })) }))
}
