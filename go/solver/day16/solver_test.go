package day16

import (
	"testing"

	// u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

func TestExampleOne(t *testing.T) {
	s := New()
	s.ParseInput(`.|...\....
		|.-.\.....
		.....|-...
		........|.
		..........
		.........\
		..../.\\..
		.-.-/..|..
		.|....-|.\
		..//.|....`)
	s.Beamify()
	assert.Equal(t, 46, s.CountEnergized())
}

func TestExampleTwo(t *testing.T) {
	s := New()
	s.ParseInput(`.|...\....
		|.-.\.....
		.....|-...
		........|.
		..........
		.........\
		..../.\\..
		.-.-/..|..
		.|....-|.\
		..//.|....`)
	assert.Equal(t, 51, s.Maximize())
}
