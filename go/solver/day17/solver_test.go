package day17

import (
	// "aoc/utils/astar"
	"testing"

	// u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

//	func TestSimple(t *testing.T) {
//		s := New()
//		s.ParseInput(
//			`11111
//			11111
//			11111
//			11111
//			11111`)
//		path, _ := FindShortest(
//			astar.Point{X: 0, Y: 0},
//			astar.Point{X: s.grid.width - 1, Y: s.grid.height - 1},
//			// astar.Point{X: 3, Y: 3},
//			s.grid,
//		)
//		assert.Equal(t, 9, len(path))
//	}
func TestExampleOne(t *testing.T) {
	s := New()
	s.ParseInput(
		`2413432311323
		3215453535623
		3255245654254
		3446585845452
		4546657867536
		1438598798454
		4457876987766
		3637877979653
		4654967986887
		4564679986453
		1224686865563
		2546548887735
		4322674655533`)
	assert.Equal(t, 102, s.MinHeatLoss())
}

func TestExampleTwo(t *testing.T) {
}
