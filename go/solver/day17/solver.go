package day17

import (
	"aoc/solver"
	"aoc/utils/astar"
	"aoc/utils/priorityqueue"
	"fmt"
	u "github.com/rjNemo/underscore"
	"math"
	"strconv"
)

type Grid struct {
	data   [][]int
	width  int
	height int
}

// Distance implements astar.Graph.
func (g *Grid) Distance(a astar.Point, b astar.Point) int {
	return int(math.Abs(float64(a.X-b.X) + math.Abs(float64(a.Y-b.Y))))
}

// FindNeighbours implements astar.Graph.
func (g *Grid) FindNeighbours(pos astar.Point) []astar.Point {
	neighbours := []astar.Point{}
	if pos.X > 1 {
		neighbours = append(neighbours, astar.Point{
			X: pos.X - 1,
			Y: pos.Y,
		})
	}
	if pos.Y > 1 {
		neighbours = append(neighbours, astar.Point{
			X: pos.X,
			Y: pos.Y - 1,
		})
	}
	if pos.X < g.width-1 {
		neighbours = append(neighbours, astar.Point{
			X: pos.X + 1,
			Y: pos.Y,
		})
	}
	if pos.Y < g.width-1 {
		neighbours = append(neighbours, astar.Point{
			X: pos.X,
			Y: pos.Y + 1,
		})
	}
	return neighbours
}

var _ astar.Graph = (*Grid)(nil)

type Solver struct {
	grid Grid
}

func New() *Solver {
	return &Solver{}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	lines := solver.InputToLines(input)
	data := make([][]int, len(lines))
	for y, line := range lines {
		for _, r := range line {
			val, _ := strconv.Atoi(string(r))
			data[y] = append(data[y], val)
		}
	}
	s.grid = Grid{
		data:   data,
		width:  len(data[0]),
		height: len(data),
	}
}

func (s *Solver) Solve() {
}

func (s *Solver) MinHeatLoss() int {
	path, found := FindShortest(
		astar.Point{X: 0, Y: 0},
		astar.Point{X: s.grid.width - 1, Y: s.grid.height - 1},
		// astar.Point{X: 3, Y: 3},
		s.grid,
	)
	if !found {
		panic("No path found")
	}
	fmt.Printf("path: %+v\n", path)
	return u.Sum(u.Map(path, func(p astar.Point) int {
		return s.grid.data[p.Y][p.X]
	}))
}

type PathOption struct {
	current   astar.Point
	prev      astar.Point
	cost      int
	dir       astar.Point
	dirLength int
	path      []astar.Point
}

func FindShortest(start astar.Point, target astar.Point, grid Grid) ([]astar.Point, bool) {
	fmt.Printf("Searching: %v -> %v\n", start, target)
	visited := make(map[string]struct{}, 0)
	// costs := make(map[astar.Point]PathOption, 0)

	frontier := priorityqueue.New[PathOption]()
	frontier.Push(PathOption{
		current: start,
		prev:    start,
		cost:    grid.data[start.Y][start.X],
		dir: astar.Point{
			X: -1,
			Y: 0,
		},
		dirLength: 0,
		path:      []astar.Point{start},
	}, 0)

	for {
		po, ok := frontier.Pop()
		if !ok {
			break
		}

		if po.current == target {
			return po.path, true
		}
		if _, ok := visited[Hash(po)]; ok {
			continue
		}
		fmt.Printf("Checking %v, %v => %v \t:: %v\n", po.current.X, po.current.Y, po.cost, po.path)
		visited[Hash(po)] = struct{}{}

		neigbhours := grid.FindNeighbours(po.current)

		for _, n := range neigbhours {
			if n.X == po.prev.X && n.Y == po.prev.Y {
				continue
			}
			dir := po.current.Diff(n)
			dirLength := 1
			if dir.X == po.dir.X && dir.Y == po.dir.Y {
				dirLength += po.dirLength
			}
			if dirLength >= 3 {
				continue
			}
			cost := po.cost + grid.data[n.Y][n.X]
			path := make([]astar.Point, len(po.path))
			copy(path, po.path)
			frontier.Push(PathOption{
				current:   n,
				prev:      po.current,
				cost:      cost,
				dir:       dir,
				dirLength: dirLength,
				path:      append(path, n),
			}, -cost)
		}
	}

	return nil, false
}

func Hash(po PathOption) string {
	return fmt.Sprintf("%v-%v-%v-%v", po.current, po.prev, po.dir, po.dirLength)
}
