package day3

import (
	// "fmt"
	"testing"

	u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

func TestExampleOne(t *testing.T) {
	solver := New()

	solver.ParseInput(`467..114..
		...*......
		..35..633.
		......#...
		617*......
		.....+.58.
		..592.....
		......755.
		...$.*....
		.664.598..
	`)

	used := solver.FindUsedNumbers()

	assert.Equal(t, 4361, u.Sum(used))
}

func TestEndOfLine(t *testing.T) {
	solver := New()

	solver.ParseInput(`467..114..
		...*......
		..35..6332
		......#...
		617*......
		.....+.58.
		..592.....
		......755.
		...$.*....
		.664.598..
	`)

	used := solver.FindUsedNumbers()

	assert.Equal(t, 10060, u.Sum(used))
}

func TestExampleTwo(t *testing.T) {
	solver := New()

	solver.ParseInput(`467..114..
		...*......
		..35..633.
		......#...
		617*......
		.....+.58.
		..592.....
		......755.
		...$.*....
		.664.598..
	`)

	power := solver.FindGearPower()

	assert.Equal(t, 467835, power)
}

func TestIdenticalNumbers(t *testing.T) {
	solver := New()

	solver.ParseInput(`467..114..
		...*......
		.467..633.
		......#...
		617*......
		.....+.58.
		..592.....
		......755.
		...$.*....
		.664.598..
	`)

	power := solver.FindGearPower()

	assert.Equal(t, 467*467+755*598, power)
}
