package day3

import (
	"aoc/solver"
	"aoc/utils/astar"
	"fmt"
	"strconv"
	"unicode"

	u "github.com/rjNemo/underscore"
)

type Number struct {
	val    int
	points []astar.Point
}

type Solver struct {
	numbers []Number
	symbols map[astar.Point]string
}

func New() *Solver {
	return &Solver{
		numbers: []Number{},
		symbols: make(map[astar.Point]string),
	}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	lines := solver.InputToLines(input)
	y := 0
	for _, line := range lines {
		acc := ""
		for x, char := range line {
			foundNumber := false
			if unicode.IsNumber(char) {
				acc += string(char)
				foundNumber = true
			} else if char != '.' {
				point := astar.Point{
					X: x,
					Y: y,
				}
				s.symbols[point] = string(char)
			}

			if (!foundNumber || x == len(line)-1) && len(acc) > 0 {
				if num, ok := strconv.Atoi(acc); ok == nil {
					points := []astar.Point{}

					for i := 1; i <= len(acc); i++ {
						points = append(points, astar.Point{
							X: x - i,
							Y: y,
						})
					}
					s.numbers = append(s.numbers, Number{val: num, points: points})
				}
				acc = ""
			}
		}
		y++
	}

}

func (s *Solver) Solve() {
	used := s.FindUsedNumbers()

	fmt.Printf("Sum of used parts: %+v\n", u.Sum(used))

	power := s.FindGearPower()
	fmt.Printf("Gear power: %+v\n", power)
}

func (s *Solver) FindUsedNumbers() []int {
	found := []int{}
	for _, number := range s.numbers {
		neighbours := u.Flatmap(number.points, func(p astar.Point) []astar.Point { return p.GetNeighbours() })
		for _, n := range u.Unique(neighbours) {
			if _, ok := s.symbols[n]; ok {
				found = append(found, number.val)
				break
			}
		}
	}
	return found
}

func (s *Solver) FindGearPower() int {
	gears := []int{}
	for pos, sym := range s.symbols {
		if sym == "*" {
			matches := []int{}
			used := []int{}
			for _, n := range pos.GetNeighbours() {
				for i, number := range s.numbers {
					if u.Contains(used, i) {
						continue
					}
					if u.Contains(number.points, n) {
						matches = append(matches, number.val)
						used = append(used, i)
						break
					}
				}
				if len(matches) > 2 {
					break
				}
			}
			if len(matches) == 2 {
				gears = append(gears, matches[0]*matches[1])
			}
		}
	}
	return u.Sum(gears)
}
