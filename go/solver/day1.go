package solver

import (
	"fmt"
	"regexp"
	"strings"

	u "github.com/rjNemo/underscore"
	"strconv"
)

type DayOne struct {
	lines []string
}

var _ Solver = (*DayOne)(nil)

func (d *DayOne) ParseInput(input string) {
	d.lines = InputToLines(input)
}

func (d *DayOne) Solve() {
	val := u.Sum[int](u.Map(d.lines, func(line string) int { return d.GetCalibrationValue(line) }))
	fmt.Printf("Sum part one: %v\n", val)
	val = d.SumAll(d.lines, d.GetNum)
	fmt.Printf("Sum part two: %v\n", val)
}

func (d *DayOne) GetCalibrationValue(line string) int {
	reg := regexp.MustCompile("[a-zA-Z]")
	stripped := reg.ReplaceAllString(line, "")
	if len(stripped) < 1 {
		return 0
	}
	stripped = string(stripped[0]) + string(stripped[len(stripped)-1])
	num, _ := strconv.Atoi(stripped)
	return num
}

func (d *DayOne) SumAll(lines []string, extract func(string) (int, bool)) int {
	var answer int

	for _, line := range lines {
		var val int
		for i := 0; i < len(line); i++ {
			if v, ok := d.GetNum(line[i:]); ok {
				val = v * 10
				break
			}
		}

		for i := len(line) - 1; i >= 0; i-- {
			if v, ok := d.GetNum(line[i:]); ok {
				val += v
				break
			}
		}

		answer += val
	}

	return answer
}

func (d *DayOne) GetNum(line string) (int, bool) {
	switch {
	case line[0] >= '0' && line[0] <= '9':
		if v, err := strconv.Atoi(string(line[0])); err == nil {
			return v, true
		}
		return 0, false
	case strings.HasPrefix(line, "one"):
		return 1, true
	case strings.HasPrefix(line, "two"):
		return 2, true
	case strings.HasPrefix(line, "three"):
		return 3, true
	case strings.HasPrefix(line, "four"):
		return 4, true
	case strings.HasPrefix(line, "five"):
		return 5, true
	case strings.HasPrefix(line, "six"):
		return 6, true
	case strings.HasPrefix(line, "seven"):
		return 7, true
	case strings.HasPrefix(line, "eight"):
		return 8, true
	case strings.HasPrefix(line, "nine"):
		return 9, true
	default:
		return 0, false
	}
}
