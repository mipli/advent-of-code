package day12

import (
	"aoc/solver"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	// "fmt"
	// "unicode"
	// "math"
	// "regexp"
	// "strconv"
	// "strings"

	u "github.com/rjNemo/underscore"
)

type Line struct {
	chars  []byte
	blocks []int
}

func (l *Line) IsValid() bool {
	blocks := []int{0}
	for i, c := range l.chars {
		switch c {
		case '?':
			return false
		case '#':
			blocks[len(blocks)-1]++
		case '.':
			if i > 0 && l.chars[i-1] == '#' {
				blocks = append(blocks, 0)
			}
		default:
			return false
		}
	}
	return reflect.DeepEqual(blocks, l.blocks)
}

func (s *Solver) ValidArrangements(line Line) int {
	valid := FindValidArrangements(line.chars, line.blocks)
	return valid
}

type Solver struct {
	lines []Line
}

func New() *Solver {
	return &Solver{
		lines: []Line{},
	}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	s.lines = u.Map(solver.InputToLines(input), func(l string) Line { return s.ParseLine(l) })

}
func (s *Solver) ParseLine(line string) Line {
	splits := strings.Split(line, " ")

	return Line{
		chars: []byte(splits[0]),
		blocks: u.Map(strings.Split(splits[1], ","), func(c string) int {
			v, _ := strconv.Atoi(c)
			return v
		}),
	}
}

func (s *Solver) Solve() {

	total := s.GetTotalArrangements()
	fmt.Printf("Total: %v\n", total)

	total = s.GetTotalMultipliedArrangements()
	fmt.Printf("Total: %v\n", total)
}

func (s *Solver) GetTotalArrangements() int {
	return u.Sum(u.Map(s.lines, func(l Line) int { return s.ValidArrangements(l) }))
}
func (s *Solver) GetTotalMultipliedArrangements() int {
	for i := 0; i < len(s.lines); i++ {
		line := s.lines[i]
		blocks := make([]int, 0)
		blocks = append(blocks, line.blocks...)
		blocks = append(blocks, line.blocks...)
		blocks = append(blocks, line.blocks...)
		blocks = append(blocks, line.blocks...)
		blocks = append(blocks, line.blocks...)
		chars := make([]string, 5)
		chars = []string{string(line.chars), string(line.chars), string(line.chars), string(line.chars), string(line.chars)}
		complete := strings.Join(chars, "?")
		s.lines[i].blocks = blocks
		s.lines[i].chars = []byte(complete)
	}
	return u.Sum(u.Map(s.lines, func(l Line) int { return s.ValidArrangements(l) }))
}

var cache = map[string]int{}

func FindValidArrangements(chars []byte, blocks []int) int {
	cacheKey := fmt.Sprintf("%v-%v", chars, blocks)
	if val, ok := cache[cacheKey]; ok {
		return val
	}
	nBlocks := make([]int, len(blocks))
	copy(nBlocks, blocks)
	for i := 0; i < len(chars); i++ {

		c := chars[i]
		if c == '#' {
			if len(nBlocks) == 0 || len(chars) < i+nBlocks[0] {
				cache[cacheKey] = 0
				return 0
			}
			valid := u.All(chars[i:i+nBlocks[0]], func(r byte) bool { return r == '#' || r == '?' })

			if !valid {
				cache[cacheKey] = 0
				return 0
			}

			i += nBlocks[0] - 1
			nBlocks = nBlocks[1:]

			if i+1 < len(chars) {
				if chars[i+1] == '#' {
					cache[cacheKey] = 0
					return 0
				} else {
					i += 1
				}
			}

			if len(nBlocks) == 0 {
				valid := u.All(chars[i+1:], func(r byte) bool { return r == '.' || r == '?' })
				if valid {
					cache[cacheKey] = 1
					return 1
				} else {
					cache[cacheKey] = 0
					return 0
				}
			}
			minLen := u.Sum(nBlocks) + (len(nBlocks) - 1)
			if len(chars)-i < minLen {
				cache[cacheKey] = 0
				return 0
			}
		} else if c == '?' {
			ar := FindValidArrangements(append([]byte("."), chars[i+1:]...), nBlocks)
			br := FindValidArrangements(append([]byte("#"), chars[i+1:]...), nBlocks)
			cache[cacheKey] = ar + br
			return ar + br
		}
	}

	valid := u.All(nBlocks, func(b int) bool { return b == 0 })
	if valid {
		cache[cacheKey] = 1
		return 1
	} else {
		cache[cacheKey] = 0
		return 0
	}
}
