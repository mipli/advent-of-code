package day12

import (
	"testing"

	// u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

func TestValidLine(t *testing.T) {
	line := Line{
		chars:  []byte("#.#..###"),
		blocks: []int{1, 1, 3},
	}
	assert.Equal(t, true, line.IsValid())
}

func TestValidArrangements(t *testing.T) {
	opts := FindValidArrangements([]byte("#???"), []int{2, 1})
	assert.Equal(t, 1, opts)

	opts = FindValidArrangements([]byte(".?????."), []int{2})
	assert.Equal(t, 4, opts)

	opts = FindValidArrangements([]byte("?###????????"), []int{3, 2, 1})
	assert.Equal(t, 10, opts)
}

func TestExampleOne(t *testing.T) {
	solver := New()

	solver.ParseInput(`???.### 1,1,3
		.??..??...?##. 1,1,3
		?#?#?#?#?#?#?#? 1,3,1,6
		????.#...#... 4,1,1
		????.######..#####. 1,6,5
		?###???????? 3,2,1
	`)

	assert.Equal(t, 21, solver.GetTotalArrangements())
}

func TestExampleTwo(t *testing.T) {
	solver := New()

	solver.ParseInput(`???.### 1,1,3
		.??..??...?##. 1,1,3
		?#?#?#?#?#?#?#? 1,3,1,6
		????.#...#... 4,1,1
		????.######..#####. 1,6,5
		?###???????? 3,2,1
	`)

	assert.Equal(t, 525152, solver.GetTotalMultipliedArrangements())
}
