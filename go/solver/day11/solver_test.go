package day11

import (
	// "fmt"
	"testing"

	// u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

func TestExampleOne(t *testing.T) {
	solver := New()

	solver.ParseInput(`...#......
		.......#..
		#.........
		..........
		......#...
		.#........
		.........#
		..........
		.......#..
		#...#.....
	`)

	pairs := solver.GetPairs(solver.Grow(2))

	assert.Equal(t, 36, len(pairs))
	assert.Equal(t, 374, solver.SumPairs(pairs))
}

func TestExampleTwo(t *testing.T) {
	solver := New()

	solver.ParseInput(`...#......
		.......#..
		#.........
		..........
		......#...
		.#........
		.........#
		..........
		.......#..
		#...#.....
	`)

	pairs := solver.GetPairs(solver.Grow(10))

	assert.Equal(t, 1030, solver.SumPairs(pairs))
}

func TestExampleTwoPartTwo(t *testing.T) {
	solver := New()

	solver.ParseInput(`...#......
		.......#..
		#.........
		..........
		......#...
		.#........
		.........#
		..........
		.......#..
		#...#.....
	`)

	pairs := solver.GetPairs(solver.Grow(100))

	assert.Equal(t, 8410, solver.SumPairs(pairs))
}
