package day11

import (
	"aoc/solver"
	"aoc/utils/astar"
	"fmt"
	"math"

	u "github.com/rjNemo/underscore"
)

type Pair struct {
	lhs astar.Point
	rhs astar.Point
}

func (p *Pair) Distance() int {
	return int(math.Floor(math.Abs(float64(p.lhs.X-p.rhs.X)) + math.Abs(float64(p.lhs.Y-p.rhs.Y))))
}

type Solver struct {
	maxX   int
	maxY   int
	points []astar.Point
}

func New() *Solver {
	return &Solver{
		points: []astar.Point{},
	}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	lines := solver.InputToLines(input)
	s.maxY = len(lines)
	s.maxX = len(lines[0])
	for y, line := range lines {
		for x, chr := range line {
			if chr == '#' {
				s.points = append(s.points, astar.Point{
					X: x,
					Y: y,
				})
			}
		}
	}

}

func (s *Solver) Grow(age int) []astar.Point {
	age = age - 1
	emptyRow := []int{}
	emptyCol := []int{}
	for x := 0; x < s.maxX; x++ {
		if u.All(s.points, func(p astar.Point) bool { return p.X != x }) {
			emptyCol = append(emptyCol, x)
		}
	}
	for y := 0; y < s.maxY; y++ {
		if u.All(s.points, func(p astar.Point) bool { return p.Y != y }) {
			emptyRow = append(emptyRow, y)
		}
	}

	points := s.points
	for i, x := range emptyCol {
		for j, p := range points {
			if p.X-(i*age) > x {
				points[j] = astar.Point{
					X: p.X + age,
					Y: p.Y,
				}
			}
		}
	}
	for i, y := range emptyRow {
		for j, p := range points {
			if p.Y-(i*age) > y {
				s.points[j] = astar.Point{
					X: p.X,
					Y: p.Y + age,
				}
			}
		}
	}
	return points
}

// Solve implements solver.Solver.
func (s *Solver) Solve() {
	pairs := s.GetPairs(s.Grow(1))
	fmt.Printf("Pair sum: %+v\n", s.SumPairs(pairs))

	pairs = s.GetPairs(s.Grow(1000000))
	fmt.Printf("Pair sum: %+v\n", s.SumPairs(pairs))
}

func (s *Solver) SumPairs(pairs []Pair) int {
	return u.Sum(u.Map(pairs, func(p Pair) int {
		return p.Distance()
	}))
}

func (s *Solver) GetPairs(points []astar.Point) []Pair {
	pairs := []Pair{}
	for i := 0; i < len(points); i++ {
		for j := i + 1; j < len(points); j++ {
			pairs = append(pairs, Pair{
				lhs: s.points[i],
				rhs: s.points[j],
			})
		}
	}
	return pairs
}
