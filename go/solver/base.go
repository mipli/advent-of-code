package solver

import (
	"strings"

	u "github.com/rjNemo/underscore"
)

type Solver interface {
	ParseInput(string)
	Solve()
}

func InputToLines(input string) []string {
	return u.
		NewPipe(strings.Split(string(input), "\n")).
		Map(func(s string) string {
			return strings.TrimSpace(s)
		}).
		Filter(func(s string) bool {
			return len(s) > 0
		}).
		Value
}

func InputToLineBlocks(input string) [][]string {
	groups := [][]string{{}}
	lines := u.
		NewPipe(strings.Split(string(input), "\n")).
		Map(func(s string) string {
			return strings.TrimSpace(s)
		}).Value

	for _, line := range lines {
		if len(line) == 0 {
			groups = append(groups, []string{})
		}
		groups[len(groups)-1] = append(groups[len(groups)-1], line)
	}

	return groups
}
