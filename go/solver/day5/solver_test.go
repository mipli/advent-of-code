package day5

import (
	// "fmt"
	"testing"

	// u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

func TestExampleOne(t *testing.T) {
	solver := New()

	solver.ParseInput(`seeds: 79 14 55 13

		seed-to-soil map:
		50 98 2
		52 50 48

		soil-to-fertilizer map:
		0 15 37
		37 52 2
		39 0 15

		fertilizer-to-water map:
		49 53 8
		0 11 42
		42 0 7
		57 7 4

		water-to-light map:
		88 18 7
		18 25 70

		light-to-temperature map:
		45 77 23
		81 45 19
		68 64 13

		temperature-to-humidity map:
		0 69 1
		1 0 69

		humidity-to-location map:
		60 56 37
		56 93 4
	`)

	assert.Equal(t, 82, solver.GetLocationForSeed(79))
	assert.Equal(t, 43, solver.GetLocationForSeed(14))
	assert.Equal(t, 86, solver.GetLocationForSeed(55))
	assert.Equal(t, 35, solver.GetLocationForSeed(13))
	assert.Equal(t, 35, solver.GetMinLocation())
}

func TestExampleTwo(t *testing.T) {
	solver := New()

	solver.ParseInput(`seeds: 79 14 55 13

		seed-to-soil map:
		50 98 2
		52 50 48

		soil-to-fertilizer map:
		0 15 37
		37 52 2
		39 0 15

		fertilizer-to-water map:
		49 53 8
		0 11 42
		42 0 7
		57 7 4

		water-to-light map:
		88 18 7
		18 25 70

		light-to-temperature map:
		45 77 23
		81 45 19
		68 64 13

		temperature-to-humidity map:
		0 69 1
		1 0 69

		humidity-to-location map:
		60 56 37
		56 93 4
	`)

	assert.Equal(t, 82, solver.GetMinSeed(46))
	assert.Equal(t, 46, solver.GetMinRangeLocation())
}
