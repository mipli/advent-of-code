package day5

import (
	"aoc/solver"
	"fmt"
	// "fmt"
	"unicode"
	// "math"
	// "regexp"
	"strconv"
	"strings"

	u "github.com/rjNemo/underscore"
)

type ConversionMap struct {
	source int
	dest   int
	rng    int
}

type ConversionSet struct {
	maps []ConversionMap
}

func (c *ConversionSet) DestToSource(seed int) int {
	for _, conversion := range c.maps {
		if seed >= conversion.dest && seed <= conversion.dest+conversion.rng {
			delta := seed - conversion.dest
			return conversion.source + delta
		}
	}
	return seed
}

func (c *ConversionSet) SourceToDest(seed int) int {
	for _, conversion := range c.maps {
		if seed >= conversion.source && seed <= conversion.source+conversion.rng {
			delta := seed - conversion.source
			return conversion.dest + delta
		}
	}
	return seed
}

type Solver struct {
	seeds []int
	sets  []ConversionSet
}

func New() *Solver {
	return &Solver{
		seeds: []int{},
		sets:  make([]ConversionSet, 7),
	}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	lines := solver.InputToLines(input)

	seedList := strings.Split(lines[0], ":")
	s.seeds = u.Map(strings.Split(strings.TrimSpace(seedList[1]), " "), func(s string) int {
		v, _ := strconv.Atoi(s)
		return v
	})

	mapIdx := -1

	for _, line := range lines[1:] {
		if unicode.IsNumber(rune(line[0])) {
			numbers := u.Map(strings.Split(strings.TrimSpace(line), " "), func(s string) int {
				v, _ := strconv.Atoi(s)
				return v
			})
			s.sets[mapIdx].maps = append(s.sets[mapIdx].maps, ConversionMap{
				dest:   numbers[0],
				source: numbers[1],
				rng:    numbers[2] - 1,
			})
		} else {
			mapIdx++
			s.sets[mapIdx] = ConversionSet{
				maps: []ConversionMap{},
			}
		}
	}
}

func (s *Solver) Solve() {
	fmt.Printf("Min location: %+v\n", s.GetMinLocation())
	fmt.Printf("Min range location: %+v\n", s.GetMinRangeLocation())
}

func (s *Solver) GetLocationForSeed(seed int) int {
	current := seed
	for _, conversionSet := range s.sets {
		current = conversionSet.SourceToDest(current)
	}
	return current
}

func (s *Solver) GetMinLocation() int {
	return u.Min(u.Map(s.seeds, func(n int) int {
		return s.GetLocationForSeed(n)
	}))
}

func (s *Solver) GetMinSeed(loc int) int {
	current := loc
	for i := len(s.sets) - 1; i >= 0; i-- {
		current = s.sets[i].DestToSource(current)
	}
	return current
}

func (s *Solver) IsValidSeed(seed int) bool {
	for i := 0; i < len(s.seeds); i += 2 {
		if seed >= s.seeds[i] && seed < s.seeds[i]+s.seeds[i+1] {
			return true
		}
	}
	return false
}

func (s *Solver) GetMinRangeLocation() int {
	for i := 0; true; i++ {
		seed := s.GetMinSeed(i)
		if s.IsValidSeed(seed) {
			return i
		}
	}

	return 0
}
