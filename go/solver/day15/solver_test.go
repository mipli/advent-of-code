package day15

import (
	"testing"

	// u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

func TestHash(t *testing.T) {
	assert.Equal(t, 52, Hash("HASH"))
}

func TestExampleOne(t *testing.T) {
	s := New()
	s.ParseInput("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7")
	assert.Equal(t, 1320, s.SumHashes())
}

func TestExampleTwo(t *testing.T) {
	s := New()
	s.ParseInput("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7")
	s.FillBoxes()
	assert.Equal(t, 145, s.GetBoxPower())
}
