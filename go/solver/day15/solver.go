package day15

import (
	"aoc/solver"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"

	u "github.com/rjNemo/underscore"
)

type Lens struct {
	label string
	power int
}

type Box struct {
	lenses []Lens
}

type Solver struct {
	seq   []string
	boxes []Box
}

func New() *Solver {
	return &Solver{
		seq:   []string{},
		boxes: make([]Box, 256),
	}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	for _, part := range strings.Split(strings.TrimSpace(input), ",") {
		s.seq = append(s.seq, part)
	}
}

func (s *Solver) Solve() {
	fmt.Printf("Sum of hashes: %v\n", s.SumHashes())
	s.FillBoxes()
	fmt.Printf("Box values: %v\n", s.GetBoxPower())
}

func (s *Solver) DisplayBoxes() {
	for i, box := range s.boxes {
		if len(box.lenses) > 0 {
			fmt.Printf("[%v] %+v\n", i, box.lenses)
		}
	}
}

func (s *Solver) GetBoxPower() int {
	sum := 0
	for i, box := range s.boxes {
		for j, lens := range box.lenses {
			p := (i + 1) * (j + 1) * lens.power
			sum += p
		}
	}
	return sum
}

func (s *Solver) FillBoxes() {
	labelRegex := regexp.MustCompile("^[a-zA-Z]+")

seq:
	for _, seq := range s.seq {
		label := labelRegex.FindString(seq)
		val := string(seq[len(seq)-1])
		boxIdx := Hash(label)
		if val == "-" {
			s.boxes[boxIdx].lenses = u.Filter(s.boxes[boxIdx].lenses, func(l Lens) bool { return l.label != label })
		} else {
			power, _ := strconv.Atoi(val)
			for idx, lens := range s.boxes[boxIdx].lenses {
				if lens.label == label {
					s.boxes[boxIdx].lenses[idx].power = power
					continue seq
				}
			}
			s.boxes[boxIdx].lenses = append(s.boxes[boxIdx].lenses, Lens{
				label: label,
				power: power,
			})

		}
	}
}

func (s *Solver) SumHashes() int {
	return u.Sum(u.Map(s.seq, func(s string) int { return Hash(s) }))
}

func Hash(input string) int {
	hash := 0
	for _, r := range input {
		hash += int(r)
		hash *= 17
		hash = int(math.Mod(float64(hash), 256))
	}
	return hash
}
