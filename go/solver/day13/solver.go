package day13

import (
	"aoc/solver"
	"fmt"
	u "github.com/rjNemo/underscore"
	"math"
)

type Grid struct {
	data [][]rune
}

type Solver struct {
	grids []Grid
}

func New() *Solver {
	return &Solver{
		grids: []Grid{},
	}
}

var _ solver.Solver = (*Solver)(nil)

func (s *Solver) ParseInput(input string) {
	s.grids = []Grid{}
	groups := solver.InputToLineBlocks(input)
	grid := Grid{
		data: make([][]rune, 1),
	}
	for _, lines := range groups {
		for _, line := range lines {
			for _, c := range line {
				grid.data[len(grid.data)-1] = append(grid.data[len(grid.data)-1], c)
			}
			grid.data = append(grid.data, []rune{})
		}
		grid.data = grid.data[:len(grid.data)-1]
		if len(grid.data) > 0 {
			s.grids = append(s.grids, grid)
			grid = Grid{
				data: make([][]rune, 0),
			}
		}
	}
}

func (s *Solver) Solve() {
	total := s.GetTotalReflectionValue()
	fmt.Printf("Reflection value: %v\n", total)

	total = s.GetCleanedTotalReflectionValue()
	fmt.Printf("Cleaned reflection value: %v\n", total)

}

func (s *Solver) GetCleanedTotalReflectionValue() int {
	return u.Sum(u.Map(s.grids, func(g Grid) int { return GetCleanedReflectionValue(g) }))
}

func GetCleanedReflectionValue(grid Grid) int {
	for col := 1; col < len(grid.data[0]); col++ {
		if IsCleanedMirrorAxisCol(grid, col) {
			return col
		}
	}

	for row := 1; row < len(grid.data); row++ {
		if IsCleanedMirrorAxisRow(grid, row) {
			return row * 100
		}
	}
	return 0
}

func IsCleanedMirrorAxisRow(grid Grid, row int) bool {
	if row < 1 || row > len(grid.data) {
		return false
	}
	toCompare := int(math.Min(float64(row), float64(len(grid.data)-row)))

	d := 0

	for i := 0; i < toCompare; i++ {
		d += GetDiffCount(grid.data[row+i], grid.data[row-i-1])
		if d > 1 {
			return false
		}
	}
	return d == 1
}

func IsCleanedMirrorAxisCol(grid Grid, col int) bool {
	if col < 1 || col > len(grid.data[0]) {
		return false
	}
	toCompare := int(math.Min(float64(col), float64(len(grid.data[0])-col)))

	d := 0

	for i := 0; i < toCompare; i++ {
		a := []rune{}
		b := []rune{}
		for y := range grid.data {
			a = append(a, grid.data[y][col+i])
			b = append(b, grid.data[y][col-i-1])
		}
		d += GetDiffCount(a, b)
		if d > 1 {
			return false
		}
	}
	return d == 1
}

func (s *Solver) GetTotalReflectionValue() int {
	return u.Sum(u.Map(s.grids, func(g Grid) int { return GetReflectionValue(g) }))
}

func GetReflectionValue(grid Grid) int {
	for col := 1; col < len(grid.data[0]); col++ {
		if IsMirrorAxisCol(grid, col) {
			return col
		}
	}

	for row := 1; row < len(grid.data); row++ {
		if IsMirrorAxisRow(grid, row) {
			return row * 100
		}
	}
	return 0
}

func IsMirrorAxisRow(grid Grid, row int) bool {
	if row < 1 || row > len(grid.data) {
		return false
	}
	toCompare := int(math.Min(float64(row), float64(len(grid.data)-row)))

	for i := 0; i < toCompare; i++ {
		if GetDiffCount(grid.data[row+i], grid.data[row-i-1]) != 0 {
			return false
		}
	}
	return true
}

func IsMirrorAxisCol(grid Grid, col int) bool {
	if col < 1 || col > len(grid.data[0]) {
		return false
	}
	toCompare := int(math.Min(float64(col), float64(len(grid.data[0])-col)))

	for i := 0; i < toCompare; i++ {
		a := []rune{}
		b := []rune{}
		for y := range grid.data {
			a = append(a, grid.data[y][col+i])
			b = append(b, grid.data[y][col-i-1])
		}
		if GetDiffCount(a, b) != 0 {
			return false
		}
	}
	return true
}

func GetDiffCount(a []rune, b []rune) int {
	if len(a) != len(b) {
		panic("Comparing different length slices")
	}

	d := 0
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			d += 1
		}
	}
	return d
}
