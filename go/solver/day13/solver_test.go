package day13

import (
	"testing"

	// u "github.com/rjNemo/underscore"
	"github.com/stretchr/testify/assert"
)

func TestExampleOne(t *testing.T) {
	solver := New()

	solver.ParseInput(
		`#.##..##.
		..#.##.#.
		##......#
		##......#
		..#.##.#.
		..##..##.
		#.#.##.#.

		#...##..#
		#....#..#
		..##..###
		#####.##.
		#####.##.
		..##..###
		#....#..#
		`)

	assert.Equal(t, 2, len(solver.grids))

	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 3))
	assert.Equal(t, true, IsMirrorAxisCol(solver.grids[0], 5))
	assert.Equal(t, false, IsMirrorAxisCol(solver.grids[0], 2))

	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[1], 2))
	assert.Equal(t, false, IsMirrorAxisCol(solver.grids[1], 5))
	assert.Equal(t, true, IsMirrorAxisRow(solver.grids[1], 4))

	assert.Equal(t, 5, GetReflectionValue(solver.grids[0]))
	assert.Equal(t, 400, GetReflectionValue(solver.grids[1]))
	assert.Equal(t, 405, solver.GetTotalReflectionValue())
}

func TestGrids(t *testing.T) {
	solver := New()

	solver.ParseInput(
		`#..###.##.#.#...#
		#.##.....#.###.#.
		#.##..#.###.#.#.#
		##........#..###.
		#..##.#.###..#.#.
		#..#.#...#....##.
		#..#.#...#....##.
		#..##.#.###..#.#.
		##........#..###.
		#.##..#.###.#.#.#
		#.##.....#.###.#.
		#..###.##...#...#
		.##...##..#..##..
		####.#...#...##..
		..##.#..#..#.#.#.
		##.##.#.#..#.#..#
		##.##.#.#..#.#..#`)

	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 1))
	assert.Equal(t, true, IsMirrorAxisRow(solver.grids[0], 16))

	solver.ParseInput(
		`#..###.##.#.#...#
		#..###.##.#.#...#
		#.##.....#.###.#.
		#.##..#.###.#.#.#
		##........#..###.
		#..##.#.###..#.#.
		#..#.#...#....##.
		#..#.#...#....##.
		#..##.#.###..#.#.
		##........#..###.
		#.##..#.###.#.#.#
		#.##.....#.###.#.
		#..###.##...#...#
		.##...##..#..##..
		####.#...#...##..
		..##.#..#..#.#.#.
		##.##.#.#..#.#..#
		##.##.#.#..#.#..#`)

	assert.Equal(t, true, IsMirrorAxisRow(solver.grids[0], 1))

	solver.ParseInput(
		`..#.#...##.
		##.#.##.##.
		##.##.##..#
		..#..##.##.
		##.#.###.##
		..#.#..#..#
		..###..#..#
		..###.##..#
		##....#####
		##..##.....
		..#..#.#..#`)
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 1))
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 2))
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 3))
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 4))
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 5))
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 6))
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 7))
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 8))
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 9))
	assert.Equal(t, false, IsMirrorAxisRow(solver.grids[0], 10))
}
