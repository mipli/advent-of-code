package solver

import (
	"fmt"
	"os"

	"github.com/bzick/tokenizer"
	u "github.com/rjNemo/underscore"
)

type DayTwo struct {
	games [][]Set
}

type Set struct {
	red   int
	blue  int
	green int
}

func (s *Set) IsValid(red int, green int, blue int) bool {
	return s.red <= red && s.green <= green && s.blue <= blue
}

var _ Solver = (*DayTwo)(nil)

func (d *DayTwo) ParseInput(input string) {
	lines := InputToLines(input)
	d.games = u.Map(lines, d.ParseLine)
}

func (d *DayTwo) Solve() {
	sum := d.SumGames()
	fmt.Printf("Sum of games: %+v\n", sum)
}

func (d *DayTwo) PartTwo(lines []string) {
	sum := d.Power(d.games)
	fmt.Printf("Power of games: %+v\n", sum)
}

func (d *DayTwo) Power(games [][]Set) int {
	sum := 0
	for _, sets := range games {
		min := u.Reduce(sets, func(s Set, acc Set) Set {
			if s.red > acc.red {
				acc.red = s.red
			}
			if s.green > acc.green {
				acc.green = s.green
			}
			if s.blue > acc.blue {
				acc.blue = s.blue
			}
			return acc
		}, Set{
			red:   0,
			green: 0,
			blue:  0,
		})
		sum += (min.red * min.green * min.blue)
	}
	return sum
}

func (d *DayTwo) SumGames() int {
	sum := 0
	for id, sets := range d.games {
		if u.All(sets, func(s Set) bool {
			return s.IsValid(12, 13, 14)
		}) {
			sum += id + 1
		}
	}
	return sum
}

func (d *DayTwo) ParseLine(line string) []Set {
	const (
		TokenGame      = 1
		TokenRed       = 2
		TokenGreen     = 3
		TokenBlue      = 4
		TokenColon     = 5
		TokenComma     = 6
		TokenSemicolon = 7
	)
	parser := tokenizer.New()

	parser.DefineTokens(TokenGame, []string{"Game"})
	parser.DefineTokens(TokenRed, []string{"red"})
	parser.DefineTokens(TokenGreen, []string{"green"})
	parser.DefineTokens(TokenBlue, []string{"blue"})
	parser.DefineTokens(TokenColon, []string{":"})
	parser.DefineTokens(TokenComma, []string{","})
	parser.DefineTokens(TokenSemicolon, []string{";"})
	stream := parser.ParseString(line)
	defer stream.Close()

	set := Set{
		red:   0,
		blue:  0,
		green: 0,
	}
	sets := []Set{}

	for stream.IsValid() {
		if stream.CurrentToken().Is(TokenGame) {
			stream.GoNext()
			if !stream.CurrentToken().Is(tokenizer.TokenInteger) {
				fmt.Fprintf(os.Stderr, "Error parsing line: %+v\nGame Number Token: %+v\n", line, stream.CurrentToken())
				os.Exit(1)
			}
			stream.GoNext()
			if !stream.CurrentToken().Is(TokenColon) {
				fmt.Fprintf(os.Stderr, "Error parsing line: %+v\nColon Token: %+v\n", line, stream.CurrentToken())
				os.Exit(1)
			}
			stream.GoNext()
			continue
		}

		if !stream.CurrentToken().Is(tokenizer.TokenInteger) {
			fmt.Fprintf(os.Stderr, "Error parsing line: %+v\nStone Token: %+v\n", line, stream.CurrentToken())
			os.Exit(1)
		}
		value := int(stream.CurrentToken().ValueInt64())
		stream.GoNext()

		if stream.CurrentToken().Is(TokenRed) {
			set.red = value
		} else if stream.CurrentToken().Is(TokenGreen) {
			set.green = value
		} else if stream.CurrentToken().Is(TokenBlue) {
			set.blue = value
		} else {
			fmt.Fprintf(os.Stderr, "Error parsing line: %+v\nToken: %+v\n", line, stream.CurrentToken())
			os.Exit(1)
		}
		stream.GoNext()
		if stream.CurrentToken().Is(TokenSemicolon) {
			sets = append(sets, set)
			set = Set{
				red:   0,
				green: 0,
				blue:  0,
			}
		} else if !stream.CurrentToken().Is(TokenComma) {
			sets = append(sets, set)
			return sets
		}
		stream.GoNext()
	}

	return sets
}
