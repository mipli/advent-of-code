package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	"github.com/ddo/rq"
	"github.com/ddo/rq/client"
	"github.com/ddo/rq/client/jar"
)

func fetchInput(path string, day int, session string) error {
	if _, err := os.Stat(path); !errors.Is(err, os.ErrNotExist) {
		return nil
	}

	cookieJar := jar.New()
	cookie := http.Cookie{
		Name:  "session",
		Value: session,
	}
	cookieJar.SetOne("adventofcode.com", &cookie)
	customClient := client.New(&client.Option{
		Jar: cookieJar,
	})
	url := fmt.Sprintf("https://adventofcode.com/2023/day/%v/input", day)
	fmt.Printf("Fetching URL: %s\n", url)
	r := rq.Get(url)
	data, res, err := customClient.Send(r, true)
	if err != nil || res.StatusCode > 299 {
		return err
	}

	return os.WriteFile(path, data, 0644)
}

func readInput(path string, day int) (string, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func getInput(day int, session string) (string, error) {
	path := fmt.Sprintf("./inputs/%v", day)
	if err := fetchInput(path, day, session); err != nil {
		return "", err
	}
	return readInput(path, day)
}
