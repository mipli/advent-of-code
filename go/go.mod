module aoc

go 1.20

require (
	github.com/bzick/tokenizer v1.4.0
	github.com/ddo/rq v0.0.0-20190828174524-b3daa55fcaba
	github.com/joho/godotenv v1.5.1
	github.com/rjNemo/underscore v0.6.1
	github.com/stretchr/testify v1.8.4
	golang.org/x/exp v0.0.0-20231127185646-65229373498e
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ddo/pick-json v0.0.0-20170207095303-c8760e09e0fe // indirect
	github.com/mitchellh/hashstructure/v2 v2.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/ddo/go-dlog.v2 v2.1.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
