package aoc

import aoc.aoc2022.*
import aoc.utils.AocSolver
import io.github.cdimascio.dotenv.dotenv
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.runBlocking
import java.io.File

fun main(args: Array<String>) {
    var days = mutableMapOf<Int, AocSolver<*, *>>()
    days.put(1, Day1())
    days.put(2, Day2())
    days.put(3, Day3())
    days.put(4, Day4())
    days.put(5, Day5())
    days.put(6, Day6())
    days.put(7, Day7())

    require(args.size <= 2) {
        println("invalid argument count")
    }

    val dayNumber = if (args.isNullOrEmpty()) {
        days.keys.maxBy { it }
    } else {
        val dayNumber = args[0].toInt()
        dayNumber
    }
    days[dayNumber]?.let {
        val path = "./inputs/day$dayNumber"
        val file = File(path)
        val input = if (!file.exists()) {
            val dotenv = dotenv()
            val session: String = dotenv["SESSION_ID"]
            fetchInput(dayNumber, path, session)
        } else {
            file.readText()
        }
        solveDay(it, input)
    } ?: println("Invalid day")
}

fun solveDay(day: AocSolver<*, *>, input: String) {
    day.parseInput(input)
    val first = day.solvePartOne()
    val snd = day.solvePartTwo()
    println("Part one: $first")
    println("Part one: $snd")
}

fun fetchInput(day: Int, path: String, session: String): String {
    val url = "https://adventofcode.com/2022/day/$day/input"
    var inp: String? = null
    runBlocking {
        val client = HttpClient(CIO)
        val response = client.get(url) {
            header("Cookie", "session=$session")
        }
        inp = response.bodyAsText()
        client.close()
    }

    val input = inp!!

    val file = File(path)
    file.writeText(input)
    return input
}
