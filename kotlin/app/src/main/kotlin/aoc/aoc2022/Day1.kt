package aoc.aoc2022

import aoc.utils.AocSolver

class Day1 : AocSolver<Int, Int> {
    var elves: List<List<Int>> = listOf()

    override fun parseInput(input: String) {
        val lines = input.lines()

        elves = lines.fold(mutableListOf(mutableListOf<Int>())) { acc, rawLine ->
            val line = rawLine.trim()
            if (line == "") {
                acc.add(mutableListOf())
            } else {
                var lst = acc.last().toMutableList()
                lst.add(line.toInt())
                acc[acc.size - 1] = lst
            }
            acc
        }
    }

    override fun solvePartOne(): Int {
        val max = elves.map { it.sumOf { it } }.maxOf { it }
        return max
    }

    override fun solvePartTwo(): Int {
        val topThree = elves.map { it.sumOf { it } }.sortedDescending().take(3).sumOf { it }
        return topThree
    }
}
