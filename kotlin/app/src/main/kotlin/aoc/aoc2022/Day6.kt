package aoc.aoc2022

import aoc.utils.AocSolver

class Day6 : AocSolver<Int, Int> {
    var sequence: String = ""

    override fun parseInput(input: String) {
        sequence = input
    }

    override fun solvePartOne(): Int {
        return findMarker(sequence, 4)
    }

    override fun solvePartTwo(): Int {
        return findMarker(sequence, 14)
    }
}

private fun findMarker(sequence: String, size: Int): Int {
    return sequence.withIndex().windowed(size).find {
        val set = it.map { it.value }.toSet()
        set.size == size
    }?.let {
        it[it.lastIndex].index + 1
    }!!
}
