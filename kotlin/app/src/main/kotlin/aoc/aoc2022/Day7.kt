package aoc.aoc2022

import aoc.utils.AocSolver

enum class Cmd {
    Cd,
    Ls,
}

data class Dir(val name: String, val children: MutableList<Dir>, val parent: Dir?, var size: Int = 0)

class Day7 : AocSolver<Int, Int> {
    var root: Dir = Dir(name = "/", children = mutableListOf(), parent = null)

    override fun parseInput(input: String) {
        val lines = input.lines().map { it.trim() }.filter { !it.isEmpty() }
        var base = Pair(0, root)
        while (base.first < lines.size) {
            base = parseLine(lines, base.first, base.second)
        }
        addDirSize(root)
    }

    override fun solvePartOne(): Int {
        return sumLessThan(100000, root)
    }

    override fun solvePartTwo(): Int {
        val neededSpace = 30_000_000 - (70_000_000 - root.size)
        return smallestBiggerThan(neededSpace, root)
    }

    private fun sumLessThan(limit: Int, current: Dir): Int {
        var size = 0

        for (child in current.children) {
            size += sumLessThan(limit, child)
        }

        if (current.size < limit) {
            size += current.size
        }

        return size
    }

    private fun smallestBiggerThan(limit: Int, current: Dir): Int {
        var size = if (current.size > limit) {
            current.size
        } else {
            Int.MAX_VALUE
        }
        val list = current.children.map { smallestBiggerThan(limit, it) }.toMutableList()
        list.add(size)

        return list.min()
    }

    fun parseLine(lines: List<String>, counter: Int, current: Dir): Pair<Int, Dir> {
        val cmd = parseCmd(lines[counter].trim())
        val inc = when (cmd.first) {
            Cmd.Cd -> {
                if (cmd.second == "/") {
                    Pair(1, root)
                } else if (cmd.second == "..") {
                    Pair(1, current.parent!!)
                } else {
                    val dir: Dir = current.children.find {
                        it.name == cmd.second
                    } ?: createChild(cmd.second, current)
                    Pair(1, dir)
                }
            }
            Cmd.Ls -> {
                var inc = 1
                while (counter + inc < lines.size && !isCommand(lines[counter + inc])) {
                    val size = getFileSize(lines[counter + inc])
                    current.size += size
                    inc += 1
                }
                Pair(inc, current)
            }
        }
        return Pair(counter + inc.first, inc.second)
    }

    private fun addDirSize(dir: Dir) {
        var size = 0

        for (child in dir.children) {
            addDirSize(child)
            size += child.size
        }

        dir.size += size
    }

    private fun getFileSize(line: String): Int {
        val parts = line.split(" ")
        if (parts[0] != "dir") {
            return parts[0].toInt()
        } else {
            return 0
        }
    }

    private fun createChild(name: String, parent: Dir): Dir {
        val dir = Dir(name = name, children = mutableListOf(), parent = parent)
        parent.children.add(dir)
        return dir
    }

    private fun isCommand(line: String): Boolean {
        return line[0] == '$'
    }

    fun parseCmd(line: String): Pair<Cmd, String> {
        val parts = line.trim().split(" ")
        if (parts[0] != "$") {
            throw IllegalArgumentException("illegal command: $line")
        }
        return when (parts[1]) {
            "ls" -> Pair(Cmd.Ls, "")
            "cd" -> Pair(Cmd.Cd, parts[2])
            else -> throw IllegalArgumentException("illegal command: $line")
        }
    }
}
