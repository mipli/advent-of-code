package aoc.aoc2022

import aoc.utils.AocSolver

data class Instruction(val count: Int, val source: Int, val dest: Int)
data class Stack(val crates: ArrayDeque<Char> = ArrayDeque()) {
    fun copy(): Stack {
        return Stack(crates = ArrayDeque(crates.map { it }))
    }
}

enum class ParseState {
    Crates,
    Instructions,
    Done,
}

class Day5 : AocSolver<String, String> {
    var instructions: List<Instruction> = listOf()
    var stacks: List<Stack> = listOf()

    override fun parseInput(input: String) {
        val lines = input.lines()

        var state = ParseState.Crates

        val instRegex = """^move (\d+) from (\d+) to (\d+)$""".toRegex()

        val insts: MutableList<Instruction> = mutableListOf()
        val stcks: MutableList<Stack> = mutableListOf()

        for (line in lines) {
            if (line.isEmpty()) {
                continue
            }
            when (state) {
                ParseState.Crates -> {
                    for (i in 1..line.length step 4) {
                        if (line[i].isLetter()) {
                            val idx = (i - 1) / 4
                            if (stcks.size <= idx) {
                                for (j in stcks.size..idx) {
                                    stcks.add(Stack())
                                }
                            }
                            stcks[idx].crates.addFirst(line[i])
                        } else if (line[i].isDigit()) {
                            state = ParseState.Instructions
                        }
                    }
                }
                ParseState.Instructions -> {
                    val data = instRegex.find(line)
                    if (data?.groups?.size == 4) {
                        val values = data.groupValues
                        insts.add(Instruction(count = values[1].toInt(), source = values[2].toInt(), dest = values[3].toInt()))
                    } else {
                        state = ParseState.Done
                    }
                }
                ParseState.Done -> break
            }
        }

        stacks = stcks.toList()
        instructions = insts.toList()
    }

    override fun solvePartOne(): String {
        val current = stacks.map { it.copy() }

        for (inst in instructions) {
            apply(inst, current)
        }

        return current.joinToString("") {
            it.crates[it.crates.size - 1].toString()
        }
    }

    override fun solvePartTwo(): String {
        val current = stacks.map { it.copy() }

        for (inst in instructions) {
            apply9001(inst, current)
        }

        return current.joinToString("") {
            it.crates[it.crates.size - 1].toString()
        }
    }
}

private fun apply(instruction: Instruction, stacks: List<Stack>) {
    for (i in 1..instruction.count) {
        val crate = stacks[instruction.source - 1].crates.removeLast()
        stacks[instruction.dest - 1].crates.addLast(crate)
    }
}

private fun apply9001(instruction: Instruction, stacks: List<Stack>) {
    val size = stacks[instruction.source - 1].crates.size
    val subSlice = stacks[instruction.source - 1].crates.subList(size - instruction.count, size)
    stacks[instruction.dest - 1].crates.addAll(subSlice)
    subSlice.clear()
}
