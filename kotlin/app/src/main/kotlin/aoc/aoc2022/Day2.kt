package aoc.aoc2022

import aoc.utils.AocSolver

class Day2 : AocSolver<Int, Int> {
    var matches: List<Pair<Choice, Choice>> = listOf()
    var unknownChoice: List<Pair<Choice, MatchResult>> = listOf()

    override fun parseInput(input: String) {
        val lines = input.lines()

        matches = lines.fold(mutableListOf<Pair<Choice, Choice>>()) { acc, rawLine ->
            val choices = rawLine.trim().split(" ")
            if (choices.size == 2) {
                val them = when (choices[0]) {
                    "A" -> Choice.Rock
                    "B" -> Choice.Paper
                    "C" -> Choice.Scissors
                    else -> throw IllegalArgumentException("Illegal match line $choices")
                }
                val self = when (choices[1]) {
                    "X" -> Choice.Rock
                    "Y" -> Choice.Paper
                    "Z" -> Choice.Scissors
                    else -> throw IllegalArgumentException("Illegal match line $choices")
                }
                acc.add(Pair(them, self))
            }
            acc
        }

        unknownChoice = lines.fold(mutableListOf<Pair<Choice, MatchResult>>()) { acc, rawLine ->
            val choices = rawLine.trim().split(" ")
            if (choices.size == 2) {
                val them = when (choices[0]) {
                    "A" -> Choice.Rock
                    "B" -> Choice.Paper
                    "C" -> Choice.Scissors
                    else -> throw IllegalArgumentException("Illegal match line $choices")
                }
                val result = when (choices[1]) {
                    "X" -> MatchResult.Loss
                    "Y" -> MatchResult.Tie
                    "Z" -> MatchResult.Win
                    else -> throw IllegalArgumentException("Illegal match line $choices")
                }
                acc.add(Pair(them, result))
            }
            acc
        }
    }

    override fun solvePartOne(): Int {
        val points = strategyResult()
        return points
    }

    override fun solvePartTwo(): Int {
        val points = unknownResult()
        return points
    }

    public fun strategyResult(): Int {
        return matches.map {
            val resultPoints = MatchResult.score(matchResult(it.first, it.second))
            val choicePoints = Choice.score(it.second)
            choicePoints + resultPoints
        }
            .sumOf { it }
    }

    public fun unknownResult(): Int {
        return unknownChoice.map {
            val self = when (it.first to it.second) {
                Choice.Rock to MatchResult.Win -> Choice.Paper
                Choice.Rock to MatchResult.Loss -> Choice.Scissors
                Choice.Rock to MatchResult.Tie -> Choice.Rock
                Choice.Paper to MatchResult.Win -> Choice.Scissors
                Choice.Paper to MatchResult.Loss -> Choice.Rock
                Choice.Paper to MatchResult.Tie -> Choice.Paper
                Choice.Scissors to MatchResult.Win -> Choice.Rock
                Choice.Scissors to MatchResult.Loss -> Choice.Paper
                Choice.Scissors to MatchResult.Tie -> Choice.Scissors
                else -> throw IllegalArgumentException("Illegal match setup")
            }
            val resultPoints = MatchResult.score(it.second)
            val choicePoints = Choice.score(self)
            choicePoints + resultPoints
        }
            .sumOf { it }
    }
}

enum class MatchResult {
    Win,
    Loss,
    Tie,
    ;

    companion object {
        fun score(self: MatchResult): Int {
            return when (self) {
                MatchResult.Win -> 6
                MatchResult.Loss -> 0
                MatchResult.Tie -> 3
            }
        }
    }
}

fun matchResult(elf: Choice, self: Choice): MatchResult {
    val res = when (elf to self) {
        Choice.Rock to Choice.Rock -> MatchResult.Tie
        Choice.Rock to Choice.Paper -> MatchResult.Win
        Choice.Rock to Choice.Scissors -> MatchResult.Loss
        Choice.Paper to Choice.Rock -> MatchResult.Loss
        Choice.Paper to Choice.Paper -> MatchResult.Tie
        Choice.Paper to Choice.Scissors -> MatchResult.Win
        Choice.Scissors to Choice.Rock -> MatchResult.Win
        Choice.Scissors to Choice.Paper -> MatchResult.Loss
        Choice.Scissors to Choice.Scissors -> MatchResult.Tie
        else -> throw IllegalArgumentException("Illegal match setup")
    }
    return res
}

enum class Choice {
    Rock,
    Paper,
    Scissors,
    ;

    companion object {
        fun score(self: Choice): Int {
            return when (self) {
                Choice.Rock -> 1
                Choice.Paper -> 2
                Choice.Scissors -> 3
            }
        }
    }
}
