package aoc.aoc2022

import aoc.utils.AocSolver

public fun containedBy(left: ClosedRange<Int>, right: ClosedRange<Int>): Boolean {
    return (left.contains(right.start) && left.contains(right.endInclusive)) ||
        (right.contains(left.start) && right.contains(left.endInclusive))
}

public fun overlaps(left: ClosedRange<Int>, right: ClosedRange<Int>): Boolean {
    return left.contains(right.start) || left.contains(right.endInclusive) ||
        right.contains(left.start) || right.contains(left.endInclusive)
}

class Day4 : AocSolver<Int, Int> {
    var pairs: List<Pair<ClosedRange<Int>, ClosedRange<Int>>> = listOf()

    override fun parseInput(input: String) {
        val lines = input.lines()

        pairs = lines.filter { !it.isEmpty() }.map {
            val pairs = it.trim().split(",")
            require(pairs.size == 2) {
                println("pair must be defined by two values: $it")
            }
            val ranges = pairs.map {
                val nums = it.split("-")
                require(nums.size == 2) {
                    println("range must be defined by two values: $it")
                }
                nums[0].toInt()..nums[1].toInt()
            }
            Pair(ranges[0], ranges[1])
        }
    }

    override fun solvePartOne(): Int {
        val contained = pairs.filter { containedBy(it.first, it.second) }.size
        return contained
    }

    override fun solvePartTwo(): Int {
        val overlaps = pairs.filter { overlaps(it.first, it.second) }.size
        return overlaps
    }
}
