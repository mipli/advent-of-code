package aoc.aoc2022

import aoc.utils.AocSolver

data class Rucksack(val left: List<Char>, val right: List<Char>)

public fun scoreRucksack(sack: Rucksack): Int {
    return sack.left.intersect(sack.right).map { scoreChar(it) }.sumOf { it }
}

fun scoreChar(char: Char): Int {
    return if (char.isUpperCase()) {
        char.code - 38
    } else {
        char.code - 96
    }
}

public fun getBadges(sacks: List<Rucksack>): List<Char> {
    return sacks.windowed(3, 3).map {
        it.fold(listOf<Char>()) { acc, sack ->
            if (acc.isEmpty()) {
                sack.left + sack.right
            } else {
                acc.intersect(sack.left + sack.right).toList()
            }
        }.get(0)
    }
}

class Day3 : AocSolver<Int, Int> {
    var rucksacks = listOf<Rucksack>()

    override fun parseInput(input: String) {
        val lines = input.lines()

        rucksacks = lines.map {
            val split = it.trim().length
            val left = it.trim().substring(0, split / 2).toList()
            val right = it.trim().substring(split / 2).toList()
            Rucksack(left, right)
        }
    }

    override fun solvePartOne(): Int {
        val sum = rucksacks.map { scoreRucksack(it) }.sumOf { it }
        return sum
    }

    override fun solvePartTwo(): Int {
        val sum = getBadges(rucksacks).map { scoreChar(it) }.sumOf { it }
        return sum
    }
}
