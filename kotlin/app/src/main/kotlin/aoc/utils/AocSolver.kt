package aoc.utils

interface AocSolver<out T, out S> {
    fun parseInput(input: String)
    fun solvePartOne(): T
    fun solvePartTwo(): S
}
