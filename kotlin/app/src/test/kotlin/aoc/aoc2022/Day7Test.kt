package aoc.aoc2022

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class Day7Test {
    val day = Day7()

    @BeforeTest
    fun setup() {
        day.parseInput(
            """$ cd /
            $ ls
            dir a
            14848514 b.txt
            8504156 c.dat
            dir d
            $ cd a
            $ ls
            dir e
            29116 f
            2557 g
            62596 h.lst
            $ cd e
            $ ls
            584 i
            $ cd ..
            $ cd ..
            $ cd d
            $ ls
            4060174 j
            8033020 d.log
            5626152 d.ext
            7214296 k""",
        )
    }

    @Test fun canParseInput() {
        assertEquals(2, day.root.children.size)
        assertEquals(48381165, day.root.size)
    }
}
