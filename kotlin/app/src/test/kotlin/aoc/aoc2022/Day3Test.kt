package aoc.aoc2022

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class Day3Test {
    val day3 = Day3()

    @BeforeTest
    fun setup() {
        day3.parseInput(
            """vJrwpWtwJgWrhcsFMMfFFhFp
            jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
            PmmdzqPrVvPwwTWBwg
            wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
            ttgJtRGJQctTZtZT
            CrZsJsPPZsGzwwsLwLmpwMDw""",
        )
    }

    @Test fun canParseInput() {
        assertEquals(6, day3.rucksacks.size)
        assertEquals(12, day3.rucksacks[0].left.size)
        assertEquals(16, scoreRucksack(day3.rucksacks[0]))
    }

    @Test fun canFindBadge() {
        assertEquals(listOf('r', 'Z'), getBadges(day3.rucksacks))
    }
}
