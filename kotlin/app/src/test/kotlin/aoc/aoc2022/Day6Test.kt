package aoc.aoc2022

import kotlin.test.Test
import kotlin.test.assertEquals

class Day6Test {
    @Test fun simpleExamples() {
        val day = Day6()
        day.parseInput("bvwbjplbgvbhsrlpgdmjqwftvncz")
        assertEquals(day.solvePartOne(), 5)
        day.parseInput("nppdvjthqldpwncqszvftbrmjlhg")
        assertEquals(day.solvePartOne(), 6)
        day.parseInput("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")
        assertEquals(day.solvePartOne(), 10)
        day.parseInput("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")
        assertEquals(day.solvePartOne(), 11)
    }
}
