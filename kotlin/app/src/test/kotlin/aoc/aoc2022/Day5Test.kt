package aoc.aoc2022

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class Day5Test {
    val day = Day5()

    @BeforeTest
    fun setup() {
        day.parseInput(
            """    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2""",
        )
    }

    @Test fun testParsing() {
        assertEquals(3, day.stacks.size)
        assertEquals('Z', day.stacks[0].crates[0])
        assertEquals('C', day.stacks[1].crates[1])

        assertEquals(1, day.instructions[0].count)
    }

    @Test fun testPartOne() {
        val result = day.solvePartOne()
        assertEquals("CMZ", result)
    }

    @Test fun testPartTwo() {
        val result = day.solvePartTwo()
        assertEquals("MCD", result)
    }
}
