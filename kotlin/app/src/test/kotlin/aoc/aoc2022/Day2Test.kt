package aoc.aoc2022

import kotlin.test.Test
import kotlin.test.assertEquals

class Day2Test {
    @Test fun canParseInput() {
        val day = Day2()
        day.parseInput(
            """A Y
            B X
            C Z""",
        )

        assertEquals(3, day.matches.size)
    }

    @Test fun calculatePartOne() {
        val day = Day2()
        day.parseInput(
            """A Y
            B X
            C Z""",
        )
        val points = day.strategyResult()

        assertEquals(15, points)
    }

    @Test fun calculatePartTwo() {
        val day = Day2()
        day.parseInput(
            """A Y
            B X
            C Z""",
        )
        val points = day.unknownResult()

        assertEquals(12, points)
    }
}
