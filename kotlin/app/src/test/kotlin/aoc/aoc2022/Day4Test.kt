package aoc.aoc2022

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class Day4Test {
    val day = Day4()

    @BeforeTest
    fun setup() {
        day.parseInput(
            """2-4,6-8
            2-3,4-5
            5-7,7-9
            2-8,3-7
            6-6,4-6
            2-6,4-8""",
        )
    }

    @Test fun canParseInput() {
        assertEquals(6, day.pairs.size)
        assertEquals(2..4, day.pairs[0].first)
    }

    @Test fun rangeContainment() {
        assert(containedBy(3..6, 4..5))
        assert(containedBy(8..9, 3..10))
        assert(!containedBy(8..9, 3..7))
    }
}
