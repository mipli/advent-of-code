package aoc.aoc2022

import kotlin.test.Test
import kotlin.test.assertEquals

class Day1Test {
    @Test fun canParseInput() {
        val day1 = Day1()
        day1.parseInput(
            """1000
            2000
            3000

            4000

            5000
            6000

            7000
            8000
            9000

            10000""",
        )

        assertEquals(5, day1.elves.size)
    }
}
